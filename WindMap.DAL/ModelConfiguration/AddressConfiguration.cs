﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable("Addresses").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.Type).IsRequired();
            builder.Property(x => x.AddressValue).IsRequired();
            builder.Property(x => x.Zip).IsRequired();

            builder.HasOne(x => x.Region).WithMany(x => x.Addresses).OnDelete(DeleteBehavior.Cascade)
             .HasForeignKey(x => x.RegionId);

            builder.HasOne(x => x.Location).WithMany(x => x.Addresses).OnDelete(DeleteBehavior.Cascade)
             .HasForeignKey(x => x.LocationId);
        }
    }
}
