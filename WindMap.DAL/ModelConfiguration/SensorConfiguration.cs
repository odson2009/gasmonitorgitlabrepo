﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class SensorConfiguration : IEntityTypeConfiguration<Sensor>
    {
        public void Configure(EntityTypeBuilder<Sensor> builder)
        {
            builder.ToTable("Sensors").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.Latitude).IsRequired();
            builder.Property(x => x.Longitude).IsRequired();
            builder.Property(x => x.MonitorIdToRight).IsRequired();
            builder.Property(x => x.AzimuthRight).IsRequired();
            builder.Property(x => x.AzimuthLeft).IsRequired();
            builder.Property(x => x.DistanceRight).IsRequired();
            builder.Property(x => x.MonitorIdToLeft).IsRequired();
            builder.Property(x => x.DistanceLeft).IsRequired();
            builder.Property(x => x.NormalAzimuthToRight).IsRequired();
            builder.Property(x => x.NormalAzimuthToLeft).IsRequired();
            builder.Property(x => x.OnsiteWindMax).IsRequired();
            builder.Property(x => x.OnsiteWindMin).IsRequired();
            builder.Property(x => x.OnsiteWindCenterAngle).IsRequired();

            builder.HasMany(x => x.SensorResults).WithOne(x => x.Sensor).HasForeignKey(x => x.SensorId).OnDelete(DeleteBehavior.Cascade).IsRequired(true);
            builder.HasMany(x => x.SensorEvents).WithOne(x => x.Sensor).HasForeignKey(x => x.SensorId).OnDelete(DeleteBehavior.Cascade).IsRequired(true);
        }
    }
}
