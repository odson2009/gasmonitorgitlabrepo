﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class SensorResultConfiguration : IEntityTypeConfiguration<SensorResult>
    {
        public void Configure(EntityTypeBuilder<SensorResult> builder)
        {
            builder.ToTable("SensorResults").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.TimeBegin).IsRequired();
            builder.Property(x => x.TimeEnd).IsRequired();
            builder.Property(x => x.Concentration).IsRequired();
            builder.Property(x => x.WindSpeed).IsRequired();
            builder.Property(x => x.WindDirection).IsRequired();
        }
    }
}
