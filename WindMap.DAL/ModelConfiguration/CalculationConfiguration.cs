﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class CalculationConfiguration : IEntityTypeConfiguration<Calculation>
    {
        public void Configure(EntityTypeBuilder<Calculation> builder)
        {
            builder.ToTable("Calculations").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.EventTime).IsRequired();
            builder.Property(x => x.SourceLatitude).IsRequired();
            builder.Property(x => x.SourceLongitude).IsRequired();
            builder.Property(x => x.ParallelWindDirection).IsRequired();
            builder.Property(x => x.CrossWindDirection).IsRequired();
            builder.Property(x => x.AverageWindDirection).IsRequired();
            builder.Property(x => x.EmissionRate).IsRequired();
        }
    }
}
