﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class AlgorithmConfiguration : IEntityTypeConfiguration<Algorithm>
    {
        public void Configure(EntityTypeBuilder<Algorithm> builder)
        {
            builder.ToTable("Algorithmes").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.MonitorType).IsRequired();

            builder.Property(x => x.DetectionPointFlux).IsRequired();
            builder.Property(x => x.DetectionWindSpeed).IsRequired();
            builder.Property(x => x.TimeInterval).IsRequired();
            builder.Property(x => x.TimeResolution).IsRequired();
            builder.Property(x => x.MinDataForAverage).IsRequired();
            builder.Property(x => x.NumberLineGroupTime).IsRequired();
            builder.Property(x => x.NumberLineGroupWindDirection).IsRequired();
            builder.Property(x => x.WindSmoothingGroup).IsRequired();
            builder.Property(x => x.MaxRangeBetweenMonitors).IsRequired();
            builder.Property(x => x.SourceLocationUpdateInterval).IsRequired();
            builder.Property(x => x.NumbersOfGroupForLinearRegression).IsRequired();

        }
    }
}
