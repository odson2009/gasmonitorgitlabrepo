﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class UiElementConfiguration : IEntityTypeConfiguration<UiElement>
    {
        public void Configure(EntityTypeBuilder<UiElement> builder)
        {
            builder.ToTable("UiElements").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.Name).IsRequired();

            builder.HasMany(x => x.UiPermissions).WithOne(x => x.UiElement).HasForeignKey(x => x.UiElementId).OnDelete(DeleteBehavior.Cascade).IsRequired(true);
        }
    }
}