﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class RegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.ToTable("Regions").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Phone).IsRequired();
            builder.Property(x => x.WebSite);
            builder.Property(x => x.Email).IsRequired();
            builder.Property(x => x.CompanyId).IsRequired();
            builder.Property(x => x.Latitude).IsRequired();
            builder.Property(x => x.Longitude).IsRequired();
            builder.Property(x => x.Zoom).IsRequired();
            builder.Property(x => x.Coordinates).IsRequired();

            builder.HasMany(x => x.Sensors).WithOne(x => x.Region).HasForeignKey(x => x.RegionId).OnDelete(DeleteBehavior.Cascade).IsRequired(true);
            builder.HasMany(x => x.Algorithmes).WithOne(x => x.Region).HasForeignKey(x => x.RegionId).OnDelete(DeleteBehavior.Cascade).IsRequired(true);
            builder.HasMany(x => x.Addresses).WithOne(x => x.Region).HasForeignKey(x => x.RegionId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(x => x.Company).WithMany(x => x.Regions).HasForeignKey(x => x.CompanyId).OnDelete(DeleteBehavior.Cascade).IsRequired(true);

            // DeleteBehavior.SetNull RelationPermissions - Region
            //builder.HasMany(x => x.RelationPermissions).WithOne(x => x.Region).HasForeignKey(x => x.RegionId).OnDelete(DeleteBehavior.Cascade).IsRequired(false);
        }
    }
}
