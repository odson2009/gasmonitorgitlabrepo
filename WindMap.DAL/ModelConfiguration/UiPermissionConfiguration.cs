﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class UiPermissionConfiguration : IEntityTypeConfiguration<UiPermission>
    {
        public void Configure(EntityTypeBuilder<UiPermission> builder)
        {
            builder.ToTable("UiPermissions").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.UserId).IsRequired();
            builder.Property(x => x.UiElementId).IsRequired();
        }
    }
}