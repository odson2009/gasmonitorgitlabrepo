﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class RelationPermissionConfiguration : IEntityTypeConfiguration<RelationPermission>
    {
        public void Configure(EntityTypeBuilder<RelationPermission> builder)
        {
            builder.ToTable("RelationPermissions").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.UserId).IsRequired(false);
            builder.Property(x => x.CompanyId).IsRequired(false);
            builder.Property(x => x.RegionId).IsRequired(false);

            builder.HasOne(x => x.User).WithMany(x => x.RelationPermissions).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.Cascade).IsRequired(false);
            builder.HasOne(x => x.Company).WithMany(x => x.RelationPermissions).HasForeignKey(x => x.CompanyId).OnDelete(DeleteBehavior.Cascade).IsRequired(false);

            //DeleteBehavior.SetNull Region - RelationPermissions
            //builder.HasOne(x => x.Region).WithMany(x => x.RelationPermissions).HasForeignKey(x => x.RegionId).IsRequired(false).OnDelete(DeleteBehavior.SetNull);
        }
    }
}