﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class SensorEventConfiguration : IEntityTypeConfiguration<SensorEvent>
    {
        public void Configure(EntityTypeBuilder<SensorEvent> builder)
        {
            builder.ToTable("SensorEvents").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.SensorId).IsRequired();
            builder.Property(x => x.PairedSensorId).IsRequired(false);

            builder.HasMany(x => x.Calculations).WithOne(x => x.SensorEvent).HasForeignKey(x => x.SensorEventId).OnDelete(DeleteBehavior.Cascade).IsRequired(true);
        }
    }
}
