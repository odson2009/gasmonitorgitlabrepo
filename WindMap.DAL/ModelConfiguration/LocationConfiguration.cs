﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.ModelConfiguration
{
    internal class LocationConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            builder.ToTable("Locations").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd().IsRequired();
            builder.Property(x => x.State).IsRequired();
            builder.Property(x => x.Region);
            builder.Property(x => x.City).IsRequired();

            builder.HasMany(x => x.Addresses).WithOne(x => x.Location);
        }
    }
}
