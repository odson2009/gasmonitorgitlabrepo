﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.SeedMigration;

namespace WindMap.DAL.ModelConfiguration
{
    internal class SeedHistoryConfiguration : IEntityTypeConfiguration<SeedHistory>
    {
        public void Configure(EntityTypeBuilder<SeedHistory> builder)
        {
            builder.ToTable("__SeedMigrationsHistory").HasKey(x => x.MigrationId);
            builder.Property(x => x.Created);
        }
    }
}
