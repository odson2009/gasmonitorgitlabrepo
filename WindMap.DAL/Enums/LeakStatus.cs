﻿namespace WindMap.DAL.Enums
{
    public enum LeakStatus
    {
        Normal = 1,
        Warning = 2,
        Danger = 3
    }

    public enum LeakColor
    {
        violet = 0,
        indigo = 1,
        blue = 2,
        green = 3,
        yellow = 4,
        orange = 5,
        red = 6,
    }
}
