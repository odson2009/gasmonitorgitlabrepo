﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WindMap.DAL.Models
{
    public class Calculation
    {
        public int Id { get; set; }

        public int SensorEventId { get; set; }

        public DateTime EventTime { get; set; }

        public double SourceLatitude { get; set; }

        public double SourceLongitude { get; set; }

        public double ParallelWindDirection { get; set; }

        public double CrossWindDirection { get; set; }

        public double AverageWindDirection { get; set; }

        public double EmissionRate { get; set; }


        public SensorEvent SensorEvent { get; set; }
    }
}
