﻿using System;
namespace WindMap.DAL.Models
{
    public class SensorResult
    {
        public int Id { get; set; }
        public DateTime TimeBegin { get; set; }
        public DateTime TimeEnd { get; set; }

        public double Concentration { get; set; }

        public double WindSpeed { get; set; }

        public double WindDirection { get; set; }

        public int SensorId { get; set; }
        public Sensor Sensor { get; set; }

    }
}
