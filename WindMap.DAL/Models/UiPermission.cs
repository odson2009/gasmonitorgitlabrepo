﻿namespace WindMap.DAL.Models
{
    public class UiPermission
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int UiElementId { get; set; }
        public virtual UiElement UiElement { get; set; }
    }
}