﻿using System.Collections.Generic;

namespace WindMap.DAL.Models
{
    public class Sensor
    {
        public int Id { get; set; }

        public string MonitorId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string MonitorIdToRight { get; set; }

        public double AzimuthRight { get; set; }

        public double DistanceRight { get; set; }

        public double NormalAzimuthToRight { get; set; }

        public string MonitorIdToLeft { get; set; }

        public double AzimuthLeft { get; set; }

        public double DistanceLeft { get; set; }

        public double NormalAzimuthToLeft { get; set; }

        public double OnsiteWindMin { get; set; }

        public double OnsiteWindMax { get; set; }

        public double OnsiteWindCenterAngle { get; set; }


        public Region Region { get; set; }
        public int RegionId { get; set; }

        public ICollection<SensorResult> SensorResults { get; set; }

        public ICollection<SensorEvent> SensorEvents { get; set; }
    }
}
