﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WindMap.DAL.Models
{
    public class SensorEvent
    {
        public int Id { get; set; }

        public int SensorId { get; set; }

        public int? PairedSensorId { get; set; }

        public Sensor Sensor { get; set; }

        public Sensor PairedSensor { get; set; }

        public ICollection<Calculation> Calculations { get; set; }


        //public int StatusId { get; set; }
        //public int? EndEventId { get; set; }
        //public SensorEvent EndEvent { get; set; }
        //public EventStatus EventStatus { get; set; }
    }
}
