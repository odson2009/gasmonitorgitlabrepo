﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace WindMap.DAL.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive  { get; set; }
        public DateTime Data { get; set; }
        public bool ChangedPassword { get; set; } = false;
        public ICollection<RelationPermission> RelationPermissions { get; set; }
        public ICollection<UiPermission> UiPermission { get; set; }
    }
}