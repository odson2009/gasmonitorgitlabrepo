﻿namespace WindMap.DAL.Models
{
    public class MonitorCount
    {
        public int MonitorsCount { get; set; }
        public int MonitorsResultsCount { get; set; }

        public MonitorCount(int monitorsCount, int monitorsResultsCount)
        {
            MonitorsCount = monitorsCount;
            MonitorsResultsCount = monitorsResultsCount;
        }

        public MonitorCount()
        {
        }
    }
}
