﻿using System.Collections.Generic;

namespace WindMap.DAL.Models
{
    public class UiElement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<UiPermission> UiPermissions { get; set; }
    }
}
