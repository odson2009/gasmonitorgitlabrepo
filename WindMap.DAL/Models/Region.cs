﻿using System.Collections.Generic;

namespace WindMap.DAL.Models
{
    public class Region
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public int CompanyId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Zoom { get; set; }
        public string Coordinates { get; set; }
        public Company Company { get; set; }

        public ICollection<Address> Addresses { get; set; }
        public IList<Sensor> Sensors { get; set; }
        public ICollection<Algorithm> Algorithmes { get; set; }
        public ICollection<RelationPermission> RelationPermissions { get; set; }
    }
}