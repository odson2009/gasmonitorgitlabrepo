﻿using System.Collections.Generic;

namespace WindMap.DAL.Models
{
    public class RelationPermission
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int? CompanyId { get; set; }
        public int? RegionId { get; set; }
        public User User { get; set; }
        public Company Company { get; set; }
        public Region Region { get; set; }
    }
}