﻿using System.Collections.Generic;

namespace WindMap.DAL.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public byte[] Logo { get; set; }
        public ICollection<Region> Regions { get; set; }
        public ICollection<RelationPermission> RelationPermissions { get; set; }
    }
}