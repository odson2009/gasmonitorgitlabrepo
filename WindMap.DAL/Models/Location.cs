﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindMap.DAL.Models
{
    public class Location
    {
        public int Id { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Region { get; set; }

        public ICollection<Address> Addresses { get; set; }
    }
}
