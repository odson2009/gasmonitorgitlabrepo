﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindMap.DAL.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string AddressValue { get; set; }
        public string Zip { get; set; }

        public int RegionId { get; set; }
        public Region Region { get; set; }

        public int LocationId { get; set; }
        public Location Location { get; set; }

    }
}
