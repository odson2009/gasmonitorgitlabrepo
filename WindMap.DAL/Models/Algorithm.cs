﻿namespace WindMap.DAL.Models
{
    public class Algorithm
    {
        public int Id { get; set; }

        public string MonitorType { get; set; }

        public double DetectionPointFlux { get; set; }

        public double DetectionWindSpeed { get; set; }

        public double TimeInterval { get; set; }

        public double TimeResolution { get; set; }

        public double MinDataForAverage { get; set; }

        public double NumberLineGroupTime { get; set; }

        public double NumberLineGroupWindDirection { get; set; }

        public double WindSmoothingGroup { get; set; }

        public double MaxRangeBetweenMonitors { get; set; }

        public double SourceLocationUpdateInterval { get; set; }

        public double NumbersOfGroupForLinearRegression { get; set; }

        public int RegionId { get; set; }
        public Region Region { get; set; }
    }
}
