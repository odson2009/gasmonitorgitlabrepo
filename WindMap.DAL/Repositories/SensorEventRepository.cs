﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WindMap.DAL.Models;

namespace WindMap.DAL.Repositories
{
    public interface ISensorEventRepository : IBaseRepository<SensorEvent>
    {
        IQueryable<SensorEvent> FindAll();
    }

    internal class SensorEventRepository : BaseRepository<SensorEvent>, ISensorEventRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public SensorEventRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<SensorEvent> FindAll()
        {
            return _dbContext.SensorEvents;
        }

    }
}
