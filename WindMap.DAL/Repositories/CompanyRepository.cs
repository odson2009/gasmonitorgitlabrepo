﻿using System.Linq;
using WindMap.DAL.Models;

namespace WindMap.DAL.Repositories
{
    public interface ICompanyRepository : IBaseRepository<Company>
    {
        IQueryable<Company> FindAll();
        IQueryable<Company> FindById(int id);
        IQueryable<Company> FindByName(string name);
    }

    internal class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public CompanyRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Company> FindAll()
        {
            return _dbContext.Companies;
        }

        public IQueryable<Company> FindById(int id)
        {
            return _dbContext.Companies.Where(a => a.Id == id);
        }

        public IQueryable<Company> FindByName(string name)
        {
            return _dbContext.Companies.Where(a => a.Name == name);
        }
    }
}
