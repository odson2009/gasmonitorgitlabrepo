﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace WindMap.DAL.Repositories
{
    public interface IDatabaseRepository
    {
        Task<bool> ExistsAsync(CancellationToken cancellationToken);
        Task<IEnumerable<string>> GetPendingMigrationsAsync();
        Task MigrateAsync(CancellationToken cancellationToken);
        IEnumerable<string> GetMigrations();
        IMigrationsAssembly GetAssembly();
        IMigrationsModelDiffer GetDiffer();
        IModel GetModel();
    }

    internal class DatabaseRepository : IDatabaseRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public DatabaseRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> ExistsAsync(CancellationToken cancellationToken)
        {
            return _dbContext.GetService<IDatabaseCreator>() is IRelationalDatabaseCreator relationalDatabaseCreator
                && await relationalDatabaseCreator.ExistsAsync(cancellationToken);
        }

        public async Task<IEnumerable<string>> GetPendingMigrationsAsync()
        {
            return await _dbContext.Database.GetPendingMigrationsAsync();
        }

        public async Task MigrateAsync(CancellationToken cancellationToken)
        {
            await _dbContext.Database.MigrateAsync(cancellationToken);
        }

        public IEnumerable<string> GetMigrations()
        {
            return _dbContext.Database.GetMigrations();
        }

        public IMigrationsAssembly GetAssembly()
        {
            return _dbContext.GetService<IMigrationsAssembly>();
        }

        public IMigrationsModelDiffer GetDiffer()
        {
            return _dbContext.GetService<IMigrationsModelDiffer>();
        }

        public IModel GetModel()
        {
            return _dbContext.Model;
        }
    }
}
