﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using WindMap.DAL.Models;


namespace WindMap.DAL.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        IQueryable<User> FindById(string id);
        IQueryable<User> FindByLogin(string login);
    }
    internal class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public UserRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        IQueryable<User> IUserRepository.FindById(string id)
        {
            return _dbContext.Users.Where(a => a.Id == id);
        }
        IQueryable<User> IUserRepository.FindByLogin(string login)
        {
            return _dbContext.Users.
                Include(c => c.UiPermission).
                ThenInclude(s => s.UiElement).
                Where(a => a.UserName.Equals(login, StringComparison.OrdinalIgnoreCase));
        }
    }
}

