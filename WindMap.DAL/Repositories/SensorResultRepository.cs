﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;

namespace WindMap.DAL.Repositories
{
    public interface ISensorResultRepository : IBaseRepository<SensorResult>
    {
        IQueryable<SensorResult> FindById(int id);
        Task<DateTime> GetMinDate(CancellationToken cancellationToken);
    }

    internal class SensorResultRepository : BaseRepository<SensorResult>, ISensorResultRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public SensorResultRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<SensorResult> Get(int id)
        {
            return _dbContext.SensorResults;
        }

        public IQueryable<SensorResult> FindById(int id)
        {
            return _dbContext.SensorResults.Where(a => a.Id == id);
        }

        public async Task<DateTime> GetMinDate(CancellationToken cancellationToken)
        {
            if (await _dbContext.SensorResults.AnyAsync(cancellationToken))
            {
                return await _dbContext.SensorResults.MinAsync(x => x.TimeBegin, cancellationToken);
            }

            return DateTime.UtcNow;
        }

    }
}
