﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;

namespace WindMap.DAL.Repositories
{
    public interface IRelationPermissionRepository : IBaseRepository<RelationPermission>
    {
        Task SetNullRegionIdAsync(int regionId, CancellationToken cancellationToken);
    }

    internal class RelationPermissionRepository : BaseRepository<RelationPermission>, IRelationPermissionRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public RelationPermissionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task SetNullRegionIdAsync(int regionId, CancellationToken cancellationToken)
        {
            var relationPermissions = await _dbContext.RelationPermissions.Where(a => a.RegionId == regionId).ToListAsync();
            foreach(var i in relationPermissions)
            {
                i.RegionId = null;
                _dbContext.RelationPermissions.Update(i);
            }
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
