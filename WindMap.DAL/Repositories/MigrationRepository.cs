﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.SeedMigration;

namespace WindMap.DAL.Repositories
{
    public interface IMigrationRepository
    {
        IQueryable<string> FindSeedApplied();
        Task<IEnumerable<Type>> FindSeedMigrateAsync(CancellationToken cancellationToken);
    }

    internal class MigrationRepository : IMigrationRepository
    {
        private ApplicationDbContext _dbContext;

        public MigrationRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<string> FindSeedApplied()
        {
            return _dbContext.SeedHistories.Select(x => x.MigrationId);
        }

        public async Task<IEnumerable<Type>> FindSeedMigrateAsync(CancellationToken cancellationToken)
        {
            var applied = await FindSeedApplied().ToListAsync(cancellationToken);
            return Assembly.GetAssembly(typeof(MigrationSeeding)).GetTypes()
                .Where(x => x.IsSubclassOf(typeof(MigrationSeeding)))
                .Where(x => x.GetCustomAttribute<DbContextAttribute>()?.ContextType == typeof(ApplicationDbContext))
                .Where(x => !applied.Contains(x.GetCustomAttribute<SeedMigrationAttribute>()?.MigrationName))
                .OrderBy(x => x.GetCustomAttribute<SeedMigrationAttribute>()?.MigrationName);
        }
    }
}
