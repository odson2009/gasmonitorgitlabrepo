﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using WindMap.DAL.Models;

namespace WindMap.DAL.Repositories
{
    public interface IRegionRepository : IBaseRepository<Region>
    {
        IQueryable<Region> FindById(int id);
        IQueryable<Region> FindById(int[] id);

        Task<List<Region>> GetLastRegionResults(DateTime startDate, DateTime endDate,
            int[] sensorIdsArray, int count, CancellationToken cancellationToken);
        Task<List<Region>> GetRegionResultsByPeriod(DateTime startDate, DateTime endDate, int[] sensorIdsArray, bool isNeedLastResults,
            CancellationToken cancellationToken);
        Task<List<Region>> GetWithSingleSensorResult(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken);
    }

    internal class RegionRepository : BaseRepository<Region>, IRegionRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public RegionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Region> Get(int id)
        {
            return _dbContext.Regions;
        }

        public IQueryable<Region> FindById(int id)
        {
            return _dbContext.Regions.Where(a => a.Id == id);
        }

        public IQueryable<Region> FindById(int[] id)
        {
            return _dbContext.Regions.Where(e => id.Contains(e.Id));
        }

        public async Task<List<Region>> GetLastRegionResults(DateTime startDate, DateTime endDate,
            int[] sensorIdsArray, int count, CancellationToken cancellationToken)
        {
            var companies = _dbContext.Regions.Join(_dbContext.Sensors,
                    region => region.Id,
                    sensor => sensor.RegionId,
                    (region, sensor) => new { Region = region, Sensor = sensor })
                .Where(s => sensorIdsArray == null || sensorIdsArray.Contains(s.Sensor.Id)).Join(_dbContext.SensorResults,
                    regionSensor => regionSensor.Sensor.Id,
                    sensorResult => sensorResult.SensorId,
                    (regionSensor, result) => new { regionSensor.Region, regionSensor.Sensor, Result = result })
                .TakeLast(count);

            var data = await companies.ToListAsync(cancellationToken);

            var selectedData = data.Select(s => s.Region).Distinct().ToList();

            return selectedData;
        }

        public async Task<List<Region>> GetRegionResultsByPeriod(DateTime startDate, DateTime endDate, int[] sensorIdsArray, bool isNeedLastResults, CancellationToken cancellationToken)
        {
            var result = _dbContext.Regions
            .Include(c => c.Sensors)
            .Select(c => new Region
            {
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                Email = c.Email,
                WebSite = c.WebSite,
                Addresses = null,
                Algorithmes = c.Algorithmes,
                Sensors = c.Sensors.Select(s => new Sensor
                {
                    Id = s.Id,
                    MonitorId = s.MonitorId,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    MonitorIdToLeft = s.MonitorIdToLeft,
                    MonitorIdToRight = s.MonitorIdToRight,
                    AzimuthLeft = s.AzimuthLeft,
                    AzimuthRight = s.AzimuthRight,
                    DistanceLeft = s.DistanceLeft,
                    DistanceRight = s.DistanceRight,
                    NormalAzimuthToLeft = s.NormalAzimuthToLeft,
                    NormalAzimuthToRight = s.NormalAzimuthToRight,
                    OnsiteWindMin = s.OnsiteWindMin,
                    OnsiteWindMax = s.OnsiteWindMax,
                    OnsiteWindCenterAngle = s.OnsiteWindCenterAngle,
                    RegionId = s.RegionId,
                    Region = null,
                    SensorResults = isNeedLastResults ? s.SensorResults.Any() ? s.SensorResults.TakeLast(100).ToList() : null :
                        s.SensorResults.Any(a => a.TimeBegin >= startDate && a.TimeBegin <= endDate)
                        ? s.SensorResults.Where(a => a.TimeBegin >= startDate && a.TimeBegin <= endDate).OrderBy(res => res.TimeBegin).ToList() : null,
                })

                .Where(s => sensorIdsArray == null
                            || sensorIdsArray.Length == 0
                            || sensorIdsArray.Contains(s.Id))

                .ToList()
            });

            return await result.OrderBy(s => s.Id)?.ToListAsync(cancellationToken);
        }

        public async Task<List<Region>> GetWithSingleSensorResult(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken)
        {
            var result = _dbContext.Regions
            .Include(c => c.Sensors)
            .Select(c => new Region
            {
                Id = c.Id,
                Name = c.Name,
                Phone = c.Phone,
                Email = c.Email,
                WebSite = c.WebSite,
                Addresses = null,
                Algorithmes = c.Algorithmes,
                Sensors = c.Sensors.Select(s => new Sensor
                {
                    Id = s.Id,
                    MonitorId = s.MonitorId,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    MonitorIdToLeft = s.MonitorIdToLeft,
                    MonitorIdToRight = s.MonitorIdToRight,
                    AzimuthLeft = s.AzimuthLeft,
                    AzimuthRight = s.AzimuthRight,
                    DistanceLeft = s.DistanceLeft,
                    DistanceRight = s.DistanceRight,
                    NormalAzimuthToLeft = s.NormalAzimuthToLeft,
                    NormalAzimuthToRight = s.NormalAzimuthToRight,
                    OnsiteWindMin = s.OnsiteWindMin,
                    OnsiteWindMax = s.OnsiteWindMax,
                    OnsiteWindCenterAngle = s.OnsiteWindCenterAngle,
                    RegionId = s.RegionId,
                    Region = null,
                    SensorResults = s.SensorResults.Any(a => a.TimeBegin == date) ?
                        s.SensorResults.Where(a => a.TimeBegin == date).Take(1).ToList() : null,
                })
                .Where(s => sensorIdsArray == null
                            || sensorIdsArray.Length == 0
                            || sensorIdsArray.Contains(s.Id))

                .ToList()
            });

            return await result.OrderBy(s => s.Id)?.ToListAsync(cancellationToken);
        }
    }
}
