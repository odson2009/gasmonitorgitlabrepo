﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WindMap.DAL.Models;

namespace WindMap.DAL.Repositories
{
    public interface IUiElementRepository : IBaseRepository<UiElement>
    {
        Task<List<UiElement>> GetAllAsync(CancellationToken cancellationToken);
        IQueryable<UiElement> FindById(int id);
    }

    internal class UiElementRepository : BaseRepository<UiElement>, IUiElementRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public UiElementRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<UiElement>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await _dbContext.UiElements.ToListAsync(cancellationToken);
        }

        public IQueryable<UiElement> FindById(int id)
        {
            return _dbContext.UiElements.Where(a => a.Id == id);
        }
    }
}
