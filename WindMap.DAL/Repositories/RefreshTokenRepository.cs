﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;


namespace WindMap.DAL.Repositories
{
    public interface IRefreshTokenRepository : IBaseRepository<RefreshToken>
    {
        IQueryable<RefreshToken> FindById(int id);
        IQueryable<RefreshToken> FindByUserId(string id);
        Task DeleteAllAsync(string userId, CancellationToken cancellationToken);

    }

    internal class RefreshTokenRepository : BaseRepository<RefreshToken>, IRefreshTokenRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public RefreshTokenRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task DeleteAllAsync(string userId, CancellationToken cancellationToken)
        {
            var list = _dbContext.RefreshTokens.Where(a => a.UserId == userId);
            _dbContext.RefreshTokens.RemoveRange(list);
            await SaveChangesAsync(cancellationToken);
        }

        public IQueryable<RefreshToken> FindById(int id)
        {
            return _dbContext.RefreshTokens.Where(a => a.Id == id);
        }

        public IQueryable<RefreshToken> FindByUserId(string id)
        {
            return _dbContext.RefreshTokens.Where(a => a.UserId == id);
        }
    }
}
