﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WindMap.DAL.Repositories
{
    public interface IBaseRepository<TModel>
       where TModel : class
    {
        Task<TModel> FindAsync(object[] keyValues, CancellationToken cancellationToken);
        Task<TModel> FindAsync(object keyValue, CancellationToken cancellationToken);
        Task AddAsync(TModel entity, CancellationToken cancellationToken);
        Task AddRangeAsync(IList<TModel> list, CancellationToken cancellationToken);
        Task UpdateAsync(TModel entity, CancellationToken cancellationToken);
        Task UpdateAsync(ICollection<TModel> entity, CancellationToken cancellationToken);
        Task UpdateAsync(IQueryable<TModel> entity, CancellationToken cancellationToken);

        Task UpdateAsync(ICollection<TModel> entities, 
            ICollection<string> exceptions, CancellationToken cancellationToken);
        Task DeleteAsync(TModel entity, CancellationToken cancellationToken);
        Task DeleteAsync(ICollection<TModel> entity, CancellationToken cancellationToken);
        Task DeleteAsync(IQueryable<TModel> entity, CancellationToken cancellationToken);
        Task SaveChangesAsync(CancellationToken cancellationToken);
        IQueryable<TModel> Get();
        Task<bool> IsExistAsync(TModel entity, CancellationToken cancellationToken);
    }

    internal class BaseRepository<TModel> : IBaseRepository<TModel>
        where TModel : class
    {
        private readonly ApplicationDbContext _dbContext;

        public BaseRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<TModel> FindAsync(object[] keyValues, CancellationToken cancellationToken)
        {
            return await _dbContext.FindAsync<TModel>(keyValues, cancellationToken);
        }

        public virtual async Task<TModel> FindAsync(object keyValue, CancellationToken cancellationToken)
        {
            return await FindAsync(new object[] { keyValue }, cancellationToken);
        }

        public virtual async Task AddAsync(TModel entity, CancellationToken cancellationToken)
        {
            await _dbContext.AddAsync(entity, cancellationToken);
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task AddRangeAsync(IList<TModel> list, CancellationToken cancellationToken)
        {
            await _dbContext.AddRangeAsync(list, cancellationToken);
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task UpdateAsync(TModel entity, CancellationToken cancellationToken)
        {
            _dbContext.Update(entity);
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task UpdateAsync(ICollection<TModel> entity, CancellationToken cancellationToken)
        {
            _dbContext.UpdateRange(entity);
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task UpdateAsync(IQueryable<TModel> entity, CancellationToken cancellationToken)
        {
            _dbContext.UpdateRange(entity);
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task UpdateAsync(ICollection<TModel> entities, ICollection<string> exceptions, CancellationToken cancellationToken)
        {
            _dbContext.UpdateRange(entities);
            foreach (var entity in entities)
            {
                foreach (var exception in exceptions)
                {
                    var property = _dbContext.Entry(entity).Property(exception);
                    if(property != null)
                        property.IsModified = false;
                }
                
            }
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task DeleteAsync(TModel entity, CancellationToken cancellationToken)
        {
            _dbContext.Remove(entity);
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task DeleteAsync(ICollection<TModel> entity, CancellationToken cancellationToken)
        {
            _dbContext.RemoveRange(entity);
            await SaveChangesAsync(cancellationToken);
        }

        public virtual async Task DeleteAsync(IQueryable<TModel> entity, CancellationToken cancellationToken)
        {
            _dbContext.RemoveRange(entity);
            await SaveChangesAsync(cancellationToken);
        }
        public virtual async Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public IQueryable<TModel> Get()
        {
            return _dbContext.Set<TModel>();
        }

        public virtual async Task<bool> IsExistAsync(TModel entity, CancellationToken cancellationToken)
        {
            return await _dbContext.Set<TModel>().AnyAsync(s => s == entity, cancellationToken);
        }
    }
}
