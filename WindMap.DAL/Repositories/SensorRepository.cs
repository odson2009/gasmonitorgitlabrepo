﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WindMap.DAL.Models;

namespace WindMap.DAL.Repositories
{
    public interface ISensorRepository : IBaseRepository<Sensor>
    {
        IQueryable<Sensor> FindById(int id);
        IQueryable<Sensor> FindById(int[] id);
        IQueryable<Sensor> GetByRegionId(int regionId);
        Task<List<Sensor>> GetByPeriod(DateTime startDate, DateTime endDate, bool isNeedLastResults, int[] sensorIdsArray, CancellationToken cancellationToken);
        Task<List<Sensor>> GetWithSingleResult(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken);
        IQueryable<object> GetResultsChartDataPeriod();
        
    }

    internal class SensorRepository : BaseRepository<Sensor>, ISensorRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public SensorRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Sensor> FindById(int id)
        {
            return _dbContext.Sensors.Where(a => a.Id == id);
        }

        public IQueryable<Sensor> FindById(int[] id)
        {
            return _dbContext.Sensors.Where(e => id.Contains(e.Id));
        }

        public IQueryable<Sensor> GetByRegionId(int regionId)
        {
            return _dbContext.Sensors.Where(a => a.RegionId == regionId);
        }

        public async Task<List<Sensor>> GetByPeriod(DateTime startDate, DateTime endDate, bool isNeedLastResults, int[] sensorIdsArray, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var result = _dbContext.Sensors.Select(s => new Sensor()
                {
                    Id = s.Id,

                    MonitorId = s.MonitorId,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    MonitorIdToLeft = s.MonitorIdToLeft,

                    MonitorIdToRight = s.MonitorIdToRight,
                    AzimuthLeft = s.AzimuthLeft,
                    AzimuthRight = s.AzimuthRight,
                    DistanceLeft = s.DistanceLeft,

                    DistanceRight = s.DistanceRight,
                    NormalAzimuthToLeft = s.NormalAzimuthToLeft,
                    NormalAzimuthToRight = s.NormalAzimuthToRight,
                    RegionId = s.RegionId,

                    SensorResults = isNeedLastResults ?
                    s.SensorResults.Any() ? s.SensorResults.TakeLast(100).ToList() : null :
                    s.SensorResults.Any(a => a.TimeBegin >= startDate && a.TimeBegin <= endDate) ? s.SensorResults
                        .Where(a => a.TimeBegin >= startDate && a.TimeBegin <= endDate)
                        .OrderBy(res => res.TimeBegin).ToList() : null,
                    Region = s.Region
                }).Where(s => (sensorIdsArray == null || sensorIdsArray.Length == 0) || sensorIdsArray.Contains(s.Id)).OrderBy(s => s.RegionId).ToList();
                return result;
            });
        }

        public async Task<List<Sensor>> GetWithSingleResult(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var result = _dbContext.Sensors.Select(s => new Sensor()
                {
                    Id = s.Id,

                    MonitorId = s.MonitorId,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    MonitorIdToLeft = s.MonitorIdToLeft,

                    MonitorIdToRight = s.MonitorIdToRight,
                    AzimuthLeft = s.AzimuthLeft,
                    AzimuthRight = s.AzimuthRight,
                    DistanceLeft = s.DistanceLeft,

                    DistanceRight = s.DistanceRight,
                    NormalAzimuthToLeft = s.NormalAzimuthToLeft,
                    NormalAzimuthToRight = s.NormalAzimuthToRight,
                    RegionId = s.RegionId,

                    SensorResults = s.SensorResults.Any(a => a.TimeBegin == date) ?
                        //s.SensorResults.Where(a => a.TimeEnd == date).Take(1).ToList() : null,
                        s.SensorResults.Where(a => a.TimeBegin == date).Take(1).ToList() : null,
                    Region = s.Region
                }).Where(s => (sensorIdsArray == null || sensorIdsArray.Length == 0) || sensorIdsArray.Contains(s.Id)).OrderBy(s => s.RegionId).ToList();
                return result;
            });
        }

        public IQueryable<object> GetResultsChartDataPeriod()
        {
            var result = from sr in _dbContext.SensorResults.OrderBy(r => r.TimeBegin)
                         group sr by new { date = sr.TimeBegin.ToString("yyyy-MM-dd"), regionId = sr.Sensor.RegionId } into srg
                         join c in _dbContext.Regions on srg.Key.regionId equals c.Id

                         select new
                         {
                             id = srg.Key.regionId,
                             regionName = c.Name,
                             date = srg.Key.date,
                             minDate = srg.Min(e => e.TimeBegin),
                             count = srg.Count()
                         };

            var groupResult = from r in result
                              group r by r.date into rg
                              select rg;

            return groupResult;
        }
    }
}
