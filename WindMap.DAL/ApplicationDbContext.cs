﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.ModelConfiguration;
using WindMap.DAL.Models;
using WindMap.DAL.SeedMigration;

namespace WindMap.DAL
{
    public class ApplicationDbContext : /*DbContext*/ IdentityDbContext<User>
    {
        private IConfiguration Configuration { get; }

        public DbSet<Region> Regions { get; set; }
        public DbSet<Sensor> Sensors { get; set; }
        public DbSet<SensorResult> SensorResults { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<SeedHistory> SeedHistories { get; set; }
        public DbSet<Algorithm> Algorithmes { get; set; }
        public DbSet<SensorEvent> SensorEvents { get; set; }
        public DbSet<Calculation> Calculations { get; set; }
        public DbSet<UiElement> UiElements { get; set; }
        public DbSet<UiPermission> UiPermissions { get; set; }
        public DbSet<RelationPermission> RelationPermissions { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public ApplicationDbContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AddressConfiguration());
            builder.ApplyConfiguration(new LocationConfiguration());
            builder.ApplyConfiguration(new RegionConfiguration());
            builder.ApplyConfiguration(new AlgorithmConfiguration());
            builder.ApplyConfiguration(new SensorConfiguration());
            builder.ApplyConfiguration(new SensorResultConfiguration());
            builder.ApplyConfiguration(new SeedHistoryConfiguration());
            builder.ApplyConfiguration(new SensorEventConfiguration());
            builder.ApplyConfiguration(new CalculationConfiguration());
            builder.ApplyConfiguration(new UiElementConfiguration());
            builder.ApplyConfiguration(new UiPermissionConfiguration());
            builder.ApplyConfiguration(new RelationPermissionConfiguration());
            builder.ApplyConfiguration(new CompanyConfiguration());
            builder.ApplyConfiguration(new RefreshTokenConfiguration());
        }
    }
}
