﻿using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace WindMap.DAL.SeedMigration
{
    public abstract class MigrationSeeding
    {
        public abstract Task MigrateAsync(IConfiguration configuration, CancellationToken cancellationToken);
        public IConfiguration _configuration;
    }

    internal abstract class MigrationSeeding<T> : MigrationSeeding
        where T : class
    {
        protected virtual IEnumerable<T> Seed { get; }
        
        public override async Task MigrateAsync(IConfiguration configuration, CancellationToken cancellationToken)
        {
            _configuration = configuration;
            var migrationContext = GetType().GetCustomAttribute<DbContextAttribute>()?.ContextType;
            var migrationName = GetType().GetCustomAttribute<SeedMigrationAttribute>()?.MigrationName;

            var history = new SeedHistory
            {
                MigrationId = migrationName,
                Created = DateTime.Now
            };

            if (Activator.CreateInstance(migrationContext, configuration) is DbContext dbContext)
            {
                await dbContext.Set<SeedHistory>().AddAsync(history, cancellationToken);
                if (Seed != null)
                    await dbContext.Set<T>().AddRangeAsync(Seed, cancellationToken);
                await ConfigureRelations(dbContext);
                await dbContext.SaveChangesAsync(cancellationToken);
            }
        }

        protected virtual Task ConfigureRelations(DbContext dbContext)
        {
            return Task.CompletedTask;
        }
    }
}
