﻿using System;

namespace WindMap.DAL.SeedMigration
{
    internal class SeedMigrationAttribute : Attribute
    {
        public string MigrationName { get; }

        public SeedMigrationAttribute(string name)
        {
            MigrationName = name;
        }
    }
}
