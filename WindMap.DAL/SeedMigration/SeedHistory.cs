﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindMap.DAL.SeedMigration
{
    public class SeedHistory
    {
        public string MigrationId { get; set; }
        public DateTime Created { get; set; }
    }
}
