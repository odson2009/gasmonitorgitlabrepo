﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Collections.Generic;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("009000000000000_Roles")]
    internal class RolesSeed : MigrationSeeding<IdentityRole>
    {
        private static string Administrator = "Administrator";
        private static string Service_Provider = "Service_Provider";
        private static string Owner = "Owner";

        public static IEnumerable<IdentityRole> List => new List<IdentityRole>
        {
            new IdentityRole
            {
                Name = Administrator,
                NormalizedName = Administrator.ToUpper()
            },
            new IdentityRole
            {
                Name = Owner,
                NormalizedName = Owner.ToUpper()
            },
            new IdentityRole
            {
                Name = Service_Provider,
                NormalizedName = Service_Provider.ToUpper()
            },
        };

        protected override IEnumerable<IdentityRole> Seed => List;
    }
}