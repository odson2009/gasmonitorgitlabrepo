﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("000100000000000_Companies")]
    internal class CompanySeed : MigrationSeeding<Company>
    {
        public IEnumerable<Company> List => new List<Company>
        {
            new Company
            {
                Name = _configuration["DefaultCompany:Name"],
                Phone = _configuration["DefaultCompany:Phone"],
                WebSite = _configuration["DefaultCompany:WebSite"],
                Email = _configuration["DefaultCompany:Email"],
            },
            new Company
            {
                Name = "Company 1",
                Phone = "1111111111",
                WebSite = "www.company.com",
                Email = "company1@company1.com"
            },
            new Company
            {
                Name = "Company 2",
                Phone = "1111111111",
                WebSite = "www.company.com",
                Email = "company2@company2.com"
            }
        };

        protected override IEnumerable<Company> Seed => List;
    }
}
