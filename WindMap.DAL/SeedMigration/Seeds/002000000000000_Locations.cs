﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;


namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("002000000000000_Locations")]
    internal class LocationSeed : MigrationSeeding<Location>
    {
        public static IEnumerable<Location> List => new List<Location>
        {
            new Location
            {
                State = "Wisconsin",
                City = "Madison"
            },
            new Location
            {
                State = "Pennsylvania",
                City = "Lansdale"
            },
        };

        protected override IEnumerable<Location> Seed => List;
    }
}
