﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("006000000000000_Algoritmes")]
    internal class AlgoritmesSeed : MigrationSeeding<Algorithm>
    {
        static DateTime _currentDate = DateTime.Now;
        public static IEnumerable<Algorithm> List => new List<Algorithm>
        {
            new Algorithm
            {
                RegionId = 1,
                MonitorType = "Type 1",
                DetectionPointFlux = 80,
                DetectionWindSpeed = 569.0006,
                TimeInterval = 300,
                TimeResolution = 5,
                MinDataForAverage = 60,
                NumberLineGroupTime = 6,
                NumberLineGroupWindDirection = 120,
                WindSmoothingGroup = 150,
                MaxRangeBetweenMonitors = 500,
                SourceLocationUpdateInterval = 30,
                NumbersOfGroupForLinearRegression = 6
            },
            new Algorithm
            {
                RegionId = 2,
                MonitorType = "Type 2",
                DetectionPointFlux = 90,
                DetectionWindSpeed = 332,
                TimeInterval = 300,
                TimeResolution = 5,
                MinDataForAverage = 60,
                NumberLineGroupTime = 5,
                NumberLineGroupWindDirection = 120,
                WindSmoothingGroup = 156,
                MaxRangeBetweenMonitors = 500,
                SourceLocationUpdateInterval = 30,
                NumbersOfGroupForLinearRegression = 6
            },
#region Pasadena Refining System, Inc.
            new Algorithm
            {
                RegionId = 3,
                MonitorType = "Type 1",
                DetectionPointFlux = 80,
                DetectionWindSpeed = 569.0006,
                TimeInterval = 300,
                TimeResolution = 5,
                MinDataForAverage = 60,
                NumberLineGroupTime = 6,
                NumberLineGroupWindDirection = 120,
                WindSmoothingGroup = 150,
                MaxRangeBetweenMonitors = 500,
                SourceLocationUpdateInterval = 30,
                NumbersOfGroupForLinearRegression = 6
            },
#endregion

#region Texas City
            new Algorithm
            {
                RegionId = 4,
                MonitorType = "Slow data",
                DetectionPointFlux = 1,
                DetectionWindSpeed = 569.0006,
                TimeInterval = 30,
                TimeResolution = 5,
                MinDataForAverage = 4,
                NumberLineGroupTime = 2,
                NumberLineGroupWindDirection = 120,
                WindSmoothingGroup = 150,
                MaxRangeBetweenMonitors = 500,
                SourceLocationUpdateInterval = 30,
                NumbersOfGroupForLinearRegression = 4,
                
            },
#endregion
        };

        protected override IEnumerable<Algorithm> Seed => List;
    }
}
