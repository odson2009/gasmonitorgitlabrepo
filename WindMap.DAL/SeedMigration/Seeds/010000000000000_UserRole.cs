﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading.Tasks;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("010000000000000_UserRole")]
    internal class UserRoleSeed : MigrationSeeding<IdentityUserRole<string>>
    {
        protected override async Task ConfigureRelations(DbContext dbContext)
        {
            await SetIdentityUserRole(dbContext, "evgenbakai", "Administrator");
            await SetIdentityUserRole(dbContext, "admin", "Administrator");
            //await SetIdentityUserRole(dbContext, "odson2009", "Administrator");
            //await SetIdentityUserRole(dbContext, "oyaromeran", "Administrator");
            //await SetIdentityUserRole(dbContext, "ram", "Administrator");
            //await SetIdentityUserRole(dbContext, "test", "Administrator");
        }

        private async Task SetIdentityUserRole(DbContext dbContext, string userName, string roleName)
        {
            var user = await dbContext.Set<User>().FirstAsync(s => s.UserName == userName);
            var role = await dbContext.Set<IdentityRole>().FirstAsync(s => s.Name == roleName);

            var userRole = new IdentityUserRole<string>();
            userRole.RoleId = role.Id;
            userRole.UserId = user.Id;

            await dbContext.Set<IdentityUserRole<string>>().AddAsync(userRole);
        }
    }
}