﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Collections.Generic;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("004000000000000_Sensors")]
    internal class SensorSeed : MigrationSeeding<Sensor>
    {
        public static IEnumerable<Sensor> List => new List<Sensor>
        {
            #region Region id = 1
            new Sensor
            {
                MonitorId = "1001",
                Latitude = 32.82123131499834,
                Longitude = 35.055097695440054,
                MonitorIdToLeft = "1002",
                MonitorIdToRight = "0",
                AzimuthRight = 45.9,
                DistanceRight = 0,
                AzimuthLeft = 80.605,
                DistanceLeft = 125.2639,
                NormalAzimuthToRight=90,
                NormalAzimuthToLeft=270,
                OnsiteWindMax = 40,
                OnsiteWindMin = 220,
                OnsiteWindCenterAngle = 306.2295,
                RegionId = 1,
            },
            new Sensor
            {
                MonitorId = "1002",
                Latitude = 32.820325187600474,
                Longitude = 35.054303761571646,
                MonitorIdToLeft = "0",
                MonitorIdToRight = "1001",
                AzimuthRight = 45.9,
                DistanceRight = 125.2639,
                AzimuthLeft = 80.605,
                DistanceLeft = 400.20,
                NormalAzimuthToRight=180,
                NormalAzimuthToLeft = 90.306,
                OnsiteWindMax = 40,
                OnsiteWindMin = 220,
                OnsiteWindCenterAngle = 306.2295,
                RegionId = 1,
            },
#endregion

            #region Region id = 2
            new Sensor
            {
                MonitorId = "3",
                Latitude = 32.82652003962289,
                Longitude = 35.04676274022245,
                MonitorIdToRight = "0",
                AzimuthRight =45.9,
                DistanceRight =667.05,
                AzimuthLeft =80.605,
                MonitorIdToLeft = "0",
                DistanceLeft = 1356,
                NormalAzimuthToRight= 95.6,
                NormalAzimuthToLeft=190,
                OnsiteWindMax = 360,
                OnsiteWindMin = 320,
                OnsiteWindCenterAngle = 0,
                RegionId = 2,
            },
            new Sensor
            {
                MonitorId = "4",
                Latitude = 32.83093757874763,
                Longitude = 35.05077532485575,
                MonitorIdToRight = "0",
                AzimuthRight = 45.9,
                DistanceRight = 550.10,
                AzimuthLeft = 80.605,
                MonitorIdToLeft = "0",
                DistanceLeft = 356,
                NormalAzimuthToRight=90,
                NormalAzimuthToLeft=270,
                OnsiteWindMax = 360,
                OnsiteWindMin = 320,
                OnsiteWindCenterAngle = 0,
                RegionId = 2,
            },
            new Sensor
            {
                MonitorId = "5",
                Latitude = 32.82644791471194,
                Longitude = 35.0534360762515,
                MonitorIdToRight = "0",
                AzimuthRight = 20.9,
                DistanceRight = 690,
                AzimuthLeft = 180.0,
                MonitorIdToLeft = "0",
                DistanceLeft = 562,
                NormalAzimuthToRight = 90,
                NormalAzimuthToLeft = 180,
                OnsiteWindMax = 360,
                OnsiteWindMin = 320,
                OnsiteWindCenterAngle = 0,
                RegionId = 2,
            },
            #endregion

            #region Pasadena Refining System (PRSI)
            new Sensor
            {
                MonitorId = "1",
                Latitude = 29.72844923080314,
                Longitude = -95.20786375383636,
                MonitorIdToRight = "4",
                AzimuthRight = 20.9,
                DistanceRight = 312.7325,
                AzimuthLeft = 180.0,
                MonitorIdToLeft = "2",
                DistanceLeft = 216.7481,
                NormalAzimuthToRight = 90,
                NormalAzimuthToLeft = 180,
                OnsiteWindMax = 40,
                OnsiteWindMin = 220,
                OnsiteWindCenterAngle = 0,
                RegionId = 3,
            },
            new Sensor
            {
                MonitorId = "2",
                Latitude = 29.726860643379023,
                Longitude = -95.20916021866697,
                MonitorIdToRight = "1",
                AzimuthRight = 20.9,
                DistanceRight = 216.7481,
                AzimuthLeft = 180.0,
                MonitorIdToLeft = "6",
                DistanceLeft = 125.0084,
                NormalAzimuthToRight = 90,
                NormalAzimuthToLeft = 180,
                //OnsiteWindMax = 40,
                //OnsiteWindMin = 220,
                OnsiteWindMax = 240,
                OnsiteWindMin = 35,
                OnsiteWindCenterAngle = 0,
                RegionId = 3,
            },
            new Sensor
            {
                MonitorId = "3",
                Latitude = 29.72541301837825,
                Longitude = -95.21147987227312,
                MonitorIdToRight = "6",
                AzimuthRight = 20.9,
                DistanceRight = 151.6611,
                AzimuthLeft = 180.0,
                MonitorIdToLeft = "0",
                DistanceLeft = 562,
                NormalAzimuthToRight = 90,
                NormalAzimuthToLeft = 180,
                OnsiteWindMax = 40,
                OnsiteWindMin = 220,
                OnsiteWindCenterAngle = 0,
                RegionId = 3,
            },
            new Sensor
            {
                MonitorId = "4",
                Latitude = 29.73063080340909,
                Longitude = -95.20582541985061,
                MonitorIdToRight = "1",
                AzimuthRight = 20.9,
                DistanceRight = 312.7325,
                AzimuthLeft = 180.0,
                MonitorIdToLeft = "5",
                DistanceLeft = 387.6547,
                NormalAzimuthToRight = 90,
                NormalAzimuthToLeft = 180,
                OnsiteWindMax = 40,
                OnsiteWindMin = 220,
                OnsiteWindCenterAngle = 0,
                RegionId = 3,
            },
            new Sensor
            {
                MonitorId = "5",
                Latitude = 29.72720484581661,
                Longitude = -95.20510658590808,
                MonitorIdToRight = "4",
                AzimuthRight = 20.9,
                DistanceRight = 387.6547,
                AzimuthLeft = 180.0,
                MonitorIdToLeft = "0",
                DistanceLeft = 0,
                NormalAzimuthToRight = 90,
                NormalAzimuthToLeft = 180,
                OnsiteWindMax = 40,
                OnsiteWindMin = 220,
                OnsiteWindCenterAngle = 0,
                RegionId = 3,
            },
            new Sensor
            {
                MonitorId = "6",
                Latitude = 29.726268935818606,
                Longitude = -95.21025928804124,
                MonitorIdToRight = "3",
                AzimuthRight = 20.9,
                DistanceRight = 151.6611,
                AzimuthLeft = 180.0,
                MonitorIdToLeft = "2",
                DistanceLeft = 125.0084,
                NormalAzimuthToRight = 90,
                NormalAzimuthToLeft = 180,
                //OnsiteWindMax = 40,
                //OnsiteWindMin = 220,
                OnsiteWindMax = 240,
                OnsiteWindMin = 60,
                OnsiteWindCenterAngle = 0,
                RegionId = 3,
            },
            #endregion
            #region Sensors for Texas City
            new Sensor
            {
                MonitorId = "302",
                Latitude = 29.381389,
                Longitude = -94.940833,
                MonitorIdToRight = "303",
                AzimuthRight = 93.3682,
                DistanceRight = 1052.634,
                AzimuthLeft = 0,
                MonitorIdToLeft = "0",
                DistanceLeft = 0,
                NormalAzimuthToRight = 3.3682,
                NormalAzimuthToLeft = 0,
                OnsiteWindMax = 303,
                OnsiteWindMin = 0,
                OnsiteWindCenterAngle = 0,
                RegionId = 4,
            },
            new Sensor
            {
                MonitorId = "303",
                Latitude = 29.380832707847773,
                Longitude = -94.93000033527613,
                MonitorIdToRight = "302",
                AzimuthRight = 0,
                DistanceRight = 0,
                AzimuthLeft = 273.3754,
                MonitorIdToLeft = "2",
                DistanceLeft = 1052.6035,
                NormalAzimuthToRight = 0,
                NormalAzimuthToLeft = 3.3754,
                OnsiteWindMax = 0,
                OnsiteWindMin = 303,
                OnsiteWindCenterAngle = 0,
                RegionId = 4,
            },
            #endregion
        };

        protected override IEnumerable<Sensor> Seed => List;
    }
}
