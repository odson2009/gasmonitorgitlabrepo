﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Collections.Generic;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("011000000000000_UiElements")]
    internal class UiElementsSeed : MigrationSeeding<UiElement>
    {
        public static IEnumerable<UiElement> List => new List<UiElement>
        {
            new UiElement
            {
                Name = "Dashboard"
            },
            new UiElement
            {
                Name = "Map"
            },
            new UiElement
            {
                Name = "Companies"
            },
            new UiElement
            {
                Name = "Sites"
            },
            new UiElement
            {
                Name = "Monitors"
            },
            new UiElement
            {
                Name = "Users"
            },
            new UiElement
            {
                Name = "Leak"
            },
            new UiElement
            {
                Name = "Tests"
            },
            new UiElement
            {
                Name = "Developer"
            },
        };

        protected override IEnumerable<UiElement> Seed => List;
    }
}