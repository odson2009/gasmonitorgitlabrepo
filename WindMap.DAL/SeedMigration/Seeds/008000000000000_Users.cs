﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Options;
using WindMap.DAL.Models;
namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("008000000000000_Users")]
    internal class UsersSeed : MigrationSeeding<User>
    {
        protected override async Task ConfigureRelations(DbContext dbContext)
        {
            await createUser(dbContext, "bakai.evgen@gmail.com", "evgenbakai", "qwerqwer");
            await createUser(dbContext, "admin@mail.com", "admin", "qwerty");
        }
        private async Task createUser(DbContext dbContext, string email, string userName, string password)
        {
            var user = new User
            {
                Email = email,
                UserName = userName,
                IsActive = true,
                Data = DateTime.UtcNow,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };
            user.NormalizedEmail = user.Email.ToUpper();
            user.NormalizedUserName = user.UserName.ToUpper();

            var passwordHasher = new PasswordHasher<User>(Options.Create(new PasswordHasherOptions()
            {
                IterationCount = 10000,
                CompatibilityMode = PasswordHasherCompatibilityMode.IdentityV3
            }));

            var hashed = passwordHasher.HashPassword(user, password);
            user.PasswordHash = hashed;

            var userStore = new UserStore<User>(dbContext);
            await userStore.CreateAsync(user);
        }
    }
}