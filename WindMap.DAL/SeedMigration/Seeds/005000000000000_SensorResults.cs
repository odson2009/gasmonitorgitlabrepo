﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("005000000000000_SensorResults")]
    internal class SensorResultsSeed : MigrationSeeding<SensorResult>
    {
        static DateTime _currentDate = DateTime.UtcNow;
        public static IEnumerable<SensorResult> List => new List<SensorResult>
        {

#region Sensor id = 1
            new SensorResult
            {
                SensorId = 1,
                TimeBegin = _currentDate.AddSeconds(5),
                TimeEnd = _currentDate.AddSeconds(10),
                Concentration = 3.00,
                WindSpeed = 1.5,
                WindDirection = 295,
            },
            new SensorResult
            {
                SensorId = 1,
                TimeBegin = _currentDate.AddSeconds(15),
                TimeEnd = _currentDate.AddSeconds(20),
                Concentration = 2.20,
                WindSpeed =1.5,
                WindDirection = 295,
            },
#endregion

#region Sensor id = 2
            new SensorResult
            {
                SensorId = 2,
                TimeBegin = _currentDate.AddSeconds(5),
                TimeEnd = _currentDate.AddSeconds(10),
                Concentration = 9.99,
                WindSpeed = 1.6,
                WindDirection = 290,
            },
            new SensorResult
            {
                SensorId = 2,
                TimeBegin = _currentDate.AddSeconds(15),
                TimeEnd = _currentDate.AddSeconds(20),
                Concentration = 3.30,
                WindSpeed = 1.3,
                WindDirection = 290,
            },

#endregion

#region Sensor id = 3
            new SensorResult
            {
                SensorId = 3,
                TimeBegin = _currentDate.AddSeconds(5),
                TimeEnd = _currentDate.AddSeconds(10),
                Concentration = 0.1,
                WindSpeed = 2.1,
                WindDirection = 305,
            },
            new SensorResult
            {
                SensorId = 3,
                TimeBegin = _currentDate.AddSeconds(15),
                TimeEnd = _currentDate.AddSeconds(20),
                Concentration = 5.6,
                WindSpeed = 2.2,
                WindDirection = 305,
            },
#endregion

#region Sensor id = 4
            new SensorResult
            {
                SensorId = 4,
                TimeBegin = _currentDate.AddSeconds(5),
                TimeEnd = _currentDate.AddSeconds(10),
                Concentration = 50.0,
                WindSpeed = 3.1,
                WindDirection = 270,
            },
            new SensorResult
            {
                SensorId = 4,
                TimeBegin = _currentDate.AddSeconds(15),
                TimeEnd = _currentDate.AddSeconds(20),
                Concentration = 15.0,
                WindSpeed = 1.1,
                WindDirection = 270,
            },
#endregion

#region Sensor id = 5
            new SensorResult
            {
                SensorId = 5,
                TimeBegin = _currentDate.AddSeconds(5),
                TimeEnd = _currentDate.AddSeconds(10),
                Concentration = 30.1,
                WindSpeed = 1.2,
                WindDirection = 295,
            },
            new SensorResult
            {
                SensorId = 5,
                TimeBegin = _currentDate.AddSeconds(15),
                TimeEnd = _currentDate.AddSeconds(20),
                Concentration = 45.0,
                WindSpeed = 7,
                WindDirection = 295,
            },
#endregion
        };

        protected override IEnumerable<SensorResult> Seed => List;
    }
}
