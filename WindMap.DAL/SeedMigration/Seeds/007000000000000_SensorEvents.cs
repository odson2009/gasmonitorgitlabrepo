﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("007000000000000_SensorEvents")]
    internal class SensorEventsSeed : MigrationSeeding<SensorEvent>
    {
        public static IEnumerable<SensorEvent> List => new List<SensorEvent>
        {
            new SensorEvent
            {
                SensorId = 1,
                PairedSensorId = 2,
                Calculations = new List<Calculation> {
                    new Calculation
                    {
                        EventTime = DateTime.Now.AddMinutes(-5),
                        SourceLatitude = 31.117205389653584,
                        SourceLongitude = 85.38043141365051,
                        ParallelWindDirection =6.705,
                        CrossWindDirection = 4.605,
                        AverageWindDirection = 6.605,
                        EmissionRate = 80.1125,
                    },
                    new Calculation
                    {
                        EventTime = DateTime.Now,
                        SourceLatitude = 31.117205389653584,
                        SourceLongitude = -85.38043141365051,
                        ParallelWindDirection =1.705,
                        CrossWindDirection = 4.605,
                        AverageWindDirection = 56.605,
                        EmissionRate = 0.1125,
                    }
                }
            }
        };

        protected override IEnumerable<SensorEvent> Seed => List;
    }
}
