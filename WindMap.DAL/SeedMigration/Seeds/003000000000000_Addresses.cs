﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("003000000000000_Addresses")]
    internal class AddressSeed : MigrationSeeding<Address>
    {
        public static IEnumerable<Address> List => new List<Address>
        {
            new Address
            {
                Type = "1",
                AddressValue = "3126 Watford Way",
                Zip = "53713",
                RegionId = 1,
                LocationId = 1
            },
            new Address
            {
                Type = "1",
                AddressValue = "4156 Oxford Str",
                Zip = "32233",
                RegionId = 2,
                LocationId = 2
            },
        };

        protected override IEnumerable<Address> Seed => List;
    }
}
