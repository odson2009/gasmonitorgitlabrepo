﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading.Tasks;
using WindMap.DAL.Models;

namespace WindMap.DAL.SeedMigration.Seeds
{
    [DbContext(typeof(ApplicationDbContext))]
    [SeedMigration("012000000000000_UiPermission")]
    internal class UiPermissionSeed : MigrationSeeding<UiPermission>
    {
        protected override async Task ConfigureRelations(DbContext dbContext)
        {
            await SetAllUiPermission(dbContext, "admin");
        }

        private async Task SetAllUiPermission(DbContext dbContext, string userName)
        {
            var user = await dbContext.Set<User>().FirstAsync(s => s.UserName == userName);
            var uiElementsId = await dbContext.Set<UiElement>().ToListAsync();

            foreach(var ui in uiElementsId)
            {
                var uiPermission = new UiPermission
                {
                    UserId = user.Id,
                    UiElementId = ui.Id
                };
                await dbContext.Set<UiPermission>().AddAsync(uiPermission);
            }
        }
    }
}
