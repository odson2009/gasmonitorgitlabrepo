﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;

namespace WindMap.DAL
{
    public interface IAppDbMigration
    {
        Task MigrateAsync();
        Task InitializeRegionAsync(IList<Region> regions);
    }

    internal class AppDbMigration : IAppDbMigration
    {
        protected readonly ApplicationDbContext _dbContext;

        public AppDbMigration(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task MigrateAsync()
        {
            return _dbContext.Database.MigrateAsync();
        }

        public async Task SeedAsync<T>(IList<T> data, Expression<Func<T, bool>> predicate, CancellationToken cancellationToken) where T : class
        {
            var entity = _dbContext.Set<T>();

            var entityExist = await entity.Where(x => data.Contains(x)).ToListAsync(cancellationToken);
            var entityCreate = data.Where(x => !entityExist.Contains(x)).ToList();

            await entity.AddRangeAsync(entityCreate, cancellationToken);

            entityExist.Union(entityCreate);
        }

        public async Task InitializeRegionAsync(IList<Region> regions)
        {
            foreach (var region in regions)
            {
                if (!await _dbContext.Regions.AnyAsync(u => u.Name == region.Name))
                {
                    await _dbContext.Regions.AddAsync(region);
                }
            }
            await _dbContext.SaveChangesAsync();
        }
    }
}
