﻿using Microsoft.Extensions.DependencyInjection;
using WindMap.DAL.Repositories;

namespace WindMap.DAL.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>();
            services.AddScoped<IAppDbMigration, AppDbMigration>();
            services.AddScoped<IMigrationRepository, MigrationRepository>();
            services.AddScoped<IDatabaseRepository, DatabaseRepository>();
            services.AddScoped<IRegionRepository, RegionRepository>();
            services.AddScoped<ISensorRepository, SensorRepository>();
            services.AddScoped<ISensorResultRepository, SensorResultRepository>();
            services.AddScoped<ISensorEventRepository, SensorEventRepository>();
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IUiElementRepository, UiElementRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRelationPermissionRepository, RelationPermissionRepository>();
            services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();  
        }
    }
}
