﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WindMap.DAL.Extensions
{
    public class AuthOptions
    {
        public const string ISSUER = "MyAuthServer";
        public const string KEY = "PV9Rab5QukSSQqZoNpmsGLwt324tFLK4Lv1tLsNKLfDkdZokkHejxMuWhun41ce";
        public const int LongLifetime = 720;              // 12 * 60 = 720 minute
        public const int ShortLifetime = 180;             // 3 * 60  = 180 minute
        public const int LifetimeForRefreshToken = 43200; // 30 * 24 * 60 = 43200 minute
        public const string Algorithm = "HS256";

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
