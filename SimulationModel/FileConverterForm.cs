﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SimulationModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OfficeOpenXml;

namespace SimulationModel
{
    public partial class FileConverterForm : Form
    {
        private FileService _fileService;
        public FileService FileService
        {
            get
            {
                return _fileService != null ? _fileService : new FileService();
            }
        }

        public FileConverterForm()
        {
            InitializeComponent();
        }

        private void FileConverterForm_Load(object sender, EventArgs e)
        {
            this.statusLabel.Text = string.Empty;
            this.errorLabel.Text = string.Empty;

            this.progressBar1.Minimum = 0;
            this.progressBar1.Maximum = 4;

            EnableDisableControls(false);
        }

        private void EnableDisableControls(bool isEnabled)
        {
            this.lbRowsProgress.Text = string.Empty;
            this.btnStart.Enabled = isEnabled;
            this.statusLabel.Text = string.Empty;
        }

        private void btnSourceFileOpen_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Title = "Open source file with raw data";
            this.openFileDialog1.Filter = "xlsx|*.xlsx";
            this.openFileDialog1.RestoreDirectory = false;
            this.openFileDialog1.CheckFileExists = true;
            this.openFileDialog1.CheckPathExists = true;

            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.tbSourceFilePath.Text = this.openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    this.errorLabel.Text = ex.Message;
                }
            }
        }

        protected class TestJsonModel {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        private void btnSaveFIle_Click(object sender, EventArgs e)
        {
            this.folderBrowserDialog1.ShowNewFolderButton = true;

            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.tbTragetFilePath.Text = this.folderBrowserDialog1.SelectedPath;
                    EnableDisableControls(true);
                }
                catch (Exception ex)
                {
                    this.errorLabel.Text = ex.Message;
                }
            }
        }

        private async void btStart_Click(object sender, EventArgs e)
        {
            try
            {
                this.progressBar1.Step = 1;
                List<string> compoundsList = new List<string>() { "Pentane", "Hexane", "Benzene", "Toluene" };

                List<SensorViewModel> sensorsList = new List<SensorViewModel>() {
                        // 31'st Street
                        new SensorViewModel(){ Id = 12, MonitorId = "302", SubstractDegree = 0, OnsiteWindMin = 0, OnsiteWindMax = 0,
                            SensorPRSIResults = new List<SensorResultPRSIViewModel>(),
                            SensorResults = new List<SensorResultViewModel>(),
                            SensorWindDataResuls = new List<SensorResultWindDataViewModel>()
                        },
                        // Logan Street
                        new SensorViewModel(){ Id = 13, MonitorId = "303", SubstractDegree = 0, OnsiteWindMin = 0, OnsiteWindMax = 0,
                            SensorPRSIResults = new List<SensorResultPRSIViewModel>(),
                            SensorResults = new List<SensorResultViewModel>(),
                            SensorWindDataResuls = new List<SensorResultWindDataViewModel>()},
                    };
                using (var ms = new MemoryStream())
                {
                    var file = File.OpenRead(this.openFileDialog1.FileName);
                    await file.CopyToAsync(ms);
                    using (ExcelPackage package = new ExcelPackage())
                    {
                        package.Load(ms);
                        ExcelWorksheet sheet_1 = package.Workbook.Worksheets[1];
                        ExcelWorksheet sheet_2 = package.Workbook.Worksheets[2];

                        foreach (string name in compoundsList)
                        {
                            sensorsList.First().CompoundName = name;
                            sensorsList.Last().CompoundName = name;

                            this.progressBar1.PerformStep();
                            this.progressBar1.Refresh();
                            this.lbRowsProgress.Text = "Parsed rows: 0 from 0";
                            this.lbRowsProgress.Refresh();

                            if (name == "Pentane")
                            {
                                //parse Sheet2 and get dates with values
                                sensorsList = FileService.GetResultsFromSheet2(sheet_2, sensorsList, lbRowsProgress);

                                //get only wind data for sensors
                                sensorsList.ForEach((sensor) => {
                                    this.statusLabel.Text = $"Working on \"{sensor.MonitorId}_WindData.json\" file...";

                                    var jsonWindDataResults = JsonConvert.SerializeObject(sensor.SensorWindDataResuls)
                                    .Replace("[{", "[\r\n\t{")
                                    .Replace("},{", "},\r\n\t{")
                                    .Replace("}]", "}\r\n]");

                                    using (var stream = new FileStream($"{this.folderBrowserDialog1.SelectedPath}/{sensor.MonitorId}_WindData.json", FileMode.Create, FileAccess.ReadWrite))
                                    {
                                        byte[] byteArray = Encoding.UTF8.GetBytes(jsonWindDataResults);
                                        stream.Write(byteArray, 0, byteArray.Length);
                                    }

                                    sensor.SensorResults = new List<SensorResultViewModel>();
                                    sensor.SensorWindDataResuls = new List<SensorResultWindDataViewModel>();
                                });

                                this.statusLabel.Text = $"Working on \"{name}\" file...";

                                //get avearage 20min data
                                sensorsList = FileService.GetAverageResults(sensorsList, lbRowsProgress);
                            }

                            //parse Sheet1 for DateTime and 4 compound
                            FileService.GetResultsFromSheet1(sheet_1, name, ref sensorsList, lbRowsProgress);
                            
                            var jsonResults = JsonConvert.SerializeObject(sensorsList)
                                .Replace("{\"id\"", "\r\n\t{\"id\"")
                                .Replace("\"sensorPRSIResults\":[{", "\r\n\t\t\"sensorPRSIResults\":[\r\n\t\t\t{")
                                .Replace("},{\"sensorId\"", "},\r\n\t\t\t{\"sensorId\"");
                            //string jsonFormatted = JValue.Parse(jsonResults).ToString(Formatting.Indented);

                            using (var stream = new FileStream($"{this.folderBrowserDialog1.SelectedPath}/{name}.json", FileMode.Create, FileAccess.ReadWrite))
                            {
                                byte[] byteArray = Encoding.UTF8.GetBytes(jsonResults);
                                stream.Write(byteArray, 0, byteArray.Length);
                            }
                        }
                    }
                }

                this.progressBar1.Value = this.progressBar1.Maximum;

                MessageBox.Show("Operation completed","Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");

                this.progressBar1.Value = this.progressBar1.Minimum;
            }
        }

        private void bntClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void statusLabel_TextChanged(object sender, EventArgs e)
        {
            ToolStripStatusLabel label = sender as ToolStripStatusLabel;
            this.statusLabel.Text = label.Text;

            this.statusStrip1.Refresh();
        }
    }
}
