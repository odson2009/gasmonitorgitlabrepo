﻿using Newtonsoft.Json;
using SimulationModel.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimulationModel
{
    public partial class MainForm : Form
    {
        private int timeOut;
        private string[] uri;
        private List<SensorViewModel> sensorsVM;
        private List<SensorResultWindDataViewModel> sensorsWindResultsVM;
        private List<SensorTempResultViewModel> sensorsTempResultsVM;
        private HttpClient _client;
        private HttpClient _secondClient;
        private bool _pause;
        private int _position;
        private int _windPosition;
        private int _concentrationPosition;
        private int _stop_on_step;
        private Action<string, HttpStatusCode, int, ListBox> action_info;
        private Action<HttpStatusCode> action_error;
        private CancellationTokenSource _cancelation;

        //For Texas algorithm
        private List<string> concentrationFilesNames = new List<string>() { "Pentane.json", "Hexane.json", "Benzene.json", "Toluene.json" };
        private List<string> windFilesNames = new List<string>() { "303_WindData.json", "302_WindData.json" };

        private FileConverterForm _fileConverterForm;
        protected FileConverterForm FileConverterForm {
            get {
                return _fileConverterForm != null ? _fileConverterForm : new FileConverterForm();
            }
        }

        private void InfoMessage(string dataString, HttpStatusCode statusCode, int count, ListBox listBoxControl)
        {
            toolStripStatusLabel1.Text = $"StatusCode: {statusCode},  count = {count}";

            var listBox = listBoxControl as ListBox;
            listBox.TopIndex = listBox.Items.Add(dataString);
        }

        private void ErrorMessage(HttpStatusCode statusCode)
        {
            toolStripStatusLabel2.Text = $"Error, StatusCode: {statusCode}";
        }

        public MainForm()
        {
            action_info = InfoMessage;
            action_error = ErrorMessage;
            InitializeComponent();

            toolStripStatusLabel1.Text = string.Empty;
            toolStripStatusLabel2.Text = string.Empty;

            SetValue();
        }

        private void SetValue()
        {
            _cancelation?.Cancel();
            _client?.Dispose();
            _secondClient?.Dispose();

            timeOut = 1000;
            uri = new string[] { "http://localhost:8000",
                                 "http://gas-monitor.azurewebsites.net",
                                 "https://gas-monitor-stage.azurewebsites.net"};
            _pause = false;
            _position = 0;
            _windPosition = 0;
            _concentrationPosition = 0;
            _stop_on_step = 0;

            if (comboBox_uri.Items.Count == 0)
            {
                comboBox_uri.Items.Clear();
                comboBox_uri.Items.AddRange(uri);
                comboBox_uri.SelectedIndex = 0;
            }

            textBox_timeOut.Text = timeOut.ToString();
            textBox_stop_on_step.Text = string.Empty;

            toolStripStatusLabel1.Text = string.Empty;
            toolStripStatusLabel2.Text = string.Empty;

            listBox1.Items.Clear();
            listBox2.Items.Clear();

            OnOffEnable("off");
        }

        private void OnOffEnable(string operation)
        {
            if(operation == "on")
            {
                comboBox_uri.Enabled = textBox_timeOut.Enabled = btn_start.Enabled = textBox_stop_on_step.Enabled = false;
                toolStripStatusLabel1.Text = string.Empty;
                toolStripStatusLabel2.Text = string.Empty;
            }
            else
            {
                comboBox_uri.Enabled = textBox_timeOut.Enabled = btn_stop.Enabled = btn_start.Enabled = textBox_stop_on_step.Enabled = true;
                toolStripStatusLabel1.Text = string.Empty;
                toolStripStatusLabel2.Text = string.Empty;
            }
        }

        private void btnConcentrationOpenFile_Click(object sender, EventArgs e)
        {
            sensorsVM = null;
            openDialogConcentration.Title = "Open file with Concentration data";
            openDialogConcentration.Filter = "json|*.json";
            openDialogConcentration.RestoreDirectory = true;
            openDialogConcentration.CheckFileExists = true;
            openDialogConcentration.CheckPathExists = true;

            if (openDialogConcentration.ShowDialog() == DialogResult.OK)
            {
                var data = File.ReadAllText(openDialogConcentration.FileName);
                tbConcentrationFilePath.Text = openDialogConcentration.FileName;
                try
                {
                    //For Test Data results
                    if (openDialogConcentration.SafeFileName.Equals("data.json", StringComparison.InvariantCultureIgnoreCase))
                    {
                        tbWindFilePath.Text = string.Empty;
                        sensorsWindResultsVM = null;
                        openDialogWind.Reset();

                        sensorsTempResultsVM = JsonConvert.DeserializeObject<List<SensorTempResultViewModel>>(data);

                        sensorsVM = new List<SensorViewModel>() {
                            new SensorViewModel(){
                                Id = 14,
                                MonitorId = "230033000c47373336373936",
                                SensorResults = new List<SensorResultViewModel>(),
                            },
                            new SensorViewModel(){
                                Id = 15,
                                MonitorId = "e00fce680511855cad44db77",
                                SensorResults = new List<SensorResultViewModel>(),
                            },
                        };

                        foreach (var sensor in sensorsVM) {

                            foreach (var tempResult in sensorsTempResultsVM)
                            {
                                if (sensor.MonitorId == tempResult.MonitorId)
                                {
                                    tempResult.TimeBegin = tempResult.TimeEnd;
                                    sensor.SensorResults.Add(new SensorResultViewModel() {
                                        SensorId = sensor.Id,
                                        WindSpeed = tempResult.WindSpeed,
                                        WindDirection = tempResult.WindDirection,
                                        Concentration = tempResult.Concentration,
                                        TimeEnd = tempResult.TimeEnd,
                                        TimeBegin = tempResult.TimeEnd
                                    }  );
                                }
                            }
                        }
                    }
                    else {
                        sensorsVM = JsonConvert.DeserializeObject<List<SensorViewModel>>(data);

                        //Reset Wind data and Use only Concentration for PRSI algorithm
                        if (openDialogConcentration.SafeFileName.ToLower().Contains("prsi"))
                        {
                            tbWindFilePath.Text = string.Empty;
                            sensorsWindResultsVM = null;
                            openDialogWind.Reset();
                        }

                        if (sensorsWindResultsVM != null && sensorsWindResultsVM.Any())
                        {
                            foreach (var s in sensorsVM)
                            {
                                s.SensorWindDataResuls = new List<SensorResultWindDataViewModel>();

                                foreach (var w in sensorsWindResultsVM)
                                {
                                    s.SensorWindDataResuls.Add(new SensorResultWindDataViewModel()
                                    {
                                        SensorId = s.Id,
                                        TimeBegin = w.TimeBegin,
                                        TimeEnd = w.TimeEnd,
                                        WindDirection = w.WindDirection,
                                        WindSpeed = w.WindSpeed,
                                    });
                                }
                            }
                        }
                        else
                        {
                            foreach (var s in sensorsVM)
                            {
                                foreach (var result in s.SensorResults)
                                {
                                    result.SensorId = s.Id;
                                }
                            }
                        }
                    }

                    toolStripStatusLabel2.Text = string.Empty;
                    groupBox1.Enabled = true;
                    groupBox3.Enabled = true;
                }
                catch
                {
                    groupBox1.Enabled = false;
                    groupBox3.Enabled = false;
                    toolStripStatusLabel2.Text = "Choose correct file !!!";
                }
            }
        }

        private void btnWindOpenFile_Click(object sender, EventArgs e)
        {
            sensorsWindResultsVM = null;
            openDialogWind.Title = "Open file with Wind data";
            openDialogWind.Filter = "json|*.json";
            openDialogWind.RestoreDirectory = true;
            openDialogWind.CheckFileExists = true;
            openDialogWind.CheckPathExists = true;

            if (openDialogWind.ShowDialog() == DialogResult.OK)
            {
                var data = File.ReadAllText(openDialogWind.FileName);
                tbWindFilePath.Text = openDialogWind.FileName;
                try
                {
                    sensorsWindResultsVM = JsonConvert.DeserializeObject<List<SensorResultWindDataViewModel>>(data);
                    sensorsWindResultsVM = sensorsWindResultsVM.OrderBy(r => r.TimeBegin).ToList();

                    foreach (var s in sensorsVM)
                    {
                        s.SensorWindDataResuls = new List<SensorResultWindDataViewModel>();

                        foreach (var w in sensorsWindResultsVM)
                        {
                            s.SensorWindDataResuls.Add(new SensorResultWindDataViewModel() {
                                SensorId = s.Id,
                                TimeBegin = w.TimeBegin,
                                TimeEnd = w.TimeEnd,
                                WindDirection = w.WindDirection,
                                WindSpeed = w.WindSpeed,
                            });
                        }
                    }

                    toolStripStatusLabel2.Text = string.Empty;
                    groupBox1.Enabled = true;
                }
                catch
                {
                    groupBox1.Enabled = false;
                    toolStripStatusLabel2.Text = "Choose correct file !!!";
                }
            }
        }

        private async void btn_start_Click(object sender, EventArgs e)
        {
            if(sensorsVM != null)
            {
                timeOut = 1000;
                //textBox_timeOut.Text = timeOut.ToString();

                int.TryParse(textBox_timeOut.Text, out timeOut);
                int.TryParse(textBox_stop_on_step.Text, out _stop_on_step);
                //---
                OnOffEnable("on");
                //---
                _cancelation = new CancellationTokenSource();
                try
                {
                    //detection of algorithm mode
                    if (concentrationFilesNames.Contains(openDialogConcentration.SafeFileName)) {

                        //Task t2 = SendWindDataAsync(_cancelation.Token);
                        //Task t1 = SendConcentrationDataAsync(_cancelation.Token);
                        //await Task.WhenAll(t2, t1);

                        await SendWindConcentrationDataAsync(_cancelation.Token);
                    }
                    else if (sensorsVM.First().SensorResults.FirstOrDefault() != null)
                    {
                        await SendTestTempDataAsync(_cancelation.Token);
                    }
                    else {
                        await SendDataAsync(_cancelation.Token);
                    }
                }
                catch(Exception ex) {
                    toolStripStatusLabel2.Text = ex.Message;
                }
            }
            else
            {
                toolStripStatusLabel2.Text = "Choose file !!!";
            }

        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            _cancelation?.Cancel();
            _position = 0;
            _windPosition = 0;
            _concentrationPosition = 0;
            timeOut = 1000;

            textBox_timeOut.Text = timeOut.ToString();
            OnOffEnable("off");
        }

        private async Task SendDataAsync(CancellationToken token)
        {
            _client = new HttpClient();

            _pause = false;

            var TMP = new List<SensorResultViewModel>();
            sensorsVM.ForEach(s => TMP.AddRange(s.SensorResults));
            TMP = TMP.OrderBy(r => r.TimeBegin).ToList();

            var TMP_NEW = new List<SensorResultPRSIViewModel>();
            sensorsVM.ForEach((s) => {
                if (s.SensorPRSIResults != null && s.SensorPRSIResults.Any()) {
                    TMP_NEW.AddRange(s.SensorPRSIResults);
                }
            });
            TMP_NEW = TMP_NEW.OrderBy(r => r.TimeBegin).ToList();

            int count = _stop_on_step != 0 ? _stop_on_step : (TMP.Any() ? TMP.Count : TMP_NEW.Count);

            for (int i = _position; i < count; i++)
            {
                if (token.IsCancellationRequested)
                    token.ThrowIfCancellationRequested();

                if (_pause)
                {
                    _position = i; break;
                }
                else
                {
                    int differentSeconds = 1;
                    var dataString = string.Empty;

                    if (TMP.Any())
                    {
                        differentSeconds = TMP[i].TimeEnd.Subtract(TMP[i].TimeBegin).Seconds;
                        dataString = JsonConvert.SerializeObject(TMP[i]);
                    }
                    else if (TMP_NEW.Any())
                    {
                        differentSeconds = 1;// TMP_NEW[i].TimeEnd.Subtract(TMP_NEW[i].TimeBegin).Seconds;
                        dataString = JsonConvert.SerializeObject(TMP_NEW[i]);
                    }

                  
                    var content = new StringContent(dataString, Encoding.UTF8, "application/json");

                    var postResult = await _client.PostAsync(comboBox_uri.SelectedItem.ToString() + "/api/development/process-data", content, token);
                    if (postResult.IsSuccessStatusCode)
                    {
                        Invoke(action_info, $"{i}.  {dataString}", postResult.StatusCode, i, this.listBox1);
                        await Task.Delay(differentSeconds * timeOut, token);
                    }
                    else
                    {
                        Invoke(action_error, postResult.StatusCode);
                    }

                    _position = i;
                }
                
            }
        }

        private async Task SendConcentrationDataAsync(CancellationToken token)
        {
            _client = new HttpClient();
            _pause = false;

            var TMP_PRSI = new List<SensorResultPRSIViewModel>();
            sensorsVM.ForEach((s) => {
                if (s.SensorPRSIResults != null && s.SensorPRSIResults.Any())
                {
                    TMP_PRSI.AddRange(s.SensorPRSIResults);
                }
            });
            TMP_PRSI = TMP_PRSI.OrderBy(r => r.TimeBegin).ToList();

            int count = _stop_on_step != 0 ? _stop_on_step : TMP_PRSI.Count;

            for (int i = _position; i < count; i++)
            {
                if (token.IsCancellationRequested)
                    token.ThrowIfCancellationRequested();

                int differentSeconds = 4;

                //skip first result
                if (i == 0)
                {
                    await Task.Delay(4 * timeOut, token);
                    continue;
                }

                if (_pause)
                {
                    _position = i; break;
                }
                else
                {
                    
                    var dataString = string.Empty;

                    //differentSeconds = TMP_PRSI[i].TimeEnd.Subtract(TMP_PRSI[i].TimeBegin).Seconds;
                    dataString = JsonConvert.SerializeObject(TMP_PRSI[i]);

                    var content = new StringContent(dataString, Encoding.UTF8, "application/json");

                    var postResult = await _client.PostAsync(comboBox_uri.SelectedItem.ToString() + "/api/development/process-data-concentration", content, token);
                    if (postResult.IsSuccessStatusCode)
                    {
                        Invoke(action_info, $"{i}.  {dataString}", postResult.StatusCode, i, this.listBox1);
                        await Task.Delay(differentSeconds * timeOut, token);
                    }
                    else
                    {
                        Invoke(action_error, postResult.StatusCode);
                    }

                    _position = i;
                }

            }
        }

        private async Task SendWindDataAsync(CancellationToken token)
        {
            _pause = false;
            _secondClient = new HttpClient();
            _windPosition = 1;

            sensorsWindResultsVM = sensorsWindResultsVM.OrderBy(r => r.TimeBegin).ToList();
            int count = _stop_on_step != 0 ? _stop_on_step : sensorsWindResultsVM.Count;

            for (int i = _windPosition; i < count; i++)
            {
                if (token.IsCancellationRequested)
                    token.ThrowIfCancellationRequested();

                if (_pause)
                {
                    _windPosition = i; break;
                }
                else
                {
                    int differentSeconds = 1;
                    var dataString = string.Empty;

                    //differentSeconds = sensorsWindResultsVM[i].TimeEnd.Subtract(sensorsWindResultsVM[i].TimeBegin).Seconds;
                    dataString = JsonConvert.SerializeObject(sensorsWindResultsVM[i-1]);

                    var content = new StringContent(dataString, Encoding.UTF8, "application/json");

                    var postResult = await _secondClient.PostAsync(comboBox_uri.SelectedItem.ToString() + "/api/development/process-data-wind", content, token);
                    if (postResult.IsSuccessStatusCode)
                    {
                        Invoke(action_info, $"{i}.  {dataString}", postResult.StatusCode, i, this.listBox2);
                        await Task.Delay(differentSeconds * timeOut, token);
                    }
                    else
                    {
                        Invoke(action_error, postResult.StatusCode);
                    }

                    _windPosition = i;
                }

            }
        }

        private bool isSavedTempResults = false;

        private async Task SendWindConcentrationDataAsync(CancellationToken token)
        {
            _client = new HttpClient();
            _pause = false;

            //var onwWindResults = sensorsVM.First().SensorWindDataResuls;

            var TMP_PRSI = new List<SensorResultPRSIViewModel>();
            sensorsVM.ForEach((s) => {

                if (sensorsWindResultsVM == null)
                {
                    TMP_PRSI.AddRange(s.SensorPRSIResults);
                }

                foreach (var w in s.SensorWindDataResuls)
                {
                    var concentrationResult = s.SensorPRSIResults.FirstOrDefault(r => r.TimeBegin == w.TimeBegin);
                    if (concentrationResult != null)
                    {
                        concentrationResult.TimeBegin = w.TimeBegin;
                        concentrationResult.TimeEnd = w.TimeEnd;
                        concentrationResult.WindSpeed = w.WindSpeed;
                        concentrationResult.WindDirection = w.WindDirection;
                        concentrationResult.IsConcentration = true;

                        TMP_PRSI.Add(concentrationResult);
                    }
                    else
                    {
                        TMP_PRSI.Add(new SensorResultPRSIViewModel()
                        {
                            SensorId = w.SensorId,
                            TimeBegin = w.TimeBegin,
                            TimeEnd = w.TimeEnd,
                            Concentration = 0,
                            WindDirection = w.WindDirection,
                            WindSpeed = w.WindSpeed,
                       
                        });
                    }
                }
            });
            TMP_PRSI = TMP_PRSI.OrderBy(r => r.TimeBegin).ThenBy(p => p.SensorId).ToList();

            //save TEM relults in file
            //if (!isSavedTempResults)
            //{
            //    var jsonResults = JsonConvert.SerializeObject(TMP_PRSI).Replace("[{", "[\r\n\t{")
            //                            .Replace("},{", "},\r\n\t{")
            //                            .Replace("}]", "}\r\n]"); ;
            //    //string jsonFormatted = JValue.Parse(jsonResults).ToString(Formatting.Indented);

            //    using (var stream = new FileStream(this.openDialogConcentration.FileName.Replace(this.openDialogConcentration.SafeFileName, "outPutResults.json"), FileMode.Create, FileAccess.ReadWrite))
            //    {
            //        byte[] byteArray = Encoding.UTF8.GetBytes(jsonResults);
            //        stream.Write(byteArray, 0, byteArray.Length);
            //    }

            //    isSavedTempResults = true;
            //}

            //!!! Half of all results 
            int count = _stop_on_step != 0 ? _stop_on_step : TMP_PRSI.Count;
            int windSensorId = 0;

            for (int i = _position; i < count; i++)
            {
                if (token.IsCancellationRequested)
                    token.ThrowIfCancellationRequested();

                //1000ms = 1sec
                int differentSeconds = 1;

                if (_pause)
                {
                    _position = i; break;
                }
                else
                {
                    var windDataString = string.Empty;
                    var fullDataString = string.Empty;
                    var prityConcentrationDataString = string.Empty;

                    //differentSeconds = TMP_PRSI[i].TimeEnd.Subtract(TMP_PRSI[i].TimeBegin).Seconds;
                    var resultModel = TMP_PRSI[i];

                    if (windSensorId == 0)
                    {
                        windSensorId = sensorsWindResultsVM != null && sensorsWindResultsVM.Any() ? sensorsWindResultsVM.First().SensorId : resultModel.SensorId;
                    }

                    var sensorWindConcentrationResultViewModel = new SensorWindConcentrationResultViewModel()
                    {
                        SensorId = resultModel.SensorId,
                        Concentration = resultModel.Concentration,
                        WindDirection = resultModel.WindDirection,
                        WindSpeed = resultModel.WindSpeed,
                        TimeBegin = resultModel.TimeBegin,
                        TimeEnd = resultModel.TimeEnd,
                        TEMP = resultModel.TEMP,
                        GUST = resultModel.GUST,
                        HZD_V = resultModel.HZD_V,
                        IsConcentration = resultModel.IsConcentration,
                    };

                    fullDataString = JsonConvert.SerializeObject(sensorWindConcentrationResultViewModel);

                    //Send both Wind and Concentration data
                    if (sensorsWindResultsVM != null && sensorsWindResultsVM.Any())
                    {
                        if (resultModel.SensorId == windSensorId)
                        {
                            windDataString = JsonConvert.SerializeObject(new SensorResultWindDataViewModel()
                            {
                                SensorId = windSensorId,
                                TimeBegin = resultModel.TimeBegin,
                                TimeEnd = resultModel.TimeEnd,
                                WindDirection = resultModel.WindDirection,
                                WindSpeed = resultModel.WindSpeed
                            });
                        }

                        if (resultModel.IsConcentration)
                        {
                            resultModel.TimeEnd = resultModel.TimeBegin.AddMinutes(20);
                            prityConcentrationDataString = JsonConvert.SerializeObject(resultModel);
                        }
                    }
                    else {
                        prityConcentrationDataString = JsonConvert.SerializeObject(resultModel);
                    }

                    //Format request data string for API
                    var content = new StringContent(fullDataString, Encoding.UTF8, "application/json");

                    var postResult = await _client.PostAsync(comboBox_uri.SelectedItem.ToString() + "/api/development/process-data-wind-concentartion", content, token);
                    if (postResult.IsSuccessStatusCode)
                    {
                        //Put data string into Wind lixt box
                        if (!String.IsNullOrEmpty(windDataString))
                        {
                            Invoke(action_info, $"{_windPosition}.  {windDataString}", postResult.StatusCode, _windPosition++, this.listBox2);
                        }

                        //Put data string into Concentration lixt box
                        if (!String.IsNullOrEmpty(prityConcentrationDataString))
                        {
                            Invoke(action_info, $"{_concentrationPosition++}.  {prityConcentrationDataString}", postResult.StatusCode, _windPosition, this.listBox1);
                        }
                      
                        await Task.Delay(differentSeconds * timeOut, token);
                    }
                    else
                    {
                        Invoke(action_error, postResult.StatusCode);
                    }

                    _position = i;
                }

            }
        }

        private async Task SendTestTempDataAsync(CancellationToken token)
        {
            _client = new HttpClient();

            _pause = false;

            var TMP = new List<SensorResultViewModel>();
            sensorsVM.ForEach(s => TMP.AddRange(s.SensorResults));
            TMP = TMP.Distinct().ToList();

            TMP = TMP.OrderBy(r => r.TimeEnd).ToList();

            int count = _stop_on_step != 0 ? _stop_on_step : TMP.Count();

            for (int i = _position; i < count; i++)
            {
                if (token.IsCancellationRequested)
                    token.ThrowIfCancellationRequested();

                if (_pause)
                {
                    _position = i; break;
                }
                else
                {
                    int differentSeconds = 1;
                    var dataString = string.Empty;


                    dataString = JsonConvert.SerializeObject(new SensorResultViewModel() {
                        SensorId = TMP[i].SensorId,
                        TimeBegin = TMP[i].TimeBegin,
                        TimeEnd = TMP[i].TimeEnd,
                        WindDirection = TMP[i].WindDirection,
                        WindSpeed = TMP[i].WindSpeed,
                        Concentration = TMP[i].Concentration,
                    });
                   

                    var content = new StringContent(dataString, Encoding.UTF8, "application/json");

                    var postResult = await _client.PostAsync(comboBox_uri.SelectedItem.ToString() + "/api/development/process-data", content, token);
                    if (postResult.IsSuccessStatusCode)
                    {
                        Invoke(action_info, $"{i}.  {dataString}", postResult.StatusCode, i, this.listBox1);
                        await Task.Delay(differentSeconds * timeOut, token);
                    }
                    else
                    {
                        Invoke(action_error, postResult.StatusCode);
                    }

                    _position = i;
                }

            }
        }

        private void btn_pause_Click(object sender, EventArgs e)
        {
            _pause = true;
            btn_start.Enabled = true;
        }

        private void btn_increase_speed_Click(object sender, EventArgs e)
        {
            timeOut = timeOut / 2;
            textBox_timeOut.Text = timeOut.ToString();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var client = new HttpClient();
            var result = await client.GetAsync(comboBox_uri.SelectedItem.ToString() + "/api/development/refresh-data");

            if (result.IsSuccessStatusCode)
            {
                toolStripStatusLabel1.Text = $"Clear cache: {result.StatusCode}";
            }
            else
            {
                toolStripStatusLabel2.Text = $"Clear cache: {result.StatusCode}";
            }
        }

        private void btn_slow_speed_Click(object sender, EventArgs e)
        {
            timeOut = timeOut == 0 ? 1 : timeOut;
            timeOut = timeOut * 2 < 1000 ? timeOut * 2 : 1000;
            textBox_timeOut.Text = timeOut.ToString();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FileConverterForm.ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            this.TopMost = checkBox.Checked;
        }

    }
}
