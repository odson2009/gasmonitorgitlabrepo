﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SimulationModel.Model
{
    public class SensorResultViewModel
    {

        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("concentration")]
        public double Concentration { get; set; }

        [JsonProperty("windSpeed")]
        public double WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        public double WindDirection { get; set; }

        [JsonProperty("sensor")]
        public SensorViewModel SensorViewModel { get; set; }
    }

    public class SensorResultPRSIViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("concentration")]
        public double Concentration { get; set; }

        [JsonIgnore]
        [JsonProperty("windSpeed")]
        public double WindSpeed { get; set; }

        [JsonIgnore]
        [JsonProperty("windDirection")]
        public double WindDirection { get; set; }

        //Additional fields
        [JsonProperty("TEMP")]
        public double TEMP { get; set; }

        [JsonProperty("GUST")]
        public double GUST { get; set; }

        [JsonIgnore]
        [JsonProperty("WSPD")]
        public double WSPD { get; set; }

        [JsonIgnore]
        [JsonProperty("WDIR")]
        public double WDIR { get; set; }

        [JsonIgnore]
        [JsonProperty("HZD_S")]
        public double HZD_S { get; set; }

        [JsonIgnore]
        [JsonProperty("RESSP")]
        public double RESSP { get; set; }

        [JsonIgnore]
        [JsonProperty("RESDR")]
        public double RESDR { get; set; }

        [JsonProperty("HZD_V")]
        public double HZD_V { get; set; }

        [JsonIgnore]
        public bool IsAverage { get; set; }

        [JsonIgnore]
        [JsonProperty("isConcentration")]
        public bool IsConcentration { get; set; }
    }

    public class SensorResultWindDataViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("windSpeed")]
        public double WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        public double WindDirection { get; set; }
    }

    public class SensorWindConcentrationResultViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("concentration")]
        public double Concentration { get; set; }

        [JsonProperty("windSpeed")]
        public double WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        public double WindDirection { get; set; }

        [JsonProperty("TEMP")]
        public double TEMP { get; set; }

        [JsonProperty("GUST")]
        public double GUST { get; set; }

        [JsonProperty("HZD_V")]
        public double HZD_V { get; set; }

        [JsonProperty("isConcentration")]
        public bool IsConcentration { get; set; }
    }

    public class SensorTempResultViewModel
    {
        [JsonProperty("sensorId")]
        public string MonitorId { get; set; }

        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("concentration")]
        public double Concentration { get; set; }

        [JsonProperty("windSpeed")]
        public double WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        public double WindDirection { get; set; }
    }

}