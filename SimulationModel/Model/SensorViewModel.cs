﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SimulationModel.Model
{
    public class SensorViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("monitorId")]
        public string MonitorId { get; set; }

        [JsonProperty("compoundName")]
        public string CompoundName { get; set; }

        [JsonProperty("onsiteWindMin")]
        public double OnsiteWindMin { get; set; }

        [JsonProperty("onsiteWindMax")]
        public double OnsiteWindMax { get; set; }

        [JsonProperty("substractDegree")]
        public int SubstractDegree { get; set; }

        [JsonProperty("sensorResults")]
        public ICollection<SensorResultViewModel> SensorResults { get; set; }

        [JsonProperty("sensorWindDataResuls")]
        public ICollection<SensorResultWindDataViewModel> SensorWindDataResuls { get; set; }

        [JsonProperty("sensorPRSIResults")]
        public ICollection<SensorResultPRSIViewModel> SensorPRSIResults { get; set; }

    }
}