﻿namespace SimulationModel
{
    partial class FileConverterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSourceFileOpen = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSourceFilePath = new System.Windows.Forms.TextBox();
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.bntClose = new System.Windows.Forms.Button();
            this.gbTarget = new System.Windows.Forms.GroupBox();
            this.tbTragetFilePath = new System.Windows.Forms.TextBox();
            this.btnSaveFIle = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnStart = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.errorLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lbRowsProgress = new System.Windows.Forms.Label();
            this.gbSource.SuspendLayout();
            this.gbTarget.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSourceFileOpen
            // 
            this.btnSourceFileOpen.Location = new System.Drawing.Point(150, 29);
            this.btnSourceFileOpen.Name = "btnSourceFileOpen";
            this.btnSourceFileOpen.Size = new System.Drawing.Size(90, 23);
            this.btnSourceFileOpen.TabIndex = 0;
            this.btnSourceFileOpen.Text = "Open file";
            this.btnSourceFileOpen.UseVisualStyleBackColor = true;
            this.btnSourceFileOpen.Click += new System.EventHandler(this.btnSourceFileOpen_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select cource XLSX file:";
            // 
            // tbSourceFilePath
            // 
            this.tbSourceFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSourceFilePath.Location = new System.Drawing.Point(251, 31);
            this.tbSourceFilePath.Name = "tbSourceFilePath";
            this.tbSourceFilePath.ReadOnly = true;
            this.tbSourceFilePath.Size = new System.Drawing.Size(692, 20);
            this.tbSourceFilePath.TabIndex = 2;
            // 
            // gbSource
            // 
            this.gbSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSource.Controls.Add(this.tbSourceFilePath);
            this.gbSource.Controls.Add(this.btnSourceFileOpen);
            this.gbSource.Controls.Add(this.label1);
            this.gbSource.Location = new System.Drawing.Point(12, 12);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(960, 73);
            this.gbSource.TabIndex = 3;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // bntClose
            // 
            this.bntClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntClose.Location = new System.Drawing.Point(890, 284);
            this.bntClose.Name = "bntClose";
            this.bntClose.Size = new System.Drawing.Size(83, 23);
            this.bntClose.TabIndex = 3;
            this.bntClose.Text = "Close";
            this.bntClose.UseVisualStyleBackColor = true;
            this.bntClose.Click += new System.EventHandler(this.bntClose_Click);
            // 
            // gbTarget
            // 
            this.gbTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTarget.Controls.Add(this.tbTragetFilePath);
            this.gbTarget.Controls.Add(this.btnSaveFIle);
            this.gbTarget.Controls.Add(this.label2);
            this.gbTarget.Location = new System.Drawing.Point(12, 103);
            this.gbTarget.Name = "gbTarget";
            this.gbTarget.Size = new System.Drawing.Size(960, 75);
            this.gbTarget.TabIndex = 4;
            this.gbTarget.TabStop = false;
            this.gbTarget.Text = "Target";
            // 
            // tbTragetFilePath
            // 
            this.tbTragetFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTragetFilePath.Location = new System.Drawing.Point(251, 31);
            this.tbTragetFilePath.Name = "tbTragetFilePath";
            this.tbTragetFilePath.ReadOnly = true;
            this.tbTragetFilePath.Size = new System.Drawing.Size(692, 20);
            this.tbTragetFilePath.TabIndex = 2;
            // 
            // btnSaveFIle
            // 
            this.btnSaveFIle.Location = new System.Drawing.Point(150, 29);
            this.btnSaveFIle.Name = "btnSaveFIle";
            this.btnSaveFIle.Size = new System.Drawing.Size(90, 23);
            this.btnSaveFIle.TabIndex = 0;
            this.btnSaveFIle.Text = "Choose folder";
            this.btnSaveFIle.UseVisualStyleBackColor = true;
            this.btnSaveFIle.Click += new System.EventHandler(this.btnSaveFIle_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Select target JSON folder:";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(162, 194);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(810, 23);
            this.progressBar1.TabIndex = 5;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(18, 194);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(126, 23);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.errorLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 313);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(984, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(39, 17);
            this.statusLabel.Text = "Status";
            this.statusLabel.TextChanged += new System.EventHandler(this.statusLabel_TextChanged);
            // 
            // errorLabel
            // 
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(35, 17);
            this.errorLabel.Text = "Error!";
            // 
            // lbRowsProgress
            // 
            this.lbRowsProgress.AutoSize = true;
            this.lbRowsProgress.Location = new System.Drawing.Point(18, 236);
            this.lbRowsProgress.Name = "lbRowsProgress";
            this.lbRowsProgress.Size = new System.Drawing.Size(109, 13);
            this.lbRowsProgress.TabIndex = 8;
            this.lbRowsProgress.Text = "Parsed rows: 0 from 0";
            // 
            // FileConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 335);
            this.Controls.Add(this.lbRowsProgress);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.gbTarget);
            this.Controls.Add(this.bntClose);
            this.Controls.Add(this.gbSource);
            this.MinimumSize = new System.Drawing.Size(355, 320);
            this.Name = "FileConverterForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "File Converter XLSX -> JSON";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FileConverterForm_Load);
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.gbTarget.ResumeLayout(false);
            this.gbTarget.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSourceFileOpen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSourceFilePath;
        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button bntClose;
        private System.Windows.Forms.GroupBox gbTarget;
        private System.Windows.Forms.TextBox tbTragetFilePath;
        private System.Windows.Forms.Button btnSaveFIle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel errorLabel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        public System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.Label lbRowsProgress;
    }
}