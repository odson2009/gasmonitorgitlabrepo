﻿using OfficeOpenXml;
using SimulationModel.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimulationModel
{
    public class FileService
    {
       
        public void GetResultsFromSheet1(ExcelWorksheet sheet, string compoundName, ref List<SensorViewModel> sensorsList, Label lbRowsProgress)
        {
            SensorViewModel firstSensor = sensorsList.Last();
            SensorViewModel secondSensor = sensorsList.First();

            int compoundColumn_1 = 0;
            int dateColumn_1 = 2;

            switch (firstSensor.CompoundName)
            {
                case "Pentane":
                    compoundColumn_1 = 3;
                    break;
                case "Hexane":
                    compoundColumn_1 = 4;
                    break;
                case "Benzene":
                    compoundColumn_1 = 5;
                    break;
                case "Toluene":
                    compoundColumn_1 = 6;
                    break;

                default: break;
            }

            int rowCount = sheet.Dimension.Rows;
            int colCount = sheet.Dimension.Columns;

            int row = 11199; //14/05/2010 12:40 //23;
            int columnCoeff = 8;

            //sensorId timeBegin timeEnd concentration windSpeed windDirection
            var col_date_1 = sheet.Cells[row, dateColumn_1];
            var col_concentartion_1 = sheet.Cells[row, compoundColumn_1];

            DateTime timeBegin = default(DateTime);
            DateTime timeEnd = default(DateTime);
            double value;
            DateTime dateTime2;

            try
            {
                for (; row <= rowCount; row++)
                {
                    lbRowsProgress.Text = $"Parse rows (sheet1): {row} from {rowCount}";
                    lbRowsProgress.Refresh();
                    var date = sheet.Cells[row, dateColumn_1 + columnCoeff].Value;

                    //parse DateTime for second sensor
                    if (date != null &&
                       !String.IsNullOrWhiteSpace(date.ToString()))
                    {
                        if (!DateTime.TryParse(date.ToString(), out dateTime2))
                        {
                            continue;
                        }

                        timeBegin = DateTime.ParseExact(date.ToString(), "dd.MM.yyyy H:mm:ss", new CultureInfo("en-US"));
                        //timeEnd = timeBegin.AddMinutes(20);
                                               
                        var existFirstSensor = firstSensor.SensorPRSIResults.FirstOrDefault(r => r.TimeBegin == timeBegin);
                        if (existFirstSensor != null)
                        {
                            //parse concentration for first sensor 
                            string concentration_1 = sheet.Cells[row, compoundColumn_1].Value != null
                                    && !String.IsNullOrWhiteSpace(sheet.Cells[row, compoundColumn_1].Value.ToString())
                                    ? sheet.Cells[row, compoundColumn_1].Value.ToString() : "0";

                            //throw new Exception($"ExistFirstSensor Exception!");
                            existFirstSensor.Concentration = Double.TryParse(concentration_1, out value) ? value : 0.0;
                        }

                        var existSecondSensor = secondSensor.SensorPRSIResults.FirstOrDefault(r => r.TimeBegin == timeBegin);
                        if (existSecondSensor != null)
                        {
                            //parse concentration for second sensor 
                            string concentration_2 = sheet.Cells[row, compoundColumn_1 + columnCoeff].Value != null
                                   && !String.IsNullOrWhiteSpace(sheet.Cells[row, compoundColumn_1 + columnCoeff].Value.ToString())
                                   ? sheet.Cells[row, compoundColumn_1 + columnCoeff].Value.ToString() : "0";

                            //throw new Exception($"ExistSecondSensor Exception!");
                            existSecondSensor.Concentration = Double.TryParse(concentration_2, out value) ? value : 0.0;
                        }

                       
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Sheet1 Exception! {ex.Message} on row {row}");
            }
        }

        public List<SensorViewModel> GetResultsFromSheet2(ExcelWorksheet sheet, List<SensorViewModel> sensorsList, Label lbRowsProgress)
        {
            SensorViewModel firstSensor = sensorsList.First();
            SensorViewModel secondSensor = sensorsList.Last();

            int dateColumn_1 = 1;
            int rowCount = sheet.Dimension.Rows;
            int colCount = sheet.Dimension.Columns;

            int row = 143585;//14.05.2010  12:40:00 //9;

            //sensorId timeBegin
            var col_date_1 = sheet.Cells[row, dateColumn_1].Start.Column;//+3

            var col_TEMP_1 = sheet.Cells[row, 5].Start.Column;//+14
            var col_GUST_1 = sheet.Cells[row, 6].Start.Column;//+14
            var col_WSPD_1 = sheet.Cells[row, 7].Start.Column;//+14
            var col_WDIR_1 = sheet.Cells[row, 8].Start.Column;//...
            var col_HZD_S_1 = sheet.Cells[row, 9].Start.Column;//...
            var col_RESSP_1 = sheet.Cells[row, 10].Start.Column;//+12 - Use like Wind Speed
            var col_RESDR_1 = sheet.Cells[row, 11].Start.Column;//+12 - use like Wind Direction
            var col_HZD_V_1 = sheet.Cells[row, 12].Start.Column;//+12

            int diff_1 = 14;
            int diff_2 = 12;

            DateTime timeBegin = default(DateTime);
            DateTime timeEnd = default(DateTime);
            DateTime dateTime2;
            double value;

            try
            {
                for (; row <= rowCount; row++)
                {
                    lbRowsProgress.Text = $"Parsed rows: {row} from {rowCount}";;
                    lbRowsProgress.Refresh();
                    var date = sheet.Cells[row, dateColumn_1].Value;

                    //parse DateTime for second sensor
                    if (date != null &&
                       !String.IsNullOrWhiteSpace(date.ToString()))
                    {
                        if (!DateTime.TryParse(date.ToString(), out dateTime2))
                        {
                            continue;
                        }

                        //string startRawData = "10.12.2009 7:20:00";
                        timeBegin = DateTime.ParseExact(date.ToString(), "dd.MM.yyyy H:mm:ss", new CultureInfo("en-US"));
                        //if (timeBegin < DateTime.ParseExact(startRawData, "dd.MM.yyyy H:mm:ss", new CultureInfo("en-US")))
                        //{
                        //    continue;
                        //}

                        timeEnd = timeBegin.AddMinutes(5);

                        //parse data for first sensor 
                        string TEMP = sheet.Cells[row, col_TEMP_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_TEMP_1].Value.ToString())
                                ? sheet.Cells[row, col_TEMP_1].Value.ToString() : "0";
                        string GUST = sheet.Cells[row, col_GUST_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_GUST_1].Value.ToString())
                                ? sheet.Cells[row, col_GUST_1].Value.ToString() : "0";
                        string WSPD = sheet.Cells[row, col_WSPD_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_WSPD_1].Value.ToString())
                                ? sheet.Cells[row, col_WSPD_1].Value.ToString() : "0";
                        string WDIR = sheet.Cells[row, col_WDIR_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_WDIR_1].Value.ToString())
                                ? sheet.Cells[row, col_WDIR_1].Value.ToString() : "0";
                        string HZD_S = sheet.Cells[row, col_HZD_S_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_HZD_S_1].Value.ToString())
                                ? sheet.Cells[row, col_HZD_S_1].Value.ToString() : "0";
                        string RESSP = sheet.Cells[row, col_RESSP_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_RESSP_1].Value.ToString())
                                ? sheet.Cells[row, col_RESSP_1].Value.ToString() : "0";
                        string RESDR = sheet.Cells[row, col_RESDR_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_RESDR_1].Value.ToString())
                                ? sheet.Cells[row, col_RESDR_1].Value.ToString() : "0";
                        string HZD_V = sheet.Cells[row, col_HZD_V_1].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_HZD_V_1].Value.ToString())
                                ? sheet.Cells[row, col_HZD_V_1].Value.ToString() : "0";

                        SensorResultPRSIViewModel sensorResult_1 = new SensorResultPRSIViewModel()
                        {
                            SensorId = firstSensor.Id,
                            TimeBegin = timeBegin,
                            TimeEnd = timeEnd,

                            WindSpeed = (Double.TryParse(RESSP, out value) ? value : 0.0),
                            WindDirection = Double.TryParse(RESDR, out value) ? value : 0.0,

                            TEMP = Double.TryParse(TEMP, out value) ? value : 0.0,
                            GUST = (Double.TryParse(GUST, out value) ? value : 0.0),
                            WSPD = (Double.TryParse(WSPD, out value) ? value : 0.0),
                            WDIR = Double.TryParse(WDIR, out value) ? value : 0.0,

                            HZD_S = Double.TryParse(HZD_S, out value) ? value : 0.0,
                            RESSP = (Double.TryParse(RESSP, out value) ? value : 0.0),
                            RESDR = Double.TryParse(RESDR, out value) ? value : 0.0,
                            HZD_V = Double.TryParse(HZD_V, out value) ? value : 0.0,
                        };

                        firstSensor.SensorPRSIResults.Add(sensorResult_1);

                        firstSensor.SensorWindDataResuls.Add(new SensorResultWindDataViewModel() {
                            SensorId = sensorResult_1.SensorId,
                            TimeBegin = sensorResult_1.TimeBegin,
                            TimeEnd = sensorResult_1.TimeEnd,
                            WindDirection = sensorResult_1.WindDirection,
                            WindSpeed = sensorResult_1.WindSpeed,
                        });

                        var secondDate = sheet.Cells[row, dateColumn_1 + diff_1].Value;
                        if (secondDate != null && !String.IsNullOrWhiteSpace(secondDate.ToString()))
                        {
                            //parse data for second sensor 
                            TEMP = sheet.Cells[row, col_TEMP_1 + diff_1].Value != null
                                    && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_TEMP_1 + diff_1].Value.ToString())
                                    ? sheet.Cells[row, col_TEMP_1 + diff_1].Value.ToString() : "0";
                            GUST = sheet.Cells[row, col_GUST_1 + diff_1].Value != null
                                    && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_GUST_1 + diff_1].Value.ToString())
                                    ? sheet.Cells[row, col_GUST_1 + diff_1].Value.ToString() : "0";
                            WSPD = sheet.Cells[row, col_WSPD_1 + diff_1].Value != null
                                    && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_WSPD_1 + diff_1].Value.ToString())
                                    ? sheet.Cells[row, col_WSPD_1 + diff_1].Value.ToString() : "0";
                            //WDIR = "0";
                            HZD_S = "0";
                            //RESSP = sheet.Cells[row, col_RESSP_1 + diff_2].Value != null
                            //        && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_RESSP_1 + diff_2].Value.ToString())
                            //        ? sheet.Cells[row, col_RESSP_1 + diff_2].Value.ToString() : "0";
                            //RESDR = sheet.Cells[row, col_RESDR_1 + diff_2].Value != null
                            //        && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_RESDR_1 + diff_2].Value.ToString())
                            //        ? sheet.Cells[row, col_RESDR_1 + diff_2].Value.ToString() : "0";
                            HZD_V = sheet.Cells[row, col_HZD_V_1 + diff_2].Value != null
                                    && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_HZD_V_1 + diff_2].Value.ToString())
                                    ? sheet.Cells[row, col_HZD_V_1 + diff_2].Value.ToString() : "0";
                        }
                        else {

                            TEMP = GUST = WSPD = WDIR = HZD_S = RESSP = RESDR = HZD_V = "0";
                        }

                        SensorResultPRSIViewModel sensorResult_2 = new SensorResultPRSIViewModel()
                        {
                            SensorId = secondSensor.Id,
                            TimeBegin = timeBegin,
                            TimeEnd = timeEnd,

                            WindSpeed = (Double.TryParse(RESSP, out value) ? value : 0.0),
                            WindDirection = Double.TryParse(RESDR, out value) ? value : 0.0,

                            TEMP = Double.TryParse(TEMP, out value) ? value : 0.0,
                            GUST = Double.TryParse(GUST, out value) ? value : 0.0,
                            WSPD = Double.TryParse(WSPD, out value) ? value : 0.0,//used from first sensor
                            WDIR = Double.TryParse(WDIR, out value) ? value : 0.0,//used from first sensor

                            HZD_S = Double.TryParse(HZD_S, out value) ? value : 0.0,
                            RESSP = Double.TryParse(RESSP, out value) ? value : 0.0,
                            RESDR = Double.TryParse(RESDR, out value) ? value : 0.0,
                            HZD_V = Double.TryParse(HZD_V, out value) ? value : 0.0,
                        };

                        secondSensor.SensorPRSIResults.Add(sensorResult_2);


                        RESSP = sheet.Cells[row, col_RESSP_1 + diff_2].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_RESSP_1 + diff_2].Value.ToString())
                                ? sheet.Cells[row, col_RESSP_1 + diff_2].Value.ToString() : "0";
                        RESDR = sheet.Cells[row, col_RESDR_1 + diff_2].Value != null
                                && !String.IsNullOrWhiteSpace(sheet.Cells[row, col_RESDR_1 + diff_2].Value.ToString())
                                ? sheet.Cells[row, col_RESDR_1 + diff_2].Value.ToString() : "0";

                        secondSensor.SensorWindDataResuls.Add(new SensorResultWindDataViewModel()
                        {
                            SensorId = sensorResult_2.SensorId,
                            TimeBegin = sensorResult_2.TimeBegin,
                            TimeEnd = sensorResult_2.TimeEnd,
                            WindDirection = Double.TryParse(RESDR, out value) ? value : 0.0,
                            WindSpeed = Double.TryParse(RESSP, out value) ? value : 0.0,
                        });
                    }
                }

                return sensorsList;
            }
            catch (Exception ex)
            {
                throw new Exception($"Sheet2 Exception! {ex.Message} on row {row}");
            }
        }

        public List<SensorViewModel> GetAverageResults(List<SensorViewModel> sensors, Label lbRowsProgress)
        {
            foreach (var sensor in sensors)
            {
                List<SensorResultPRSIViewModel> tempResultsList = new List<SensorResultPRSIViewModel>();
                SensorResultPRSIViewModel tempResult = new SensorResultPRSIViewModel();

                int devider = 5;

                for (int i = 0; i < sensor.SensorPRSIResults.Count; i++)
                {
                    lbRowsProgress.Text = $"Average result rows ({sensor.MonitorId}): {i} from {sensor.SensorPRSIResults.Count}"; ;
                    lbRowsProgress.Refresh();

                    if (i == 0)
                    {
                        tempResult = sensor.SensorPRSIResults.First();
                        tempResult.TimeEnd = tempResult.TimeBegin;
                    }
                    else if (i % 4 == 0)
                    {
                        tempResult = sensor.SensorPRSIResults.Skip(i - 4).Take(devider).GroupBy(r => r.SensorId).Select(s => new SensorResultPRSIViewModel
                        {
                            SensorId = s.Key,
                            TimeBegin = s.Last().TimeBegin.AddMinutes(-20),
                            TimeEnd = s.Last().TimeBegin,

                            WindSpeed = Math.Round(s.Sum(v => v.WindSpeed) / devider, 1),
                            WindDirection = Math.Round(s.Sum(v => v.WindDirection) / devider, 1),

                            TEMP = Math.Round(s.Sum(v => v.TEMP) / devider, 1),
                            GUST = Math.Round(s.Sum(v => v.GUST) / devider, 1),
                            WSPD = Math.Round(s.Sum(v => v.WSPD) / devider, 1),
                            WDIR = Math.Round(s.Sum(v => v.WDIR) / devider, 1),

                            HZD_S = Math.Round(s.Sum(v => v.HZD_S) / devider, 1),
                            RESSP = Math.Round(s.Sum(v => v.RESSP) / devider, 1),
                            RESDR = Math.Round(s.Sum(v => v.RESDR) / devider, 1),
                            HZD_V = Math.Round(s.Sum(v => v.HZD_V) / devider, 1),
                        }).First();
                    }
                    else
                    {
                        continue;
                    }

                    tempResult.IsAverage = true;
                    tempResultsList.Add(tempResult);
                }

                sensor.SensorPRSIResults = tempResultsList;
            }

            return sensors;
        }
    }
}
