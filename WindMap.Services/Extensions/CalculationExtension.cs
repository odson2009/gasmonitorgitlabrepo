﻿using System;
using System.Collections.Generic;
using System.Text;
using WindMap.Services.Models;

namespace WindMap.Services.Extensions
{
    public static class CalculationExtension
    {
        /// <summary>
        /// Calculate average result for Poin Flux and Twd
        /// </summary>
        public static SmoothingData Mean(this List<SmoothingData> values)
        {
            double sumPf = 0;
            double sumTwd = 0;
            foreach (var e in values)
            {
                sumPf += e.Pf;
                sumTwd += e.Twd;
            }

            return new SmoothingData
            {
                Pf = sumPf / values.Count,
                Twd = sumTwd / values.Count
            };
        }
    }
}
