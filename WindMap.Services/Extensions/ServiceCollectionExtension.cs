﻿using Microsoft.Extensions.DependencyInjection;
using WindMap.DAL.Extensions;
using WindMap.Services.CalculationServices;
using WindMap.Services.CoreServices;
using WindMap.Services.ModelServices;

namespace WindMap.Services.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddSingleton<ICacheService, CacheService>();
            services.AddScoped<IDatabaseService, DatabaseService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IMigrationService, MigrationService>();
            services.AddScoped<ICalculationService, CalculationService>();
            services.AddScoped<IIntersectionService, IntersectionService>();
            services.AddScoped<ILeakService, LeakService>();
            services.AddScoped<ISmoothingService, SmoothingService>();
            services.AddScoped<IRegionService, RegionService>();
            services.AddScoped<IGeneralService, GeneralService>();
            services.AddScoped<ISensorEventService, SensorEventService>();
            services.AddScoped<ISensorService, SensorService>();
            services.AddScoped<IResultService, ResultService>();
            services.AddScoped<ICompanyService, CompanyService>();
            services.AddScoped<IUiElementService, UiElementService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IRefreshTokenService, RefreshTokenService>();
            services.AddTransient<IEmailService, EmailService>();

            services.AddRepositories();
        }
    }
}
