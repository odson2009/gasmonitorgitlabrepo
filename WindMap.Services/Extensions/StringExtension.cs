﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindMap.Services.Extensions
{
    public static class StringExtension
    {

        public static string ToNotEmptyString(this string str)
        {
            return ConvertToNotEmptyString(str);
        }

        public static string ConvertToNotEmptyString(this string value)
        {
            return !string.IsNullOrWhiteSpace(value) ? value : "---";
        }

    }
}
