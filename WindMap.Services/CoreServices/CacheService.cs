﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;
using WindMap.Services.Models;

namespace WindMap.Services.CoreServices
{
    public interface ICacheService
    {
        Task<Region> GetRegionAsync(int id);
        Task<CacheSensor> GetSensorAsync(int id);
        Task AddResult(SensorResult result);
        Task AddResults(IList<SensorResult> results);
        Task<bool> ContainSensor(int id);
        void Refresh();
    }

    internal class CacheService : ICacheService
    {
        private readonly Task _initializationTask;
        private readonly IRegionRepository _regionRepository;
        private readonly ISensorRepository _sensorRepository;

        private Dictionary<int, Region> _region;
        private Dictionary<int, CacheSensor> _sensors;

        public CacheService(
            IRegionRepository regionRepository,
            ISensorRepository sensorRepository)
        {
            _regionRepository = regionRepository;
            _sensorRepository = sensorRepository;

            _initializationTask = Initialize();
        }

        private async Task Initialize()
        {
            _region = await _regionRepository.Get()
                .Include(e => e.Algorithmes)
                .ToDictionaryAsync(e => e.Id);

            var sensors = await _sensorRepository.Get()
                .Include(e => e.SensorResults).ToListAsync();

            _sensors = sensors
                .Select(e => (CacheSensor)e)
                .ToDictionary(e => e.Id);
        }

        public async Task<Region> GetRegionAsync(int id)
        {
            if (!_initializationTask.IsCompleted)
                await _initializationTask;

            if (!_region.Keys.Contains(id) && _region[id].Algorithmes == null)
                throw new NullReferenceException("Cannot find region or algorithm in cache");

            return _region[id];
        }

        public async Task<CacheSensor> GetSensorAsync(int id)
        {
            if (!_initializationTask.IsCompleted)
                await _initializationTask;

            if (!_sensors.Keys.Contains(id))
                throw new NullReferenceException("Cannot find sensor in cache");

            return _sensors[id];
        }

        public async Task AddResult(SensorResult result)
        {
            if (result == null)
                return;

            var sensor = await GetSensorAsync(result.SensorId);
            sensor.NewSensorResults.Add(result);
        }

        public async Task AddResults(IList<SensorResult> results)
        {
            if (results == null || !results.Any())
                return;

            CacheSensor sensor = null;

            foreach (var result in results)
            {
                try
                {
                    if (sensor == null || sensor.Id != result.SensorId)
                        sensor = await GetSensorAsync(result.SensorId);

                    sensor.NewSensorResults.Add(result);
                }
                catch (NullReferenceException) { }
            }
        }

        public async Task<bool> ContainSensor(int id) {
            if (!_initializationTask.IsCompleted)
                await _initializationTask;

            return _sensors.ContainsKey(id);
        } 

        public async void Refresh()
        {
            if (!_initializationTask.IsCompleted)
                await _initializationTask;

            foreach (var sensor in _sensors.Values)
            {
                sensor.NewSensorResults.Clear();
                sensor.ProcessedSensorResults.Clear();
            }
        }
    }
}
