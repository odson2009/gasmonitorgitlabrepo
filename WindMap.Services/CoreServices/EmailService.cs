﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WindMap.DAL.Enums;

namespace WindMap.Services.CoreServices
{
    public interface IEmailService
    {
        Task SendEmail(string email, string message);
        Task SendNotificationProviders(List<string> emails, string subject, string message);
    }

    internal class EmailService : IEmailService, IDisposable
    {
        private readonly SmtpClient _client;
        private readonly MailMessage _emailMessage;

        public EmailService(IConfiguration configuration)
        {
            var credential = new NetworkCredential
            {
                UserName = configuration["Email:Email"],
                Password = configuration["Email:Password"]
            };
            _client = new SmtpClient
            {
                Credentials = credential,
                Host = configuration["Email:Host"],
                Port = int.Parse(configuration["Email:Port"]),
                EnableSsl = true
            };
            //----
            _emailMessage = new MailMessage
            {
                From = new MailAddress(configuration["Email:Email"])
            };
        }

        public void Dispose()
        {
            _client?.Dispose();
            _emailMessage?.Dispose();
        }

        public async Task SendEmail(string email, string message)
        {
            _emailMessage.To.Add(new MailAddress(email));
            _emailMessage.Subject = "Password";
            _emailMessage.Body = message;
            _client.Send(_emailMessage);

            await Task.CompletedTask;
        }

        public async Task SendNotificationProviders(List<string> emails, string subject, string message)
        {
            emails.ForEach(mail => _emailMessage.To.Add(new MailAddress(mail)));
            _emailMessage.Subject = subject;
            _emailMessage.Body = message;
            _emailMessage.IsBodyHtml = true;

            _client.Send(_emailMessage);

            await Task.CompletedTask;
        }
    }
}
