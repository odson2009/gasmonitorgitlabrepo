﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL;
using WindMap.DAL.Repositories;
using WindMap.DAL.SeedMigration;

namespace WindMap.Services.CoreServices
{
    public interface IMigrationService : IDisposable
    {
        Task MigrateAsync(CancellationToken cancellationToken);
        Task SeedMigrateAsync(CancellationToken cancellationToken);
    }

    internal class MigrationService : IMigrationService
    {
        private IConfiguration _configuration;
        private IMigrationRepository _migrationRepository;
        private IAppDbMigration _dbMigration;

        public MigrationService(
            IConfiguration configuration,
            IMigrationRepository migrationRepository,
            IAppDbMigration dbMigration)
        {
            _configuration = configuration;
            _migrationRepository = migrationRepository;
            _dbMigration = dbMigration;
        }

        public void Dispose()
        {
            _configuration = null;
            _migrationRepository = null;
            _dbMigration = null;
        }

        public async Task SeedMigrateAsync(CancellationToken cancellationToken)
        {
            var migrations = await _migrationRepository.FindSeedMigrateAsync(cancellationToken);
            foreach (var migrate in migrations)
            {
                var seed = Activator.CreateInstance(migrate) as MigrationSeeding;
                await seed.MigrateAsync(_configuration, cancellationToken);
            }
        }

        public Task MigrateAsync(CancellationToken cancellationToken)
        {
            return _dbMigration.MigrateAsync();
        }
    }
}
