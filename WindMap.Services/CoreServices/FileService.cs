﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.Services.Extensions;
using WindMap.Services.Models;

namespace WindMap.Services
{
    public interface IFileService
    {
        Task<List<SensorResult>> ParseSensorResultFile(MemoryStream memoryStream, bool isCsv);
        Task<List<Sensor>> ParseSensorFile(MemoryStream memoryStream, bool isCsv);
        Task<List<Region>> ParseRegionFile(MemoryStream memoryStream, bool isCsv);
        Task GenerateFile(FileInfo fileInfo, SensorBufferReportModel[] mainBuffer);
    }

    internal class FileService : IFileService
    {
        public FileService()
        {
        }

        public async Task GenerateFile(FileInfo fileInfo, SensorBufferReportModel[] mainBuffer)
        {
            await Task.Run(() =>
            {
                try
                {
                    string[] columnsNames = new[] {
                        "Begin time", "End time", "C", "WD", "WS", "TWD", "PF",
                        "step","time",
                            "SMOOTHED:",
                                "a (fminsearh)","sigma (fminsearh)","mu (fminsearh)","a (polyfit)","sigma (polyfit)","mu (polyfit)","A (polyfit)","B (polyfit)","C (polyfit)",
                            "AVERAGE:",
                                "a (fminsearh)","sigma (fminsearh)","mu (fminsearh)","a (polyfit)","sigma (polyfit)","mu (polyfit)","A (polyfit)","B (polyfit)","C (polyfit)",
                            "LINEAR REGRESSION:",
                                "step","#","TWD_Average","PF_Average","m","b","r","x_intercept",
                    };

                    using (ExcelPackage package = new ExcelPackage(fileInfo))
                    {
                        for (int i = 0; i < mainBuffer.Length; i++)
                        {
                            var current_buffer = mainBuffer[i];

                            // add a new worksheet to the empty workbook
                            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(i == 0 ? "1001" : "1002");

                            //First add the headers
                            for (int col_i = 0; col_i < columnsNames.Length; col_i++)
                            {
                                worksheet.Cells[1, col_i + 1].Value = columnsNames[col_i];
                            }

                            //set default style for header cells
                            worksheet.SelectedRange[1, 1, 1, columnsNames.Length].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.SelectedRange[1, 1, 1, columnsNames.Length].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);

                            //add initial data ProcessedSensorResults
                            worksheet.Cells[2, 1].LoadFromCollection(current_buffer.SensorResultsAll.Select(s => s.TimeBegin));
                            worksheet.Cells[2, 2].LoadFromCollection(current_buffer.SensorResultsAll.Select(s => s.TimeEnd));
                            worksheet.Cells[2, 3].LoadFromCollection(current_buffer.SensorResultsAll.Select(s => s.Concentration.ToString()));
                            worksheet.Cells[2, 4].LoadFromCollection(current_buffer.SensorResultsAll.Select(s => s.WindDirection.ToString()));
                            worksheet.Cells[2, 5].LoadFromCollection(current_buffer.SensorResultsAll.Select(s => s.WindSpeed.ToString()));
                            worksheet.Cells[2, 6].LoadFromCollection(current_buffer.SensorResultsAll.Select(s => s.TWD.ToString()));
                            worksheet.Cells[2, 7].LoadFromCollection(current_buffer.SensorResultsAll.Select(s => s.PF.ToString()));

                            worksheet.SelectedRange[1, 1, 1, 7].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Aquamarine);

                            //add initial data TestResults
                            worksheet.Cells[2, 8].LoadFromCollection(current_buffer.TestResults.Select(s => s.Step));
                            worksheet.Cells[2, 9].LoadFromCollection(current_buffer.TestResults.Select(s => s.EndDate));

                            //smoothed fminsearch
                            worksheet.Cells[2, 11].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Fminsearch.A.ToNotEmptyString()));
                            worksheet.Cells[2, 12].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Fminsearch.Sigma.ToNotEmptyString()));
                            worksheet.Cells[2, 13].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Fminsearch.Mu.ToNotEmptyString()));

                            //smoothed polyfit
                            worksheet.Cells[2, 14].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Polyfit.A.ToNotEmptyString()));
                            worksheet.Cells[2, 15].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Polyfit.Sigma.ToNotEmptyString()));
                            worksheet.Cells[2, 16].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Polyfit.Mu.ToNotEmptyString()));
                            worksheet.Cells[2, 17].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Polyfit.CoefficientA.ToNotEmptyString()));
                            worksheet.Cells[2, 18].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Polyfit.CoefficientB.ToNotEmptyString()));
                            worksheet.Cells[2, 19].LoadFromCollection(current_buffer.TestResults.Select(s => s.Smoothed.Polyfit.CoefficientC.ToNotEmptyString()));

                            //average fminsearch
                            worksheet.Cells[2, 21].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Fminsearch.A.ToNotEmptyString()));
                            worksheet.Cells[2, 22].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Fminsearch.Sigma.ToNotEmptyString()));
                            worksheet.Cells[2, 23].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Fminsearch.Mu.ToNotEmptyString()));

                            //average polyfit
                            worksheet.Cells[2, 24].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Polyfit.A.ToNotEmptyString()));
                            worksheet.Cells[2, 25].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Polyfit.Sigma.ToNotEmptyString()));
                            worksheet.Cells[2, 26].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Polyfit.Mu.ToNotEmptyString()));
                            worksheet.Cells[2, 27].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Polyfit.CoefficientA.ToNotEmptyString()));
                            worksheet.Cells[2, 28].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Polyfit.CoefficientB.ToNotEmptyString()));
                            worksheet.Cells[2, 29].LoadFromCollection(current_buffer.TestResults.Select(s => s.Average.Polyfit.CoefficientC.ToNotEmptyString()));

                            var step = 0;

                            //linear regression results
                            for (int a = 0; a < current_buffer.AverageSummary.Length; a++)
                            {
                                var averageSummary = current_buffer.AverageSummary[a];
                                step++;

                                for (int r = 0; r < averageSummary.AverageResults.Length; r++)
                                {
                                    var averageResult = averageSummary.AverageResults[r];

                                    step++;

                                    worksheet.Cells[step, 32].Value = r + 1;//"#";
                                    worksheet.Cells[step, 33].Value = averageResult.AverageData.TWD_Average.ToNotEmptyString();// "TWD_Average";
                                    worksheet.Cells[step, 34].Value = averageResult.AverageData.PF_Average.ToNotEmptyString();//"PF_Average";
                                    worksheet.Cells[step, 35].Value = averageResult.LinearRegressionResult.M.ToNotEmptyString(); //m;
                                    worksheet.Cells[step, 36].Value = averageResult.LinearRegressionResult.B.ToNotEmptyString(); //"b";
                                    worksheet.Cells[step, 37].Value = averageResult.LinearRegressionResult.R.ToNotEmptyString(); //"r";
                                    worksheet.Cells[step, 38].Value = averageResult.LinearRegressionResult.XIntercept.ToNotEmptyString(); //"x_intersept";
                                }

                                var mergedCells = worksheet.SelectedRange[1 + step, 32, 1 + step, 34];
                                mergedCells.Merge = true;//"step";
                                mergedCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                mergedCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Coral);
                                mergedCells.Value = "Linear regression result:";

                                worksheet.Cells[1 + step, 31].Value = averageSummary.Step;//"step";
                                worksheet.Cells[1 + step, 35].Value = averageSummary.LinearRegressionResults.M.ToNotEmptyString(); //m;
                                worksheet.Cells[1 + step, 36].Value = averageSummary.LinearRegressionResults.B.ToNotEmptyString(); //"b";
                                worksheet.Cells[1 + step, 37].Value = averageSummary.LinearRegressionResults.R.ToNotEmptyString(); //"r";
                                worksheet.Cells[1 + step, 38].Value = averageSummary.LinearRegressionResults.XIntercept.ToNotEmptyString(); //"x_intersept";
                            }

                            //set header cells background colors
                            worksheet.SelectedRange[1, 8, 1, 9].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                            //smoothed results
                            worksheet.SelectedRange[1, 11, 1, 13].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightPink);
                            worksheet.SelectedRange[1, 14, 1, 19].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.GreenYellow);

                            //average results
                            worksheet.SelectedRange[1, 21, 1, 23].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightPink);
                            worksheet.SelectedRange[1, 24, 1, 29].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.GreenYellow);

                            //linear regression results
                            worksheet.SelectedRange[1, 31, 1, 32].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                            worksheet.SelectedRange[1, 33, 1, 38].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);

                            worksheet.Cells.AutoFitColumns();
                        }

                        package.Save(); //Save the workbook.
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            });
        }

        public async Task<List<SensorResult>> ParseSensorResultFile(MemoryStream memoryStream, bool isCsv)
        {
            return await Task.Run(() =>
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    bool isNormalizedDocument = true;

                    ExcelWorksheet worksheet;

                    if (isCsv)
                    {
                        bool firstRowIsHeader = true;
                        var xformat = new ExcelTextFormat() { Delimiter = ',' };

                        memoryStream.Position = 0;
                        var sr = new StreamReader(memoryStream);
                        var myStr = sr.ReadToEnd();

                        worksheet = package.Workbook.Worksheets.Add("sheet1");
                        worksheet.Cells["A1"].LoadFromText(myStr, xformat, OfficeOpenXml.Table.TableStyles.Medium25, firstRowIsHeader);
                    }
                    else
                    {
                        package.Load(memoryStream);
                        worksheet = package.Workbook.Worksheets[1];
                    }

                    int rowCount = worksheet.Dimension.Rows;
                    int ColCount = worksheet.Dimension.Columns;

                    var col_first = worksheet.Cells[1, 1];
                    var col_second = worksheet.Cells[1, 2];

                    //check type of the document: normalized or default template
                    //normalized used colums: sensorDd, timeBegin (dd/MM/yyyy HH:mm:ss), timeEnd (dd/MM/yyyy HH:mm:ss), concentration, windSpeed windDirection
                    //default used columns: monitorID, date (dd/MM/yyyy), begin time (HH:mm:ss), end time (HH:mm:ss), concentraion, ws, wd
                    if (col_first.Value.Equals("monitorID") && col_second.Value.Equals("date"))
                    {
                        isNormalizedDocument = false;
                    }

                    //sensorId timeBegin timeEnd concentration windSpeed windDirection
                    var col_sensorId = worksheet.Cells[1, isNormalizedDocument ? 1 : 1];
                    var col_date = worksheet.Cells[1, isNormalizedDocument ? 1 : 2];
                    var col_timeBegin = worksheet.Cells[1, isNormalizedDocument ? 2 : 3];
                    var col_timeEnd = worksheet.Cells[1, isNormalizedDocument ? 3 : 4];
                    var col_concentration = worksheet.Cells[1, isNormalizedDocument ? 4 : 5];
                    var col_windSpeed = worksheet.Cells[1, isNormalizedDocument ? 5 : 6];
                    var col_windDirection = worksheet.Cells[1, isNormalizedDocument ? 6 : 7];

                    //checking exists columns
                    if (isNormalizedDocument)
                    {
                        //checking all needed columns for normalized document
                        if (!(col_sensorId.Value.Equals("sensorId")
                            && col_timeBegin.Value.Equals("timeBegin")
                            && col_timeEnd.Value.Equals("timeEnd")
                            && col_concentration.Value.Equals("concentration")
                            && col_windSpeed.Value.Equals("windSpeed")
                            && col_windDirection.Value.Equals("windDirection")))
                        {
                            throw new Exception("Invalid columns for normalized document");
                        }

                    }
                    else
                    {
                        //checking all needed columns for default document
                        if (!(col_sensorId.Value.Equals("monitorID")
                            && col_date.Value.Equals("date")
                            && col_timeBegin.Value.Equals("begin time")
                            && col_timeEnd.Value.Equals("end time")
                            && col_concentration.Value.Equals("concentraion")
                            && col_windSpeed.Value.Equals("ws")
                            && col_windDirection.Value.Equals("wd")))
                        {
                            throw new Exception("Invalid columns for default document");
                        }
                    }

                    List<SensorResult> resultsList = new List<SensorResult>();
                    DateTime timeBegin = default(DateTime);
                    DateTime timeEnd = default(DateTime);
                    int row = 2;
                    try
                    {
                        for (; row <= rowCount; row++)
                        {
                            if (worksheet.Cells[row, col_sensorId.Start.Column].Value != null &&
                                !String.IsNullOrWhiteSpace(worksheet.Cells[row, col_sensorId.Start.Column].Value.ToString()))
                            {
                                if (isNormalizedDocument)
                                {
                                    timeBegin = DateTime.ParseExact(worksheet.Cells[row, col_timeBegin.Start.Column].Value.ToString(), "dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                                    timeEnd = DateTime.ParseExact(worksheet.Cells[row, col_timeEnd.Start.Column].Value.ToString(), "dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                                }
                                else
                                {
                                    timeBegin = DateTime.ParseExact($"{worksheet.Cells[row, col_date.Start.Column].Value.ToString()} {worksheet.Cells[row, col_timeBegin.Start.Column].Text}", "d/M/yyyy h:mm:ss tt", new CultureInfo("en-US"));
                                    timeEnd = DateTime.ParseExact($"{worksheet.Cells[row, col_date.Start.Column].Value.ToString()} {worksheet.Cells[row, col_timeEnd.Start.Column].Text}", "d/M/yyyy h:mm:ss tt", new CultureInfo("en-US"));
                                }

                            SensorResult sensorResult = new SensorResult
                            {
                                SensorId = Convert.ToInt32(worksheet.Cells[row, col_sensorId.Start.Column].Value),
                                TimeBegin = timeBegin,
                                TimeEnd = timeEnd,
                                Concentration = Convert.ToDouble(worksheet.Cells[row, col_concentration.Start.Column].Value.ToString()),
                                WindSpeed = Convert.ToDouble(worksheet.Cells[row, col_windSpeed.Start.Column].Value.ToString()),
                                WindDirection = Convert.ToDouble(worksheet.Cells[row, col_windDirection.Start.Column].Value.ToString()),
                            };

                                resultsList.Add(sensorResult);
                            }
                        }

                        return resultsList;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"{ex.Message} on row {row}");
                    }
                }
            });
        }

        public async Task<List<Sensor>> ParseSensorFile(MemoryStream memoryStream, bool isCsv)
        {
            return await Task.Run(() =>
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet worksheet;

                    if (isCsv)
                    {
                        bool firstRowIsHeader = true;
                        var xformat = new ExcelTextFormat() { Delimiter = ',' };

                        memoryStream.Position = 0;
                        var sr = new StreamReader(memoryStream);
                        var myStr = sr.ReadToEnd();

                        worksheet = package.Workbook.Worksheets.Add("sheet1");
                        worksheet.Cells["A1"].LoadFromText(myStr, xformat, OfficeOpenXml.Table.TableStyles.Medium25, firstRowIsHeader);
                    }
                    else
                    {
                        package.Load(memoryStream);
                        worksheet = package.Workbook.Worksheets[1];
                    }

                    int rowCount = worksheet.Dimension.Rows;
                    int ColCount = worksheet.Dimension.Columns;

                    //sensorId timeBegin timeEnd concentration windSpeed windDirection and etc
                    var col_monitorId = worksheet.Cells[1, 1];
                    var col_latitude = worksheet.Cells[1, 2];
                    var col_longitude = worksheet.Cells[1, 3];
                    var col_monitorIdToRight = worksheet.Cells[1, 4];

                    var col_azimuthRight = worksheet.Cells[1, 5];
                    var col_distanceRight = worksheet.Cells[1, 6];
                    var col_azimuthLeft = worksheet.Cells[1, 7];
                    var col_monitorIdToLeft = worksheet.Cells[1, 8];

                    var col_distanceLeft = worksheet.Cells[1, 9];
                    var col_normalAzimuthToRight = worksheet.Cells[1, 10];
                    var col_normalAzimuthToLeft = worksheet.Cells[1, 11];
                    var col_regionId = worksheet.Cells[1, 12];

                    //checking all needed columns
                    if (!(col_monitorId.Value.Equals("monitorId")
                        && col_latitude.Value.Equals("latitude")
                        && col_longitude.Value.Equals("longitude")
                        && col_monitorIdToRight.Value.Equals("monitorIdToRight")
                        && col_azimuthRight.Value.Equals("azimuthRight")
                        && col_distanceRight.Value.Equals("distanceRight")
                        && col_azimuthLeft.Value.Equals("azimuthLeft")
                        && col_monitorIdToLeft.Value.Equals("monitorIdToLeft")
                        && col_distanceLeft.Value.Equals("distanceLeft")
                        && col_normalAzimuthToRight.Value.Equals("normalAzimuthToRight")
                        && col_normalAzimuthToLeft.Value.Equals("normalAzimuthToLeft")
                        && col_regionId.Value.Equals("regionId")))
                    {
                        throw new Exception("Invalid columns");
                    }

                    List<Sensor> sensorsList = new List<Sensor>();

                for (int row = 2; row <= rowCount; row++)
                {
                    if (worksheet.Cells[row, col_monitorId.Start.Column].Value != null && 
                        !String.IsNullOrWhiteSpace(worksheet.Cells[row, col_monitorId.Start.Column].Value.ToString()))
                    {
                        Sensor sensor = new Sensor
                        {
                            MonitorId = worksheet.Cells[row, col_monitorId.Start.Column].Value.ToString(),
                            Latitude = Convert.ToDouble(worksheet.Cells[row, col_latitude.Start.Column].Value.ToString()),
                            Longitude = Convert.ToDouble(worksheet.Cells[row, col_longitude.Start.Column].Value.ToString()),
                            MonitorIdToRight = worksheet.Cells[row, col_monitorIdToRight.Start.Column].Value.ToString(),
                            AzimuthRight = Convert.ToDouble(worksheet.Cells[row, col_azimuthRight.Start.Column].Value.ToString()),
                            DistanceRight = Convert.ToDouble(worksheet.Cells[row, col_distanceRight.Start.Column].Value.ToString()),
                            AzimuthLeft = Convert.ToDouble(worksheet.Cells[row, col_azimuthLeft.Start.Column].Value.ToString()),
                            MonitorIdToLeft = worksheet.Cells[row, col_monitorIdToLeft.Start.Column].Value.ToString(),
                            DistanceLeft = Convert.ToDouble(worksheet.Cells[row, col_distanceLeft.Start.Column].Value.ToString()),
                            NormalAzimuthToRight = Convert.ToDouble(worksheet.Cells[row, col_normalAzimuthToRight.Start.Column].Value.ToString()),
                            NormalAzimuthToLeft = Convert.ToDouble(worksheet.Cells[row, col_normalAzimuthToLeft.Start.Column].Value.ToString()),
                            RegionId = Convert.ToInt32(worksheet.Cells[row, col_regionId.Start.Column].Value),
                        };

                            sensorsList.Add(sensor);
                        }
                    }

                    return sensorsList;
                }
            });
        }

        public async Task<List<Region>> ParseRegionFile(MemoryStream memoryStream, bool isCsv)
        {
            return await Task.Run(() =>
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet worksheet;

                    if (isCsv)
                    {
                        bool firstRowIsHeader = true;
                        var xformat = new ExcelTextFormat() { Delimiter = ',' };

                        memoryStream.Position = 0;
                        var sr = new StreamReader(memoryStream);
                        var myStr = sr.ReadToEnd();

                        worksheet = package.Workbook.Worksheets.Add("sheet1");
                        worksheet.Cells["A1"].LoadFromText(myStr, xformat, OfficeOpenXml.Table.TableStyles.Medium25, firstRowIsHeader);
                    }
                    else
                    {
                        package.Load(memoryStream);
                        worksheet = package.Workbook.Worksheets[1];
                    }

                    int rowCount = worksheet.Dimension.Rows;
                    int ColCount = worksheet.Dimension.Columns;

                    //sensorId timeBegin timeEnd concentration windSpeed windDirection
                    var col_name = worksheet.Cells[1, 1];
                    var col_phone = worksheet.Cells[1, 2];
                    var col_email = worksheet.Cells[1, 3];
                    var col_webSite = worksheet.Cells[1, 4];

                    //checking all needed columns
                    if (!(col_name.Value.Equals("name") && col_phone.Value.Equals("phone") && col_email.Value.Equals("email")
                        && col_webSite.Value.Equals("webSite")))
                    {
                        throw new Exception("Invalid columns");
                    }

                    List<Region> companiesList = new List<Region>();

                    for (int row = 2; row <= rowCount; row++)
                    {
                        if (worksheet.Cells[row, col_name.Start.Column].Value != null &&
                            !String.IsNullOrWhiteSpace(worksheet.Cells[row, col_name.Start.Column].Value.ToString()))
                        {
                            Region region = new Region
                            {
                                Name = worksheet.Cells[row, col_name.Start.Column].Value.ToString(),
                                Phone = worksheet.Cells[row, col_phone.Start.Column].Value.ToString(),
                                Email = worksheet.Cells[row, col_email.Start.Column].Value.ToString(),
                                WebSite = worksheet.Cells[row, col_webSite.Start.Column].Value.ToString(),
                            };

                            companiesList.Add(region);
                        }
                    }

                    return companiesList;
                }
            });
        }
    }
}
