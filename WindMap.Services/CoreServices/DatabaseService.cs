﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Repositories;

namespace WindMap.Services.CoreServices
{
    public interface IDatabaseService : IDisposable
    {
        Task<bool> IsLastVersion(CancellationToken cancellationToken);
        Task MigrateAsync(CancellationToken cancellationToken);
    }

    internal class DatabaseService : IDatabaseService
    {
        private IDatabaseRepository _databaseRepository;
        private IMigrationService _migration;

        public DatabaseService(IDatabaseRepository databaseRepository, IMigrationService migration)
        {
            _databaseRepository = databaseRepository;
            _migration = migration;
        }

        public void Dispose()
        {
            _databaseRepository = null;
        }

        public async Task<bool> IsLastVersion(CancellationToken cancellationToken)
        {
            var databaseExists = await _databaseRepository.ExistsAsync(cancellationToken);

            var migrationsAssembly = _databaseRepository.GetAssembly();
            var modelDiffer = _databaseRepository.GetDiffer();

            var pendingModelChanges =
                (!databaseExists || migrationsAssembly.ModelSnapshot != null)
                && modelDiffer.HasDifferences(migrationsAssembly.ModelSnapshot?.Model, _databaseRepository.GetModel());

            var pendingMigrations = (databaseExists
                ? await _databaseRepository.GetPendingMigrationsAsync()
                : _databaseRepository.GetMigrations()).ToArray();

            return !(pendingModelChanges || pendingMigrations.Any());
        }

        public async Task MigrateAsync(CancellationToken cancellationToken)
        {
            await _databaseRepository.MigrateAsync(cancellationToken);
            await _migration.SeedMigrateAsync(cancellationToken);
        }
    }
}
