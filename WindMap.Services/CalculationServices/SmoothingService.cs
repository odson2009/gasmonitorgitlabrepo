﻿using System;
using System.Collections.Generic;
using System.Linq;
using WindMap.DAL.Models;
using WindMap.Services.CalculationServices.ApproximationAlgorithms;
using WindMap.Services.Extensions;
using WindMap.Services.Models;

namespace WindMap.Services.CalculationServices
{
    public interface ISmoothingService
    {
        SmoothingResult Smooth(CacheSensor sensor, Algorithm algorithm);
    }

    internal class SmoothingService : ISmoothingService
    {
        private readonly ICalculationService _calculationService;
        private readonly IAproximationAlgorithm _fminsearchAproximationAlgorithm;
        private readonly IAproximationAlgorithm _polyfitAproximationAlgorithm;

        public SmoothingService(ICalculationService calculationService)
        {
            _calculationService = calculationService;
            _fminsearchAproximationAlgorithm = new FminsearchAlgorithm();
            _polyfitAproximationAlgorithm = new PolyfitAlgorithm();
        }

        public SmoothingResult Smooth(CacheSensor sensor, Algorithm algorithm)
        {
            var result = new SmoothingResult();

            result.TransformedData = TransformData(sensor.ProcessedSensorResults, sensor.OnsiteWindCenterAngle);
            result.MovingAverage = _calculationService.MovingAverage(result.TransformedData, (int)algorithm.NumberLineGroupTime)
                .OrderBy(e => e.Twd).ToList();
            result.GroupedMovingAverage = GroupMovingAverage(result.MovingAverage, (int)algorithm.NumberLineGroupTime);

            // commited for simulation process
            //result.GroupedMovingAverageRegression = CalculateGroupedMovingAverageRegression(result.GroupedMovingAverage);


            //result.FminsearchAproximation = Aproximate(result.MovingAverage, _fminsearchAproximationAlgorithm);
            //result.PolyfitAproximation = Aproximate(result.MovingAverage, _polyfitAproximationAlgorithm);
            //result.FminsearchAproximationAverage = Aproximate(result.GroupedMovingAverage, _fminsearchAproximationAlgorithm);
            //result.PolyfitAproximationAverage = Aproximate(result.GroupedMovingAverage, _polyfitAproximationAlgorithm);

            return result;
        }

        private List<SmoothingData> TransformData(IList<SensorResult> results, double onsiteWindCenterAngle)
        {
            var res = new List<SmoothingData>();

            foreach (var result in results)
            {
                var twd = result.WindDirection - onsiteWindCenterAngle;
                var pf = result.Concentration * result.WindSpeed;


                if (twd > 180)
                    twd -= 360;
                else if (twd < -180)
                    twd += 360;


                res.Add(new SmoothingData(pf, twd));
            }

            return res;
        }

        private List<GropedSmoothingData> GroupMovingAverage(List<SmoothingData> movingAverage, int numberOfGroups)
        {
            var result = new List<GropedSmoothingData>();

            var step = movingAverage.Count / numberOfGroups;

            for (var i = 0; i < numberOfGroups; i++)
            {
                var item = new GropedSmoothingData();

                item.Items.AddRange(movingAverage.GetRange(i * step, step));

                result.Add(item);
            }

            //add odd results to last group
            var residual = movingAverage.Count % step;
            result.LastOrDefault()?.Items.AddRange(movingAverage.GetRange(movingAverage.Count - residual, residual));

            //Calculation linear regression and average data
            foreach (var e in result)
            {
                e.RegressionResult = _calculationService.CalculateLinearRegression(e.Items.Select(i =>
                    new RegressionParameter
                    {
                        X = i.Twd,
                        Y = i.Pf
                    }).ToList());

                e.AverageData = e.Items.Mean();
            }

            return result;
        }

        private RegressionResult CalculateGroupedMovingAverageRegression(List<GropedSmoothingData> groupedMovingAverage)
        {
            return _calculationService.CalculateLinearRegression(
                groupedMovingAverage.Select(i => new RegressionParameter
                {
                    X = i.AverageData.Twd,
                    Y = i.AverageData.Pf
                }).ToList());
        }

        private AproximationResult Aproximate(IList<GropedSmoothingData> data, IAproximationAlgorithm algorithm)
        {
            var xArr = data.Select(e => e.AverageData.Twd).ToArray();
            var yArr = data.Select(e => e.AverageData.Pf).ToArray();

            var init = new[] { yArr.Sum(), 10, 50 };


            return algorithm.Calculate(xArr, yArr, init);
        }

        private AproximationResult Aproximate(IList<SmoothingData> data, IAproximationAlgorithm algorithm)
        {
            var xArr = data.Select(e => e.Twd).ToArray();
            var yArr = data.Select(e => e.Pf).ToArray();

            var init = new[] { yArr.Sum(), 10, 50 };


            return algorithm.Calculate(xArr, yArr, init);
        }
    }
}
