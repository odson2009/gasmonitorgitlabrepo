﻿using System;
using System.Collections.Generic;
using WindMap.DAL.Models;
using WindMap.Services.CoreServices;
using WindMap.Services.Models;

namespace WindMap.Services.CalculationServices
{
    public interface ICalculationService
    {
        PointFluxResult GetPointFlux(IReadOnlyList<SensorResult> results);
        RegressionResult CalculateLinearRegression(List<RegressionParameter> parameters);
        List<SmoothingData> MovingAverage(List<SmoothingData> inputResults, int step);
    }

    internal class CalculationService : ICalculationService
    {
        private readonly ICacheService _cacheService;

        public CalculationService(
            ICacheService cacheService)
        {
            _cacheService = cacheService;
        }


        public PointFluxResult GetPointFlux(IReadOnlyList<SensorResult> results)
        {
            double PF1_Average = 0;
            double PF2_Average = 0;
            double WD_Average_TMP = 0;

            if (results.Count > 0)
            {
                double FIFO_LENGH = results.Count;
                double WS_V = 0;
                double WS_U = 0;
                double MEAN_WS_V = 0;
                double MEAN_WS_U = 0;
                double C_Average = 0;
                double WS_Average = 0;
                double WS_Average_for_vector = 0;
                double WD_Average = 0;
                double WD_Intermadiate_Average = 0;

                for (var i = 0; i < FIFO_LENGH; i++)
                {
                    var item = results[i];
                    C_Average += item.Concentration;
                    WS_Average += item.WindSpeed;

                    WS_V += -1 * item.WindSpeed * Math.Sin(Math.PI / 180 * item.WindDirection);
                    WS_U += item.WindSpeed * Math.Cos(Math.PI / 180 * item.WindDirection);

                    PF2_Average += item.Concentration * item.WindSpeed;

                    WD_Average_TMP += item.WindDirection;
                }

                MEAN_WS_V = WS_V / FIFO_LENGH;
                MEAN_WS_U = WS_U / FIFO_LENGH;
                C_Average = C_Average / FIFO_LENGH;
                WS_Average = WS_Average / FIFO_LENGH;
                WD_Average_TMP = WD_Average_TMP / FIFO_LENGH;

                WS_Average_for_vector = Math.Sqrt(Math.Pow(MEAN_WS_V, 2) + Math.Pow(MEAN_WS_U, 2));
                WD_Intermadiate_Average = -180 / Math.PI * Math.Atan(MEAN_WS_V / MEAN_WS_U);

                if (MEAN_WS_U > 0)
                {
                    if (WD_Intermadiate_Average < 0)
                    {
                        WD_Average = WD_Intermadiate_Average + 360;
                    }
                    else
                    {
                        WD_Average = WD_Intermadiate_Average;
                    }
                }
                else
                {
                    WD_Average = WD_Intermadiate_Average + 180;
                }

                PF1_Average = C_Average * WS_Average;
                PF2_Average = PF2_Average / FIFO_LENGH;
            }

            var result = new PointFluxResult
            {
                PF1_Average = PF1_Average,
                PF2_Average = PF2_Average,
                Wd_Average = WD_Average_TMP
            };

            return result;
        }

        public List<SmoothingData> MovingAverage(List<SmoothingData> inputResults, int step)
        {
            var result = new List<SmoothingData>();

            for (var i = 0; i < inputResults.Count - step + 1; i++)
            {
                double sumPf = 0;
                double sumTwd = 0;
                for (var j = 0; j < step; j++)
                {
                    sumTwd += inputResults[i + j].Twd;
                    sumPf += inputResults[i + j].Pf;
                }

                sumPf /= step;
                sumTwd /= step;

                result.Add(new SmoothingData(sumPf, sumTwd));
            }

            return result;
        }

        public void TransformWindDirection(SensorResult[] sensorResults, double onsiteWindCenterAngle)
        {
            for (var i = 0; i < sensorResults.Length; i++)
            {
                var tempRes = sensorResults[i];
                var X_TWD_TMP = tempRes.WindDirection - onsiteWindCenterAngle;
                var Y_PF_TMP = tempRes.Concentration * tempRes.WindSpeed;

                if (-180 <= X_TWD_TMP)
                {
                    X_TWD_TMP = X_TWD_TMP + 360;
                }
                if (180 >= X_TWD_TMP)
                {
                    X_TWD_TMP = X_TWD_TMP - 360;
                }
            }
        }

        public RegressionResult CalculateLinearRegression(List<RegressionParameter> parameters)
        {
            double sx = 0, sy = 0, sxx = 0, sxy = 0, syy = 0;
            double n = parameters.Count;

            for (var i = 0; i < n; i++)
            {
                var x = parameters[i].X;
                var y = parameters[i].Y;

                sx += x;
                sxx += x * x;
                sy += y;
                sxy += x * y;
                syy += y * y;
            }

            var result = new RegressionResult();

            result.M = (n * sxy - sx * sy) / (n * sxx - sx * sx);
            result.B = 1 / n * sy - result.M * 1 / n * sx;
            result.R = (n * sxy - sx * sy) / Math.Sqrt((n * sxx - Math.Pow(sx, 2)) * (n * syy - Math.Pow(sy, 2)));
            result.Interception = -result.B / result.M;

            return result;
        }
    }
}
