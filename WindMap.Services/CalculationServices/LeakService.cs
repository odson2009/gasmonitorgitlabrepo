﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.Services.CoreServices;
using WindMap.Services.Models;
using WindMap.Services.ModelServices;

namespace WindMap.Services.CalculationServices
{
    public interface ILeakService
    {
        Task<SensorCheckResult> CheckSensor(int sensorId);
    }

    public class LeakService : ILeakService
    {
        private readonly ICacheService _cacheService;
        private readonly ISensorService _sensorService;
        private readonly ISensorEventService _sensorEventService;
        private readonly ISmoothingService _smoothingService;
        private readonly ICalculationService _calculationService;

        public LeakService(
            ICacheService cacheService,
            ISensorService sensorService,
            ISensorEventService sensorEventService,
            ISmoothingService smoothingService,
            ICalculationService calculationService)
        {
            _cacheService = cacheService;
            _sensorService = sensorService;
            _sensorEventService = sensorEventService;
            _smoothingService = smoothingService;
            _calculationService = calculationService;
        }

        public async Task<SensorCheckResult> CheckSensor(int sensorId)
        {
            var sensor = await _cacheService.GetSensorAsync(sensorId);
            var region = await _cacheService.GetRegionAsync(sensor.RegionId);
            var algorithm = region.Algorithmes.First();

            if (!ValidInputResults(sensor, algorithm))
                return null;

            var pointFlux = _calculationService.GetPointFlux(
                sensor.ProcessedSensorResults.TakeLast((int)algorithm.MinDataForAverage).ToList());

            var result = new SensorCheckResult();

            result.ProcessedSensorResults.AddRange(sensor.ProcessedSensorResults);

            if (CheckSensorLeaking(sensor, pointFlux, algorithm))
            {
                result.SmoothingResult = _smoothingService.Smooth(sensor, algorithm);

                await AddOrUpdateLeakEvent(sensor, pointFlux);

                sensor.IsLeak = true;
            }
            else
            {
                sensor.IsLeak = false;
            }

            result.IsLeak = sensor.IsLeak;

            return result;
        }

        /// <summary>
        /// Check input results and if they are valid return true
        /// </summary>
        private bool ValidInputResults(CacheSensor sensor, Algorithm algorithm)
        {
            var minNumberForCheck = 1;// algorithm.MinDataForAverage / algorithm.TimeResolution;

            if (sensor.NewSensorResults.Count < minNumberForCheck)
                return false;

            sensor.ProcessedSensorResults.AddRange(sensor.NewSensorResults);
            sensor.NewSensorResults.Clear();

            if (sensor.ProcessedSensorResults.Count < algorithm.MinDataForAverage)
                return false;

            //We need to accumulate results only if our sensor has been leaking, so we remove odd results if it hasn't
            if (!sensor.IsLeak)
                sensor.ProcessedSensorResults.RemoveRange(0, sensor.ProcessedSensorResults.Count - (int)algorithm.MinDataForAverage);

            return true;
        }

        private bool CheckSensorLeaking(CacheSensor sensor, PointFluxResult pointFlux, Algorithm algorithm)
        {
            var detectionPointFluxMax = algorithm.DetectionPointFlux;//80;//40;
            var detectionPointFluxMin = 0;// 30;

            var wdAverage = pointFlux.Wd_Average;
            var onsiteWdMin = sensor.OnsiteWindMin;
            var onsiteWdMax = sensor.OnsiteWindMax;

            if (onsiteWdMax < onsiteWdMin)
            {
                //do transform for ONSITE_WD_MIN -> 0 degrees by rotation
                var difference = 360 - onsiteWdMin;
                var twdMin = 0;
                var twdMax = (onsiteWdMax + difference) % 360;
                var twdAverage = (wdAverage + difference) % 360;

                return (pointFlux.PF1_Average >= detectionPointFluxMax ||
                        pointFlux.PF1_Average >= detectionPointFluxMin && sensor.IsLeak)
                       && twdMin < twdAverage && twdAverage < twdMax;
            }

            return (pointFlux.PF1_Average >= detectionPointFluxMax ||
                    pointFlux.PF1_Average >= detectionPointFluxMin && sensor.IsLeak)
                   && onsiteWdMin < wdAverage && wdAverage < onsiteWdMax;
        }

        private async Task AddOrUpdateLeakEvent(CacheSensor sensor, PointFluxResult pointFlux)
        {
            var leakEvent = sensor.IsLeak ?
                await _sensorService.GetLastSensorEvent(sensor.Id, CancellationToken.None) :
                new SensorEvent { SensorId = sensor.Id, Calculations = new List<Calculation>() };

            leakEvent.Calculations.Add(new Calculation
            {
                EventTime = sensor.ProcessedSensorResults.Last().TimeEnd,
                SourceLatitude = sensor.Latitude,
                SourceLongitude = sensor.Longitude,
                AverageWindDirection = pointFlux.Wd_Average
            });
            await _sensorEventService.UpdateAsync(leakEvent, CancellationToken.None);
        }
    }
}
