﻿using System;
using System.Collections.Generic;
using System.Text;
using WindMap.Services.Models;

namespace WindMap.Services.CalculationServices.ApproximationAlgorithms
{
    internal interface IAproximationAlgorithm
    {
        AproximationResult Calculate(double[] xArr, double[] yArr, double[] init);
    }
}
