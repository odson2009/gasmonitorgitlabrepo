﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using WindMap.Services.Models;

namespace WindMap.Services.CalculationServices.ApproximationAlgorithms
{
    internal class PolyfitAlgorithm : IAproximationAlgorithm
    {
        public AproximationResult Calculate(double[] xArr, double[] yArr, double[] init)
        {
            var length = Math.Max(xArr.Length, yArr.Length);

            var xyTable = new double[2, length];

            for (var i = 0; i < xArr.Length; i++)
                xyTable[0, i] = xArr[i];

            for (var i = 0; i < yArr.Length; i++)
                xyTable[1, i] = Math.Log(yArr[i]);


            const int basis = 3; // power + 1
            var matrix = MakeSystem(xyTable, basis);

            var result = Gauss(matrix, basis, basis + 1);
            //if (result == null)
            //{
            //    Console.Write("Невозможно найти частное решение составленной системы уравнений\n");
            //    return;
            //}

            var sigma = Math.Sqrt(-1 / (2 * result[2]));
            var mu = result[1] * Math.Pow(sigma, 2);
            var a = Math.Exp(result[0] + Math.Pow(mu, 2) / (2 * Math.Pow(sigma, 2)));

            return new AproximationResult
            {
                Sigma = sigma,
                Mu = mu,
                A = a
            };
        }

        private static double[,] MakeSystem(double[,] xyTable, int basis)
        {
            var matrix = new double[basis, basis + 1];
            for (var i = 0; i < basis; i++)
            {
                for (var j = 0; j < basis; j++)
                {
                    matrix[i, j] = 0;
                }
            }
            for (var i = 0; i < basis; i++)
            {
                for (var j = 0; j < basis; j++)
                {
                    double sumA = 0, sumB = 0;
                    for (var k = 0; k < xyTable.GetLength(1); k++)
                    {
                        sumA += Math.Pow(xyTable[0, k], i) * Math.Pow(xyTable[0, k], j);
                        sumB += xyTable[1, k] * Math.Pow(xyTable[0, k], i);
                    }
                    matrix[i, j] = sumA;
                    matrix[i, basis] = sumB;
                }
            }
            return matrix;
        }

        private static double[] Gauss(double[,] matrix, int rowCount, int colCount)
        {
            int i;
            var mask = new int[colCount - 1];
            for (i = 0; i < colCount - 1; i++) mask[i] = i;
            if (GaussDirectPass(ref matrix, ref mask, colCount, rowCount))
            {
                var answer = GaussReversePass(ref matrix, mask, colCount, rowCount);
                return answer;
            }
            else return null;
        }
        private static bool GaussDirectPass(ref double[,] matrix, ref int[] mask,
            int colCount, int rowCount)
        {
            int i, j, k, maxId, tmpInt;
            double maxVal, tempDouble;
            for (i = 0; i < rowCount; i++)
            {
                maxId = i;
                maxVal = matrix[i, i];
                for (j = i + 1; j < colCount - 1; j++)
                    if (Math.Abs(maxVal) < Math.Abs(matrix[i, j]))
                    {
                        maxVal = matrix[i, j];
                        maxId = j;
                    }
                if (maxVal == 0) return false;
                if (i != maxId)
                {
                    for (j = 0; j < rowCount; j++)
                    {
                        tempDouble = matrix[j, i];
                        matrix[j, i] = matrix[j, maxId];
                        matrix[j, maxId] = tempDouble;
                    }
                    tmpInt = mask[i];
                    mask[i] = mask[maxId];
                    mask[maxId] = tmpInt;
                }
                for (j = 0; j < colCount; j++) matrix[i, j] /= maxVal;
                for (j = i + 1; j < rowCount; j++)
                {
                    var tempMn = matrix[j, i];
                    for (k = 0; k < colCount; k++)
                        matrix[j, k] -= matrix[i, k] * tempMn;
                }
            }
            return true;
        }

        private static double[] GaussReversePass(ref double[,] matrix, int[] mask,
            int colCount, int rowCount)
        {
            int i, j, k;
            for (i = rowCount - 1; i >= 0; i--)
                for (j = i - 1; j >= 0; j--)
                {
                    var tempMn = matrix[j, i];
                    for (k = 0; k < colCount; k++)
                        matrix[j, k] -= matrix[i, k] * tempMn;
                }
            var answer = new double[rowCount];
            for (i = 0; i < rowCount; i++) answer[mask[i]] = matrix[i, colCount - 1];
            return answer;
        }
    }
}
