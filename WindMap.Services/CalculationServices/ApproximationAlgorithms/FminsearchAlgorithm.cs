﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using WindMap.Services.Models;

namespace WindMap.Services.CalculationServices.ApproximationAlgorithms
{
    internal class Simplex
    {
        public double[] Arr { get; set; }
        public double fx { get; set; }
        public double id { get; set; }

        internal Simplex()
        { }

        internal Simplex(Simplex s)
        {
            Arr = s.Arr.Clone() as double[];
            fx = s.fx;
            id = s.id;
        }
    }

    internal class FminsearchAlgorithm : IAproximationAlgorithm
    {
        public AproximationResult Calculate(double[] xArr, double[] yArr, double[] init)
        {
            double Func(IReadOnlyList<double> constants)
            {
                double summ = 0;

                for (int i = 0, len = xArr.Length; i < len; i++)
                {
                    var yRegress = constants[0] / (Math.Sqrt(2 * Math.PI) * constants[1]) * Math.Exp(-.5 * (Math.Pow(constants[2] - xArr[i], 2)) / Math.Pow((constants[1]), 2));
                    summ += Math.Pow(yRegress - yArr[i], 2);
                }

                return summ;
            }

            var x0 = new Simplex
            {
                Arr = init,
                fx = Func(init),
                id = 0
            };


            const int maxIterations = 500;
            const double nonZeroDelta = 1.05;
            const double minErrorDelta = 1e-8;
            const double minTolerance = 1e-5;
            const int rho = 1;
            const int chi = 2;
            const double psi = -0.5;
            const double sigma = 0.5;

            // initialize simplex.
            var n = x0.Arr.Length;
            var simplex = new Simplex[n + 1];


            simplex[0] = new Simplex
            {
                Arr = x0.Arr,
                fx = Func(x0.Arr),
                id = 0
            };

            for (var i = 0; i < n; ++i)
            {
                var point = x0.Arr.Clone() as double[];

                point[i] = point[i] * nonZeroDelta;

                simplex[i + 1] = new Simplex
                {
                    Arr = point,
                    fx = Func(point),
                    id = i + 1
                };
            }

            var centroid = x0.Arr.Clone() as double[];
            var reflected = new Simplex(x0);
            var contracted = new Simplex(x0);
            var expanded = new Simplex(x0);

            for (var iteration = 0; iteration < maxIterations; ++iteration)
            {
                simplex = simplex.OrderBy(e => e.fx).ToArray();

                double maxDiff = 0;
                for (var i = 0; i < n; ++i)
                {
                    maxDiff = Math.Max(maxDiff, Math.Abs(simplex[0].Arr[i] - simplex[1].Arr[i]));
                }

                if ((Math.Abs(simplex[0].fx - simplex[n].fx) < minErrorDelta) &&
                    (maxDiff < minTolerance))
                {
                    break;
                }

                // compute the centroid of all but the worst point in the simplex
                for (var i = 0; i < n; ++i)
                {
                    centroid[i] = 0;
                    for (var j = 0; j < n; ++j)
                    {
                        centroid[i] += simplex[j].Arr[i];
                    }
                    centroid[i] /= n;
                }

                // reflect the worst point past the centroid  and compute loss at reflected
                // point
                var worst = new Simplex(simplex[n]);
                WeightedSum(reflected.Arr, 1 + rho, centroid, -rho, worst);
                reflected.fx = Func(reflected.Arr);

                // if the reflected point is the best seen, then possibly expand
                if (reflected.fx < simplex[0].fx)
                {
                    WeightedSum(expanded.Arr, 1 + chi, centroid, -chi, worst);
                    expanded.fx = Func(expanded.Arr);
                    if (expanded.fx < reflected.fx)
                    {
                        UpdateSimplex(simplex, expanded, n);
                    }
                    else
                    {
                        UpdateSimplex(simplex, reflected, n);
                    }
                }

                // if the reflected point is worse than the second worst, we need to
                // contract
                else if (reflected.fx >= simplex[n - 1].fx)
                {
                    var shouldReduce = false;

                    if (reflected.fx > worst.fx)
                    {
                        // do an inside contraction
                        WeightedSum(contracted.Arr, 1 + psi, centroid, -psi, worst);
                        contracted.fx = Func(contracted.Arr);
                        if (contracted.fx < worst.fx)
                        {
                            UpdateSimplex(simplex, contracted, n);
                        }
                        else
                        {
                            shouldReduce = true;
                        }
                    }
                    else
                    {
                        // do an outside contraction
                        WeightedSum(contracted.Arr, 1 - psi * rho, centroid, psi * rho, worst);
                        contracted.fx = Func(contracted.Arr);
                        if (contracted.fx < reflected.fx)
                        {
                            UpdateSimplex(simplex, contracted, n);
                        }
                        else
                        {
                            shouldReduce = true;
                        }
                    }

                    if (shouldReduce)
                    {
                        // if we don't contract here, we're done
                        if (sigma >= 1) break;

                        // do a reduction
                        for (var i = 1; i < simplex.Length; ++i)
                        {
                            WeightedSum(simplex[i].Arr, 1 - sigma, simplex[0].Arr, sigma, simplex[i]);
                            simplex[i].fx = Func(simplex[i].Arr);
                        }
                    }
                }
                else
                {
                    UpdateSimplex(simplex, reflected, n);
                }
            }

            simplex = simplex.OrderBy(e => e.fx).ToArray();
            
            return new AproximationResult
            {
                A = simplex[0].Arr[0],
                Sigma = simplex[0].Arr[1],
                Mu = simplex[0].Arr[2]
            };
        }

        private void UpdateSimplex(Simplex[] simplex, Simplex value, int N)
        {
            for (var i = 0; i < value.Arr.Length; i++)
            {
                simplex[N].Arr[i] = value.Arr[i];
            }
            simplex[N].fx = value.fx;
        }

        private void WeightedSum(double[] ret, double w1, double[] v1, double w2, Simplex v2)
        {
            for (var j = 0; j < ret.Length; ++j)
            {
                ret[j] = w1 * v1[j] + w2 * v2.Arr[j];
            }
        }
    }
}
