﻿using System;
using System.Collections.Generic;
using WindMap.Services.Models;

namespace WindMap.Services.CalculationServices
{
    public interface IIntersectionService
    {
        IntersectionResult CalculateIntersection(IntersectionSensor firstSensor, IntersectionSensor secondSensor);
    }

    internal class IntersectionService : IIntersectionService
    {
        public IntersectionResult CalculateIntersection(IntersectionSensor firstSensor, IntersectionSensor secondSensor)
        {
            var ray11 = firstSensor.FirstRay;
            var ray12 = firstSensor.SecondRay;
            var ray21 = secondSensor.FirstRay;
            var ray22 = secondSensor.SecondRay;

            var intersections = new List<Point>
            {
                RayToRayIntersection(ray11, ray21),
                RayToRayIntersection(ray11, ray22),
                RayToRayIntersection(ray12, ray22),
                RayToRayIntersection(ray12, ray21)
            };

            intersections.RemoveAll(e => e == null);

            var result = new IntersectionResult();

            if (intersections.Count == 3 || intersections.Count == 4)
                result = CalculateCenterOfMass(intersections);

            return result;
        }

        private Point RayToRayIntersection(Ray firsRay, Ray secondRay)
        {
            var x1 = firsRay.StartPoint.X;
            var y1 = firsRay.StartPoint.Y;
            var x2 = firsRay.Point.X;
            var y2 = firsRay.Point.Y;

            var x3 = secondRay.StartPoint.X;
            var y3 = secondRay.StartPoint.Y;
            var x4 = secondRay.Point.X;
            var y4 = secondRay.Point.Y;


            Point result = null;
            //Make sure the lines aren't parallel
            if ((y2 - y1) / (x2 - x1) != (y4 - y3) / (x4 - x3))
            {
                //check if one ray contains another one
                var d = (x2 - x1) * (y4 - y3) - (y2 - y1) * (x4 - x3);
                if (d != 0)
                {
                    var r = ((y1 - y3) * (x4 - x3) - (x1 - x3) * (y4 - y3)) / d;
                    var s = ((y1 - y3) * (x2 - x1) - (x1 - x3) * (y2 - y1)) / d;
                    if (r >= 0)
                    {
                        if (s >= 0)
                        {
                            result = new Point(x1 + r * (x2 - x1), y1 + r * (y2 - y1));
                        }
                    }
                }
            }
            return result;
        }

        private IntersectionResult CalculateCenterOfMass(IReadOnlyList<Point> intersections)
        {
            var triangles = new List<Triangle>();

            for (var i = 0; i < intersections.Count - 1; i = i + 2)
            {
                triangles.Add(new Triangle
                {
                    P1 = intersections[i],
                    P2 = intersections[i + 1],
                    P3 = intersections[(i + 2) % 4]
                });
            }

            var result = new IntersectionResult();
            result.Triangles = triangles;


            result.Center = triangles.Count == 1 ?
                triangles[0].Center :
                GetCenterMassPolynom(triangles[0].Center, triangles[1].Center, triangles[0].Area, triangles[1].Area);

            return result;
        }

        private Point GetCenterMassPolynom(Point p1, Point p2, double area1, double area2)
        {
            var x = (area1 * p1.X + area2 * p2.X) / (area1 + area2);
            var y = (area1 * p1.Y + area2 * p2.Y) / (area1 + area2);

            return new Point(x, y);
        }

        public double ToRad(double degrees)
        {
            return degrees % 360 * (Math.PI / 180);
        }
    }
}
