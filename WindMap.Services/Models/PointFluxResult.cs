﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindMap.Services.Models
{
    public class PointFluxResult
    {
        public double PF1_Average { get; set; }
        public double PF2_Average { get; set; }
        public double Wd_Average { get; set; }
    }
}
