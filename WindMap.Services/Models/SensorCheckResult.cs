﻿using System;
using System.Collections.Generic;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.Services.Models
{
    public class SensorCheckResult
    {
        public bool IsLeak { get; set; }
        public SmoothingResult SmoothingResult { get; set; }

        public List<SensorResult> ProcessedSensorResults = new List<SensorResult>();
    }

    public class SmoothingResult
    {
        public List<SmoothingData> TransformedData { get; set; }
        public List<SmoothingData> MovingAverage { get; set; }
        public List<GropedSmoothingData> GroupedMovingAverage { get; set; }
        public RegressionResult GroupedMovingAverageRegression { get; set; }
        public AproximationResult FminsearchAproximation { get; set; }
        public AproximationResult PolyfitAproximation { get; set; }
        public AproximationResult FminsearchAproximationAverage { get; set; }
        public AproximationResult PolyfitAproximationAverage { get; set; }
    }
}
