﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindMap.DAL.Models;

namespace WindMap.Services.Models
{
    public class CacheSensor
    {
        public int Id { get; set; }

        public int RegionId { get; set; }

        public string MonitorId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string MonitorIdToRight { get; set; }

        public double AzimuthRight { get; set; }

        public double DistanceRight { get; set; }

        public double NormalAzimuthToRight { get; set; }

        public string MonitorIdToLeft { get; set; }

        public double AzimuthLeft { get; set; }

        public double DistanceLeft { get; set; }

        public double NormalAzimuthToLeft { get; set; }

        public double OnsiteWindMin { get; set; }

        public double OnsiteWindMax { get; set; }

        public double OnsiteWindCenterAngle { get; set; }


        public bool IsLeak { get; set; }

        public bool IsPreviousLeak { get; set; }

        public double MaxPintPF { get; set; }

        public List<SensorResult> ProcessedSensorResults = new List<SensorResult>();

        public List<SensorResult> NewSensorResults = new List<SensorResult>();


        //public static implicit operator Sensor(CacheSensor sensor)
        //{
        //    return new Sensor
        //    {
        //        Id = sensor.Id,
        //        RegionId = sensor.RegionId,
        //        MonitorId = sensor.MonitorId,
        //        Latitude = sensor.Latitude,
        //        Longitude = sensor.Longitude,
        //        MonitorIdToRight = sensor.MonitorIdToRight,
        //        AzimuthRight = sensor.AzimuthRight,
        //        DistanceRight = sensor.DistanceRight,
        //        NormalAzimuthToRight = sensor.NormalAzimuthToRight,
        //        MonitorIdToLeft = sensor.MonitorIdToLeft,
        //        AzimuthLeft = sensor.AzimuthLeft,
        //        DistanceLeft = sensor.DistanceLeft,
        //        NormalAzimuthToLeft = sensor.NormalAzimuthToLeft,
        //        OnsiteWindMin = sensor.OnsiteWindMin,
        //        OnsiteWindMax = sensor.OnsiteWindMax,
        //        OnsiteWindCenterAngle = sensor.OnsiteWindCenterAngle,

        //    };
        //}

        public static implicit operator CacheSensor(Sensor sensor)
        {
            return new CacheSensor
            {
                Id = sensor.Id,
                RegionId = sensor.RegionId,
                MonitorId = sensor.MonitorId,
                Latitude = sensor.Latitude,
                Longitude = sensor.Longitude,
                MonitorIdToRight = sensor.MonitorIdToRight,
                AzimuthRight = sensor.AzimuthRight,
                DistanceRight = sensor.DistanceRight,
                NormalAzimuthToRight = sensor.NormalAzimuthToRight,
                MonitorIdToLeft = sensor.MonitorIdToLeft,
                AzimuthLeft = sensor.AzimuthLeft,
                DistanceLeft = sensor.DistanceLeft,
                NormalAzimuthToLeft = sensor.NormalAzimuthToLeft,
                OnsiteWindMin = sensor.OnsiteWindMin,
                OnsiteWindMax = sensor.OnsiteWindMax,
                OnsiteWindCenterAngle = sensor.OnsiteWindCenterAngle,
                ProcessedSensorResults = sensor.SensorResults.TakeLast(60).Where(e=>(DateTime.Now - e.TimeEnd).TotalHours < 24).ToList(),
            };
        }
    }
}
