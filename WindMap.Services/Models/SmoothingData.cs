﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindMap.Services.Models
{
    public struct SmoothingData
    {
        public double Pf { get; set; }
        public double Twd { get; set; }

        public SmoothingData(double pf, double twd)
        {
            Pf = pf;
            Twd = twd;
        }
    }


    public class GropedSmoothingData
    {
        public List<SmoothingData> Items { get; set; }
        public SmoothingData AverageData { get; set; }
        public RegressionResult RegressionResult { get; set; }

        public GropedSmoothingData()
        {
            Items = new List<SmoothingData>();
        }
    }
}
