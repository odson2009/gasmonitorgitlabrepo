﻿namespace WindMap.Services.Models
{
    public class RegressionParameter
    {
        public double X { get; set; }
        public double Y { get; set; }
    }

    public class RegressionResult
    {
        public double M { get; set; }
        public double B { get; set; }
        public double R { get; set; }
        public double Interception { get; set; }
    }
}
