﻿using System.Collections.Generic;

namespace WindMap.Services.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }

        public List<FieldError> Errors { get; set; }
    }

    public class FieldError
    {
        public string Field { get; set; }

        public List<string> Errors { get; set; }
    }
}
