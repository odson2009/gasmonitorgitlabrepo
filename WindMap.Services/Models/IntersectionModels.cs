﻿using System;
using System.Collections.Generic;
using System.Text;
using WindMap.Services.Extensions;

namespace WindMap.Services.Models
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
    public class Ray
    {
        public Point StartPoint { get; }

        public Point Point { get; private set; }

        public double Angle { get; }

        public Ray(double x, double y, double angle)
        {
            StartPoint = new Point(x, y);
            Angle = angle;

            Point = CalculatePoint();
        }

        private Point CalculatePoint()
        {
            const double interval = 1;

            if (Math.Abs(Angle) < 0.001)
                return new Point(StartPoint.X, StartPoint.Y + interval);

            if (Math.Abs(Angle - Math.PI) < 0.001)
                return new Point(StartPoint.X, StartPoint.Y - interval);

            var newX = Angle < Math.PI ? StartPoint.X + interval : StartPoint.X - interval;

            var k = Math.Tan(Angle);
            var c = StartPoint.X - StartPoint.Y * k;

            var newY = Math.Round((newX - c) / k, 2);

            return new Point(newX, newY);
        }
    }

    public class Triangle
    {
        public Point P1 { get; set; }
        public Point P2 { get; set; }
        public Point P3 { get; set; }
        public Point Center
        {
            get
            {
                if (P1 == null || P2 == null || P3 == null)
                    return null;

                var x = (P1.X + P2.X + P3.X) / 3;
                var y = (P1.Y + P2.Y + P3.Y) / 3;

                return new Point(x, y);
            }
        }
        public double Area
        {
            get
            {
                if (P1 == null || P2 == null || P3 == null)
                    return 0;

                return Math.Abs(((P1.X - P3.X) * (P2.Y - P3.Y) - (P2.X - P3.X) * (P1.Y - P3.Y)) / 2);
            }
        }
    }

    public class IntersectionSensor
    {
        public int Id { get; set; }

        public double X { get; set; }

        public double Y { get; set; }

        public double FirstAngle { get; set; }

        public double SecondAngle { get; set; }

        public Ray FirstRay => new Ray(X, Y, FirstAngle % 360 * (Math.PI / 180));

        public Ray SecondRay => new Ray(X, Y, SecondAngle % 360 * (Math.PI / 180));
    }

    public class IntersectionResult
    {
        public Point Center { get; set; }
        public List<Triangle> Triangles { get; set; }
    }
}
