﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Services.Models
{
    public class SensorResultReportModel
    {
        public SensorResultTmp[] SensorResults { get; set; }
        public TestResult[] TestResults { get; set; }
    }

    public class TMP
    {

    }

    public class TestResult
    {

    }

    public class SensorResultTmp
    {
        public string TimeBegin { get; set; }

        public string TimeEnd { get; set; }

        public float Concentration { get; set; }

        public float WindSpeed { get; set; }

        public float WindDirection { get; set; }

        public float TWD { get; set; }

        public float PF { get; set; }

    }
}
