﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Services.Models
{
    public class SensorBufferReportModel
    {
        public SensorResultTmp[] SensorResults { get; set; }
        public TestResult[] TestResults { get; set; }
        public GroupTMP[] AverageSummary { get; set; }
        public SensorResultTmp[] SensorResultsAll { get; set; }
    }

    public class GroupTMP
    {
        public string Step { get; set; }
        public ResultsData[] AverageResults { get; set; }
        public LinearRegression LinearRegressionResults { get; set; }
    }

    public class ResultsData
    {
        public AverageData AverageData { get; set; }

        public LinearRegression LinearRegressionResult { get; set; }
    }

    public class AverageData
    {
        public string TWD_Average { get; set; }
        public string PF_Average { get; set; }
    }

    public class LinearRegression
    {
        public string M { get; set; }
        public string B { get; set; }
        public string R { get; set; }
        public string XIntercept { get; set; }

    }

    public class TestResult
    {
        public string Step { get; set; }
        public string EndDate { get; set; }
        public FitData Smoothed { get; set; }
        public FitData Average { get; set; }
    }

    public class FitData
    {
        public Fminsearch Fminsearch { get; set; }
        public Polyfit Polyfit { get; set; }
    }

    public class Fminsearch
    {
        public string A { get; set; }
        public string Sigma { get; set; }
        public string Mu { get; set; }
    }

    public class Polyfit
    {
        public string A { get; set; }
        public string Sigma { get; set; }
        public string Mu { get; set; }
        public string CoefficientA { get; set; }
        public string CoefficientB { get; set; }
        public string CoefficientC { get; set; }
    }

    public class SensorResultTmp
    {
        public string TimeBegin { get; set; }
        public string TimeEnd { get; set; }
        public float Concentration { get; set; }
        public float WindSpeed { get; set; }
        public float WindDirection { get; set; }
        public float TWD { get; set; }
        public float PF { get; set; }
    }
}
