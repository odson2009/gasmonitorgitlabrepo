﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindMap.Services.Models
{
    public class AproximationResult
    {
        public double A { get; set; }
        public double Mu { get; set; }
        public double Sigma { get; set; }
    }
}
