﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;
using WindMap.Services.CoreServices;

namespace WindMap.Services.ModelServices
{
    public interface IRegionService : IBaseService<Region>
    {
        Task<List<Region>> GetAllAsync(CancellationToken cancellationToken);
        Task<List<Region>> GetCompaniesWithResultsByPeriod(DateTime startDate, DateTime endDate, bool isNeedLastResults, int[] sensorIdsArray, CancellationToken cancellationToken);
        Task<List<Region>> GetCompaniesWithSingleSensorResult(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken);
        Task<List<Region>> GetRegionsByIdsAsync(int[] ids, CancellationToken cancellationToken);
        Task<Region> FindByIdAsync(int id, CancellationToken cancellationToken);
        Task<Region> FindByIdWithEventsAsync(int id, CancellationToken cancellationToken);
        Task DeleteAsyncWithPermissionAsync(Region entity, CancellationToken cancellationToken);
    }

    internal class RegionService : BaseService<Region, IRegionRepository>, IRegionService
    {
        private ICacheService _cacheService;
        private IRegionRepository _regionRepository;
        private IRelationPermissionRepository _relationPermissionRepository;

        public RegionService(ICacheService cacheService, IRegionRepository regionRepository, IRelationPermissionRepository relationPermissionRepository) : base(regionRepository)
        {
            _cacheService = cacheService;
            _regionRepository = regionRepository;
            _relationPermissionRepository = relationPermissionRepository;
        }

        public override void Dispose()
        {
            _regionRepository = null;
        }

        public async Task<Region> FindByIdAsync(int id, CancellationToken cancellationToken)
        {
            return await _regionRepository.FindById(id)
                .Include(c => c.Addresses).ThenInclude(c => c.Location)
                .Include(c => c.Algorithmes)
                .Include(c => c.RelationPermissions)
                .Include(c => c.Company)
                .Include(c => c.Sensors)
                .ThenInclude(c => c.SensorResults)
                .FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<Region> FindByIdWithEventsAsync(int id, CancellationToken cancellationToken)
        {
            return await _regionRepository.FindById(id)
                .Include(c => c.Sensors)
                .ThenInclude(c => c.SensorEvents)
                .FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<List<Region>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await _regionRepository.Get()
                .Include(c => c.Addresses).ThenInclude(c => c.Location)
                .Include(c => c.Sensors)
                .Include(c => c.Algorithmes)
                .Include(c => c.RelationPermissions)
                .Include(c => c.Company)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<Region>> GetRegionsByIdsAsync(int[] ids, CancellationToken cancellationToken)
        {
            return await _regionRepository.FindById(ids)
                .Include(c => c.Company)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<Region>> GetCompaniesWithResultsByPeriod(DateTime startDate, DateTime endDate, bool isNeedLastResults, int[] sensorIdsArray, CancellationToken cancellationToken)
        {
            return await _regionRepository.GetRegionResultsByPeriod(startDate, endDate, sensorIdsArray, isNeedLastResults, cancellationToken);
        }

        public async Task<List<Region>> GetCompaniesWithSingleSensorResult(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken)
        {
            return await _regionRepository.GetWithSingleSensorResult(date, sensorIdsArray, cancellationToken);
        }
        
        public override Task AddAsync(Region entity, CancellationToken cancellationToken)
        {
            //_cacheService.GetRegionAsync(entity.Id).Add(entity);
            return base.AddAsync(entity, cancellationToken);
        }

        public override Task DeleteAsync(Region entity, CancellationToken cancellationToken)
        {
            //_cacheService.Values.Remove(entity);
            return base.DeleteAsync(entity, cancellationToken);
        }
        public async Task DeleteAsyncWithPermissionAsync(Region entity, CancellationToken cancellationToken)
        {
            await _relationPermissionRepository.SetNullRegionIdAsync(entity.Id, cancellationToken);
            await _regionRepository.DeleteAsync(entity, cancellationToken);
            //return base.DeleteAsync(entity, cancellationToken);
        }

        public override Task UpdateAsync(Region entity, CancellationToken cancellationToken)
        {
            //_cacheService.Values.RemoveAll(e => e.Id == entity.Id);
            //_cacheService.Values.Add(entity);
            return base.UpdateAsync(entity, cancellationToken);
        }
    }
}
