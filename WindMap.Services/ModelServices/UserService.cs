﻿using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;
using System.Linq;

namespace WindMap.Services.ModelServices
{
    public interface IUserService : IBaseService<User>
    {
        Task<User> GetUserByIdAsync(string id, CancellationToken cancellationToken);
        Task<User> GetUserByLoginAsync(string login, CancellationToken cancellationToken);
        Task<User> GetByIdAsync(string id, CancellationToken cancellationToken);
    }

    internal class UserService : BaseService<User, IUserRepository>, IUserService
    {
        private IUserRepository _userRepository;

        public UserService(IUserRepository userRepository) : base(userRepository)
        {
            _userRepository = userRepository;
        }
        public override void Dispose()
        {
            _userRepository = null;
        }
        public Task<User> GetUserByIdAsync(string id, CancellationToken cancellationToken)
        {
            return _userRepository.FindById(id)
              .Include(c => c.UiPermission)
              .Include(c => c.RelationPermissions)
                  .ThenInclude(c => c.Company)
                        .ThenInclude(c => c.Regions)
                            .ThenInclude(c => c.Sensors)
                                .ThenInclude(s => s.SensorResults)

              .Include(c => c.RelationPermissions)
                  .ThenInclude(c => c.Company)
                        .ThenInclude(c => c.Regions)
                              .ThenInclude(c => c.Algorithmes)
              .FirstOrDefaultAsync(cancellationToken);
        }

        public Task<User> GetByIdAsync(string id, CancellationToken cancellationToken)
        {
            return _userRepository.FindById(id)
                .Include(c => c.UiPermission).ThenInclude(e => e.UiElement)
                .FirstOrDefaultAsync(cancellationToken);
        }

        public Task<User> GetUserByLoginAsync(string login, CancellationToken cancellationToken)
        {
            return _userRepository.FindByLogin(login)
              .FirstOrDefaultAsync(cancellationToken);
        }
    }
}