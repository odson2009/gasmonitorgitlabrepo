﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Repositories;

namespace WindMap.Services.ModelServices
{
    public interface IBaseService<TModel> : IDisposable
        where TModel : class
    {
        Task<TModel> FindAsync(object[] keyValues, CancellationToken cancellationToken);
        Task AddAsync(TModel entity, CancellationToken cancellationToken);
        Task AddRangeAsync(IList<TModel> list, CancellationToken cancellationToken);
        Task UpdateAsync(TModel entity, CancellationToken cancellationToken);
        Task UpdateAsync(ICollection<TModel> entity, CancellationToken cancellationToken);
        Task UpdateAsync(ICollection<TModel> entities,
            ICollection<string> exceptions, CancellationToken cancellationToken);
        Task DeleteAsync(TModel entity, CancellationToken cancellationToken);
        Task DeleteAsync(IList<TModel> entity, CancellationToken cancellationToken);
        Task<bool> IsExistAsync(TModel entity, CancellationToken cancellationToken);
        IQueryable<TModel> Get();
    }

    public abstract class BaseService<TModel, TRepository> : IBaseService<TModel>
        where TModel : class
        where TRepository : IBaseRepository<TModel>
    {
        private readonly TRepository _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository;
        }

        public abstract void Dispose();

        public virtual Task<TModel> FindAsync(object[] keyValues, CancellationToken cancellationToken)
        {
            return _repository.FindAsync(keyValues, cancellationToken);
        }

        public virtual Task AddAsync(TModel entity, CancellationToken cancellationToken)
        {
            return _repository.AddAsync(entity, cancellationToken);
        }

        public virtual Task AddRangeAsync(IList<TModel> list, CancellationToken cancellationToken)
        {
            return _repository.AddRangeAsync(list, cancellationToken);
        }

        public virtual Task UpdateAsync(TModel entity, CancellationToken cancellationToken)
        {
            return _repository.UpdateAsync(entity, cancellationToken);
        }

        public virtual Task UpdateAsync(ICollection<TModel> entity, CancellationToken cancellationToken)
        {
            return _repository.UpdateAsync(entity, cancellationToken);
        }

        public Task UpdateAsync(ICollection<TModel> entities, ICollection<string> exceptions, CancellationToken cancellationToken)
        {
            return _repository.UpdateAsync(entities, exceptions, cancellationToken);
        }

        public virtual Task DeleteAsync(TModel entity, CancellationToken cancellationToken)
        {
            return _repository.DeleteAsync(entity, cancellationToken);
        }

        public virtual Task DeleteAsync(IList<TModel> entity, CancellationToken cancellationToken)
        {
            return _repository.DeleteAsync(entity, cancellationToken);
        }

        public virtual async Task<bool> IsExistAsync(TModel entity, CancellationToken cancellationToken)
        {
            return await _repository.IsExistAsync(entity, cancellationToken);
        }

        public IQueryable<TModel> Get()
        {
            return _repository.Get();
        }
    }
}
