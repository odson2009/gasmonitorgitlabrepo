﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;

namespace WindMap.Services.ModelServices
{
    public interface IGeneralService
    {
        Task<MonitorCount> GetCount(CancellationToken cancellationToken);
        Task<List<Sensor>> GetListSensors(CancellationToken cancellationToken);
    }

    internal class GeneralService : IGeneralService
    {
        private readonly ISensorRepository _sensorRepository;

        public GeneralService(ISensorRepository sensorRepository)
        {
            _sensorRepository = sensorRepository;
        }

        public async Task<MonitorCount> GetCount(CancellationToken cancellationToken)
        {
            var sensors = await _sensorRepository.Get()
                .Include(c => c.SensorResults)
                .ToListAsync(cancellationToken);

            var sensorResultCount = sensors.Sum(sensor => sensor.SensorResults.Count);

            return new MonitorCount(sensors.Count, sensorResultCount);
        }
        public async Task<List<Sensor>> GetListSensors(CancellationToken cancellationToken)
        {
            return await _sensorRepository.Get()
                .Include(c => c.SensorResults)
                .ToListAsync(cancellationToken);
        }
    }
}