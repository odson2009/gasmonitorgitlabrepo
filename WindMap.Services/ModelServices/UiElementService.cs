﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;

namespace WindMap.Services.ModelServices
{
    public interface IUiElementService : IBaseService<UiElement>
    {
        Task<List<UiElement>> GetAllAsync(CancellationToken cancellationToken);
        Task<UiElement> GetById(int id, CancellationToken cancellationToken);
    }

    internal class UiElementService : BaseService<UiElement, IUiElementRepository>, IUiElementService
    {
        private IUiElementRepository _uiElemenRepository;

        public UiElementService(IUiElementRepository uiElemenRepository) : base(uiElemenRepository)
        {
            _uiElemenRepository = uiElemenRepository;
        }
        public override void Dispose()
        {
            _uiElemenRepository = null;
        }

        public Task<List<UiElement>> GetAllAsync(CancellationToken cancellationToken)
        {
            return _uiElemenRepository.GetAllAsync(cancellationToken);
        }
        public Task<UiElement> GetById(int id, CancellationToken cancellationToken)
        {
            return _uiElemenRepository.FindById(id).FirstOrDefaultAsync(cancellationToken);
        }
    }
}