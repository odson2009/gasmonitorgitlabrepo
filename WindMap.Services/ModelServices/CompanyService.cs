﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;

namespace WindMap.Services.ModelServices
{
    public interface ICompanyService : IBaseService<Company>
    {
        Task<List<Company>> FindAllAsync(CancellationToken cancellationToken);
        Task<Company> FindByIdAsync(int id, CancellationToken cancellationToken);
        Task<Company> FindByNameAsync(string name, CancellationToken cancellationToken);
    }

    internal class CompanyService : BaseService<Company, ICompanyRepository>, ICompanyService
    {
        private ICompanyRepository _companyRepository;

        public CompanyService(ICompanyRepository companyRepository) : base(companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public override void Dispose()
        {
            _companyRepository = null;
        }

        public async Task<List<Company>> FindAllAsync(CancellationToken cancellationToken)
        {
            return await _companyRepository.FindAll()
                .Include(c => c.Regions)
                .ThenInclude(c => c.Sensors)
                .Include(c => c.RelationPermissions)
                .ToListAsync(cancellationToken);
        }

        public async Task<Company> FindByIdAsync(int id, CancellationToken cancellationToken)
        {
            return await _companyRepository.FindById(id)
                .Include(c => c.Regions)
                .ThenInclude(c => c.Sensors)
                .Include(c => c.RelationPermissions)
                .SingleOrDefaultAsync(cancellationToken);
        }

        public async Task<Company> FindByNameAsync(string name, CancellationToken cancellationToken)
        {
            return await _companyRepository.FindByName(name)
                .SingleOrDefaultAsync(cancellationToken);
        }
    }
}
