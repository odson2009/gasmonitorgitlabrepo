﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;
using WindMap.Services.CoreServices;
using WindMap.Services.Extensions;
using WindMap.Services.Models;

namespace WindMap.Services.ModelServices
{
    public interface ISensorService : IBaseService<Sensor>
    {
        Task<Sensor> FindByIdAsync(int id, CancellationToken cancellationToken);
        Task<List<Sensor>> FindByIdAsync(int[] id, CancellationToken cancellationToken);
        Task<List<Sensor>> GetAllAsync(CancellationToken cancellationToken);
        Task<List<Sensor>> GetAllByRegionIdAsync(int regionId, CancellationToken cancellationToken);
        Task<List<Sensor>> GetSensorsWithResultsByPeriod(DateTime startDate, DateTime endDate, bool isNeedLastResults, int[] sensorIdsArray, CancellationToken cancellationToken);
        Task<List<Sensor>> GetSensorsWithSingleResults(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken);
        List<object> GetSensorsChartDataPeriod(CancellationToken cancellationToken);
        Task<DateTime> GetMinDate(CancellationToken cancellationToken);
        //Task<List<CacheSensor>> GetSensorsDTOAsync(CancellationToken cancellationToken);
        Task<SensorEvent> GetLastSensorEvent(int sensorId, CancellationToken cancellationToken);
    }

    internal class SensorService : BaseService<Sensor, ISensorRepository>, ISensorService
    {
        private ICacheService _cacheService;
        private ISensorRepository _sensorRepository;
        private ISensorResultRepository _sensorResultRepository;

        public SensorService(
            ICacheService cacheService,
            ISensorRepository sensorRepository,
            ISensorResultRepository sensorResultRepository) : base(sensorRepository)
        {
            _cacheService = cacheService;
            _sensorRepository = sensorRepository;
            _sensorResultRepository = sensorResultRepository;
        }

        public override void Dispose()
        {
            _sensorRepository = null;
        }

        public Task<Sensor> FindByIdAsync(int id, CancellationToken cancellationToken)
        {
            return _sensorRepository.FindById(id)
                .Include(c => c.SensorResults)
                .Include(c => c.SensorEvents)
                .Include(c => c.Region).ThenInclude(r => r.RelationPermissions).ThenInclude(u => u.User)
                .Include(c => c.Region).ThenInclude(s => s.Sensors)
                .FirstOrDefaultAsync(cancellationToken);
        }

        public Task<List<Sensor>> FindByIdAsync(int[] id, CancellationToken cancellationToken)
        {
            return _sensorRepository.FindById(id)
                .Include(c => c.SensorResults)
                .ToListAsync(cancellationToken);
        }

        public Task<List<Sensor>> GetAllAsync(CancellationToken cancellationToken)
        {
            return _sensorRepository.Get()
                .Include(c => c.Region)
                .Include(c => c.SensorResults)
                .ToListAsync(cancellationToken);
        }

        public Task<List<Sensor>> GetAllByRegionIdAsync(int regionId, CancellationToken cancellationToken)
        {
            return _sensorRepository.GetByRegionId(regionId)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<Sensor>> GetSensorsWithResultsByPeriod(DateTime startDate, DateTime endDate, bool isNeedLastResults, int[] sensorIdsArray, CancellationToken cancellationToken)
        {
            return await _sensorRepository.GetByPeriod(startDate, endDate, isNeedLastResults, sensorIdsArray, cancellationToken);
        }

        public async Task<List<Sensor>> GetSensorsWithSingleResults(DateTime date, int[] sensorIdsArray, CancellationToken cancellationToken)
        {
            return await _sensorRepository.GetWithSingleResult(date, sensorIdsArray, cancellationToken);
        }

        public List<object> GetSensorsChartDataPeriod(CancellationToken cancellationToken)
        {
            return _sensorRepository.GetResultsChartDataPeriod().ToList();
        }

        public async Task<DateTime> GetMinDate(CancellationToken cancellationToken)
        {
            return await _sensorResultRepository.GetMinDate(cancellationToken);
        }

        public async Task<SensorEvent> GetLastSensorEvent(int sensorId, CancellationToken cancellationToken)
        {
            var result = await _sensorRepository.FindById(sensorId)
                .Include(s => s.SensorEvents)
                .ThenInclude(e => e.Calculations)
                .SingleOrDefaultAsync(cancellationToken);
                //.Select(s => s.SensorEvents);
                //.LastOrDefaultAsync(cancellationToken);

            return result.SensorEvents.Last();
        }

        //public Task<List<CacheSensor>> GetSensorsDTOAsync(CancellationToken cancellationToken)
        //{
        //    return _sensorRepository.Get().Select(e => e.ToCacheSensor()).ToListAsync(cancellationToken);
        //}

        public override Task UpdateAsync(Sensor entity, CancellationToken cancellationToken)
        {
            var region = _cacheService.GetSensorAsync(entity.Id);
            return base.UpdateAsync(entity, cancellationToken);
        }
    }
}
