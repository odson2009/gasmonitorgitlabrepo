﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using WindMap.DAL.Extensions;
using WindMap.DAL.Models;

namespace WindMap.Services.ModelServices
{
    public interface IAccountService
    {
        string CreateAccessToken(string userId, string userName, string userEmail, string permissions, bool userChangedPassword, string role, bool rememberMe);
        string CreateRefreshToken(string userId);
        JsonWebToken GetToken(string userId, string userName, string userEmail, string permissions, bool userChangedPassword, string role, bool rememberMe);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }

    internal class AccountService : IAccountService
    {
        public string CreateAccessToken(string userId, string userName, string userEmail, string permissions, bool userChangedPassword, string role, bool rememberMe)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId),
                new Claim(JwtRegisteredClaimNames.GivenName, userName),
                new Claim(JwtRegisteredClaimNames.Email, userEmail),
                new Claim(ClaimTypes.Role, role),
                new Claim("permissions", string.Join(",", permissions)),
                new Claim("changedPassword", userChangedPassword.ToString().ToLower()),
                new Claim("role", role),

            };
            var token = new JwtSecurityToken(
                claims: claims,
                expires: rememberMe
                    ? DateTime.UtcNow.AddMinutes(AuthOptions.LongLifetime)
                    : DateTime.UtcNow.AddMinutes(AuthOptions.ShortLifetime),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), AuthOptions.Algorithm)
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public string CreateRefreshToken(string userId)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId),
            };
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(AuthOptions.LifetimeForRefreshToken),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), AuthOptions.Algorithm)
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public JsonWebToken GetToken(string userId, string userName, string userEmail, string permissions, bool userChangedPassword, string role, bool rememberMe)
        {
            return new JsonWebToken
            {
                AccessToken = CreateAccessToken(userId, userName, userEmail, permissions, userChangedPassword, role, rememberMe),
                RefreshToken = CreateRefreshToken(userId)
            };
        }

        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey()
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(AuthOptions.Algorithm, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }
    }
}
