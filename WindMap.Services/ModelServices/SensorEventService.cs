﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;

namespace WindMap.Services.ModelServices
{
    public interface ISensorEventService : IBaseService<SensorEvent>
    {
        Task<List<SensorEvent>> FindAllAsync(CancellationToken cancellationToken);
        Task<List<SensorEvent>> FindAllBySensorIdAsync(int sensorId, CancellationToken cancellationToken);
        Task<List<SensorEvent>> FindAllByRegionIdAsync(int regionId, CancellationToken cancellationToken);
    }

    internal class SensorEventService : BaseService<SensorEvent, ISensorEventRepository>, ISensorEventService
    {
        private ISensorRepository _sensorRepository;
        private ISensorEventRepository _sensorEventRepository;

        public SensorEventService(
            ISensorRepository sensorRepository,
            ISensorEventRepository sensorEventRepository) : base(sensorEventRepository)
        {
            _sensorRepository = sensorRepository;
            _sensorEventRepository = sensorEventRepository;
        }

        public override void Dispose()
        {
            _sensorRepository = null;
            _sensorEventRepository = null;
        }

        public async Task<List<SensorEvent>> FindAllAsync(CancellationToken cancellationToken)
        {
            return await _sensorEventRepository.FindAll()
                .Include(e => e.Sensor)
                .ThenInclude(e => e.Region)
                //.Include(e => e.EventStatus)
                //.Include(e => e.EndEvent)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<SensorEvent>> FindAllBySensorIdAsync(int sensorId, CancellationToken cancellationToken)
        {
            return await _sensorEventRepository.FindAll()
                .Include(e => e.Sensor)
                .ThenInclude(e => e.Region)
                //.Include(e => e.EventStatus)
                //.Include(e => e.EndEvent)
                .Where(e => e.SensorId == sensorId).ToListAsync(cancellationToken);
        }

        public async Task<List<SensorEvent>> FindAllByRegionIdAsync(int regionId, CancellationToken cancellationToken)
        {
            return await _sensorEventRepository.FindAll()
                .Include(e => e.Sensor)
                .ThenInclude(e => e.Region)
                //.Include(e => e.EventStatus)
                //.Include(e => e.EndEvent)
                .Where(e => e.Sensor.RegionId == regionId).ToListAsync(cancellationToken);
        }

    }
}
