﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;

namespace WindMap.Services.ModelServices
{
    public interface IRefreshTokenService : IBaseService<RefreshToken>
    {
        Task<List<RefreshToken>> GetAllByUserId(string userId, CancellationToken cancellationToken);
        Task<RefreshToken> GetById(int id, CancellationToken cancellationToken);
        Task DeleteAllAsync(string userId, CancellationToken cancellationToken);
    }

    internal class RefreshTokenService : BaseService<RefreshToken, IRefreshTokenRepository>, IRefreshTokenService
    {
        private IRefreshTokenRepository _refreshTokenRepository;

        public RefreshTokenService(IRefreshTokenRepository refreshTokenRepository) : base(refreshTokenRepository)
        {
            _refreshTokenRepository = refreshTokenRepository;
        }

        public Task DeleteAllAsync(string userId, CancellationToken cancellationToken)
        {
            return _refreshTokenRepository.DeleteAllAsync(userId, cancellationToken);
        }

        public override void Dispose()
        {
            _refreshTokenRepository = null;
        }

        public Task<List<RefreshToken>> GetAllByUserId(string userId, CancellationToken cancellationToken)
        {
            return _refreshTokenRepository.FindByUserId(userId).ToListAsync(cancellationToken);
        }

        public Task<RefreshToken> GetById(int id, CancellationToken cancellationToken)
        {
            return _refreshTokenRepository.FindById(id).FirstAsync(cancellationToken);
        }
    }
}

