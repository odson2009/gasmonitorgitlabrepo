﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WindMap.DAL.Models;
using WindMap.DAL.Repositories;
using WindMap.Services.CalculationServices;
using WindMap.Services.CoreServices;
using WindMap.Services.Models;

namespace WindMap.Services.ModelServices
{
    public interface IResultService
    {
        Task<List<List<SensorResult>>> GetResultsByIds(int[] ids, CancellationToken cancellationToken);
        Task<SensorCheckResult> AddResultWithCache(SensorResult result, CancellationToken cancellationToken);
        Task<SensorCheckResult> AddResultWithCache(IList<SensorResult> results, CancellationToken cancellationToken);

        void Refresh();
        //Task AddResults(IList<SensorResult> results, CancellationToken cancellationToken);
    }

    internal class ResultService : IResultService
    {
        private readonly ICacheService _cacheService;
        private readonly ICalculationService _calculationService;
        private readonly ILeakService _leakService;
        private readonly ISensorResultRepository _sensorResultRepository;

        public ResultService(
            ICacheService cacheService,
            ICalculationService calculationService,
            ILeakService leakService,
            ISensorResultRepository sensorResultRepository)
        {
            _cacheService = cacheService;
            _calculationService = calculationService;
            _leakService = leakService;
            _sensorResultRepository = sensorResultRepository;
        }

        public async Task<List<List<SensorResult>>> GetResultsByIds(int[] ids, CancellationToken cancellationToken)
        {
            var results = await _sensorResultRepository.Get().Where(e=>ids.Contains(e.SensorId)).ToListAsync(cancellationToken);
            var res = results.GroupBy(e => e.SensorId);
            var t = res.Select(e => e.ToList()).ToList();

            return t;
        }

        public async Task<SensorCheckResult> AddResultWithCache(SensorResult result, CancellationToken cancellationToken)
        {
            //TODO: Uncomment
            //await _sensorResultRepository.AddAsync(result, cancellationToken);
            await _cacheService.AddResult(result);
            return await _leakService.CheckSensor(result.SensorId);
        }

        public async Task<SensorCheckResult> AddResultWithCache(IList<SensorResult> results, CancellationToken cancellationToken)
        {
            //TODO: Uncomment
            //await _sensorResultRepository.AddRangeAsync(results, cancellationToken);
            await _cacheService.AddResults(results);

            var uniqueIds = results.GroupBy(e => e.SensorId).Select(e=>e.Key);

            var result = new SensorCheckResult();

            foreach (var id in uniqueIds)
                result = await _leakService.CheckSensor(id);

            return result;
        }

        public void Refresh()
        {
            _cacheService.Refresh();
        }
    }
}
