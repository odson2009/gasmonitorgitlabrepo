﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindMap.RequestSender
{
    public partial class Form1 : Form
    {
        private IHubProxy HubProxy { get; set; }
        private HubConnection Connection { get; set; }

        protected FileResultModel _fileResultModel;
        protected HttpClient _client = new HttpClient();

        public Form1()
        {
            InitializeComponent();

            try
            {
                LoadSensorsResults();
                BindData();
                LoadSettings();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load results", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void LoadSensorsResults()
        {
            string filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).Replace("WindMap.RequestSender\\bin\\Debug", ""),
                @"WindMap.Web\src\sensorResults.json");
            string jsonFromFile = File.ReadAllText(filePath);

            _fileResultModel = JsonConvert.DeserializeObject<FileResultModel>(jsonFromFile);

            lbLoadedCount1001.Text = _fileResultModel.Sensors[0].SensorResults.Length.ToString();
            lbLoadedCount1002.Text = _fileResultModel.Sensors[1].SensorResults.Length.ToString();
        }

        protected void BindData()
        {
            dataGridView1001.DataSource = new BindingList<SensorResultModel>(_fileResultModel.Sensors[0].SensorResults);
            dataGridView1002.DataSource = new BindingList<SensorResultModel>(_fileResultModel.Sensors[1].SensorResults);
        }

        protected void LoadSettings()
        {
            tbApiAddress.Text = "http://localhost:7000/api/sensors/results";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try {
               SendRequest();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "Send result", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected async Task SendRequest()
        {
            List<SensorRequestModel> requestList = new List<SensorRequestModel>();

            SensorRequestModel sensorRequest = new SensorRequestModel()
            {
                SensorId = _fileResultModel.Sensors[0].Id,  
            };
            sensorRequest.SensorResults = new List<SensorResultModel>();
            sensorRequest.SensorResults.Add(_fileResultModel.Sensors[0].SensorResults[0]);

            requestList.Add(sensorRequest);

            var jsonResult = JsonConvert.SerializeObject(requestList);

            var content = new StringContent(jsonResult.ToString(), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync(
               this.tbApiAddress.Text, content);

            MessageBox.Show(response.EnsureSuccessStatusCode().ToString(), "Request result", MessageBoxButtons.OK, MessageBoxIcon.Information);

            string responseBody = await response.Content.ReadAsStringAsync();
            var eventResponseObj = JsonConvert.DeserializeObject<SensorEventModel>(responseBody);
        }

        //Connect to APIs hub
        private async void button1_Click(object sender, EventArgs e)
        {
            await ConnectAsync();

            this.lbHubConnetion.Text = Connection.State.ToString();
        }

        private async Task ConnectAsync()
        {
            Connection = new HubConnection("http://localhost:8000/sensor", false);
            
            HubProxy = Connection.CreateHubProxy("SensorHub");

        
            //Handle incoming event from server: use Invoke to write to console from SignalR's thread 
            HubProxy.On<string, string>("Send", (name, message) =>
                this.Invoke((Action)(() =>
                    this.richTextBox1.AppendText(String.Format("{0}: {1}" + Environment.NewLine, name, message))
                ))
            );
            try
            {
                await Connection.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Hub connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //No connection: Don't enable Send button or show chat UI 
            }

        }
    }
}
