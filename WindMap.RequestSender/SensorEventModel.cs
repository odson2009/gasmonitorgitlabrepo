﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindMap.RequestSender
{
    public class SensorEventModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("monitorId")]
        public int MonitorId { get; set; }

        [JsonProperty("pairedMonitorId")]
        public int? PairedSensorId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("eventTime")]
        public DateTime EventTime { get; set; }

        [JsonProperty("sourceLatitude")]
        public decimal SourceLatitude { get; set; }

        [JsonProperty("sourceLongitude")]
        public decimal SourceLongitude { get; set; }

        [JsonProperty("parallelWindDirection")]
        public decimal ParallelWindDirection { get; set; }

        [JsonProperty("crossWindDirection")]
        public decimal CrossWindDirection { get; set; }

        [JsonProperty("averageWindDirection")]
        public decimal AverageWindDirection { get; set; }

        [JsonProperty("emissionRate")]
        public decimal EmissionRate { get; set; }

        [JsonProperty("companyId")]
        public string CompanyId { get; set; }

        [JsonProperty("companyName")]
        public string CompanyName { get; set; }

    }
}
