﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindMap.RequestSender
{

    public class FileResultModel
    {
        [JsonProperty("sensors")]
        public SensorModel[] Sensors { get; set; }
    }

    public class SensorModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("monitorId")]
        public string MonitorId { get; set; }

        [JsonProperty("onsiteWindMin")]
        public string OnsiteWindMin { get; set; }

        [JsonProperty("onsiteWindMax")]
        public string OnsiteWindMax { get; set; }

        [JsonProperty("sensors")]
        public SensorResultModel[] Sensors { get; set; }

        [JsonProperty("sensorResults")]
        public SensorResultModel[] SensorResults { get; set; }
    }

    public class SensorResultModel
    {
        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("concentration")]
        public decimal Concentration { get; set; }

        [JsonProperty("windSpeed")]
        public decimal WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        public decimal WindDirection { get; set; }
    }
}
