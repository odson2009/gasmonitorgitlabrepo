x = [11 31 64 112 176 259 362]% the independent data set - force
y = [2 3 4 5 6 7 8]% the dependent data set - deflection
plot(x,y) %make a dot plot
[coefficents, f, g] = polyfit(x,y,1)% finds coefficients for a first degree line
Y = polyval(coefficients, x)% use coefficients from above to estimate a new set of y values
plot(x,y,'*',x,Y,':')% plot the original data with * and the estimated line with --