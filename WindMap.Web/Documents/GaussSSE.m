function y = GaussSSE(x)

global inputC
global wd
global NN
global INTA
global Iter

% for g=1:NN
%     INTA(g) = Gauss_inside([x(1) x(2) x(3) wd(g)]); 
%     SE(g) = (INTA(g)-inputC(g))^2;
% end 
%  
% %define SSE
%  y=sum(SE);
% 
% function yy = Gauss_inside(xx)
%     yy = xx(1)/(sqrt(2*pi)*xx(2))*exp(-.5*((xx(3)-xx(4))^2)/(xx(2))^2);
for g=1:NN
    %INTA(g) = Gauss_inside([x(1) x(2) x(3) wd(g)]);
    INTA(g) = x(1)/(sqrt(2*pi)*x(2))*exp(-.5*((x(3)-wd(g))^2)/(x(2))^2); 
    SE(g) = (INTA(g)-inputC(g))^2;

end 
 
Iter=Iter+1;

%define SSE
y=sum(SE);
