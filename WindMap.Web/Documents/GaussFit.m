clear all

NN=6;
% for i=1:NN 
%   fix=randn;  
%   wd1(i)=(fix+5)*10;
% end
% 
% wd1=sort(wd1);
% mean1=mean(wd1)*(1+0.2*randn);
% std1=std(wd1)*(1+0.3*randn);
% A1=10*(1+0.2*randn);
% 
% for i=1:NN
%     fix1=randn;   
%     pc1(i)=A1/(sqrt(2*pi)*std1)*exp(-.5*((mean1-wd1(i))^2)/(std1)^2)*(1+0.4*fix1);
% end;

global inputC
global wd
global NN
global INTA

global Iter
Iter = 0
%60
% wd1 = [20.9655 21.7087 22.0723 22.3847 22.641 23.1175];
% pc1 = [58.4901 69.5809 67.222 77.83952 78.66343 99.37954];

%260
% wd1 = [20.7104 21.4541 21.8235 22.1423 22.5134 23.792];
% pc1 = [55.3839 60.5589 66.222 69.2068 75.3131 88.6486];

%360
% wd1 = [20.8854 21.6911, 22.1455 22.7193 26.2414 27.7046];
% pc1 = [55.9255 64.4963 69.6513 79.493 90.2812 84.1145];

%460
wd1 = [21.0183 21.8781 22.5056 25.6237 27.1744 27.9542];
pc1 = [57.2286 66.3427 76.0791 92.3195 87.5349 79.2924];

inputC=pc1;
wd=wd1;

%ss=simple(ss)
%ss=strrep(ss,'Re','real');

lb=[0,0,0,0];
ub=[10000,100,6, 240];

options1=optimset('MaxFunEval',2000,'TolX',1e-8,'TolFun',1e-8,'MaxIter',500)%,'LargeScale','off')
[x]=fminsearch('GaussSSE',[sum(pc1),10,50],options1)


for i=(round(x(3))-30):(round(x(3))+30)
    wd2(i)=i;
    fitted(i)=x(1)/(sqrt(2*pi)*x(2))*exp(-.5*((x(3)-i)^2)/(x(2))^2);
end


figure
scatter(wd1,pc1)%plot points
hold
plot(wd2,fitted,'r.')%plot Gaussian fit

%print results----------------
    %DistanceCoeff=std1/std(wd1)
    x
    A = x(1)
    sigma = x(2)
    mu = x(3)
   

