﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace WindMap.Web.Extensions
{
    public class SensorHub : Hub
    {
        public async Task Send(string message)
        {
            await Clients.All.InvokeAsync("sensors", message);
        }
        public async Task SendWindDirection(string message)
        {
            await Clients.All.InvokeAsync("windDirection", message);
        }
        public async Task SendLeak(string message)
        {
            await Clients.All.InvokeAsync("leak", message);
        }
    }
}