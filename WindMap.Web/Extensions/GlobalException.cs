﻿using System;
using WindMap.Services.Models;
using WindMap.Web.Enums;

namespace WindMap.Web.Extensions
{
    public class GlobalException : Exception
    {
        public HttpCodes HttpCode { get; }

        public ErrorModel ErrorModel { get; set; }

        public GlobalException(string message, HttpCodes httpCode) : base(message)
        {
            HttpCode = httpCode;
            ErrorModel = new ErrorModel { Message = message };
        }
    }
}
