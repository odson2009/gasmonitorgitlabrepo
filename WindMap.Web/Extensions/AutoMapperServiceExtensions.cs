﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using WindMap.Web.MappingProfiles;

namespace WindMap.Web.Extensions
{
    internal static class AutoMapperServiceExtensions
    {
        public static void AddConfigureMapper(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(configure =>
            {
                configure.AddProfile(new ViewModelToDomain());
                configure.AddProfile(new DomaineToViewModel());
                configure.AddProfile(new DomaineToDomaine());
            });

            configuration.AssertConfigurationIsValid();

            services.AddSingleton(configuration.CreateMapper());
            services.AddAutoMapper();
        }
    }
}
