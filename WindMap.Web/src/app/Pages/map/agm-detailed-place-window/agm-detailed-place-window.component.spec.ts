import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgmDetailedPlaceWindowComponent } from './agm-detailed-place-window.component';

describe('AgmDetailedPlaceWindowComponent', () => {
  let component: AgmDetailedPlaceWindowComponent;
  let fixture: ComponentFixture<AgmDetailedPlaceWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgmDetailedPlaceWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgmDetailedPlaceWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
