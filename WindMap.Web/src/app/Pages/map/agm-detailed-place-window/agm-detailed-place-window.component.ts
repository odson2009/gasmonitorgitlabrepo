import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'cagm-detailed-place-window',
    templateUrl: './agm-detailed-place-window.component.html',
    styleUrls: ['./agm-detailed-place-window.component.scss']
})
export class AgmDetailedPlaceWindowComponent {
    @Input() name;
    @Input() sensor;
    @Input() concentration;
    @Input() windSpeed;
    @Input() windDirection;
    @Input() maxRadiusConstant;
    @Input() newMaxPF;
    @Input() maxPFPoint;

    @Output() clickClose = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    closeWindow() {
        this.clickClose.emit();
    }
}
