import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { MapPage } from './map.page';

const routes: Routes = [
     {
        path: '',
        component: MapPage
    }, 
    {
        path: ':id',
        component: MapPage
    },
    {
        path: ':id/:date',
        component: MapPage
    },
    //{
    //    path: '',
    //    redirectTo: '0'
    //},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MapRoutingModule {}
