import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapShortStatisticComponent } from './map-short-statistic.component';

describe('MapShortStatisticComponent', () => {
  let component: MapShortStatisticComponent;
  let fixture: ComponentFixture<MapShortStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapShortStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapShortStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
