import { Component, Input } from '@angular/core';

@Component({
  selector: 'map-short-statistic',
  templateUrl: './map-short-statistic.component.html',
  styleUrls: ['./map-short-statistic.component.scss']
})
export class MapShortStatisticComponent {
  @Input() startDate;
  @Input() endDate;
  @Input() minDate;
  @Input() monitors;
  @Input() zoom;
}
