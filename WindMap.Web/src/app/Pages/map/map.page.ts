import { Component, OnDestroy, OnInit, ViewChild, ElementRef, NgZone, Inject} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MapsAPILoader, AgmMap, AgmPolygon } from '@agm/core';
import { } from 'googlemaps';
import { HubConnection } from '@aspnet/signalr-client';
import { SensorService } from '../../services/sensor.service';
import { CalculationService } from '../../Services/calculation.service';
import { SensorModel, SensorRTUpdModel, LeakModel, Point } from '../../Models/sensor.model';
import { RegressionParameter, RegressionResult } from '../../Models/regression.model';
import { RegionModel} from '../../Models/region.model';
import { environment } from '../../../environments/environment';
import { RegionService } from '../../Services/region.service';
import { MapService } from './map.service';
import { PermissionService } from '../../Services/permission.service';
import { AlgorithmService } from '../../Services/algorithm.service';
import { DOCUMENT } from '@angular/platform-browser';
//import { setTimeout } from 'timers';
//import { google } from '@agm/core/services/google-maps-types';

@Component({
    selector: 'map-page',
    templateUrl: './map.page.html',
    styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit, OnDestroy {
    @ViewChild(AgmMap) map: any;
    polygonsObjects: any = [];
    polygons: any = [];

    // temp data for testing
    selectedRegion: any;
    selectedSensor: any;

    private _hubConnection: HubConnection;

    // autocomplete control
    @ViewChild('search')
    searchElementRef: ElementRef;

    // initial map parameters
    place: any;
    latitude = 29.724581592791687;
    longitude = -95.2080208307018;
    mapZoom = 17;
    minZoom = 1; // 14;
    maxZoom = 22; // 22;
    mapType = 'satellite';  //satellite
    leakSensorId: number = 0;
    activeInfoWindow: boolean = false;
    
    sensorLatlng: any;
    latlngBounds: any;
    sensorIds: Array<number>;
    visibleIds: string; // TODO:remove

    sensorLeak: boolean; // if true we get 1 result per sensor

    clusterStyles =
        [
            { url: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m1.png', textColor: '#fff', textSize: '14', height: '50', width: '50', backgroundPosition: 'center center' },
            { url: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m2.png', textColor: '#fff', textSize: '14', height: '50', width: '50', backgroundPosition: 'center center' },
            { url: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m3.png', textColor: '#fff', textSize: '14', height: '50', width: '50', backgroundPosition: 'center center' },
            { url: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m4.png', textColor: '#fff', textSize: '14', height: '50', width: '50', backgroundPosition: 'center center' },
            { url: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m5.png', textColor: '#fff', textSize: '14', height: '50', width: '50', backgroundPosition: 'center center' }
        ];

    private lastRecievedCounter = 0;

    isLoaded = false;
    regions: Array<RegionModel>;
    currentRegion: RegionModel;
    leaks: { [key: number]: LeakModel } = {};
    isInfoWindowOpened = false;
    isShowRegions = true;
    isShowUnits = false;

    private regionId: number;
    private regionMinDate: Date;
    // calculation constants
    regressionParameters: RegressionParameter[];
    linearRegression: RegressionResult;
    windDirect = 0;
    simulation: any;
    activeSensor: SensorModel = null;
    activeEllipse: any = null;
    cachedSensors: Array<SensorModel> = null; // prepared sensors for real-time updating

    // for Test panel
    isAdministrator: boolean = true;
    extendViewMode: number = 0;

    getCircleRadius(regionId: number) {
        if (regionId) {
            const region = this.regions.find((region: RegionModel) => region.id === regionId);
            const r = this.regions[0] && this.regions[0].algorithm && this.regions[0].algorithm.detectionPointFlux;

            return r * this.mapService.PASHAS_COSTANT || 0;
        }
        return 0;
    }
    constructor( @Inject(DOCUMENT) private document: any,
        private calculationService: CalculationService,
        private regionService: RegionService,
        private sensorService: SensorService,
        private mapsAPILoader: MapsAPILoader,
        private algorithmService: AlgorithmService,
        private ngZone: NgZone,
        private route: ActivatedRoute,
        public mapService: MapService,
        private permissionService: PermissionService
    ) { }

    bindPolygons(coordinates: any) {
        const path = [];
        coordinates.forEach(c => path.push({ lat: c.latitude, lng: c.longitude }));
        this.polygonsObjects.push({
            paths: path,
            strokeColor: '#018801',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#018801',
            fillOpacity: 0.2,
            editable: false,
        });
    }

    showHideRegions(isShowRegions: boolean) {
        if (this.polygonsObjects.length > 0) {
            if (!isShowRegions) {
                this.polygons.forEach(polygon => polygon.setMap(null));
                this.polygons = [];
            } else {
                this.polygonsObjects.forEach(polygonObj => this.map._mapsWrapper.createPolygon(polygonObj).then(polygon => {
                    this.polygons.push(polygon);

                    // extend region
                    polygonObj.paths.forEach(p => {
                        this.latlngBounds.extend(new google.maps.LatLng(p.lat, p.lng));
                    });
                }));
            }
        }
    }

    isFirstTimeOpened: boolean = true;

    showHideInfoWindow(isShow: boolean, sensor: SensorModel) {  
        const self = this;

        if (!isShow) {
            setTimeout(() => { sensor.isTolTipVisible = isShow; }, 500);
        }
        else {
            sensor.isTolTipVisible = isShow;

            setTimeout(function () { self.removeCloseButtons(); }, self.isFirstTimeOpened ? 60 : 10);
            self.isFirstTimeOpened = false;
        }

    }

    // show green circle, when data starts coming
    initDefaultSensorsStates() {
        if(this.currentRegion)
        {
            this.mapService.LEAK_SENSORS.find(p => p.regionId == this.currentRegion.id).sensors.forEach((s) => {
                let sensorRadiuse = this.mapService.LEAK_SENSORS.find(p => p.regionId == this.currentRegion.id).radiuses.find(r => r.monitorId == s.monitorId).value; //this.getSensorRadiuse(s.id, s.monitorId);
                let minPf = s.minPf;
                let maxPf = sensorRadiuse;
                let minTWD = s.minTWD;
                let maxTWD = s.maxTWD;
    
                let leakTestData: LeakModel = this.regionId == 4 ? { sensorId: s.id, status: 0, status2: 0, maxPfRadiusConstant: sensorRadiuse, maxPintPF: 0, newMaxPintPF: 0, averageData: [] }
                    : { sensorId: s.id, status: 0, status2: 0, maxPfRadiusConstant: sensorRadiuse, maxPintPF: 0, newMaxPintPF: 0, averageData: [] };
    
                this.leaks[s.id] = leakTestData;
            });
        }
    }

    getConstantRadiuse(regionId, monitorId) {
        var radiuses = this.mapService.LEAK_SENSORS.find(p => p.regionId == regionId).radiuses;
        return radiuses.find(p => p.monitorId == monitorId).value;
    }

    changedSensorRadiuse(value, sensorId) {
        //console.log('value = ', value);
        this.leaks[sensorId].newMaxPintPF = value
    }

    getSensorRadiuse(sensorId: number, monitorId: number) {
        var distancesArr = [];

        this.currentRegion.sensors.forEach((sensor) => {
            if (sensor.id == sensorId) {
                if (sensor.monitorIdToLeft !== "0" && sensor.distanceLeft > 0) {
                    distancesArr.push(sensor.distanceLeft);
                }
                if (sensor.monitorIdToRight !== "0" && sensor.distanceRight > 0) {
                    distancesArr.push(sensor.distanceRight);
                }
            }
        });
        console.log('distancesArr >>> sensorId = ' + monitorId, distancesArr);
        return distancesArr.length ? Number((Math.min(...distancesArr) / 2).toFixed(4)) : 0;
    }

    changedRole(value) {
        console.log(this.isAdministrator);
    }

    changedViewMode(value) {
        //console.log(value);
        var isNewBounds = false;

        if (value == 0) {
            // extend region
            this.latlngBounds = new google.maps.LatLngBounds();
            this.currentRegion.coordinates.forEach(p => {
                this.latlngBounds.extend(new google.maps.LatLng(p.latitude, p.longitude));
            });
        }
        else if (value == 1) {
            // sensors
            this.latlngBounds = new google.maps.LatLngBounds();
            this.currentRegion.sensors.forEach(p => {
                this.latlngBounds.extend(new google.maps.LatLng(p.latitude, p.longitude));
            });
        }
        else if (value == 2) {
            // leaks sensors
            //console.log(this.leaks);           
            this.currentRegion.sensors.forEach((s) => {
                var leakSensor = this.currentRegion.sensors.find(p => p.id == this.leaks[s.id].sensorId); 
                if (leakSensor && this.leaks[s.id].status > 0) {

                    if (!isNewBounds) {
                        this.latlngBounds = new google.maps.LatLngBounds();
                        isNewBounds = true;
                    }
                  
                    this.latlngBounds.extend(new google.maps.LatLng(leakSensor.latitude, leakSensor.longitude));
                }
            });

            // region ellipse
            if (this.center.latLng && this.pointOnEllipse.latLng && this.activeEllipse) {
                this.latlngBounds.extend(this.center.latLng);
                this.latlngBounds.extend(this.pointOnEllipse.latLng);
            }  
        }
    }

    getBoobleCoordinates(sensorId: number) {
        if (!this.currentRegion)
            return 0;

        var sensor = this.currentRegion.sensors.find(p => p.id == sensorId);
        if (!sensor)
            return 0;

        var distance = (this.leaks[sensorId] ? this.leaks[sensorId].maxPfRadiusConstant : 90) / 1000;
        return this.calculationService.getVectorCoordinates(sensor.latitude, sensor.longitude, distance, 90);
    }

    removeCloseButtons(){
        var elements = this.document.getElementsByClassName('gm-style-iw');
        for(let i = 0; i < elements.length; i++)
        {
            var button = elements[i].parentElement.childNodes[2];

            if (button)
                button.remove();
        }
    }

    // Lifecycle methods
    ngOnInit() {

        //only for test
        //let testData: LeakModel = { sensorId: 1, status: 3, maxPfRadiusConstant: 100, maxPintPF: 909.0001, averageData: [] };
        //this.leaks[1] = testData;

        this.isAdministrator = this.permissionService.getRole() == 'Administrator';

        this.route.params.subscribe(params => {
            this.regionId = + params['id'];
            if (params['date']) {
                this.regionMinDate = new Date(params['date']);
            }
        });

        // load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ['address']
            });

            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    // get the place result
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();

                    // verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }

                    // set latitude, longitude and zoom
                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                    this.mapZoom = 17;

                    this.place = place;
                });
            });

            // load sensors with results
            this.loadSensors(true).then(() => {
                this.latlngBounds = new google.maps.LatLngBounds();

                // build LatLng
                if (this.regions && this.regions[0]) {
                    for (let c = 0; c < this.regions.length; c++) {

                        this.bindPolygons(this.regions[c].coordinates);

                        for (let s = 0; s < this.regions[c].sensors.length; s++) {

                            if (!this.regionId) {

                                // extend region sensor
                                this.latlngBounds.extend(
                                    new google.maps.LatLng(this.regions[c].sensors[s].latitude, this.regions[c].sensors[s].longitude)
                                );
                            } else if (this.regionId === this.regions[c].id) {
                                // extend region sensor
                                this.latlngBounds.extend(
                                    new google.maps.LatLng(this.regions[c].sensors[s].latitude, this.regions[c].sensors[s].longitude)
                                );
                            }

                            this.regions[c].sensors[s].latLng =
                                new google.maps.LatLng(this.regions[c].sensors[s].latitude, this.regions[c].sensors[s].longitude);
                        }
                    }
                }
                this.showHideRegions(this.isShowRegions);
                this.bindVisibleSensors();
                this.initDefaultSensorsStates();

                //draw ellipse
                //this.center.latLng = new google.maps.LatLng(this.regions[0].sensors[0].latitude, this.regions[0].sensors[0].longitude);
                //this.pointOnEllipse.latLng = new google.maps.LatLng(this.regions[0].sensors[1].latitude, this.regions[0].sensors[1].longitude);
            });
        });

        this.initHubConnection();
    }

    showTestEllipse() {
        const regionEllipse = this.mapService.ELLIPSES.find((r) => r.regionId === this.regionId);

        if (regionEllipse) {
            this.center.latLng = new google.maps.LatLng(regionEllipse.center_lat, regionEllipse.center_lng);
            this.pointOnEllipse.latLng = new google.maps.LatLng(regionEllipse.point_lat, regionEllipse.point_lng);

            this.drawEllipse();
        }
    }

    getCenterEllipse() : Point
    {
        if (this.startPoinEllipseLat && this.endPoinEllipse) {
            let distance = this.calculationService.getDistance(this.startPoinEllipseLat, this.startPoinEllipseLng,
                this.endPoinEllipse.latitude, this.endPoinEllipse.longitude);
            let azimuth = this.calculationService.angleFromCoordinateNormal(this.startPoinEllipseLat, this.startPoinEllipseLng,
                this.endPoinEllipse.latitude, this.endPoinEllipse.longitude);
            let coordinates = this.calculationService.getVectorCoordinates(this.startPoinEllipseLat, this.startPoinEllipseLng, distance / 1000 / 2, azimuth);
            return coordinates;
        }
        else return null;
    }

    regionSelected(regionId: number) {
        this.latlngBounds = new google.maps.LatLngBounds();
        this.regionId = regionId;
        // extend region
        this.currentRegion = this.regions.find(p => p.id == regionId);
        this.currentRegion.coordinates.forEach(p => {
            this.latlngBounds.extend(new google.maps.LatLng(p.latitude, p.longitude));
        });
    }

    ngOnDestroy() {
        if (this._hubConnection
            != null) {
            this._hubConnection.stop();
        }

        this.regions = [];
        this.sensorIds = [];
        this.sensorService.dispose();
    }

    // binding sensor results
    mapSensors(recievedData: any, isRecievedFromHub: boolean = false) {

        if (recievedData && recievedData.length > 0) {
            for (let regionIndex = 0; regionIndex < this.regions.length; regionIndex++) {
                for (let sensorIndex = 0; sensorIndex < this.regions[regionIndex].sensors.length; sensorIndex++) {
                    // Commented by Vitaliy when sensor real-itme date has been aplying
                    if (this.sensorLeak) {
                        if (!this.regions[regionIndex].sensors[sensorIndex].bufferSensorResults) {
                            this.regions[regionIndex].sensors[sensorIndex].bufferSensorResults = [];
                        }

                        if (recievedData[regionIndex].sensors[sensorIndex]
                            && recievedData[regionIndex].sensors[sensorIndex].sensorResults) {
                            for (let j = 0; j < recievedData[regionIndex].sensors[sensorIndex].sensorResults.length; j++) {
                                for (let resCnt = 0;
                                    resCnt < recievedData[regionIndex].sensors[sensorIndex].sensorResults.length; resCnt++) {
                                    this.regions[regionIndex].sensors[sensorIndex].bufferSensorResults
                                        .push(recievedData[regionIndex].sensors[sensorIndex].sensorResults[resCnt]);

                                    console.log(this.regions[regionIndex].sensors[sensorIndex].bufferSensorResults.length + '===' + this.regions[regionIndex].algorithm.numberLineGroupTime +
                                        ' getResultsToReceiveCount= ' + this.regions[regionIndex].algorithm.getResultsToReceiveCount);

                                    // check NUMBER_OF_LINE_GROUPING_TIME and SOURCE_LOCATION_UP_DATE_INTERVAL
                                    if (this.regions[regionIndex].sensors[sensorIndex].bufferSensorResults.length === this.regions[regionIndex].algorithm.numberLineGroupTime) {

                                        console.log('|*************do the MOVING_Average_METHOD for the groupe of results');

                                        this.regions[regionIndex].sensors[sensorIndex].bufferSensorResults = [];
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    // algorithm for PRSI region and all other
    updMapSensorsRealtime(recievedData: SensorRTUpdModel) {// Received data by one sensor
        // - 180 degrees
        //if (recievedData.sensorId == 7)
        //    recievedData.windDirection = Number((recievedData.windDirection - 160).toFixed(4));
        //else
        //    recievedData.windDirection = Number((recievedData.windDirection - 175).toFixed(4));

        const cacheSensors = (regions: Array<RegionModel> = []) => {
            return regions.reduce((prev: Array<SensorModel>, cur: RegionModel) => prev.concat(cur.sensors), []);
        };
        // Validation�
        if (!recievedData) {
            return;
        }
        // Preparation sensors
        if (!this.cachedSensors) {
            this.cachedSensors = cacheSensors(this.regions);
        }

        const sensorForUpd = this.cachedSensors.find((sensor: SensorModel) => sensor.id === recievedData.sensorId);

        if (sensorForUpd) {
            sensorForUpd.averageResult.concentration = recievedData.concentration;
            sensorForUpd.averageResult.windDirection = recievedData.windDirection;
            sensorForUpd.averageResult.windSpeed = recievedData.windSpeed;

            if (!sensorForUpd.vecEndPoints)
                sensorForUpd.vecEndPoints = [];

            if (this.leaks)
                var isLeak = this.leaks[recievedData.sensorId].status > 0;

            if (isLeak) {
                // scale all sensors points by new PF maximum
                var pf_radiuse = this.leaks[sensorForUpd.id].maxPfRadiusConstant;
                var incoming_pf = recievedData.concentration * recievedData.windSpeed;
                sensorForUpd.scale = this.cachedSensors.find((sensor: SensorModel) => sensor.id === recievedData.sensorId).scale;

                if (incoming_pf > pf_radiuse) {
                    //this.leaks[sensorForUpd.id].newMaxPintPF = Number(incoming_pf.toFixed(4));

                    var downScale = pf_radiuse / incoming_pf;
                    if (downScale < sensorForUpd.scale) {
                        this.cachedSensors.find((sensor: SensorModel) => sensor.id === recievedData.sensorId).scale = pf_radiuse / incoming_pf;
                        sensorForUpd.scale = downScale;
                    }
                }

                sensorForUpd.vecEndPoints.forEach((element) => {
                    element.isNew = false;
                    if (incoming_pf > pf_radiuse) {
                        element.point = this.calculationService.getVectorCoordinates(sensorForUpd.latitude, sensorForUpd.longitude,
                            element.pf * sensorForUpd.scale / 1000, element.wd);
                    }
                });

                var pointLocation = this.calculationService.getVectorCoordinates(sensorForUpd.latitude, sensorForUpd.longitude,
                    incoming_pf * sensorForUpd.scale / 1000, recievedData.windDirection);

                sensorForUpd.vecEndPoints.push({ point: pointLocation, pf: incoming_pf, wd: recievedData.windDirection, isNew: true });

                //separate behavoir for two sensors
                if (recievedData.sensorId === 11) {
                    if (isLeak && sensorForUpd.vecEndPoints.length === 4) {
                        //draw ellipse
                        var firstWDAverage = 0;
                        sensorForUpd.vecEndPoints.forEach((point) => {
                            firstWDAverage += point.wd;
                        });

                        this.azimuth = firstWDAverage / sensorForUpd.vecEndPoints.length;

                        var startPoint = this.calculationService.getVectorCoordinates(sensorForUpd.latitude, sensorForUpd.longitude,
                            this.distanceConst / 1000, this.azimuth);

                        var endPoint = this.calculationService.getVectorCoordinates(sensorForUpd.latitude, sensorForUpd.longitude,
                            this.distanceConst * 10 / 1000, this.azimuth);

                        this.center.latLng = new google.maps.LatLng(startPoint.latitude, startPoint.longitude);
                        this.pointOnEllipse.latLng = new google.maps.LatLng(endPoint.latitude, endPoint.longitude);

                        this.drawEllipse();
                    }
                    else if (sensorForUpd.vecEndPoints.length > 4) {
                        this.updateLeakEllipse(false);
                    }

                    if (sensorForUpd.vecEndPoints.length === 5) {
                        // extend map with leak sensors and ellipse
                        this.activeEllipse = true;
                        this.changedViewMode(2);
                    }
                }
                else if (recievedData.sensorId === 7 && this.leaks[recievedData.sensorId].status > 0 && !this.isRotated) {
                    this.updateLeakEllipse(true);
                }
                else if ((recievedData.sensorId === 12 || recievedData.sensorId === 13) && sensorForUpd.vecEndPoints.length === 2) {
                    this.activeEllipse = true;
                    this.changedViewMode(1);
                }
            }
        }
    }

    // algorithm for Texas only region
    updMapTexasSensorsRealtime(recievedData: SensorRTUpdModel) {// Received data by one sensor
        const cacheSensors = (regions: Array<RegionModel> = []) => {
            return regions.reduce((prev: Array<SensorModel>, cur: RegionModel) => prev.concat(cur.sensors), []);
        };
        // Validation�
        if (!recievedData) {
            return;
        }
        // Preparation sensors
        if (!this.cachedSensors) {
            this.cachedSensors = cacheSensors(this.regions);
        }

        const sensorForUpd = this.cachedSensors.find((sensor: SensorModel) => sensor.id === recievedData.sensorId);

        if (sensorForUpd) {
            sensorForUpd.averageResult.concentration = recievedData.concentration;
            sensorForUpd.averageResult.windDirection = recievedData.windDirection;
            sensorForUpd.averageResult.windSpeed = recievedData.windSpeed;
        }

        if (sensorForUpd && recievedData.concentration > 0 && this.leaks[recievedData.sensorId] && this.leaks[recievedData.sensorId].status2 > 0) {
        //if (sensorForUpd && this.leaks[recievedData.sensorId].status > 0) {
            if (!sensorForUpd.vecEndPoints)
                sensorForUpd.vecEndPoints = [];

            // scale all sensors points by new PF maximum
            var pf_radiuse = this.leaks[sensorForUpd.id].maxPfRadiusConstant;
            var incoming_pf = recievedData.concentration * recievedData.windSpeed;
            sensorForUpd.scale = this.cachedSensors.find((sensor: SensorModel) => sensor.id === recievedData.sensorId).scale;

            // update Scale
            var newScale = pf_radiuse / incoming_pf;
            if (newScale != sensorForUpd.scale) {

                sensorForUpd.vecEndPoints.forEach((element) => {
                    element.isNew = false;
                    if (newScale < sensorForUpd.scale) {

                        if (element.pf < incoming_pf) {
                            element.point = this.calculationService.getVectorCoordinates(sensorForUpd.latitude, sensorForUpd.longitude,
                                element.pf * newScale / 1000, element.wd);
                        }
                    }
                });

                if (newScale < sensorForUpd.scale || sensorForUpd.scale == 1) {
                    this.cachedSensors.find((sensor: SensorModel) => sensor.id === recievedData.sensorId).scale = newScale;
                    sensorForUpd.scale = newScale;
                }
            }

            var pointLocation = this.calculationService.getVectorCoordinates(sensorForUpd.latitude, sensorForUpd.longitude,
                incoming_pf * sensorForUpd.scale / 1000, recievedData.windDirection);

            sensorForUpd.vecEndPoints.push({ point: pointLocation, pf: incoming_pf, wd: recievedData.windDirection, isNew: true });
       }
    }

    distanceConst: number = 67;
    isRotated: boolean = false;
    azimuth: number = 0;
    distance_start: number = 0;
    distance_end: number = 0;
    zoomCoefficient: number = 0.0005;
    startLatLng: any;
    middleLatLng: any;
    
    startPoinEllipseLat: any;
    startPoinEllipseLng: any;
    endPoinEllipse: any;

    updateLeakEllipse(needToRotate) {
        if (!this.center.latLng) {
            return;
        }

        var firstPointLat = this.center.latLng.lat();
        var firstPointLng = this.center.latLng.lng();

        var secondPointLat = this.pointOnEllipse.latLng.lat();
        var secondPointLng = this.pointOnEllipse.latLng.lng();

        const firstSensor = this.cachedSensors.find((sensor: SensorModel) => sensor.id === 11);

        if (needToRotate && !this.isRotated) {
            this.isRotated = true;
            this.middleLatLng = new google.maps.LatLng(29.726581044573592, -95.20970796435131);
            var intersectionLatLng = new google.maps.LatLng(29.724245570432814, -95.2091205363979);
            this.azimuth = this.calculationService.angleFromCoordinateNormal(this.middleLatLng.lat(), this.middleLatLng.lng(),
                intersectionLatLng.lat(), intersectionLatLng.lng());
        }
        
        var startLat = this.middleLatLng ? this.middleLatLng.lat() : firstSensor.latitude;
        var startLng = this.middleLatLng ? this.middleLatLng.lng() : firstSensor.longitude;

        // for culculate center ellipse
        this.startPoinEllipseLat = startLat; 
        this.startPoinEllipseLng = startLng;

        var firstWDAverage = 0;
        firstSensor.vecEndPoints.forEach((point) => {
            firstWDAverage += point.wd;
        });

        this.distance_start = this.calculationService.getDistance(startLat, startLng, firstPointLat, firstPointLng);
        this.distance_end = this.calculationService.getDistance(startLat, startLng, secondPointLat, secondPointLng);

        if (!this.isRotated)
            this.azimuth = firstWDAverage / firstSensor.vecEndPoints.length;

        if (this.distance_end - this.distance_start < 100) {
             return;
        }

        var startPoint = this.calculationService.getVectorCoordinates(startLat, startLng,
            this.distance_start / 1000 + this.zoomCoefficient, this.azimuth);
        this.center.latLng = new google.maps.LatLng(startPoint.latitude, startPoint.longitude);

        var endPoint = this.calculationService.getVectorCoordinates(startLat, startLng,
            (this.distance_end / 1000) - (this.zoomCoefficient * 4.00), this.azimuth);
        this.pointOnEllipse.latLng = new google.maps.LatLng(endPoint.latitude, endPoint.longitude);

        this.points = this.calculationService.getEllipsePoints(this.center.latLng, this.pointOnEllipse.latLng, 100);
        
        // for culculate center ellipse
        this.endPoinEllipse = endPoint;
    }

    leakColorsList = [];
    getEllipseColor() {
        
        if (this.isRotated) {
            var secondSensor = this.cachedSensors.find((sensor: SensorModel) => sensor.id === 7);

            if (secondSensor.vecEndPoints.length > 3) {
                return "Red";
            }
        }

        if (!this.center.latLng)
            return this.mapService.CIRCLES_STYLES[this.mapService.CIRCLES_STYLES.length - 1].fillColor;

        var radiuse = this.calculationService.getDistance(this.center.latLng.lat(), this.center.latLng.lng(),
            this.pointOnEllipse.latLng.lat(), this.pointOnEllipse.latLng.lng());

        //change ellipse color
        if (this.leakColorsList.length === 0) {
            var step = (radiuse + 200) / this.mapService.CIRCLES_STYLES.length;

            //Sets ranges for leak power status
            for (var i = 0; i < this.mapService.CIRCLES_STYLES.length; i++) {
                this.leakColorsList.push(
                    {
                        LeakColor: this.mapService.CIRCLES_STYLES[this.mapService.CIRCLES_STYLES.length - (i+1)].fillColor,
                        Min: i * step,
                        Max: (i + 1) * step
                    });
            }
        }
        var entry = this.leakColorsList.find(p => p.Min < radiuse && p.Max > radiuse);
        var leakColor = entry != null ? entry.LeakColor : this.mapService.CIRCLES_STYLES[0].fillColor;

        //return leakColor; 
        return leakColor === 'red' ? 'orange' : leakColor;
    }

    setActiveSensorByData(sensorId: number) {
        var sensor = this.currentRegion.sensors.find(p => p.id == sensorId);
        if (sensor) {
            sensor.isActive = true;
        }
    }

    initHubConnection() {
        this._hubConnection = new HubConnection(environment.hubUrl);
        this._hubConnection.on('sensors', (data: SensorRTUpdModel) => {
            const recievedData = data;
            
            this.setActiveSensorByData(data.sensorId);
            this.lastRecievedCounter = 0;

            // Using only for Texas region with specific algorithm
            if (this.regionId === 4) {
                this.updMapTexasSensorsRealtime(recievedData);

                var newPF = recievedData.windSpeed * recievedData.concentration;
                if (newPF > this.leaks[data.sensorId].newMaxPintPF) {
                    this.leaks[data.sensorId].newMaxPintPF = newPF;
                }

                this.leaks[data.sensorId].maxPintPF = newPF;
                this.leaks[data.sensorId].maxPintPF = recievedData.concentration * recievedData.windSpeed;
            }
            else {
                this.updMapSensorsRealtime(recievedData);

                if (this.leaks[data.sensorId] && recievedData.concentration === 0) {
                    this.leaks[data.sensorId].status2 = 0;
                    this.leaks[data.sensorId].maxPintPF = recievedData.concentration;
                    //this.activeEllipse = null;
                }      
            }

           
        });

        this._hubConnection.on('windDirection', (data: any) => {
            /**
             * Here avg was wind direction
             */
            this.windDirect = data.windDirection;
            this.setActiveSensorByData(data.sensorId);
        });

        this._hubConnection.on('leakEnd', (sensorId: number) => {
            this.leaks[sensorId].maxPintPF = 0;
            this.leaks[sensorId].status = 0;
            //this.leaks[sensorId].status2 = 0;
        })

        this._hubConnection.on('leak', (data: LeakModel) => {
            this.setActiveSensorByData(data.sensorId);

            if (this.leaks[data.sensorId]) {
                if(data.maxPintPF != 0)
                    this.leakSensorId = data.sensorId;

                // event logic for Texas region
                if (this.regionId === 4) {
                  
                    if (this.leaks[data.sensorId].status2 == 0) {
                        this.changedViewMode(1);

                        setTimeout(() => { this.mapZoom = 16; }, 50);
                    }

                    this.leaks[data.sensorId].status = 1;
                    this.leaks[data.sensorId].status2 = 2;
                }
                // event logic for PRSI region and other
                else {
                    if (this.leaks[data.sensorId].newMaxPintPF < data.maxPintPF) {
                        this.leaks[data.sensorId].newMaxPintPF = data.maxPintPF;

                        this.leaks[data.sensorId].status = this.leaks[data.sensorId].status < data.status ? data.status : this.leaks[data.sensorId].status;
                        this.leaks[data.sensorId].status2 = this.leaks[data.sensorId].status;

                        // extend map with leak sensors and ellipse
                        if (this.leaks[data.sensorId].status == 0 && data.status > 0) {
                            this.changedViewMode(2);
                        }
                    }
                    this.leaks[data.sensorId].maxPintPF = data.maxPintPF;
                }
            }
            // commited for first simple simulation - will be needs in the feature!!!!
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //this.leaks[data.sensorId] = data;
            // Vec endpoints
            //const sensorForUpd = this.cachedSensors.find((sensor: SensorModel) => sensor.id === data.sensorId);
            //sensorForUpd.vecEndPoints = data.averageData.map((avg: AvgLeakItem) => {
            //    // avg.pf need translate from m to km
            //    return this.calculationService.getVectorCoordinates(sensorForUpd.latitude, sensorForUpd.longitude, avg.pf / 1000, avg.twd);
            //});
        });

        this._hubConnection.start()
            .then(() => {
                console.log('Hub connection started');
            })
            .catch(err => {
                console.log('Error while establishing connection', err);
            });

        this.isLoaded = true;
    }

    onBoundsChanged(bounds: any) {
        this.sensorLatlng = bounds;
        this.bindVisibleSensors();
    }

    points: any = [];
    center: SensorModel = new SensorModel();
    pointOnEllipse: SensorModel = new SensorModel();
    drawEllipse(event?: any, point?: string) {
        let e = event;
        let p = point;

        if (p && e) {
            if (p === 'point1')
                this.center.latLng = new google.maps.LatLng(e.coords.lat, e.coords.lng);
            else if (p === 'point2') {
                this.pointOnEllipse.latLng = new google.maps.LatLng(e.coords.lat, e.coords.lng);
            }
        }

        this.points = this.calculationService.getEllipsePoints(this.center.latLng, this.pointOnEllipse.latLng, 100);
    }

    bindVisibleSensors() {
        if (!this.sensorLatlng) {
            console.warn('this.latlngBounds is undefined');
        }

        if (this.regions && this.regions[0].sensors && this.regions[0].sensors.length > 0 && this.sensorLatlng) {
            //extend region sensor
            this.visibleIds = '';
            this.sensorIds = [];
            this.regions.forEach((region: any) => {

                region.sensors.forEach((sensor: any) => {
                    const isContains = this.sensorLatlng.contains(sensor.latLng);
                    sensor.isVisible = isContains;

                    if (isContains) {
                        this.visibleIds += (sensor.monitorId + ', ');
                        this.sensorIds.push(sensor.id);
                    }
                });
            });
        }
    }

    // load sensors results by period
    /**
     * 
     * @param isNeedToLoadSensor - what is the parametr?
     */
    loadSensors(isNeedToLoadSensor: boolean): Promise<void> {

        const handler = (result) => {
            if (isNeedToLoadSensor) {
                this.regions = result;
                if (this.regionId)
                    this.currentRegion = this.regions.find((region: RegionModel) => region.id === this.regionId);
            }
            else {
                this.mapSensors(result);
            }
        }

        const reqPromise = (this.regionId && this.regionId > 0) ?
            this.sensorService.getRegionByParameters(this.regionId) :
            this.regionService.getRegions();

        return reqPromise.then(handler);
    }

    clickedMarker(sensor: SensorModel) {
        //this.isInfoWindowOpened = false;
        sensor.iconUrl = sensor.iconUrl.replace(sensor.regionId + '.png', 'active.png'); 
        sensor.iconUrlDanger = sensor.iconUrlDanger.replace('danger.png', 'danger_active.png');
        sensor.iconUrlEmpty = sensor.iconUrlEmpty.replace('empty.png', 'empty_active.png');
        
        this.activeSensor = sensor;

        //change marker icon active/inactive
        this.currentRegion.sensors.forEach((regionSensor: any) => {

            if (regionSensor.id != sensor.id) {
                regionSensor.iconUrl = regionSensor.iconUrl.replace('active.png', sensor.regionId + '.png');
                regionSensor.iconUrlDanger = regionSensor.iconUrlDanger.replace('danger_active.png', 'danger.png');
                regionSensor.iconUrlEmpty = regionSensor.iconUrlEmpty.replace('empty_active.png', 'empty.png');
            }
        });
    }

    infoWindowClosed(label: string, index: number) {
        //console.log(`infoWindowClosed: ${label || index}`)//TODO:remove
    }

    mapClicked($event: MouseEvent) {
        console.log($event)//TODO:remove
    }

    zoomChanged($event: any) {
        this.map._mapsWrapper._map.tilt = 0;
        this.mapZoom = $event;
        this.map._mapsWrapper.setMapOptions({tilt:0});

        //redraw all visible arrows
        if (this.regions) {
            this.regions.forEach((region: any) => {
                if (region.sensors) {
                    region.sensors.forEach((sensor: any) => {

                        if (sensor.pair && this.sensorLatlng && this.sensorLatlng.contains(sensor.pair.latLng) && sensor.arrow) {
                            sensor.arrow = this.calculationService.drawArrow(sensor.latitude, sensor.longitude, sensor.pair.latitude, sensor.pair.longitude, this.mapZoom);
                        }
                    });
                }
            });
        }
    }

    ellipseClick(e) {
        this.activeEllipse = true;
    }

    switcherMode: boolean = false;

    getTwoWeeksConcentration() {
        this.switcherMode = !this.switcherMode;

        if (this.switcherMode) {     
            this.algorithmService.getConcentrationDuring14Days(this.regionId).then((data) => {
                console.log(data);

                data.forEach((element: any) => {
                    var sensor = this.currentRegion.sensors.find(p => p.id == element.sensorId);
                    if (sensor) {
                        sensor.twoWeeksConcentration = element.aveargeConcentration.toFixed(1);
                    }
                });


            });
        }
    }
}
