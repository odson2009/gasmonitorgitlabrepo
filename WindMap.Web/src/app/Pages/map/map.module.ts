import { NgModule } from '@angular/core';
import { MapPage } from './map.page';
import { SharedModule } from '../../Modules/shared.module';
import { MapRoutingModule } from './map.routing';
import { AgmDetailedPlaceWindowComponent } from './agm-detailed-place-window/agm-detailed-place-window.component';
import { EmissionRateWindowComponent } from './emission-rate-window/emission-rate-window.component';
import { MapShortStatisticComponent } from './map-short-statistic/map-short-statistic.component';
import { MapService } from './map.service';

@NgModule({
    declarations: [
        MapPage,
        AgmDetailedPlaceWindowComponent,
        EmissionRateWindowComponent,
        MapShortStatisticComponent
    ],
    imports: [
        SharedModule,
        MapRoutingModule,
    ],
    providers: [MapService]
})
export class MapModule {}