import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmissionRateWindowComponent } from './emission-rate-window.component';

describe('EmissionRateWindowComponent', () => {
    let component: EmissionRateWindowComponent;
    let fixture: ComponentFixture<EmissionRateWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [EmissionRateWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(EmissionRateWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
