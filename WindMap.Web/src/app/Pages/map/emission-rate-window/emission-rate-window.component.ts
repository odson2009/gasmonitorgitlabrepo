import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'emission-rate-window',
    templateUrl: './emission-rate-window.component.html',
    styleUrls: ['./emission-rate-window.component.scss']
})
export class EmissionRateWindowComponent {
    @Input() center;
    @Input() newMaxPF;
    @Input() maxPFPoint;
    @Output() clickClose = new EventEmitter();
    
    public get getEmissionRate() : number {
        return Number(((this.newMaxPF + this.maxPFPoint)/2/10).toFixed(0));
    }

    constructor() {}

    ngOnInit() { }

    closeWindow() {
        this.clickClose.emit();
    }
}
