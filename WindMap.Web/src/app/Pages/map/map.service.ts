import { Injectable } from '@angular/core';

@Injectable()
export class MapService {
    readonly PASHAS_COSTANT = 1; // special constant for circles from Pasha EXCLUSIVE!
    readonly POINTS_RADIUSES = [0.16, 0.16, 0.16, 0.16, 0.16, 0.16, 0.16, 0.16, 0.16, 0.16, 0.16, 0.16, 0.16,
        0.16, 0.16, 1.8, 3.75, 2.17, 1.66, 1.05, 0.7, 0.3, 0.2];
    readonly UNITS_COEFFICIENTS = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0.012, 0.006,
        0.003, 0.0015, 0.00075, 0.000375, 0.0001875, 0.00009375, 0.000046875, 0.0000234375];
    readonly UNIT_ICO = '/assets/images/units.png';
    readonly MAX_PF_RADIUS_CONSTANT = 200;

    readonly CIRCLES_STYLES = [
        { fillColor: 'violet', fillOpacity: 0.50, strokeColor: 'violet', strokeWeight: 2 },
        { fillColor: 'indigo', fillOpacity: 0.50, strokeColor: 'indigo', strokeWeight: 2 },
        { fillColor: 'blue', fillOpacity: 0.50, strokeColor: 'blue', strokeWeight: 2 },
        { fillColor: 'green', fillOpacity: 0.50, strokeColor: 'green', strokeWeight: 2 },
        { fillColor: 'yellow', fillOpacity: 0.50, strokeColor: 'yellow', strokeWeight: 2 },
        { fillColor: 'orange', fillOpacity: 0.50, strokeColor: 'orange', strokeWeight: 2 },
        { fillColor: 'red', fillOpacity: 0.50, strokeColor: 'red', strokeWeight: 2 },
    ];

    readonly ELLIPSES = [
        {
            regionId: 1, center_lat: 32.82188147526311, center_lng: 35.053932999935114,
            point_lat: 32.822925080513436, point_lng: 35.05348507102963
        },
        {
            regionId: 2, center_lat: 32.82727414540384, center_lng: 35.05012752768425,
            point_lat: 32.8267692740837, point_lng: 35.051854870289844
        },
        {
            regionId: 3, center_lat: 29.725671681831656, center_lng: -95.20990404893132,
            point_lat: 29.7214509819018, point_lng: -95.20798358727666
        },
    ];

    readonly LEAK_SENSORS = [
        {
            regionId: 1,
            radiuses: [{ monitorId: 1, value: 0 }, { monitorId: 2, value: 0 }, { monitorId: 12, value: 0 }],
            sensors: [
                {
                    id: 1,
                    //monitorId: 1001,
                    monitorId: 1,
                    minPf: 30,
                    maxPf: 200,
                    minTWD: 324,
                    maxTWD: 347,
                },
                {
                    id: 2,
                    // monitorId: 1002,
                    monitorId: 2,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 340,
                    maxTWD: 360,
                },
                {
                    id: 12,
                    // monitorId: 1003,
                    monitorId: 12,
                    minPf: 10,
                    maxPf: 200,
                    minTWD: 100,
                    maxTWD: 120,
                },
            ]
        },
        {
            regionId: 2,
            radiuses: [{ monitorId: 3, value: 0 }, { monitorId: 4, value: 0 }, { monitorId: 5, value: 0 }],
            sensors: [
                {
                    id: 3,
                    monitorId: 3,
                    minPf: 20,
                    maxPf: 200,
                    minTWD: 80,
                    maxTWD: 100,
                },
                {
                    id: 4,
                    monitorId: 4,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 160,
                    maxTWD: 170,
                },
                {
                    id: 5,
                    monitorId: 5,
                    minPf: 10,
                    maxPf: 200,
                    minTWD: 100,
                    maxTWD: 120,
                },
            ]
        },
        {
            regionId: 3,
            radiuses: [
                { monitorId: 3, value: 60 },
                { monitorId: 6, value: 60 },
                { monitorId: 2, value: 60 },
                { monitorId: 1, value: 60 },
                { monitorId: 4, value: 60 },
                { monitorId: 5, value: 60 }],
            sensors: [
                {
                    id: 6,
                    monitorId: 1,
                    minPf: 20,
                    maxPf: 200,
                    minTWD: 290,
                    maxTWD: 305,
                },
                {
                    id: 7,
                    monitorId: 2,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 330,
                    maxTWD: 350,
                },
                {
                    id: 8,
                    monitorId: 3,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 330,
                    maxTWD: 350,
                },
                {
                    id: 9,
                    monitorId: 4,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 330,
                    maxTWD: 350,
                },
                {
                    id: 10,
                    monitorId: 5,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 330,
                    maxTWD: 350,
                },
                {
                    id: 11,
                    monitorId: 6,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 330,
                    maxTWD: 350,
                },
            ]
        },
        {
            regionId: 4,
            radiuses: [{ monitorId: 12, value: 300 }, { monitorId: 13, value: 300 }],
            sensors: [
                {
                    id: 12,
                    //monitorId: 302,
                    monitorId: 12,
                    minPf: 30,
                    maxPf: 200,
                    minTWD: 324,
                    maxTWD: 347,
                },
                {
                    id: 13,
                    // monitorId: 303,
                    monitorId: 13,
                    minPf: 5,
                    maxPf: 200,
                    minTWD: 340,
                    maxTWD: 360,
                },
            ]
        },

    ];
}
