import { ApiMap } from '../interfaces/request.interface';

export const apiMap: ApiMap = {
    /**
     * Sign in
     * body: {
     *     login: string,
     *     password: string,
     *     rememberMe: string
     * }
     *
    */
    login: {
        url: 'api/auth',
        method: 'post'
    },
    logout: {
        url: 'api/auth/logout',
        method: 'post'
    },
    /**
     * refresh token
     * body: {
     *   refreshToken: <old token>
     * }
     */
    refreshToken: {
        url: 'api/auth/refresh-token',
        method: 'post',
    },
    /**
     * Get all regions
     */
    getRegions: {
        url: 'api/regions',
        method: 'get',
    },
    getRegionsCount: {
        url: 'api/regions',
        method: 'get'
    },
    getCompanies: {
        url: 'api/companies/list',
        method: 'get'
    },
    getCompany: {
        url: 'api/companies/<%= urlParams.id %>',
        method: 'get'
    },
    addCompany: {
        url: 'api/companies/create',
        method: 'post'
    },
    updateCompany: {
        url: 'api/companies/update',
        method: 'put'
    },
    deleteCompany: {
        url: 'api/companies/<%= urlParams.id %>',
        method: 'delete'
    }
};
