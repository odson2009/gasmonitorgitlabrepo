export const API_CFG_DEFAULTS = {
    data: {},
    disableErrorHandler: false,
    disableTokenCheck: false,
};
