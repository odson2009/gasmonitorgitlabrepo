//agm core
import { AgmCoreModule } from "@agm/core";
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { FileUploadModule } from 'ng2-file-upload';
import { ScrollToModule } from 'ng2-scroll-to';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { DateTimePickerModule } from '../components/dateTimePicker/dateTimePicker.module';
import { DefaultComponent } from '../components/default/default.component';
import { DeveloperComponent } from '../components/developer/developer.component';
import { HomeComponent } from '../components/home/home.component';
import { NavMenuComponent } from '../components/navmenu/navmenu.component';
import { RegionListComponent } from "../components/regions/region-list.component";
import { RegionActionComponent } from "../components/regions/region-action.component";
import { MonitorListComponent } from "../components/monitors/monitor-list.component";
import { EventsListComponent } from "../components/events/events-list.component";
import { MonitorActionComponent } from "../components/monitors/monitor-action.component";
import { TestsComponent } from "../components/tests/tests.component";
import { AlgorithmsComponent } from "../components/algorithms/algorithms.component";
import { FileInputComponent } from "../components/fileInput/fileInput.component";
import { ImagePreview } from "../Directives/image-preview.directive";
import { RegionsFilter, RegionsFilter2, FloatValuePipe } from "../Pipes/pipes";
import { LoginComponent } from "../components/login/login.component";
import { UserActionComponent } from "../components/users/user-action.component";
import { UserListComponent } from "../components/users/user-list.component";
import { CompanyListComponent } from "../components/companies/company-list.component";
import { CompanyActionComponent } from "../components/companies/company-action.component";
import { CompasComponent } from "../components/compas/compas.component";
import { ChangePasswordComponent } from "../components/changePassword/changePassword.component";

@NgModule({
    declarations: [
        NavMenuComponent,
        HomeComponent,
        DefaultComponent,
        ChangePasswordComponent,
        DeveloperComponent,
        RegionListComponent,
        RegionActionComponent,
        MonitorListComponent,
        EventsListComponent,
        MonitorActionComponent,
        TestsComponent,
        AlgorithmsComponent,
        FileInputComponent,
        ImagePreview,
        RegionsFilter,
        RegionsFilter2,
        FloatValuePipe,
        LoginComponent,
        UserActionComponent,
        UserListComponent,
        CompanyListComponent,
        CompanyActionComponent,
        CompasComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD4q1_gHTa5rVraKlIavQ9yw2SGRNYX2NU',
            libraries: ["places", "geometry"]
        }),
        AgmJsMarkerClustererModule,
        AmChartsModule,
        ToasterModule,
        SlimLoadingBarModule.forRoot(),
        DateTimePickerModule,
        ScrollToModule.forRoot(),
        FileUploadModule,
    ],
    exports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        AgmCoreModule,
        AgmJsMarkerClustererModule,
        AmChartsModule,
        ToasterModule,
        SlimLoadingBarModule,
        DateTimePickerModule,
        ScrollToModule,
        FileUploadModule,
        //components
        NavMenuComponent,
        HomeComponent,
        DefaultComponent,
        ChangePasswordComponent,
        DeveloperComponent,
        RegionListComponent,
        RegionActionComponent,
        MonitorListComponent,
        EventsListComponent,
        MonitorActionComponent,
        TestsComponent,
        AlgorithmsComponent,
        FileInputComponent,
        ImagePreview,
        RegionsFilter,
        RegionsFilter2,
        FloatValuePipe,
        LoginComponent,
        UserActionComponent,
        UserListComponent,
        CompanyListComponent,
        CompanyActionComponent,
        CompasComponent,

    ],
})
export class SharedModule {}