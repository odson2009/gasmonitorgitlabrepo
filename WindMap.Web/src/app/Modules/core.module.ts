import { NgModule, Optional, SkipSelf } from '@angular/core';
import { GoogleMapsAPIWrapper } from "@agm/core";
import { Globals } from '../app.globals';
import { ToasterService } from '../../../node_modules/angular2-toaster';
import { EventService } from '../Services/event.service';
import { RegionService } from '../Services/region.service';
import { SensorService } from '../services/sensor.service';
import { CalculationService } from '../Services/calculation.service';
import { SensorEventService } from '../Services/sensor-event.service';
import { GaussianFitService } from '../Services/gaussian-fit.service';
import { AlgorithmService } from '../Services/algorithm.service';
import { AuthGuard } from '../components/guards/auth.guard';
import { PermissionGuard } from '../components/guards/permission.guard';
import { AuthenticationService } from '../Services/authentication.service';
import { UserService } from '../Services/user.service';
import { LogoService } from '../Services/logo.service';
import { PermissionService } from '../Services/permission.service';
import { CompanyService } from '../Services/company.service';
import { EventBusService } from '../Services/event-bus.service';
import { DataService } from '../Services/data.service';
import { TokensService } from '../Services/tokens.service';

@NgModule({
    providers: [
        Globals,
        ToasterService,
        EventService,
        RegionService,
        SensorService,
        CalculationService,
        SensorEventService,
        GaussianFitService,
        AlgorithmService,
        AuthGuard,
        PermissionGuard,
        AuthenticationService,
        UserService,
        LogoService,
        PermissionService,
        CompanyService,
        GoogleMapsAPIWrapper,
        EventBusService,
        DataService,
        TokensService,
    ],
})
export class CoreModule {
    // This guards against the CoreModule from being imported into any
    // module except the root-module (AppModule).
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
    }
}