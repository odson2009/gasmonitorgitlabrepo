import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'floatValuePipe'
})
@Injectable()
export class FloatValuePipe implements PipeTransform {
    transform(value: any, toNumbers: number = 4,): number {
        return !isNaN(value) ? parseFloat(value.toFixed(toNumbers)) : value;
    }
}

@Pipe({
    name: 'regionFilter'
})
@Injectable()
export class RegionsFilter implements PipeTransform {

    transform(sensors: any, regionId: number): any {

        if (regionId < 0) {
            return sensors;
        }

        return sensors.filter(sensor => sensor.regionId == regionId);
    }
}

@Pipe({
    name: 'regionsFilter'
})
@Injectable()
export class RegionsFilter2 implements PipeTransform {

    transform(sensorEvents: any, regionId: number): any {

        if (regionId < 0) {
            return sensorEvents;
        }

        return sensorEvents.filter(sensorEvent => sensorEvent.regionId == regionId);
    }
}

