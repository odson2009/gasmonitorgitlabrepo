import { Component, Inject, OnDestroy, OnInit, NgZone, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { MapsAPILoader } from "@agm/core";
import { } from 'googlemaps';

import { SensorService } from "../../services/sensor.service";
import { SensorModel, SensorResultTmpModel, Main_Buffer, TMP, GroupTMP, AverageData, SensorDetailModel, TestResult, GroupAverageResult, GroupAverageResultSummary } from "../../Models/sensor.model";
import { RegressionParameter, RegressionResult } from "../../Models/regression.model";
import { CalculationService } from "../../Services/calculation.service";
import { Route, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpParams } from "@angular/common/http";
//chart
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import PolynomialRegression from 'ml-regression-polynomial';

//source: https://github.com/benfred/fmin
import * as fmin from 'fmin';
import { SensorEventModel } from "../../Models/sensorEvent.model";
import { SensorEventService } from "../../Services/sensor-event.service";

//timer panel
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { Observable } from "rxjs";
import 'rxjs/Rx';
import { HubConnection } from "@aspnet/signalr-client/dist/src";
import { environment } from "../../../environments/environment";


@Component({
    selector: 'tests',
    templateUrl: './tests.component.html',
    styleUrls: ['./tests.component.scss']
})

export class TestsComponent implements OnInit {
    private chart: AmChart;
    private chartLines: AmChart;

    private _hubConnection: HubConnection;
    private _renderer;
    @ViewChild("eventIcon_1001", { read: ElementRef }) eventIcon_1001: ElementRef;
    @ViewChild("eventIcon_1002", { read: ElementRef }) eventIcon_1002: ElementRef;

    private baseUrl: string;
    //initial map parameters
    mapType: string = 'roadmap'; // 'satellite' // 'terrain'
    latitude: number = 31.115276502286978;
    longitude: number = -85.37748098373413;
    mapZoom: number = 11;
    minZoom: number = 1;// 14;
    maxZoom: number = 22;// 22;

    center: SensorModel = new SensorModel();
    pointOnEllipse: SensorModel = new SensorModel();

    polygon: any;
    points: any = [];
    monitorTabIndex: number = 0;

    //complex buffer
    isToggled: boolean = true;
    isToggledPinPanel: boolean = false;

    //for all sensors results
    sensorResultsSource: SensorResultTmpModel[][] = [,];
    main_Buffers: Main_Buffer[] = [];
    newResultsCount: number = 60;//12;//5;
    groupsCount: number = 6;
    onsiteWindCenterAngle: number = 306.2295;
    NUMBER_OF_LINE_GROUPING_TIME: number = 6;
    minimumNumberDataForAverage: number = 60;
    minNewResultsCount: number = 12;
    clearChart: boolean = true;
    doTransformWind: boolean = false;
    sourceFromFile: boolean = false;
    isFminSearchMethod: boolean = true;
    isPolyfitMethod: boolean = true;
    isNegativeA: boolean = false;
    calculateWhenLeak: boolean = false;
    pinPanel: boolean = false;
    minI: number = -100;
    maxI: number = 100;
    downloadLink: string;
    skipCount: number = 0;

    //data for chart
    isBuildChart: boolean = false;
    isBuildIntersection: boolean = false;
    chartData: any = [];

    currentAnglesRange: any = null;
    anglesRanges: any = [
        { a1_1001: 22, a2_1001: 32, a1_1002: 30, a2_1002: 42, iterations: 18 }, //18
        { a1_1001: 25, a2_1001: 29, a1_1002: 33, a2_1002: 40, iterations: 22 }, //22
        { a1_1001: 26, a2_1001: 28, a1_1002: 35, a2_1002: 39, iterations: 16 }, //16
        { a1_1001: 26, a2_1001: 28, a1_1002: 36, a2_1002: 38, iterations: 3 },]; //3
    oneMoreFlag: number = 0;


    //test data for fminsearch and polyfit 
    isSmoothing: boolean = true;
    i: number = 1;
    steps: number;

    //linjear regression params
    lr_params: Array<RegressionParameter> = [
        { X_TWD: 1.47, Y_PF: 52.21 },
        { X_TWD: 1.50, Y_PF: 53.12 },
        { X_TWD: 1.52, Y_PF: 54.48 },
        { X_TWD: 1.55, Y_PF: 55.84 },
        { X_TWD: 1.57, Y_PF: 57.20 },
        { X_TWD: 1.60, Y_PF: 58.57 },
        { X_TWD: 1.63, Y_PF: 59.93 },
        { X_TWD: 1.65, Y_PF: 61.29 },
        { X_TWD: 1.68, Y_PF: 63.11 },
        { X_TWD: 1.70, Y_PF: 64.47 },
        { X_TWD: 1.73, Y_PF: 66.28 },
        { X_TWD: 1.75, Y_PF: 68.10 },
        { X_TWD: 1.78, Y_PF: 69.92 },
        { X_TWD: 1.80, Y_PF: 72.19 },
        { X_TWD: 1.83, Y_PF: 74.46 },
    ];

    lr_results: RegressionResult;

    //test for lines interseption
    sensors: any[] = [];
    centerObject: any;
    linesChartData: any = [];
    rayY: number = 800;

    //timer panel
    activeButton: number = 6;
    alive: boolean;
    interval: number;
    timerSubscription: any;
    timerSensorDataSubscription: any;

    timeStep: number = 1000;
    totalResultsMaxCount: number;
    stopStepNumber: number = 749;
    lastRecievedCounter: number = 1;
    maxDate: Date;
    currentDate: Date;

    circle: any;
    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        private calculationService: CalculationService,
        private sensorService: SensorService,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private route: ActivatedRoute,
        private amChartsService: AmChartsService,
        private renderer: Renderer2
    ) {
        this.baseUrl = baseUrl;
        this._renderer = renderer;
    }

    ngOnInit(): void {
        //for simulation mouving WD
        for (let i = 0; i < this.anglesRanges.length - 1; i++) {
            this.anglesRanges[i].step_a1_1001 = (this.anglesRanges[i + 1].a1_1001 - this.anglesRanges[i].a1_1001) / this.anglesRanges[i].iterations;
            this.anglesRanges[i].step_a2_1001 = (this.anglesRanges[i + 1].a2_1001 - this.anglesRanges[i].a2_1001) / this.anglesRanges[i].iterations;
            this.anglesRanges[i].step_a1_1002 = (this.anglesRanges[i + 1].a1_1002 - this.anglesRanges[i].a1_1002) / this.anglesRanges[i].iterations;
            this.anglesRanges[i].step_a2_1002 = (this.anglesRanges[i + 1].a2_1002 - this.anglesRanges[i].a2_1002) / this.anglesRanges[i].iterations;
        }

        //console.log(' this.anglesRanges', this.anglesRanges);

        //for Moving average method
        this.bindInitialResults();

        //init web socket connection
        this.initEventHubConnection();

        //for timer
        this.stopTimer();
    }

    initEventHubConnection() {
        this._hubConnection = new HubConnection(environment.hubUrl);
        this._hubConnection.on('SendEvent', (data: any) => {
            this.eventHighlight(data.status, data.monitorId);
        });

        this._hubConnection.start()
            .then(() => {
                console.log('Hub connection started');
            })
            .catch(err => {
                console.log('Error while establishing connection', err);
            });
    }

    eventHighlight(status: string, monitorId: number) {
        let _self = this;
        let newColor: string;

        switch (status) {
            case 'Start':
                newColor = 'red'; break;
            case 'During':
                newColor = 'blue'; break;
            case 'End':
                newColor = '#18ff37'; break;
            default: newColor = '#c5c5c5'; break;
        }

        let activeTab = monitorId === 1001 ? this.eventIcon_1001 : this.eventIcon_1002;

        if (activeTab.nativeElement) {
            this._renderer.setStyle(activeTab.nativeElement, 'color', newColor);

            setTimeout(function () {
                _self._renderer.setStyle(activeTab.nativeElement, 'color', '#c5c5c5');
            }, 2000);
        }
    }

    setTimeStep(butn: number, interval: number, addSeconds: number) {
        if (butn === this.activeButton && butn !== 8) {
            return;
        }
        else {
            this.timersUnsubscribe();
            this.alive = true;

            this.timeStep = (butn === 8) ? (this.timeStep > interval ? interval : this.timeStep / 2) : interval
            this.timerSensorDataSubscription = TimerObservable.create(0, this.timeStep)
                .takeWhile(() => this.alive)
                .subscribe(() => {

                    this.currentDate = new Date(this.currentDate.setSeconds(this.currentDate.getSeconds() + 1));
                    this.lastRecievedCounter++;

                    if ((this.stopStepNumber && this.stopStepNumber <= this.sensors[1].step) || this.currentDate.getTime() > this.maxDate.getTime()) {
                        this.stopTimer();
                    }
                    else {
                        //console.log('tick = ' + this.lastRecievedCounter + ' -|||- currentDate = ' + this.currentDate);

                        //get results for 1001 and 1002 monitor
                        var finded_1001 = this.main_Buffers[0].sensorResultsAll.find(x => new Date(x.timeEnd).getTime() == this.currentDate.getTime());
                        var finded_1002 = this.main_Buffers[1].sensorResultsAll.find(x => new Date(x.timeEnd).getTime() == this.currentDate.getTime());

                        //add in newResults buffer
                        if (finded_1001) {
                            this.main_Buffers[0].newResults.push(finded_1001);
                            this.sensors[0].step++; //this.lastRecievedCounter;
                        }
                        else if (finded_1002) {
                            this.main_Buffers[1].newResults.push(finded_1002);
                            this.sensors[1].step++; //= this.lastRecievedCounter;
                        }

                        if (this.main_Buffers[0].newResults.length == this.main_Buffers[1].newResults.length
                            && this.main_Buffers[0].newResults.length == this.minNewResultsCount) {
                            //console.warn('do calculation');
                            this.runComplexTest();
                        }
                    }
                });
        }
    }

    stopTimer() {
        //activate pause button
        this.activeButton = 4;
        this.alive = false;
        this.timeStep = 1000;
        this.timersUnsubscribe();
    }

    timersUnsubscribe() {
        if (this.timerSubscription)
            this.timerSubscription.unsubscribe();

        if (this.timerSensorDataSubscription)
            this.timerSensorDataSubscription.unsubscribe();
    }

    pinPanelClick(e: any) {
        this.isToggledPinPanel = !e;
    }

    refreshResults() {
        this.stopTimer();
        this.clearData();
        this.bindInitialResults();
        this.lastRecievedCounter = 0;
        this.stopStepNumber = undefined;
    }

    getSensorResultsFromDB(): Promise<any> {
        return new Promise(resolve => {
            this.http.get<SensorDetailModel[]>(this.baseUrl + 'api/sensors', {
                params: new HttpParams().set('ids', '1,2')
            }).subscribe(
                response => {
                    resolve(response);
                },
                error => {
                    resolve(error);
                });
        });
    }

    getSensorResultsFromFile(): Promise<any> {
        return new Promise(resolve => {
            this.http.get<any>('sensorResults.json').subscribe(
                response => {
                    resolve(response.sensors);
                },
                error => {
                    resolve(error);
                });
        });
    }

    //load results from file
    bindInitialResults() {
        if (this.sourceFromFile) {

            //get results from File
            this.getSensorResultsFromFile().then(results => {
                this.resolveData(results);
            });
        }
        else {
            //get results from DB
            this.getSensorResultsFromDB().then(results => {
                this.resolveData(results);
            });
        }
    }

    resolveData(results: any) {
        if (results && results.length > 0) {

            this.sensors = [];

            for (let i = 0, len = results.length; i < len; i++) {

                this.main_Buffers.push(new Main_Buffer());

                let newSensor: any = new SensorModel();
                newSensor.id = results[i].id;
                newSensor.monitorId = results[i].monitorId;
                newSensor.onsiteWindMin = results[i].onsiteWindMin;
                newSensor.onsiteWindMax = results[i].onsiteWindMax;
                newSensor.step = 0;//?

                this.sensors.push(newSensor);
                this.sensors[i].detectionPointFluxMax = 40;
                this.sensors[i].detectionPointFluxMin = 30;
                this.sensorResultsSource[i] = results[i].sensorResults;

                //sort by timeBegin
                this.sensorResultsSource[i].sort((a, b) => {
                    let aD: any = new Date(a.timeBegin), bD: any = new Date(b.timeBegin);
                    return aD - bD;
                });
            }

            this.fillAllSensorsResults();

            //set totalResultsMaxCount and min current date
            this.totalResultsMaxCount = Math.max(this.sensorResultsSource[0].length, this.sensorResultsSource[1].length);
            this.currentDate = new Date(this.sensorResultsSource[0][0].timeBegin);
            this.currentDate = new Date(this.currentDate.setSeconds(this.currentDate.getSeconds() - 1));

            this.maxDate = new Date(this.sensorResultsSource[1][this.sensorResultsSource[1].length - 1].timeEnd);
        }
    }

    //fill and calculate buffer for all sensor results
    fillAllSensorsResults() {
        for (let i = 0, len = this.sensorResultsSource.length; i < len; i++) {
            for (let j = 0; j < this.sensorResultsSource[i].length; j++) {
                let tmpRes = this.sensorResultsSource[i][j];
                tmpRes.TWD = tmpRes.windDirection - this.onsiteWindCenterAngle;
                tmpRes.PF = tmpRes.windSpeed * tmpRes.concentration;

                //resolve PF = 0 -> PF = 0.1
                if (tmpRes.PF == 0)
                    tmpRes.PF = 0.1;

                this.main_Buffers[i].sensorResultsAll.push(tmpRes);
            }
        }
    }

    addNewResults() {
        for (let i = 0, len = this.main_Buffers.length; i < len; i++) {
            let currentBuffer = this.main_Buffers[i];
            let newStep = Math.min(this.sensors[i].step + this.newResultsCount + this.skipCount, this.sensorResultsSource[i].length);
            let tmp = this.sensorResultsSource[i].slice(this.sensors[i].step + this.skipCount, newStep);

            if (tmp.length == this.minNewResultsCount) {
                currentBuffer.newResults = tmp;
            }
            else {
                currentBuffer.newResults = currentBuffer.newResults.concat(tmp);
            }

            this.sensors[i].step = newStep;
        }

        this.newResultsCount = 12;
        this.skipCount = 0;
    }

    getWDAverage(monitorTabIndex: number) {
        return this.main_Buffers[monitorTabIndex].sensorResults
            .map(function (item) { return item.windDirection; })
            .reduce(function (a, b) { return a + b; }, 0) / this.main_Buffers[monitorTabIndex].sensorResults.length;
    }

    //complex tests for fminsearch and polyfit methods
    runComplexTest() {
        if (this.clearChart) {
            this.chartData = [];
        }

        //run testRunMovingAverage for fminsearch
        for (let i = 0, len = this.main_Buffers.length; i < len; i++) {
            let currentBuffer = this.main_Buffers[i];

            //set last end date of the new results
            if (currentBuffer.newResults[0])
                currentBuffer.endDate = currentBuffer.newResults[currentBuffer.newResults.length - 1].timeEnd;


            //for Intersections chart 
            var secondTempLimit = (this.sensors[0].detectionPointFluxMax == 40 && this.sensors[1].detectionPointFluxMax == 40) ? 276 : 300;
            //for 1001 monitor only
            if (i == 0 && this.oneMoreFlag != this.sensors[0].step) {

                if (this.sensors[0].step >= 60 && this.sensors[0].step < secondTempLimit) {
                    if (this.sensors[0].step == 60) {
                        this.currentAnglesRange = this.anglesRanges[0];
                        this.sensors[0].a1 = this.currentAnglesRange.a1_1001;
                        this.sensors[0].a2 = this.currentAnglesRange.a2_1001;
                        this.sensors[1].a1 = this.currentAnglesRange.a1_1002;
                        this.sensors[1].a2 = this.currentAnglesRange.a2_1002;
                    }
                    else {
                        this.sensors[0].a1 += this.currentAnglesRange.step_a1_1001;
                        this.sensors[0].a2 += this.currentAnglesRange.step_a2_1001;
                        this.sensors[1].a1 += this.currentAnglesRange.step_a1_1002;
                        this.sensors[1].a2 += this.currentAnglesRange.step_a2_1002;
                    }

                    console.log('range I');
                }
                else if (this.sensors[0].step >= secondTempLimit && this.sensors[0].step < 540) {
                    if (this.sensors[0].step == secondTempLimit) {
                        this.currentAnglesRange = this.anglesRanges[1];
                        this.sensors[0].a1 = this.currentAnglesRange.a1_1001;
                        this.sensors[0].a2 = this.currentAnglesRange.a2_1001;
                        this.sensors[1].a1 = this.currentAnglesRange.a1_1002;
                        this.sensors[1].a2 = this.currentAnglesRange.a2_1002;
                    }
                    else {
                        this.sensors[0].a1 += this.currentAnglesRange.step_a1_1001;
                        this.sensors[0].a2 += this.currentAnglesRange.step_a2_1001;
                        this.sensors[1].a1 += this.currentAnglesRange.step_a1_1002;
                        this.sensors[1].a2 += this.currentAnglesRange.step_a2_1002;
                    }

                    console.log('range II');
                }
                else if (this.sensors[0].step >= 540 && this.sensors[0].step < 744) {
                    if (this.sensors[0].step == 540) {
                        this.currentAnglesRange = this.anglesRanges[2];
                        this.sensors[0].a1 = this.currentAnglesRange.a1_1001;
                        this.sensors[0].a2 = this.currentAnglesRange.a2_1001;
                        this.sensors[1].a1 = this.currentAnglesRange.a1_1002;
                        this.sensors[1].a2 = this.currentAnglesRange.a2_1002;
                    }
                    else {
                        this.sensors[0].a1 += this.currentAnglesRange.step_a1_1001;
                        this.sensors[0].a2 += this.currentAnglesRange.step_a2_1001;
                        this.sensors[1].a1 += this.currentAnglesRange.step_a1_1002;
                        this.sensors[1].a2 += this.currentAnglesRange.step_a2_1002;
                    }

                    console.log('range III');
                }
                else if (this.sensors[0].step >= 744) {
                    if (this.sensors[0].step == 744) {
                        this.currentAnglesRange = this.anglesRanges[3];
                        this.sensors[0].a1 = this.currentAnglesRange.a1_1001;
                        this.sensors[0].a2 = this.currentAnglesRange.a2_1001;
                        this.sensors[1].a1 = this.currentAnglesRange.a1_1002;
                        this.sensors[1].a2 = this.currentAnglesRange.a2_1002;
                    }
                    else {
                        this.sensors[0].a1 += this.currentAnglesRange.step_a1_1001;
                        this.sensors[0].a2 += this.currentAnglesRange.step_a2_1001;
                        this.sensors[1].a1 += this.currentAnglesRange.step_a1_1002;
                        this.sensors[1].a2 += this.currentAnglesRange.step_a2_1002;
                    }

                    console.log('range IV');
                }

            }

            this.oneMoreFlag = this.sensors[0].step;

            currentBuffer.sensorResults = currentBuffer.sensorResults.concat(currentBuffer.newResults);
            currentBuffer.newResults = [];

            if (currentBuffer.sensorResults.length < this.minimumNumberDataForAverage) {
                continue;
            }

            //fill PF Averages and calculate for last 60 results
            var detectPFArray = currentBuffer.sensorResults.slice(
                currentBuffer.sensorResults.length >= this.minimumNumberDataForAverage
                    ? currentBuffer.sensorResults.length - this.minimumNumberDataForAverage
                    : currentBuffer.sensorResults.length, currentBuffer.sensorResults.length);
            var pf_Results = this.calculationService.getPointFluxData(detectPFArray);

            this.sensors[i].WD_Average = pf_Results.wd_Average;
            this.sensors[i].PF1_Average = pf_Results.pf1_Average;
            this.sensors[i].PF2_Average = pf_Results.pf2_Average;

            //check sensor leak event
            let isLeak: boolean = this.checkLeakEvent(i);

            this.sensors[i].wasLeak = this.sensors[i].isLeak;
            this.sensors[i].isLeak = isLeak;

            //remove first N results from begin and add new results to end
            if (!this.sensors[i].isLeak && currentBuffer.sensorResults.length >= this.minimumNumberDataForAverage) {
                currentBuffer.sensorResults.splice(0, this.minNewResultsCount);
            }

            //send event to API
            let statusesArr = ['Start', 'During', 'End'];

            if (!this.sensors[i].wasLeak && isLeak) {
                this.sendEvent(i, statusesArr[0]);
            }
            else if (this.sensors[i].wasLeak && isLeak) {
                this.sendEvent(i, statusesArr[1]);
            }
            else if (this.sensors[i].wasLeak && !isLeak) {
                this.sendEvent(i, statusesArr[2]);
            }

            if (!this.calculateWhenLeak && !isLeak) {
                //clear old data
                currentBuffer.sensorResults = detectPFArray;
                continue;
            }

            //do calculation of smoothing and Moving average method
            this.runMovingAverage(i);

            //do calculation of fminsearch and polyfit methods
            let newTestResult: TestResult = this.calculateFittings(i);
            if (currentBuffer.testResults[currentBuffer.testResults.length - 1] && currentBuffer.testResults[currentBuffer.testResults.length - 1].step == this.sensors[i].step) {
                currentBuffer.testResults.pop();
            }

            newTestResult.step = this.sensors[i].step;
            newTestResult.endDate = currentBuffer.endDate;
            currentBuffer.testResults.push(newTestResult);

            this.sensors[i].solution = newTestResult;

            if (this.isBuildChart) {            //calculate Gaussian fit
                this.myGaussian(i);

                //draw charts
                let _self = this;
                setTimeout(function () { _self.drawGaussianChart(); }, 300);
            }

            this.calculateMainIntersections();
        }
    }

sendEvent(sensorNumber: number, eventStatus: string) {
    let sensorEvent = new SensorEventModel();

    sensorEvent.sensorId = this.sensors[sensorNumber].id;
    sensorEvent.monitorId = this.sensors[sensorNumber].monitorId;
    sensorEvent.pairedMonitorId = null;
    sensorEvent.status = eventStatus;
    sensorEvent.eventTime = new Date(this.main_Buffers[sensorNumber].endDate);
    sensorEvent.sourceLatitude = this.sensors[sensorNumber].latitude;
    sensorEvent.sourceLongitude = this.sensors[sensorNumber].longitude;
    sensorEvent.parallelWindDirection = 0;
    sensorEvent.crossWindDirection = 0;
    sensorEvent.averageWindDirection = this.getWDAverage(sensorNumber);
    sensorEvent.emissionRate = 0;

    this.sensorService.addSensorEvent(sensorEvent).then(() => { }, () => { });

    this.eventHighlight(eventStatus, this.sensors[sensorNumber].monitorId);
}

checkLeakEvent(sensorNumber: number): boolean {
    let WD_AVERAGE: number = this.sensors[sensorNumber].WD_Average;
    let ONSITE_WD_MIN: number = this.sensors[sensorNumber].onsiteWindMin;
    let ONSITE_WD_MAX: number = this.sensors[sensorNumber].onsiteWindMax;

    if (ONSITE_WD_MAX < ONSITE_WD_MIN) {
        //do transform for ONSITE_WD_MIN -> 0 degrees by rotation
        var difference = 360 - ONSITE_WD_MIN,
            TWD_MIN = 0,
            TWD_MAX = (ONSITE_WD_MAX + difference) % 360,
            TWD_AVERAGE = (WD_AVERAGE + difference) % 360;

        return (this.sensors[sensorNumber].PF1_Average >= this.sensors[sensorNumber].detectionPointFluxMax ||
            this.sensors[sensorNumber].PF1_Average >= this.sensors[sensorNumber].detectionPointFluxMin && this.sensors[sensorNumber].wasLeak)
            && TWD_MIN < TWD_AVERAGE && TWD_AVERAGE < TWD_MAX;
    }
    else {
        return (this.sensors[sensorNumber].PF1_Average >= this.sensors[sensorNumber].detectionPointFluxMax ||
            this.sensors[sensorNumber].PF1_Average >= this.sensors[sensorNumber].detectionPointFluxMin && this.sensors[sensorNumber].wasLeak)
            && ONSITE_WD_MIN > WD_AVERAGE && WD_AVERAGE > ONSITE_WD_MAX;
    }
}

runMovingAverage(sensorNumber: number) {
    let currentBuffer = this.main_Buffers[sensorNumber];
    let startRecord: number = 0;
    let endRecord: number = currentBuffer.sensorResults.length;
    let averageDataBuffer: AverageData[] = [];

    currentBuffer.tmp_buffer = new TMP();//clear tmp results

    //let summary = new GroupAverageResultSummary();
    let summary = currentBuffer.averageSummary.find(e => e.step == this.sensors[this.monitorTabIndex].step);

    if (!summary) {
        summary = new GroupAverageResultSummary();
        summary.step = this.sensors[this.monitorTabIndex].step;
        currentBuffer.averageSummary.push(summary);
    } else {
        summary.results = [];
    }

    //calculate TMP results for TWD, PF
    for (let i = 0; i < currentBuffer.sensorResults.length; i++) {
        let tmpRes = currentBuffer.sensorResults[i];
        tmpRes.TWD = tmpRes.windDirection - this.onsiteWindCenterAngle;
        tmpRes.PF = tmpRes.windSpeed * tmpRes.concentration;

        //resolve PF = 0 -> PF = 0.1
        if (tmpRes.PF == 0)
            tmpRes.PF = 0.1;
    }

    //do smoothing
    for (let i = startRecord; i < endRecord - this.NUMBER_OF_LINE_GROUPING_TIME + 1; i++) {

        let tmpArr = currentBuffer.sensorResults.slice(i, (i + this.NUMBER_OF_LINE_GROUPING_TIME));
        let TWD_arr = [];
        let PF_arr = [];

        for (let i = 0; i < tmpArr.length; i++) {
            TWD_arr.push(tmpArr[i].TWD);
            PF_arr.push(tmpArr[i].PF);
        }

        //TWD_Average, PF_Average
        var TWD_mean = this.calculationService.mean(TWD_arr);
        var PF_mean = this.calculationService.mean(PF_arr);

        currentBuffer.tmp_buffer.AverageData.push({
            TWD_Average: TWD_mean,
            PF_Average: PF_mean
        });
    }

    //sort by WD
    currentBuffer.tmp_buffer.AverageData.sort(function (a, b) { return a.TWD_Average - b.TWD_Average });

    //split average data into groups
    var numberOfGroupingLines = Math.floor(currentBuffer.tmp_buffer.AverageData.length / this.groupsCount);

    for (let i = 0; i < currentBuffer.tmp_buffer.AverageData.length / numberOfGroupingLines; i++) {

        startRecord = i * numberOfGroupingLines;
        endRecord = i * numberOfGroupingLines + numberOfGroupingLines;

        if (endRecord > currentBuffer.tmp_buffer.AverageData.length) {
            endRecord = currentBuffer.tmp_buffer.AverageData.length;
        }

        let tmpAverageData = currentBuffer.tmp_buffer.AverageData.slice(startRecord, endRecord);
        let avgResult = new GroupAverageResult();

        //if we have only 1 result in new group - need to add to previos group
        if (tmpAverageData.length == numberOfGroupingLines) {

            let groupTMP = new GroupTMP();
            groupTMP.AverageData = tmpAverageData;

            averageDataBuffer = averageDataBuffer.concat(groupTMP.AverageData);
            currentBuffer.tmp_buffer.GroupTMP.push(groupTMP);

            //linear regression
            let regressionResults = this.calculationService.calculateLinearRegressionForAverage(tmpAverageData, this.onsiteWindCenterAngle, this.doTransformWind);
            groupTMP.LR_Result = regressionResults.result;
            groupTMP.AverageResults = regressionResults.averageResults;

            avgResult.averageData = regressionResults.averageResults;
            avgResult.linearRegressionResult = regressionResults.result;

            //add average points to chart
            this.chartData.push(sensorNumber == 0 ?
                { TWD_Average_1001: groupTMP.AverageResults.TWD_Average, PF_Average_1001: groupTMP.AverageResults.PF_Average } :
                { TWD_Average_1002: groupTMP.AverageResults.TWD_Average, PF_Average_1002: groupTMP.AverageResults.PF_Average });
        }
        else {
            let index = currentBuffer.tmp_buffer.GroupTMP[i - 1] ? i - 1 : i;
            currentBuffer.tmp_buffer.GroupTMP[index].AverageData = currentBuffer.tmp_buffer.GroupTMP[index].AverageData.concat(tmpAverageData);

            //linear regression
            let regressionResults = this.calculationService.calculateLinearRegressionForAverage(currentBuffer.tmp_buffer.GroupTMP[index].AverageData, this.onsiteWindCenterAngle, this.doTransformWind);
            currentBuffer.tmp_buffer.GroupTMP[index].LR_Result = regressionResults.result;
            currentBuffer.tmp_buffer.GroupTMP[index].AverageResults = regressionResults.averageResults;

            summary.results.pop();
            avgResult.averageData = regressionResults.averageResults;
            avgResult.linearRegressionResult = regressionResults.result;
        }

        summary.results.push(avgResult);
    }

    //calculate average Linear regression
    let regressionResultsAverage = this.calculationService.calculateLinearRegressionForAverage(averageDataBuffer, this.onsiteWindCenterAngle, this.doTransformWind);
    currentBuffer.tmp_buffer.LR_Result = regressionResultsAverage.result;
    summary.regression = regressionResultsAverage.result;
}

calculateFittings(sensorNumber: number): TestResult {
    let tmp_buffer = this.main_Buffers[sensorNumber].tmp_buffer;
    let newtestResult: TestResult = new TestResult();
    let solution: any;

    //fill TWD_arr and PF_arr
    let TWD_array: number[] = [];
    let PF_array: number[] = [];

    //run for smoothed data
    for (let i = 0, len = tmp_buffer.AverageData.length; i < len; i++) {
        TWD_array.push(tmp_buffer.AverageData[i].TWD_Average);
        PF_array.push(tmp_buffer.AverageData[i].PF_Average);
    }

    //fminsearch (Nelder-Mead algorithm)-----------------------------------------------------------------------------
    //options1=optimset('MaxFunEval',2000,'TolX',1e-8,'TolFun',1e-8,'MaxIter',500)%,'LargeScale','off')
    var options = {
        zeroDelta: 2000, //MaxFunEval MatLab parameter equivalent
        minErrorDelta: 1e-8,
        maxIterations: 500
    };

    //fminSearch for smoothed data
    solution = this.fminSearch(TWD_array, PF_array, this.calculationService.sum(PF_array), 10, 50, options);

    newtestResult.smoothed.fminsearch.a = solution.A;
    newtestResult.smoothed.fminsearch.sigma = solution.sigma;
    newtestResult.smoothed.fminsearch.mu = solution.mu;

    //polyfit for smoothed data
    solution = this.getPolyfit(TWD_array, PF_array, 2);

    newtestResult.smoothed.polyfit.a = solution.A;
    newtestResult.smoothed.polyfit.sigma = solution.sigma;
    newtestResult.smoothed.polyfit.mu = solution.mu;
    newtestResult.smoothed.polyfit.A = solution.coefficients[0];
    newtestResult.smoothed.polyfit.B = solution.coefficients[1];
    newtestResult.smoothed.polyfit.C = solution.coefficients[2];

    //--------AVERAGE DATA----------------------------------

    TWD_array = [];
    PF_array = [];

    //run for average data
    for (let i = 0, len = tmp_buffer.GroupTMP.length; i < len; i++) {

        TWD_array.push(tmp_buffer.GroupTMP[i].AverageResults.TWD_Average);
        PF_array.push(tmp_buffer.GroupTMP[i].AverageResults.PF_Average);
    }

    //fminSearch for average data
    solution = this.fminSearch(TWD_array, PF_array, this.calculationService.sum(PF_array), 10, 50, options);

    newtestResult.average.fminsearch.a = solution.A;
    newtestResult.average.fminsearch.sigma = solution.sigma;
    newtestResult.average.fminsearch.mu = solution.mu;

    //polyfit for average data
    solution = this.getPolyfit(TWD_array, PF_array, 2);

    newtestResult.average.polyfit.a = solution.A;
    newtestResult.average.polyfit.sigma = solution.sigma;
    newtestResult.average.polyfit.mu = solution.mu;
    newtestResult.average.polyfit.A = solution.coefficients[0];
    newtestResult.average.polyfit.B = solution.coefficients[1];
    newtestResult.average.polyfit.C = solution.coefficients[2];

    return newtestResult;
}

myGaussian(sensorNumber: number) {
    let solution = this.sensors[sensorNumber].solution.average;
    let fitted_fminsearch: number, fitted_polyfit: number;

    for (let i = this.minI; i <= this.maxI; i++) {

        //use for fminsearch
        if (this.isFminSearchMethod) {

            fitted_fminsearch = solution.fminsearch.a / (Math.sqrt(2 * Math.PI) * solution.fminsearch.sigma) * Math.exp(-.5 * (Math.pow(solution.fminsearch.mu - i, 2)) / (Math.pow(solution.fminsearch.sigma, 2)));

            if (!isNaN(fitted_fminsearch)) {
                this.chartData.push(sensorNumber == 0 ?
                    { f_1001_x: i, f_1001_y: fitted_fminsearch } :
                    { f_1002_x: i, f_1002_y: fitted_fminsearch });
            }
        }
        //use for polyfit
        if (this.isPolyfitMethod) {

            //new from myGaussian func: y=A * exp( -(x-mu)^2 / (2*sigma^2) )
            //use for polyfit
            fitted_polyfit = solution.polyfit.a * Math.exp(-Math.pow((i - solution.polyfit.mu), 2) / (2 * Math.pow((solution.polyfit.sigma), 2)));

            if (!isNaN(fitted_polyfit)) {
                this.chartData.push(sensorNumber == 0 ?
                    { p_1001_x: i, p_1001_y: fitted_polyfit } :
                    { p_1002_x: i, p_1002_y: fitted_polyfit });
            }
        }
    }
}

fminSearch(xArr: number[], yArr: number[], xInit: number, xLower: number, xUpper: number, options: any) {
    let _self = this;

    function GaussSSE(X) {

        var INTA = [], SE = [];

        for (let i = 0, len = xArr.length; i < len; i++) {
            INTA[i] = X[0] / (Math.sqrt(2 * Math.PI) * X[1]) * Math.exp(-.5 * (Math.pow(X[2] - xArr[i], 2)) / Math.pow((X[1]), 2));
            SE[i] = Math.pow(INTA[i] - yArr[i], 2);
        }

        return _self.calculationService.sum(SE);
    }

    var solution = fmin.nelderMead(GaussSSE, [xInit, 10, 50], options);

    return { A: solution.x[0], sigma: solution.x[1], mu: solution.x[2] };
}

getPolyfit(xArr: number[], yArr: number[], n: number = 2) {

    let yLog = [];

    for (let i = 0, len = yArr.length; i < len; i++) {
        yLog[i] = Math.log(yArr[i]);
    }

    //n - setup the maximum degree of the polynomial 
    let regression = new PolynomialRegression(xArr, yLog, n);

    //console.log(regression.predict(80)); // Apply the model to some x value. Prints 2.6. 
    //console.log(regression.coefficients); // Prints the coefficients in increasing order of power (from 0 to degree). 
    //console.log(regression.toString(3)); // Prints a human-readable version of the function. 
    //console.log(regression.toLaTeX());
    //console.log(regression.score(x, y));

    //calculation A, sigma, mu
    let A0: number = regression.coefficients[0];
    let A1: number = regression.coefficients[1];
    let A2: number = regression.coefficients[2];

    let sigma: number = Math.sqrt(-1 / (2 * (this.isNegativeA && A2 > 0 ? -1 * A2 : A2)));
    let mu: number = -A1 / 2 / A2;
    let A: number = Math.exp(A0 + Math.pow(mu, 2) / (2 * Math.pow(sigma, 2)));

    //console.log('getPolyfit', regression);

    return {
        A: A, mu: mu, sigma: sigma, coefficients: regression.coefficients
    };
}

drawGaussianChart() {
    //console.log('this.chartData', this.chartData);
    this.chart = this.amChartsService.makeChart("chartdiv", {
        "type": "xy",
        "theme": "light",
        "autoMarginOffset": 20,
        "dataProvider": this.chartData,
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "position": "bottom",
            "axisAlpha": 0,
            "dashLength": 1,
            "title": "TWD Axis"
        },
        {
            "axisAlpha": 0,
            "dashLength": 1,
            "position": "left",
            "title": "PF Axis"
        }
        ],
        "startDuration": 0,
        "graphs": [
            //Average results
            {
                "title": "Average results 1001",
                "balloonText": "TWD:[[TWD_Average_1001]] PF:[[PF_Average_1001]]",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletSize": 8,
                "lineAlpha": 0,
                "xField": "TWD_Average_1001",
                "yField": "PF_Average_1001",
                "bulletColor": "#FFFFFF",
                "lineColor": "#20e413",
                "fillAlphas": 0,
                "useLineColorForBulletBorder": true,
            },
            {
                "title": "Average results 1002",
                "balloonText": "TWD:[[TWD_Average_1002]] PF:[[PF_Average_1002]]",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletSize": 8,
                "lineAlpha": 0,
                "xField": "TWD_Average_1002",
                "yField": "PF_Average_1002",
                "bulletColor": "#FFFFFF",
                "lineColor": "#e01eec",
                "fillAlphas": 0,
                "useLineColorForBulletBorder": true,
            },
            //for gaussian by fminsearh
            {
                "title": "Gaussian (fminsearch) 1001",
                "balloonText": "x:[[f_1001_x]] y:[[f_1001_y]]",
                "bullet": "round",
                "bulletSize": 2,
                "xField": "f_1001_x",
                "yField": "f_1001_y",
                "lineColor": "#ff0000",
                "fillAlphas": 0,
                "lineThickness": 1,
                "type": "line",
            },
            {
                "title": "Gaussian (fminsearch) 1002",
                "balloonText": "x:[[f_1002_x]] y:[[f_1002_y]]",
                "bullet": "round",
                "bulletSize": 2,
                "xField": "f_1002_x",
                "yField": "f_1002_y",
                "lineColor": "#21ec1e",
                "fillAlphas": 0,
                "lineThickness": 1,
                "type": "line",
            },
            //for gaussian by polyfit
            {
                "title": "Gaussian (polyfit) 1001",
                "balloonText": "x:[[p_1001_x]] y:[[p_1001_y]]",
                "bullet": "round",
                "bulletSize": 2,
                "xField": "p_1001_x",
                "yField": "p_1001_y",
                "lineColor": "#074afb",
                "fillAlphas": 0,
                "lineThickness": 1,
                "type": "line",
            },
            {
                "title": "Gaussian (polyfit) 1002",
                "balloonText": "x:[[p_1002_x]] y:[[p_1002_y]]",
                "bullet": "round",
                "bulletSize": 2,
                "xField": "p_1002_x",
                "yField": "p_1002_y",
                "lineColor": "#1ecfec",
                "fillAlphas": 0,
                "lineThickness": 1,
                "type": "line",
            }
        ],

        "marginLeft": 64,
        "marginBottom": 60,
        "chartScrollbar": {},
        "chartCursor": {},
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    });
}

generateFile() {
    this.downloadLink = null;

    this.sensorService.generateResultFile(this.main_Buffers).then((response) => {
        this.downloadLink = '/api/sensors/results/getFile?fileName=' + response.fileName;
    }, () => {
        this.downloadLink = null;
    });
}

drawEllipse(event ?: any, point ?: string) {
    let e = event;
    let p = point;

    if (p && e) {
        if (p === 'point1')
            this.center.latLng = new google.maps.LatLng(e.coords.lat, e.coords.lng);
        else if (p === 'point2') {
            this.pointOnEllipse.latLng = new google.maps.LatLng(e.coords.lat, e.coords.lng);
        }
    }

    this.points = this.calculationService.getEllipsePoints(this.center.latLng, this.pointOnEllipse.latLng, 100);
}

drawCircle(event?: any) {
    let e = event;
    if (e) {
        this.center.latLng = new google.maps.LatLng(e.coords.lat, e.coords.lng);
    }
    this.points = this.calculationService.getCirclePoints(this.center.latLng, 30000);

    this.circle = [
        { lat: 31.10, lng: -85.37, radius: 600, color: 'blue' },
        { lat: 31.10, lng: -85.28, radius: 600, color: 'blue' },
        { lat: 31.10, lng: -85.50, radius: 600, color: 'blue' },
    ];
}

testLinearRegression() {
    let sx = 0, sy = 0, sxx = 0, sxy = 0, syy = 0;
    let n = this.lr_params.length;
    let X_TWD_TMP = 0, Y_PF_TMP = 0;
    let regressionParameters: RegressionParameter[] = [];

    for (let i = 0; i < this.lr_params.length; i++) {
        let x = this.lr_params[i].X_TWD;
        let y = this.lr_params[i].Y_PF;

        sx += x;
        sxx += x * x;
        sy += y;
        sxy += x * y;
        syy += y * y;
    }

    let result: RegressionResult = new RegressionResult();

    result.m = (n * sxy - sx * sy) / (n * sxx - sx * sx);
    result.b = (sy / n) - (result.m * sx / n);
    result.r = (n * sxy - sx * sy) / Math.sqrt((n * sxx - Math.pow(sx, 2)) * (n * syy - Math.pow(sy, 2)));

    this.lr_results = result;
}

testSensorEllipse() {
    this.center.latitude = 31.115276502286978;
    this.center.longitude = -85.37748098373413;
    this.center.iconUrl = '/assets/images/marker_icon_1.png';
    this.center.label = 'Focus 1';
    this.center.labelOptions = {
        color: '#ffffff',
        fontFamily: '',
        fontSize: '14px',
        fontWeight: 'bold',
        text: this.center.label,
    };

    this.pointOnEllipse.latitude = 31.215276502286978;
    this.pointOnEllipse.longitude = -85.27748098373413;
    this.pointOnEllipse.iconUrl = '/assets/images/marker_icon_2.png';
    this.pointOnEllipse.label = 'Focus 2';
    this.pointOnEllipse.labelOptions = {
        color: '#ffffff',
        fontFamily: '',
        fontSize: '14px',
        fontWeight: 'bold',
        text: this.pointOnEllipse.label,
    };

    this.mapsAPILoader.load().then(() => {
        this.center.latLng = new google.maps.LatLng(this.center.latitude, this.center.longitude);
        this.pointOnEllipse.latLng = new google.maps.LatLng(this.pointOnEllipse.latitude, this.pointOnEllipse.longitude);
        this.drawEllipse();
       // this.drawCircle();
    });
}

removeMonitorsResults() {
    var userAnsver = confirm("Are you sure?");
    if (userAnsver == true) {
        this.sensorService.deleteAllSensorsResults().then(response => {
            this.sensorResultsSource = [];
            this.main_Buffers = [];
        });
    }
}

//center of the mass
loadInitialCenterOfMass() {
    this.rayY = 800;
    this.centerObject = null;
    this.linesChartData = [];
    this.sensors =
        [{
            monitorId: 1001,
            x: 0,
            y: 0,
            a1: 24,
            a2: 30,
        },
        {
            monitorId: 1002,
            x: -120,
            y: 0,
            a1: 34,
            a2: 40
        }];

    this.calculateIntersections();
}

getRayCoordinates(x: number, y: number, a: number) {
    a = a % 360;
    let angle: number = (a) % 90, direction: number = (a <= 90 || a >= 270 ? 1 : -1)
    if (angle == 0)
        angle++;

    //console.log('angle - 1', angle);
    angle = ((a <= 90 || (a <= 270 && a >= 180))) ? 90 - angle : angle;

    //console.log('angle - 2', angle);
    let rX = (a >= 0 && a <= 180 ? 1 : -1) * this.rayY / Math.tan(this.calculationService.degToRad2(angle)) + x;
    let rY = (direction * this.rayY) + y;

    return { x: rX, y: rY };
}

calculateMainIntersections() {
    this.centerObject = null;
    this.linesChartData = [];

    this.sensors[0].x = 0;
    this.sensors[0].y = 0;

    this.sensors[1].x = -120;
    this.sensors[1].y = 0;

    for (let i = 0; i < this.sensors.length; i++) {

        //set average angles
        var maxAngle = this.sensors[i].a1;
        var minAngle = this.sensors[i].a2;
        var avg = (maxAngle + minAngle) / 2;

        this.sensors[i].a_average = maxAngle > minAngle ? (avg + 180) % 360 : avg;

        //monitors coordinates
        this.linesChartData.push({
            monitorId: this.sensors[i].monitorId,
            x: this.sensors[i].x,
            y: this.sensors[i].y,
        });
    }

    this.calculateCenterOfMass();

    //console.log('this.linesChartData', this.linesChartData);

    this.chartLines = this.amChartsService.makeChart("chartIntersect", {
        "type": "xy",
        "theme": "light",
        "autoMarginOffset": 20,
        "dataProvider": this.linesChartData,
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "position": "bottom",
            "axisAlpha": 0,
            "dashLength": 1,
            "title": "x",
            "capMaximum": 100,
            "maximum": 650,
            "minimum": -150,
            "autoGridCount": false,
            "gridCount": 20
        },
        {
            "axisAlpha": 0,
            "dashLength": 1,
            "position": "left",
            "title": "y",
            "maximum": this.rayY,
            "minimum": 0
        }
        ],
        "startDuration": 0,
        "graphs": [
            //Monitors
            {
                "title": "Monitors",
                "balloonText": "x:[[x]] y:[[y]]",
                "bullet": "bubble",
                "bulletSize": 13,
                "lineAlpha": 0,
                "xField": "x",
                "yField": "y",
                "lineColor": "#20e413",
                "fillAlphas": 0,
                "labelText": "[[monitorId]]"
            },
            //Rays for 1001
            {
                "title": "Ray 1 for 1001",
                "balloonText": "x:[[xRay1001_1]] y:[[yRay1001_1]]",
                "lineAlpha": 1,
                "xField": "xRay1001_1",
                "yField": "yRay1001_1",
                "lineColor": "#269abc",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            {
                "title": "Ray 2 for 1001",
                "balloonText": "x:[[xRay1001_2]] y:[[yRay1001_2]]",
                "lineAlpha": 1,
                "xField": "xRay1001_2",
                "yField": "yRay1001_2",
                "lineColor": "#269abc",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            //Average ray for 1001
            {
                "title": "Average ray for 1001",
                "balloonText": "x:[[xRay1001_average]] y:[[yRay1001_average]]",
                "lineAlpha": 1,
                "xField": "xRay1001_average",
                "yField": "yRay1001_average",
                "lineColor": "#269abc",
                "fillAlphas": 0,
            },
            //Rays for 1002
            {
                "title": "Ray 1 for 1002",
                "balloonText": "x:[[xRay1002_1]] y:[[yRay1002_1]]",
                "lineAlpha": 1,
                "xField": "xRay1002_1",
                "yField": "yRay1002_1",
                "lineColor": "#f71111",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            {
                "title": "Ray 2 for 1002",
                "balloonText": "x:[[xRay1002_2]] y:[[yRay1002_2]]",
                "lineAlpha": 1,
                "xField": "xRay1002_2",
                "yField": "yRay1002_2",
                "lineColor": "#f71111",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            //Average ray for 1002
            {
                "title": "Average ray for 1002",
                "balloonText": "x:[[xRay1002_average]] y:[[yRay1002_average]]",
                "lineAlpha": 1,
                "xField": "xRay1002_average",
                "yField": "yRay1002_average",
                "lineColor": "#f71111",
                "fillAlphas": 0,
            },
            //Lines intersections
            {
                "title": "M intersections",
                "balloonText": "x:[[Mx]] y:[[My]]",
                "bullet": "bubble",
                "bulletSize": 6,
                "lineAlpha": 0,
                "xField": "Mx",
                "yField": "My",
                "lineColor": "#ff09ca",
                "fillAlphas": 0,
                "labelText": "[[name]]",
            },
            //Triangles
            {
                "title": "Triangle A",
                "balloonText": "x:[[triangle1_x] y:[[triangle1_y]]",
                "lineAlpha": 1,
                "xField": "triangle1_x",
                "yField": "triangle1_y",
                "lineColor": "#ff17ad",
                "fillAlphas": 0.2,
                "lineThickness": 2,
            },
            //new Center average rays
            {
                "title": "New center",
                "balloonText": "x:[[averageRaysCenter_x]] y:[[averageRaysCenter_y]]",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "useLineColorForBulletBorder": true,
                "bulletColor": "#FFFFFF",
                "bulletSizeField": "10",
                "dashLengthField": "dashLength",
                "fillAlphas": 0,
                "xField": "averageRaysCenter_x",
                "yField": "averageRaysCenter_y",
                "labelText": "New C",
            }
        ],

        "marginLeft": 64,
        "marginBottom": 60,
        "chartScrollbar": {},
        "chartCursor": {},
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    });
}

calculateIntersections() {
    this.centerObject = null;
    this.linesChartData = [];

    for (let i = 0; i < this.sensors.length; i++) {

        //set average angles
        var maxAngle = this.sensors[i].a1;
        var minAngle = this.sensors[i].a2;
        var avg = (maxAngle + minAngle) / 2;

        this.sensors[i].a_average = maxAngle > minAngle ? (avg + 180) % 360 : avg;

        //monitors coordinates
        this.linesChartData.push({
            monitorId: this.sensors[i].monitorId,
            x: this.sensors[i].x,
            y: this.sensors[i].y,
        });
    }

    this.calculateCenterOfMass();

    //console.log('this.linesChartData', this.linesChartData);

    this.chartLines = this.amChartsService.makeChart("chartdivLines", {
        "type": "xy",
        "theme": "light",
        "autoMarginOffset": 20,
        "dataProvider": this.linesChartData,
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "position": "bottom",
            "axisAlpha": 0,
            "dashLength": 1,
            "title": "x",
            "capMaximum": 100,
            "maximum": 400,
            "minimum": -150
        },
        {
            "axisAlpha": 0,
            "dashLength": 1,
            "position": "left",
            "title": "y",
            "maximum": 800,
            "minimum": 0

        }
        ],
        "startDuration": 0,
        "graphs": [
            //Monitors
            {
                "title": "Monitors",
                "balloonText": "x:[[x]] y:[[y]]",
                "bullet": "bubble",
                "bulletSize": 13,
                "lineAlpha": 0,
                "xField": "x",
                "yField": "y",
                "lineColor": "#20e413",
                "fillAlphas": 0,
                "labelText": "[[monitorId]]"
            },
            //Rays for 1001
            {
                "title": "Ray 1 for 1001",
                "balloonText": "x:[[xRay1001_1]] y:[[yRay1001_1]]",
                "lineAlpha": 1,
                "xField": "xRay1001_1",
                "yField": "yRay1001_1",
                "lineColor": "#269abc",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            {
                "title": "Ray 2 for 1001",
                "balloonText": "x:[[xRay1001_2]] y:[[yRay1001_2]]",
                "lineAlpha": 1,
                "xField": "xRay1001_2",
                "yField": "yRay1001_2",
                "lineColor": "#269abc",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            //Average ray for 1001
            {
                "title": "Average ray for 1001",
                "balloonText": "x:[[xRay1001_average]] y:[[yRay1001_average]]",
                "lineAlpha": 1,
                "xField": "xRay1001_average",
                "yField": "yRay1001_average",
                "lineColor": "#269abc",
                "fillAlphas": 0,
            },
            //Rays for 1002
            {
                "title": "Ray 1 for 1002",
                "balloonText": "x:[[xRay1002_1]] y:[[yRay1002_1]]",
                "lineAlpha": 1,
                "xField": "xRay1002_1",
                "yField": "yRay1002_1",
                "lineColor": "#f71111",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            {
                "title": "Ray 2 for 1002",
                "balloonText": "x:[[xRay1002_2]] y:[[yRay1002_2]]",
                "lineAlpha": 1,
                "xField": "xRay1002_2",
                "yField": "yRay1002_2",
                "lineColor": "#f71111",
                "fillAlphas": 0,
                "dashLength": 3,
            },
            //Average ray for 1002
            {
                "title": "Average ray for 1002",
                "balloonText": "x:[[xRay1002_average]] y:[[yRay1002_average]]",
                "lineAlpha": 1,
                "xField": "xRay1002_average",
                "yField": "yRay1002_average",
                "lineColor": "#f71111",
                "fillAlphas": 0,
            },
            //Lines intersections
            {
                "title": "M intersections",
                "balloonText": "x:[[Mx]] y:[[My]]",
                "bullet": "bubble",
                "bulletSize": 6,
                "lineAlpha": 0,
                "xField": "Mx",
                "yField": "My",
                "lineColor": "#ff09ca",
                "fillAlphas": 0,
                "labelText": "[[name]]",
            },
            //Triangles
            {
                "title": "Triangle A",
                "balloonText": "x:[[triangle1_x] y:[[triangle1_y]]",
                "lineAlpha": 1,
                "xField": "triangle1_x",
                "yField": "triangle1_y",
                "lineColor": "#ff17ad",
                "fillAlphas": 0.2,
                "lineThickness": 2,
            },
            //new Center average rays
            {
                "title": "New center",
                "balloonText": "x:[[averageRaysCenter_x]] y:[[averageRaysCenter_y]]",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "useLineColorForBulletBorder": true,
                "bulletColor": "#FFFFFF",
                "bulletSizeField": "10",
                "dashLengthField": "dashLength",
                "fillAlphas": 0,
                "xField": "averageRaysCenter_x",
                "yField": "averageRaysCenter_y",
                "labelText": "New C",
            }
        ],

        "marginLeft": 64,
        "marginBottom": 60,
        "chartScrollbar": {},
        "chartCursor": {},
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    });
}

calculateCenterOfMass() {
    var pointsArr = [];

    var m1 = this.calculateIntersectionCoordinates(this.sensors[0].a1, this.sensors[0].x, this.sensors[0].y, this.sensors[1].a1, this.sensors[1].x, this.sensors[1].y);
    var m2 = this.calculateIntersectionCoordinates(this.sensors[0].a1, this.sensors[0].x, this.sensors[0].y, this.sensors[1].a2, this.sensors[1].x, this.sensors[1].y);
    var m3 = this.calculateIntersectionCoordinates(this.sensors[0].a2, this.sensors[0].x, this.sensors[0].y, this.sensors[1].a2, this.sensors[1].x, this.sensors[1].y);
    var averageRaysCenter = this.calculateIntersectionCoordinates(this.sensors[0].a_average, this.sensors[0].x, this.sensors[0].y, this.sensors[1].a_average, this.sensors[1].x, this.sensors[1].y);

    if (m1)
        pointsArr.push(m1);
    if (m2)
        pointsArr.push(m2);
    if (m3)
        pointsArr.push(m3);

    //console.log('m1', m1);
    //console.log('m2', m2);
    //console.log('m3', m3);

    //check count of intersetion points
    if (pointsArr.length < 3) {
        console.error('bad points!!!');
        return;
    }

    var a = Math.sqrt(Math.pow(m2.x - averageRaysCenter.x, 2) + Math.pow(m2.y - averageRaysCenter.y, 2));
    var b = Math.sqrt(Math.pow(m1.x - m3.x, 2) + Math.pow(m1.y - m3.y, 2)) / 2;

    //console.log('a', a);
    //console.log('b', b);

    //first triangle
    this.linesChartData.push({
        triangle1_x: m1.x,
        triangle1_y: m1.y,
    });
    this.linesChartData.push({
        triangle1_x: m2.x,
        triangle1_y: m2.y,
    });
    this.linesChartData.push({
        triangle1_x: m3.x,
        triangle1_y: m3.y,
    });
    this.linesChartData.push({
        triangle1_x: m1.x,
        triangle1_y: m1.y,
    });

    //average rays intersection
    this.linesChartData.push({
        averageRaysCenter_x: averageRaysCenter.x,
        averageRaysCenter_y: averageRaysCenter.y,
    })

    this.centerObject = {};

    this.centerObject.m1 = m1;
    this.centerObject.m2 = m2;
    this.centerObject.m3 = m3;

    this.centerObject.a = a;
    this.centerObject.b = b;

    this.centerObject.averageRaysCenter_x = averageRaysCenter.x,
        this.centerObject.averageRaysCenter_y = averageRaysCenter.y,

        //console.log('centerObject', this.centerObject);

        this.linesChartData.push({
            Mx: m1.x,
            My: m1.y,
            name: 'M1',
        });
    this.linesChartData.push({
        Mx: m2.x,
        My: m2.y,
        name: 'M2',
    });
    this.linesChartData.push({
        Mx: m3.x,
        My: m3.y,
        name: 'M3',
    });


    let sensor_1 = this.sensors[0];
    let sensor_2 = this.sensors[1];

    //rays
    this.linesChartData.push({
        xRay1001_1: sensor_1.x,
        yRay1001_1: sensor_1.y,
        xRay1001_2: sensor_1.x,
        yRay1001_2: sensor_1.y,
        xRay1001_average: sensor_1.x,
        yRay1001_average: sensor_1.y,

        xRay1002_1: sensor_2.x,
        yRay1002_1: sensor_2.y,
        xRay1002_2: sensor_2.x,
        yRay1002_2: sensor_2.y,
        xRay1002_average: sensor_2.x,
        yRay1002_average: sensor_2.y,
    });

    let ray11 = this.getRayCoordinates(sensor_1.x, sensor_1.y, sensor_1.a1);
    let ray12 = this.getRayCoordinates(sensor_1.x, sensor_1.y, sensor_1.a2);
    let ray1_average = this.getRayCoordinates(sensor_1.x, sensor_1.y, sensor_1.a_average);

    let ray21 = this.getRayCoordinates(sensor_2.x, sensor_2.y, sensor_2.a1);
    let ray22 = this.getRayCoordinates(sensor_2.x, sensor_2.y, sensor_2.a2);
    let ray2_average = this.getRayCoordinates(sensor_2.x, sensor_2.y, sensor_2.a_average);

    this.linesChartData.push({
        xRay1001_1: ray11.x,
        yRay1001_1: ray11.y,
        xRay1001_2: ray12.x,
        yRay1001_2: ray12.y,
        xRay1001_average: ray1_average.x,
        yRay1001_average: ray1_average.y,

        xRay1002_1: ray21.x,
        yRay1002_1: ray21.y,
        xRay1002_2: ray22.x,
        yRay1002_2: ray22.y,
        xRay1002_average: ray2_average.x,
        yRay1002_average: ray2_average.y,
    });

    this.centerObject.m1newCavg2 = this.calculationService.getAngleBetweenVectors(averageRaysCenter.x, averageRaysCenter.y, m1.x, m1.y, ray2_average.x, ray2_average.y);
}

getCenterMassTriangle(a: any, b: any, c: any) {
    var x = (a.x + b.x + c.x) / 3;
    var y = (a.y + b.y + c.y) / 3;

    return { x: x, y: y };
}

getCenterMassPolynom(c_a: any, c_b: any, s_a: number, s_b: number) {
    var x = (s_a * c_a.x + s_b * c_b.x) / (s_a + s_b);
    var y = (s_a * c_a.y + s_b * c_b.y) / (s_a + s_b);

    return { x: x, y: y };
}

getTriangleSquare(a: any, b: any, c: any) {
    let s = Math.abs(((a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y)) / 2);

    return s;
}

calculateIntersectionCoordinates(a1: number, x1: number, y1: number, a2: number, x2: number, y2: number) {
    let a1rad = this.calculationService.degToRad2(90 - a1 % 360);
    let a2rad = this.calculationService.degToRad2(90 - a2 % 360);
    let l1a, l1b, l1c, l2a, l2b, l2c, D1, D2, D3, resX, resY;

    //if lines are paralel to x
    if ((a1 == 0) || (a1 == 180)) {
        l1a = 1;
        l1b = 0;
        l1c = x1;
    }
    else {
        l1a = -(Math.tan(a1rad));
        l1b = 1;
        l1c = y1 - (Math.tan(a1rad) * x1);
    }
    if ((a2 == 0) || (a2 == 180)) {
        l2a = 1;
        l2b = 0;
        l2c = x2;
    }
    else {
        l2a = -(Math.tan(a2rad));
        l2b = 1;
        l2c = y2 - (Math.tan(a2rad) * x2);
    }

    D1 = l1a * l2b;
    D2 = l2a * l1b;
    D3 = D1 - D2;

    //if lines are parallels 
    if (D3 == 0) {
        resX = NaN;
        resY = NaN;
    }
    else {
        resX = ((l1c * l2b) - (l2c * l1b)) / D3;
        resY = ((l1a * l2c) - (l2a * l1c)) / D3;
    }

    return {
        x: resX,
        y: resY,
    };
}

ngOnDestroy() {
    this.clearData();
    this.timersUnsubscribe();
    this.alive = false;
    this.lastRecievedCounter = 0;
}

clearData() {
    if (this.chart)
        this.amChartsService.destroyChart(this.chart);

    //complex buffer
    this.isToggled = true;
    this.sensorResultsSource = [];
    this.main_Buffers = [];
    this.newResultsCount = 60;
    this.groupsCount = 6;
    this.onsiteWindCenterAngle = 306; this.NUMBER_OF_LINE_GROUPING_TIME = 6;
    this.doTransformWind = false;
    this.chartData = [];
    this.linesChartData = [];
    this.skipCount = 0;
    this.downloadLink = null;

    this.currentAnglesRange = null;
    // this.sensors[0].detectionPointFluxMax = 40;
    // this.sensors[0].detectionPointFluxMin = 30;
    // this.sensors[1].detectionPointFluxMax = 40;
    // this.sensors[1].detectionPointFluxMin = 30;
    this.sensors.forEach((item)=>{
        item.detectionPointFluxMax = 40;
        item.detectionPointFluxMin = 30;
    })
}

setMonitorTabIndex(index: number) {
    this.monitorTabIndex = index;
}
}