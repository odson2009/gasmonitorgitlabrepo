import { Component, ViewChild, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyModel } from "../../Models/company.model";
import { CompanyService } from "../../Services/company.service";
import { ChangeDetectorRef } from '@angular/core';
import { isEqual, cloneDeep } from 'lodash';
import { FileInputComponent } from '../../components/fileInput/fileInput.component';
import { FileUploader } from 'ng2-file-upload';
import { RegionService } from '../../Services/region.service';
import { FormControl, Validators } from '@angular/forms';
import { elementAt } from 'rxjs/operators';
import { EventBusService } from '../../Services/event-bus.service';

@Component({
    selector: 'company-action',
    templateUrl: './company-action.component.html',
    styleUrls: ['./company-action.component.scss']
})
export class CompanyActionComponent implements OnInit, OnDestroy {
    @ViewChild('f') private form: any;
    @ViewChild(FileInputComponent) fileInputComponent: FileInputComponent;

    private baseUrl: string;
    isEditMode: boolean = true;
    defaultCompanyData: CompanyModel;
    existsFileName: string;
    company: CompanyModel;
    showLogo: boolean;
    id: number = null;
    dataFile: any;

    formGroup: any =
    {
        file: new FormControl('', [Validators.required]),
    };

    showButton = false;

    constructor(@Inject('BASE_URL') baseUrl: string,
                private activeRoute: ActivatedRoute,
                private router: Router,
                private companyService:CompanyService,
                private cdRef:ChangeDetectorRef,
                private regionService: RegionService,
                private _eventBusService: EventBusService
            ) {
        this.baseUrl = baseUrl;
    }


    ngAfterViewChecked()
    {
        this.showButton = true;
        this.cdRef.detectChanges();
    }
    
    ngOnInit() {
        this.isEditMode = this.router.url.includes('company/edit');
        this.activeRoute.params.subscribe(params => {
            this.id = params['id'];
        });
    
        if (this.isEditMode) {
            this.companyService.getCompany(this.id).then(resData => {
                this.company = resData;
                this.defaultCompanyData = cloneDeep(this.company);
                this.existsFileName = this.fileInputComponent && this.fileInputComponent.inputValue? this.fileInputComponent.inputValue.name : null;
                this.dataFile = this.company.imageBase64;
                if (this.company.imageBase64 && this.company.imageBase64.length > 5)
                    this.showLogo = true;
            });
        }
        else {
            this.company = new CompanyModel();
            this.company.name = '';
            this.company.email = '';
            this.company.regions = [];
            this.company.website = '';
            this.showLogo = false;
        }
    }

    ngOnDestroy() {
        this.companyService.dispose();
    }

    save() {
        if (this.form.valid && !this.fileInputComponent.fileTypeError) {
            if (this.fileInputComponent.isValid) {
                var reader = new FileReader();
                reader.readAsDataURL(this.dataFile);
                reader.onloadend = () => {
                    this.company.imageBase64 = reader.result;
                    if (this.isEditMode) {
                        this.companyService.updateCompany(this.company).then(this.onRefresh);
                    }
                    else {
                        this.companyService.addCompany(this.company).then(this.onRefresh);
                    }
                }
            }
            else {
                this.company.imageBase64 = this.dataFile;
                if (this.isEditMode) {
                    this.companyService.updateCompany(this.company).then(this.onRefresh);
                }
                else {
                    this.companyService.addCompany(this.company).then(this.onRefresh);
                }
            }
        }
    }

    navigate(path: string) {
        setTimeout(() => {
            this.router.navigate([path]);
        }, 1000);
    }

    onRefresh = () => {
        this.router.navigate(['/navmenu/companies']).then(() => this._eventBusService.emit('LOGO_UPDATED'));
    }

    removeLogo = function () {
        this.showLogo = false;
        this.company.imageBase64 = null;
        this.dataFile = null;
    }

    get disabledSubmit() {
        if (this.form) {
            if (this.isEditMode && isEqual(this.company, this.defaultCompanyData) && (this.fileInputComponent && this.fileInputComponent.inputValue && this.existsFileName == this.fileInputComponent.inputValue.name)
            || this.fileInputComponent.fileTypeError) {
                return true;
            }
            return this.form.invalid;
        }
        return true;
    }

    addRegion(){
        if (this.isEditMode) {
            window.open('/navmenu/site/add;companyId='+this.company.id, '_blank')
        } else {
            window.open('/navmenu/site/add', '_blank')
        }
    }

    editRegion(id:number){ 
        window.open('/navmenu/site/edit/'+id, '_blank')
    }

    deleteRegion(region: any) {
        this.regionService.deleteRegion(region.id).then(result => {
            this.company.regions.splice(this.company.regions.indexOf(region), 1);
        });
    }
}
