import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { CompanyService } from "../../Services/company.service";
import { Router } from "@angular/router";
import { CompanyModel } from '../../Models/company.model';

@Component({
    selector: 'company-list',
    templateUrl: './company-list.component.html',
    styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit {
    private baseUrl: string;
    public data: CompanyModel[];
    public toggleArr: number[] = [];

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private companyService: CompanyService,
        private router: Router) {

        this.baseUrl = baseUrl;
    }

    ngOnInit() {
        this.loadCompanies();
    }

    loadCompanies() {
        this.companyService.getCompanies().then(resData => {
            this.data = resData;
        });
    }

    ngOnDestroy() {
        this.data = [];
        this.companyService.dispose();
    }

    delete(company: any) {
        this.companyService.deleteCompany(company.id).then(result => {
            this.data.splice(this.data.indexOf(company), 1);
        });
    }

    setToggleArr(i: number) {
        if (this.toggleArr.includes(i)) {
            this.toggleArr.splice(this.toggleArr.indexOf(i), 1);
        }
        else {
            this.toggleArr.push(i);
        }
    }
}
