import { Component, ViewChild, OnInit, OnDestroy, NgZone, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MapsAPILoader } from "@agm/core";
import { } from 'googlemaps';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SensorModel, SensorDetailModel } from "../../Models/sensor.model";
import { AlgorithmModel, RegionSensorsModel } from "../../Models/region.model";
import { Globals } from "../../app.globals";
import { SensorService } from '../../services/sensor.service';
import { RegionService } from "../../Services/region.service";
import { CalculationService } from "../../Services/calculation.service";
import { filter } from "rxjs/operator/filter";

@Component({
    selector: 'monitor-action',
    templateUrl: './monitor-action.component.html',
    styleUrls: ['./monitor-action.component.scss']
})

export class MonitorActionComponent implements OnInit {
    @ViewChild('f') private form: any;
    // autocomplete control
    @ViewChild('search')
    searchElementRef: ElementRef;
    isEditMode: boolean = true;
    sensor: SensorDetailModel;
    undefValue: any;
    rectangle: any = [];
    polygon: any;
    icon: any = {
        url: '../assets/images/circle.png',
        scaledSize: {
            width: 10,
            height: 10
        },
    };
    autoCompleteLat = 0;
    autoCompleteLng = 0;
    autoCompleteZoom = 1;

    private leftSensor: SensorDetailModel;
    private rightSensor: SensorDetailModel;

    private id: number;

    private regions: RegionSensorsModel[] = [];
    private currentRegion: RegionSensorsModel;
    private regionSensors: SensorDetailModel[] = [];

    lat = 31.115276502286978;
    lng = -85.37748098373413;
    mapZoom = 0;

    private isAutodetectMonitors = false;

    private isNeedToExtend = true;
    private latlngBounds: any;
    private regionCenter: any;

    constructor(private globals: Globals,
        private calculationService: CalculationService,
        private sensorService: SensorService,
        private regionService: RegionService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
    ) { }

    ngOnInit() {
        this.isEditMode = this.router.url.includes('monitor/edit');

        this.regionService.getRegionsSensors().then(result => {
            this.regions = result;


            if (this.isEditMode) {

                this.activeRoute.params.subscribe(params => {
                    this.id = params['id'];
                });

                this.currentRegion = this.regions.find(c => c.sensors.some(s => +s.id === +this.id));
                this.regionSensors = this.currentRegion.sensors;

                this.sensor = this.regionSensors.find(e => +e.id === +this.id);

                this.leftSensor = this.regionSensors.find(e => +e.monitorId === +this.sensor.monitorIdToLeft);
                this.rightSensor = this.regionSensors.find(e => +e.monitorId === +this.sensor.monitorIdToRight);

                if (this.regionSensors.includes(this.sensor)) {
                    this.regionSensors.splice(this.regionSensors.indexOf(this.sensor), 1);
                }

                this.lat = this.sensor.latitude;
                this.lng = this.sensor.longitude;
            } else {
                this.sensor = new SensorDetailModel();

                this.sensor.azimuthLeft = 0;
                this.sensor.azimuthRight = 0;
                this.sensor.distanceLeft = 0;
                this.sensor.distanceRight = 0;

                this.sensor.monitorIdToLeft = "0";
                this.sensor.monitorIdToRight = "0";
                this.sensor.normalAzimuthToLeft = 0;
                this.sensor.normalAzimuthToRight = 0;

                this.sensor.onsiteWindMax = 0;
                this.sensor.onsiteWindMin = 0;
            }
        }, error => { })
        .then(() => this.mapsAPILoader.load())
        .then(() => {
            // TODO weird hack. Needs a more elegant solution
            setTimeout(() => this.activateMap(), 0);

            if (this.isEditMode) {
                this.initSensors();
                // calculate nearest monitors params and draw lines
                this.calculateSensors();

                this.currentRegion.coordinates.forEach(c => this.rectangle.push(c));
            }
        });
    }

    activateMap() {
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
            types: ['address']
        });

        autocomplete.addListener('place_changed', () => {
            this.ngZone.run(() => {
                // get the place result
                const place: google.maps.places.PlaceResult = autocomplete.getPlace();

                // verify result
                if (place.geometry === undefined || place.geometry === null) {
                    return;
                }

                // set latitude, longitude and zoom
                this.lat = place.geometry.location.lat();
                this.lng = place.geometry.location.lng();
                this.mapZoom = 17;
            });
        });
    }

    initSensors() {
        for (let i: number = 0; i < this.regionSensors.length; i++) {
            this.regionSensors[i].iconUrl = '/assets/images/marker_icon_' + (this.sensor.regionId <= 10 ? this.sensor.regionId : 'default') + '.png';
            this.regionSensors[i].label = this.regionSensors[i].monitorId;
            this.regionSensors[i].labelOptions = {
                color: '#ffffff',
                fontFamily: '',
                fontSize: '14px',
                fontWeight: 'bold',
                text: this.regionSensors[i].label
            };
        }
    }

    regionSelected(regionId: number) {
        this.unboundSensor(this.leftSensor);
        this.unboundSensor(this.rightSensor);
        this.clearNearestSensors();

        if (regionId > 0) {

            //sel map zoom and center by selected region
            this.setMapLocationByRegion(regionId);
            this.sensor.regionId = regionId;

            this.currentRegion = this.regions.find(c => c.id == regionId);
            this.regionSensors = this.currentRegion.sensors;

            this.initSensors();
            this.checkExistingMonitorsIds(this.sensor.monitorId);

            this.calculateSensors();

            // draw rectangle
            this.rectangle = [];
            this.currentRegion.coordinates.forEach(c => this.rectangle.push(c));

            this.extendRegion();
            if (this.checkContains(this.sensor.latitude, this.sensor.longitude) == false) {
                this.setMarkerInRegionCenter();
            }

        }
    }

    setMapLocationByRegion(regionId: number) {
        for (var i = 0; i < this.regions.length; i++) {
            if (this.regions[i].id == regionId) {
                var selectedRegion = this.regions[i];

                if (selectedRegion.sensors.length > 1) {
                    this.isNeedToExtend = true;
                    this.extendMap();
                }
                else {
                    //this.mapZoom = selectedRegion.zoom;
                    //this.lat = selectedRegion.latitude;
                    //this.lng = selectedRegion.longitude;
                    this.isNeedToExtend = false;
                }
            }
        }
    }

    monitorSelected(leftMonitorId?: string, rightMonitorId?: string) {

        //this.clearNearestSensors();

        this.isAutodetectMonitors = false;


        let newLeftSensor = leftMonitorId ?
            this.regionSensors.find(c => c.monitorId == leftMonitorId) : null;

        let newRightSensor = rightMonitorId ?
            this.regionSensors.find(c => c.monitorId == rightMonitorId) : null;

        this.boundSensors(
            newLeftSensor,
            newRightSensor
        );

        this.calculateSensors();
    }

    calculateSensors() {

        if (this.isAutodetectMonitors)
            this.detectMonitors();

        if (this.isNeedToExtend) {
            this.extendMap();
        }

        //this.sensor.monitorIdToLeft = this.leftSensor && this.leftSensor.monitorId;
        this.calculate(this.sensor, this.leftSensor);

        if (this.leftSensor && this.leftSensor.monitorIdToRight == this.sensor.monitorId)
            this.calculate(this.leftSensor, this.sensor, false);


        //this.sensor.monitorIdToRight = this.rightSensor && this.rightSensor.monitorId;
        this.calculate(this.sensor, this.rightSensor, false);

        if (this.rightSensor && this.sensor.monitorId == this.rightSensor.monitorIdToLeft)
            this.calculate(this.rightSensor, this.sensor);


        this.sensor.onsiteWindCenterAngle = (this.sensor.normalAzimuthToLeft || this.sensor.normalAzimuthToRight) ?
            this.calculationService.calculateWindCenterAngle(this.sensor.normalAzimuthToLeft, this.sensor.normalAzimuthToRight) : 0;

        this.drawArrows();
    }

    calculate(main: SensorDetailModel, target?: SensorDetailModel, targetIsLeft = true) {

        let distance = 0;
        let azimuth = 0;
        let normalAzimuth = 0;

        if (target) {

            distance = parseFloat(google.maps.geometry.spherical.computeDistanceBetween(
                new google.maps.LatLng(main.latitude, main.longitude),
                new google.maps.LatLng(target.latitude, target.longitude)).toFixed(4));

            azimuth = this.calculationService.angleFromCoordinateNormal(
                main.latitude,
                main.longitude,
                target.latitude,
                target.longitude);

            normalAzimuth = this.calculationService.getNormalAzimuth(azimuth, targetIsLeft);
        }

        if (targetIsLeft) {
            main.distanceLeft = distance;
            main.azimuthLeft = azimuth;
            main.normalAzimuthToLeft = normalAzimuth;
        } else {
            main.distanceRight = distance;
            main.azimuthRight = azimuth;
            main.normalAzimuthToRight = normalAzimuth;
        }
    }

    save() {
        if (this.form.valid) {
            if (this.isEditMode) {
                this.regionSensors.push(this.sensor);
                this.regionService.updateRegionSensors(this.currentRegion).then(result => {
                    this.navigate('/navmenu/monitors');
                });
            }
            else {
                this.sensorService.addSensor(this.sensor).then(result => {
                    this.navigate('/navmenu/monitors');
                });
            }
        }
    }

    extendRegion() {
        this.latlngBounds = new google.maps.LatLngBounds();
        var latLngArr = [];

        for (var i = 0; i < this.rectangle.length; i++) {
            latLngArr.push(new google.maps.LatLng(this.rectangle[i].latitude, this.rectangle[i].longitude));
            this.latlngBounds.extend(new google.maps.LatLng(this.rectangle[i].latitude, this.rectangle[i].longitude));
        }
        this.polygon = new google.maps.Polygon({ paths: latLngArr });
        this.regionCenter = { lat: this.latlngBounds.getCenter().lat(), lng: this.latlngBounds.getCenter().lng() };
    }

    checkContains(lat: any, lng: any): boolean {
        return google.maps.geometry.poly.containsLocation(new google.maps.LatLng(lat, lng), this.polygon);
    }

    setMarkerInRegionCenter() {
        var self = this;
        setTimeout(function () {
            self.sensor.latitude = self.regionCenter.lat;
            self.sensor.longitude = self.regionCenter.lng;
        }, 100);     
    }

    mapClicked($event: MouseEvent) {
        let center: any = $event;
        this.sensor.latitude = center.coords.lat;
        this.sensor.longitude = center.coords.lng;

        if (this.regionSensors.length != 0)
            this.calculateSensors();

        if (this.checkContains(center.coords.lat, center.coords.lng) == false) {
            this.setMarkerInRegionCenter();
        }
    }

    markerDragEnd($event: MouseEvent) {
        let center: any = $event;
        this.sensor.latitude = center.coords.lat;
        this.sensor.longitude = center.coords.lng;

        this.calculateSensors();

        if (this.checkContains(center.coords.lat, center.coords.lng) == false) {
            this.setMarkerInRegionCenter();
        }
    }

    removeMonitor() {
        this.isAutodetectMonitors = true;
        this.sensor.latitude = 0;
        this.sensor.longitude = 0;
    }

    //drawing arrows for vectors
    drawArrows() {
        if (this.leftSensor)
            this.leftSensor.arrow = this.calculationService.drawArrow(this.sensor.latitude, this.sensor.longitude, this.leftSensor.latitude, this.leftSensor.longitude, this.mapZoom);

        if (this.rightSensor)
            this.rightSensor.arrow = this.calculationService.drawArrow(this.sensor.latitude, this.sensor.longitude, this.rightSensor.latitude, this.rightSensor.longitude, this.mapZoom);
    }

    extendMap() {
        if (this.currentRegion.sensors.length > 0) {

            this.latlngBounds = new google.maps.LatLngBounds();

            //extend region sensors
            this.currentRegion.sensors.forEach((sensor: any) => {
                this.latlngBounds.extend(new google.maps.LatLng(sensor.latitude, sensor.longitude));
            });
        }

        if (this.sensor.latitude && this.sensor.longitude) {
            //extend current sensor
            this.latlngBounds.extend(new google.maps.LatLng(Number(this.sensor.latitude), Number(this.sensor.longitude)));
        }
    }

    checkExistingMonitorsIds(monitorId: string) {
        //check existing monitorId
        if (monitorId !== "0") {
            if (this.regionSensors && this.regionSensors.length > 0) {
                var existMonitor = this.regionSensors.find(c => c.monitorId == monitorId);

                if (existMonitor) {
                    this.form.controls['monitorId'].setErrors({ 'exist': true });
                }
                else {
                    this.form.controls['monitorId'].setErrors(null);
                }
            }
            else {
                this.form.controls['monitorId'].setErrors(null);
            }
        }
    }

    boundSensors(leftSensor: SensorDetailModel, rightSensor: SensorDetailModel) {
        this.unboundSensor(this.leftSensor);
        this.leftSensor = leftSensor;
        this.sensor.monitorIdToLeft = this.leftSensor ? this.leftSensor.monitorId : "0";

        this.unboundSensor(this.rightSensor);
        this.rightSensor = rightSensor;
        this.sensor.monitorIdToRight = this.rightSensor ? this.rightSensor.monitorId : "0";
    }


    //remove data relative to the main sensor
    unboundSensor(sensor: SensorDetailModel) {
        if (sensor && sensor.monitorId !== "0") {
            if (this.sensor.monitorIdToRight == sensor.monitorId) {
                this.sensor.monitorIdToRight = "0";
                sensor.distanceLeft = 0;
                sensor.azimuthLeft = 0;
                sensor.normalAzimuthToLeft = 0;
            }
            else if (this.sensor.monitorIdToLeft == sensor.monitorId) {
                this.sensor.monitorIdToLeft = "0";
                sensor.distanceRight = 0;
                sensor.azimuthRight = 0;
                sensor.normalAzimuthToRight = 0;
            }
        }
    }

    compareByDistance(a: any, b: any) {
        const genreA = a.distance;
        const genreB = b.distance;

        let comparison = 0;
        if (genreA > genreB) {
            comparison = 1;
        } else if (genreA < genreB) {
            comparison = -1;
        }
        return comparison;
    }

    detectMonitors() {
        let leftValueArray: SensorDetailModel[] = [];
        let rightValueArray: SensorDetailModel[] = [];

        for (let i: number = 0; i < this.regionSensors.length; i++) {

            if (this.regionSensors[i]) {
                this.regionSensors[i].distance = parseFloat(google.maps.geometry.spherical.computeDistanceBetween(
                    new google.maps.LatLng(this.sensor.latitude, this.sensor.longitude),
                    new google.maps.LatLng(this.regionSensors[i].latitude, this.regionSensors[i].longitude)).toFixed(4));

                if (this.sensor.longitude > this.regionSensors[i].longitude)
                    leftValueArray.push(this.regionSensors[i]);

                if (this.sensor.longitude < this.regionSensors[i].longitude)
                    rightValueArray.push(this.regionSensors[i]);

                let newLeftSensor = leftValueArray.length > 0
                    ? leftValueArray.sort(this.compareByDistance)[0]
                    : null;

                let newRightSensor = rightValueArray.length > 0
                    ? rightValueArray.sort(this.compareByDistance)[0]
                    : null;

                this.boundSensors(
                    newLeftSensor,
                    newRightSensor
                );
            }
        }
    }

    clearNearestSensors(): void {
        this.leftSensor = null;
        this.rightSensor = null;
    }

    zoomChanged($event: any) {
        this.mapZoom = $event;
        this.drawArrows();
    }

    navigate(path: string) {
        setTimeout(() => {
            this.router.navigate([path]);
        }, 1000);
    }

    ngOnDestroy() {
        this.calculationService.dispose();
        this.regionService.dispose();
        this.sensorService.dispose();
    }
}
