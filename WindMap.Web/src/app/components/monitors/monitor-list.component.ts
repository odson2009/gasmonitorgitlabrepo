import { Component, AfterViewInit, OnDestroy, ViewChild, Inject, OnInit, Injectable, PipeTransform } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SensorService } from '../../services/sensor.service';
import { RegionService } from '../../Services/region.service';
import { EventService } from "../../Services/event.service";
import { Router } from "@angular/router";
import { Globals } from "../../app.globals";
import { FileInputComponent } from '../../components/fileInput/fileInput.component';
import { FileUploader } from 'ng2-file-upload';
import { RegionSensorsModel } from '../../Models/region.model';
import { finalize } from 'rxjs/operators/finalize';
import { TokensService } from '../../Services/tokens.service';

@Component({
    selector: 'monitor-list',
    templateUrl: './monitor-list.component.html',
    styleUrls: ['./monitor-list.component.scss']
})
export class MonitorListComponent implements OnInit {
    @ViewChild(FileInputComponent) fileInputComponent: FileInputComponent;
    private baseUrl: string;
    sensors: any;
    toggleArr: any = [];
    endDate: Date;
    stratDate: Date;

    regionsNames: RegionSensorsModel[];
    selectedRegionId = -1;

    settings = {
        bigBanner: true,
        timePicker: true,
        format: 'dd-MM-yyyy HH:mm:ss',
        defaultOpen: false,
        closeOnSelect: false
    };

    dataFile: any;

    formGroup: any =
    {
        file: new FormControl('', [Validators.required]),
    };

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private globals: Globals,
        private eventService: EventService,
        private sensorService: SensorService,
        private regionService: RegionService,
        private tokenService: TokensService,
        private router: Router) {

        this.baseUrl = baseUrl;
    }

    ngOnInit() {
        this.endDate = new Date();
        this.stratDate = new Date();
        this.stratDate.setSeconds(this.endDate.getSeconds() - 300);
        this.loadSensors(true);

        this.eventService.startLoading();

        this.regionService.getRegionsSensors().then(result => {
            this.eventService.completeLoading();
            this.regionsNames = result;
        }, error => {
            this.eventService.completeLoading();
        });
    }

    uploadFile() {
        let formData: FormData = new FormData();
        formData.append('file', this.fileInputComponent.uploader.queue[0]._file, this.fileInputComponent.uploader.queue[0]._file.name);

        this.fileInputComponent.uploader.queue[0].formData = formData;
        this.fileInputComponent.uploader.setOptions({
            authToken: 'Bearer ' + this.tokenService.getToken('access'),
            url: this.baseUrl + 'api/sensors/results/file',
            isHTML5: true,
        });
        this.fileInputComponent.uploader.queue[0].onBeforeUpload = () => {
            this.eventService.startLoading();
        }
        this.fileInputComponent.uploader.queue[0].upload();
        this.fileInputComponent.uploader.queue[0].onComplete = (response: string, status: number, headers: any) => {
            if (status == 200) {
                this.eventService.showToast('success');
                this.loadSensors(true);
            }
            else {
                this.eventService.showToast('error', { _body: response, status: status });
            }

            this.eventService.stopLoading();
            this.fileInputComponent.uploader.queue[0].remove();
            this.fileInputComponent.inputValue = null;
        }
    }

    setToggleArr(i: number) {
        if (this.toggleArr.includes(i)) {
            this.toggleArr.splice(this.toggleArr.indexOf(i), 1);
        }
        else {
            this.toggleArr.push(i);
        }
    }

    onStartDateSelect($event: any) {
        this.stratDate = $event;
        this.stratDate.setSeconds(0);
        this.loadSensors();
    }

    onEndDateSelect($event: any) {
        this.endDate = $event;
        this.endDate.setSeconds(0);
        this.loadSensors(false);
    }

    loadSensors(isNeedLastResults: boolean = false) {
        this.sensorService.getSensorsFullListByPeriod(this.stratDate, this.endDate, isNeedLastResults).then(result => {
            this.sensors = result;
        });
    }

    delete(sensor: any) {
        this.sensorService.deleteSensor(sensor.id).then(response => {
            this.sensors.splice(this.sensors.indexOf(sensor), 1);
        });
    }

    ngOnDestroy() {
        this.sensors = [];
        this.toggleArr = [];
        this.sensorService.dispose();
    }
}
