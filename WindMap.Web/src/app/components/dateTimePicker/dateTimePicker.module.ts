﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateTimePicker } from './dateTimePicker.component';
import { FormsModule } from '@angular/forms';
import { ClickOutsideDirective } from './clickOutside';
@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [DateTimePicker, ClickOutsideDirective],
    exports: [DateTimePicker, FormsModule, ClickOutsideDirective]
})
export class DateTimePickerModule {

}