import { Component, OnInit, OnDestroy, Directive, ElementRef, Input, NgZone, NgModule, Injectable, ViewChild } from '@angular/core';
import { RegionService } from "../../Services/region.service";
import { SensorService } from "../../services/sensor.service";
import { WindVector, Point } from "../../Models/sensor.model";
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Router } from "@angular/router";
import { RegionSensorsModel } from '../../Models/region.model';
import { CompanyService } from '../../Services/company.service';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnDestroy {
    @ViewChild("chartContainer", { read: ElementRef }) chartContainer: ElementRef;

    private _navigate: any;
    private chart: AmChart;
    private regionsNames: RegionSensorsModel[] = [];

    isLoading: boolean = true;
    private sensors: any = [];
    sensorsChartData: any;

    companiesCount: number;
    regionsCount: number;
    monitorCount: number;
    monitorResultsCount: number;

    constructor(private amChartsService: AmChartsService,
                private regionService: RegionService,
                private companyService: CompanyService,
                private sensorService: SensorService,
                private router: Router) {
        this.sensorsChartData = [];
        this._navigate = this.navigate.bind(this);
    }

    loadSensors(): Promise<void> {
        // load sensors results by period
        return this.sensorService.getSensorsChartData().then(responseData => {
            this.sensors = responseData;
            this.isLoading = false;
        });
    }

    pontClick() {
        // console.log('pontClick');
    }

    ngOnInit() {

        this.regionService.getRegionsCount().then(resData => {
            this.regionsCount = resData;
        });

        this.companyService.getCompaniesCount().then(resData => {
            this.companiesCount = resData;
        });

        this.sensorService.getMonitorsAndResultsCount().then(result => {
            this.monitorCount = result.monitorCount;
            this.monitorResultsCount = result.monitorResultsCount;
        });

        this.regionService.getRegionsSensors().then(result => {
            this.regionsNames = result;

            // load sensors with results
            this.loadSensors().then(() => {

                for (let i: number = 0; i < this.sensors.length; i++) {
                    let obj: any = [];
                    obj['date'] = this.sensors[i][0].date;

                    // regions
                    for (let c: number = 0; c < this.regionsNames.length; c++) {
                        obj[this.regionsNames[c].name] = 0;

                        for (let j: number = 0; j < this.sensors[i].length; j++) {
                            var element = this.sensors[i][j];

                            if (this.regionsNames[c].name == element.regionName) {
                                obj[this.regionsNames[c].name] = element.count;
                            }
                        }
                    }

                    this.sensorsChartData.push(obj);
                }

                this.addChart(this.sensorsChartData).then(() => {
                    this.clearAelement();
                });
            });
        }, error => {  });
    }

    clearAelement() {
        if (this.chartContainer.nativeElement.querySelector('a'))
            this.chartContainer.nativeElement.querySelector('a').remove();
    }

    ngOnDestroy() {
        if (this.chart)
            this.amChartsService.destroyChart(this.chart);

        this.regionService.dispose();
        this.sensorService.dispose();
    }

    addChart(data: any): Promise<void> {
        var graphs: any = [];

        for (let i: number = 0; i < this.regionsNames.length; i++) {
            graphs.push({
                "balloonText": "monitors results count: [[value]]",
                "bullet": "round",
                "title": this.regionsNames[i].name,
                "id": this.regionsNames[i].id,
                "valueField": this.regionsNames[i].name,
                "fillAlphas": 0,
                "lineThickness": 0,
                "numberFormatter": { precision: -1, decimalSeparator: '.', thousandsSeparator: '' },
            });
        }
        
        this.chart = this.amChartsService.makeChart(
            "chartdiv", {
            "type": "serial",
            "theme": "dark",
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": data,
            "valueAxes": [{
                "integersOnly": true,
                //"maximum": 1,
                "minimum": 1,
                "reversed": false,
                "axisAlpha": 0,
                "dashLength": 3,
                "gridCount": 10,
                "position": "left",
                "title": "Monitors results",
                "strictMinMax": false,
            }],
            //"startDuration": 0.5,
            "mouseWheelZoomEnabled": true,
            "graphs": graphs,
            "chartScrollbar": {
                "autoGridCount": true,
                "graph": "g1",
                "scrollbarHeight": 20,
                "gridAlpha": 0,
            },
            "chartCursor": {
                "cursorAlpha": 0,
                "zoomable": true,
                "oneBalloonOnly": true,
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "valueLineAlpha": 0.5,
                "fullWidth": true
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "fillAlpha": 0.05,
                "fillColor": "#000000",
                "gridAlpha": 0,
                "position": "bottom",
                //"parseDates": true,
                "axisColor": "#DADADA",
                "dashLength": 1,
                "minorGridEnabled": true,
            },
            "categoryAxesSettings": {
                "parseDates": true,
                "equalSpacing": false,
                "gridAlpha": 0,
                "axisAlpha": 0,
                "inside": false,
            },

            "dataSetSelector": {
                "position": "bottom"
            },
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
            });

        //this.chart.addListener("clickGraphItem", this._navigate); NEVER WRITE THIS AGAIN

        this.amChartsService.addListener(this.chart, "clickGraphItem", this._navigate);

        return new Promise<void>((resolve, reject) => {
            resolve();
        });
    }


    navigate(event: any) {
        //console.log(this.sensors);
        let minTime: any;

        for (let i = 0; i < this.sensors.length; i++) {

            for (let j = 0; j < this.sensors[i].length; j++) {

                if (this.sensors[i][j].id === event.graph.id &&
                    this.sensors[i][j].date === event.item.category) {
                    minTime = this.sensors[i][j].minDate;
                    break;
                }
            }

            if (minTime) {
                //console.log("break");
                break;
            }
        }

        this.router.navigate(["/navmenu/map", event.graph.id, minTime]);
    }
}
