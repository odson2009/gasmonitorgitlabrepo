import { Component, AfterViewInit, OnDestroy, ViewChild, Inject, OnInit, Injectable, Pipe, PipeTransform } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { EventService } from "../../Services/event.service";
import { RegionService } from '../../Services/region.service';
import { Router } from "@angular/router";
import { Globals } from "../../app.globals";
import { SensorEventService } from "../../Services/sensor-event.service";
import { RegionSensorsModel } from '../../Models/region.model';

@Component({
    selector: 'events-list',
    templateUrl: './events-list.component.html',
    styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent implements OnInit{
    private baseUrl: string;
    public sensorEvents: any;
    public toggleArr: any = [];

    public regionsNames: RegionSensorsModel[];
    public selectedRegionId: number = -1;
   
    constructor(
        private globals: Globals,
        private eventService: EventService,
        @Inject('BASE_URL') baseUrl: string,
        private regionService: RegionService,
        private sensorEventService: SensorEventService,
        private router: Router) {

        this.baseUrl = baseUrl;
    }

    ngOnInit() {
        this.eventService.startLoading();

        this.regionService.getRegionsSensors().then(result => {
            this.eventService.completeLoading();
            this.regionsNames = result;
        }, error => {
            this.eventService.completeLoading();
        });

        this.sensorEventService.getAllSensorsEvents().then(responseData => {
            this.sensorEvents = responseData;
        });
    }

    setToggleArr(i: number) {
        if (this.toggleArr.includes(i)) {
            this.toggleArr.splice(this.toggleArr.indexOf(i), 1);
        }
        else {
            this.toggleArr.push(i);
        }
    }

    deleteRegionSensorsEvents()
    {
        if (this.selectedRegionId > 0){
            this.sensorEventService.deleteRegionEvents(this.selectedRegionId).then(() => {
                this.sensorEventService.getAllSensorsEvents().then(responseData => {
                    this.sensorEvents = responseData;
                });
            });
        }
    }

    ngOnDestroy() {
        this.sensorEvents = [];
        this.toggleArr = [];
        this.regionService.dispose();
    }
}
