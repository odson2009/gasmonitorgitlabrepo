import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../Services/authentication.service';


@Component({
    selector: 'login-component',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService) { }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            login: ['', Validators.required],
            password: ['', Validators.required],
            rememberMe: new FormControl(true)
        });

        // reset login status
        // this.authenticationService.logout();
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.f.login.value, this.f.password.value, this.f.rememberMe.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/navmenu']);
                    this.loading = false;
                },
            error => {
                this.error = '';
                var parsedError = JSON.parse(error.error);
                if (parsedError.errors && parsedError.errors.length){
                    parsedError.errors.forEach(element => {
                            element.errors.forEach(item => {
                                this.error += item + ' ';
                            });
                        });
                    } else {
                    this.error = parsedError.message ? parsedError.message : 'Please try again later';
                    }
                    this.loading = false;
                });
    }
}