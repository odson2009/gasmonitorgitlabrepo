import { Component, ViewChild, OnInit, OnDestroy, ElementRef, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { } from 'googlemaps';

import { RegionService } from '../../Services/region.service';
import { RegionModel, AlgorithmModel } from '../../Models/region.model';
import { CompanyService } from '../../Services/company.service';

@Component({
    selector: 'region-action',
    templateUrl: './region-action.component.html',
    styleUrls: ['./region-action.component.scss']
})
export class RegionActionComponent implements OnInit, OnDestroy { 
    minZoom = 1; // 14;
    maxZoom = 22; // 22;
    mapType = 'satellite';
    @ViewChild('f')
    private form: any;
     // autocomplete control
    @ViewChild('search')
    searchElementRef: ElementRef;
    isEditMode = true;
    region: RegionModel;
    id: any;
    companyId: number;
    companies: any;
    isDrawRectangle = false;
    isRectangleCollapsed = false;
    rectangle: any = [];
    tmpRectangle: any = null;
    icon: any = {
        url: '../assets/images/circle.png',
        scaledSize: {
            width: 10,
            height: 10
        },
    };
    polygon: any;
    latlngBounds: any;

    autoCompleteLat = 0;
    autoCompleteLng = 0;
    autoCompleteZoom = 1;

    constructor(
        private regionService: RegionService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private companyService: CompanyService,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
    ) { }

    ngOnInit() {
        var self = this;
        // load Places Autocomplete

        this.isEditMode = this.router.url.includes('site/edit');
        this.activeRoute.params.subscribe(params => {
            this.id = params['id'];
            this.companyId = parseInt(params.companyId, 10);
        });

        if (this.isEditMode) {
            this.regionService.getRegion(this.id).then(resData => {
                this.region = resData;
                
                resData.coordinates.forEach(c => this.rectangle.push(c));

                this.isRectangleCollapsed = resData.coordinates.length > 0;
                setTimeout(()=>{
                    self.activateMap();
                },500);
            });
        } else {
            this.region = new RegionModel();
            this.region.name = '';
            this.region.email = '';
            this.region.phone = '';
            this.region.webSite = '';
            this.region.latitude = null;
            this.region.longitude = null;
            this.region.zoom = 2;
            if (this.companyId) {
                this.region.companyId = this.companyId;
            }
            this.region.algorithm = new AlgorithmModel();
            setTimeout(()=>{
                self.activateMap();
            },500);
        }

        this.companyService.getCompanies().then(resData => {
            this.companies = resData;
            this.companies = resData.map((company: any) => {
                return {
                    id: company.id,
                    name: company.name
                };
            });
        });
    }

    activateMap() {
        this.mapsAPILoader.load().then(() => {
            this.extendRegion();
            
            const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ['address']
            });

            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    // get the place result
                    const place: google.maps.places.PlaceResult = autocomplete.getPlace();

                    // verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }

                    // set latitude, longitude and zoom
                    this.autoCompleteLat = place.geometry.location.lat();
                    this.autoCompleteLng = place.geometry.location.lng();
                    this.autoCompleteZoom = 17;
                });
            });
        });
    }

    drawRectangle(lat, lng) {
        if (this.isDrawRectangle) {
            this.rectangle.push({ latitude: lat, longitude: lng });
            this.tmpRectangle = { latitude: lat, longitude: lng };
        }
    }

    extendRegion() {
        this.latlngBounds = new google.maps.LatLngBounds();
        const latLngArr = [];

        for (let i = 0; i < this.rectangle.length; i++) {
            latLngArr.push(new google.maps.LatLng(this.rectangle[i].latitude, this.rectangle[i].longitude));
            this.latlngBounds.extend(new google.maps.LatLng(this.rectangle[i].latitude, this.rectangle[i].longitude));
        }
        this.polygon = new google.maps.Polygon({ paths: latLngArr });
    }

    checkContains(lat: any, lng: any): boolean {
        const isContains = google.maps.geometry.poly.containsLocation(new google.maps.LatLng(lat, lng), this.polygon);
        return isContains;
    }

    clearRectangle() {
        this.rectangle = [];
        this.isDrawRectangle = true;
        this.isRectangleCollapsed = false;
        this.tmpRectangle = null;
        this.region.coordinates = [];
        this.polygon = null;
    }

    setMarkerInRegionCenter() {
        setTimeout(() => {
            this.region.latitude = this.latlngBounds.getCenter().lat();
            this.region.longitude = this.latlngBounds.getCenter().lng();
        }, 100);
    }

    mapClicked($event: MouseEvent) {
        const center: any = $event;
        this.drawRectangle(center.coords.lat, center.coords.lng);
    }

    collapseRegion(e: any) {
        this.isRectangleCollapsed = true;
        this.isDrawRectangle = false;
        this.rectangle.push({ latitude: this.rectangle[0].latitude, longitude: this.rectangle[0].longitude });
        this.extendRegion();
     
        for (let i = 0; i < this.rectangle.length; i++) {
            this.region.coordinates[i] = this.rectangle[i];
        }
        this.setMarkerInRegionCenter();
        e.preventDefault();
    }

    ngOnDestroy() {
        this.regionService.dispose();
    }

    save() {
        if (this.form.valid) {
            if (this.isEditMode) {
                this.regionService.updateRegion(this.region).then(() => {
                    this.navigate('/navmenu/sites');
                });
            } else {
                this.regionService.addRegion(this.region).then(() => {
                    this.navigate('/navmenu/sites');
                });
            }
        }
    }

    navigate(path: string) {
        setTimeout(() => {
            this.router.navigate([path]);
        }, 1000);
    }

    zoomChanged(event: any) {
        this.region.zoom = event;
    }

    markerMoved(event) {
        this.region.latitude = event.coords.lat;
        this.region.longitude = event.coords.lng;

        if (this.rectangle.length > 2) {
            if (this.checkContains(event.coords.lat, event.coords.lng) === false) {
                this.setMarkerInRegionCenter();
            }
        }
    }

    pointMoved(event: any, i: number) {
        // move collapsed points - 0 and last
        var lastIndex = this.region.coordinates.length - 1;
        if (i == 0) {
            this.region.coordinates[lastIndex].latitude = event.coords.lat;
            this.region.coordinates[lastIndex].longitude = event.coords.lng;
            this.rectangle[lastIndex].latitude = event.coords.lat;
            this.rectangle[lastIndex].longitude = event.coords.lng;
        }
        else if (i == lastIndex) {
            this.region.coordinates[0].latitude = event.coords.lat;
            this.region.coordinates[0].longitude = event.coords.lng;
            this.rectangle[0].latitude = event.coords.lat;
            this.rectangle[0].longitude = event.coords.lng;
        }

        this.region.coordinates[i].latitude = event.coords.lat;
        this.region.coordinates[i].longitude = event.coords.lng;
        this.rectangle[i].latitude = event.coords.lat;
        this.rectangle[i].longitude = event.coords.lng;
    }
}
