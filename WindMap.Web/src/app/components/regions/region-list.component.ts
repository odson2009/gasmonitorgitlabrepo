import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { RegionService } from "../../Services/region.service";
import { EventService } from "../../Services/event.service";
import { Router } from "@angular/router";
import { SensorService } from "../../services/sensor.service";
import { Globals } from "../../app.globals";
import { FileInputComponent } from '../../components/fileInput/fileInput.component';
import { FileUploader } from 'ng2-file-upload';
import { RegionModel } from '../../Models/region.model';
import { TokensService } from '../../Services/tokens.service';

@Component({
    selector: 'region-list',
    templateUrl: './region-list.component.html',
    styleUrls: ['./region-list.component.scss']
})
export class RegionListComponent implements OnInit {
    @ViewChild(FileInputComponent) fileInputComponent: FileInputComponent;
    private baseUrl: string;
    public data: RegionModel[];
    public toggleArr: number[] = [];

    dataFile: any;

    formGroup: any =
    {
        file: new FormControl('', [Validators.required]),
    };

    constructor(
        private globals: Globals,
        private eventService: EventService,
        @Inject('BASE_URL') baseUrl: string,
        private regionService: RegionService,
        private sensorService: SensorService,
        private tokenService: TokensService,
        private router: Router) {

        this.baseUrl = baseUrl;
    }

    ngOnInit() {
        this.loadRegions();
    }

    uploadFile() {
        let formData: FormData = new FormData();
        formData.append('file', this.fileInputComponent.uploader.queue[0]._file, this.fileInputComponent.uploader.queue[0]._file.name);

        this.fileInputComponent.uploader.queue[0].formData = formData;
        this.fileInputComponent.uploader.setOptions({
            authToken: 'Bearer ' + this.tokenService.getToken('access'),
            url: this.baseUrl + 'api/regions/file',
            isHTML5: true,
        });
        this.fileInputComponent.uploader.queue[0].onBeforeUpload = () => {
            this.eventService.startLoading();
        }
        this.fileInputComponent.uploader.queue[0].upload();
        this.fileInputComponent.uploader.queue[0].onComplete = (response: string, status: number, headers: any) => {
            if (status == 200) {
                this.eventService.showToast('success');
                this.loadRegions();
            }
            else {
                this.eventService.showToast('error', { _body: response, status: status });
            }

            this.eventService.stopLoading();
            this.fileInputComponent.uploader.queue[0].remove();
            this.fileInputComponent.inputValue = null;
        }
    }

    setToggleArr(i: number) {
        if (this.toggleArr.includes(i)) {
            this.toggleArr.splice(this.toggleArr.indexOf(i), 1);
        }
        else {
            this.toggleArr.push(i);
        }
    }

    loadRegions() {
        this.regionService.getRegions().then(resData => {
            this.data = resData;
        });
    }

    ngOnDestroy() {
        this.data = [];
        this.toggleArr = [];
        this.regionService.dispose();
        this.sensorService.dispose();
    }

    delete(region: any) {
        this.regionService.deleteRegion(region.id).then(result => {
            this.data.splice(this.data.indexOf(region), 1);
        });
    }

    deleteSensor(i: number, sensor: any) {
        this.sensorService.deleteSensor(sensor.id).then(result => {
            this.data[i].sensors.splice(this.data[i].sensors.indexOf(sensor), 1);
        });
    }

}
