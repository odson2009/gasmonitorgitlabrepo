﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../Services/authentication.service';
import { UserService } from '../../Services/user.service';
import { PermissionService } from '../../Services/permission.service';
import { PasswordValidation } from '../../Validation/password.validation';

@Component({
    selector: 'changePassword.component',
    templateUrl: 'changePassword.component.html',
    styleUrls: ['changePassword.component.scss']
})

export class ChangePasswordComponent implements OnInit {

    changePasswordForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private authenticationService: AuthenticationService,
                private userService: UserService,
                private permissionService: PermissionService) { }

    ngOnInit() {
        this.changePasswordForm = this.formBuilder.group({
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            confirmPassword: ['', Validators.required]
        }, {
                validator: PasswordValidation.validate.bind(this)
        });
    }

    get f() { return this.changePasswordForm.controls; }

    onSubmit() {


        this.submitted = true;

        if (this.changePasswordForm.invalid) {
            return;
        }

        if (this.f.password.value == this.f.confirmPassword.value) {
            this.loading = true;

            this.userService.changePassword(this.f.password.value).then(() => {
                let login = this.permissionService.getUserLogin();
                //reset login status
                this.authenticationService.deleteToken();

                // new authentication
                this.authenticationService.login(login, this.f.password.value, true)
                    .subscribe(
                        data => {
                            this.router.navigate(['/navmenu']);
                            this.loading = false;
                        },
                        error => {
                            this.error = '';
                            if (error.error.errors && error.error.errors.length) {
                                error.error.errors.forEach(element => {
                                    element.errors.forEach(item => {
                                        this.error += item + ' ';
                                    });
                                });
                            } else {
                                this.error = error.error.message ? error.error.message : 'Please try again later';
                            }
                            this.loading = false;
                        });
            });
        }
        else {
            this.error = "Those passwords didn't match. Try again";
        }
    }
}
