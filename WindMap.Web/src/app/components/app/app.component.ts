import { Component } from '@angular/core';
import { ToasterConfig } from 'angular2-toaster';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    public toasterconfig: ToasterConfig = new ToasterConfig({
        limit: 5,
        tapToDismiss: false,
        showCloseButton: false,
        mouseoverTimerStop: true,
        timeout: 1500,
        positionClass: 'toast-top-right'
    });
}
