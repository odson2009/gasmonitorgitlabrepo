import { Component, ViewChild, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { UserService } from "../../Services/user.service";
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { UserModel } from "../../Models/user.model";
import { CompanyService } from "../../Services/company.service";
import { ChangeDetectorRef } from '@angular/core';
import {isEqual, cloneDeep} from 'lodash';
import { PermissionService } from '../../Services/permission.service';
import { fail } from 'assert';

@Component({
    selector: 'user-action',
    templateUrl: './user-action.component.html',
    styleUrls: ['./user-action.component.scss']
})
export class UserActionComponent implements OnInit, OnDestroy {
    selectAll = {
        uiElementsId: false,
        companyObjs: false,
        regions: false
    };
    @ViewChild('f') private form: any;
    isEditMode: boolean = true;
    defaultUserData: UserModel;
    user: UserModel;
    id: number = null;
    roles = [];
    uiElementsId = [];
    companyObjs :any;
    activeCompanyIndex: number = null;
    showButton = false;
    isAdmin: boolean = false;
    availableCreateEditUser: boolean = false;
    availableRoles: boolean = false;

    constructor(private userService: UserService,
                private activeRoute: ActivatedRoute,
                private router: Router,
                private companyService:CompanyService,
                private cdRef: ChangeDetectorRef,
                private permissionService: PermissionService) {
    }

    ngAfterViewChecked()
    {
        this.showButton = true;
        this.cdRef.detectChanges();
    }
    
    ngOnInit() {
        this.isEditMode = this.router.url.includes('user/edit');
        this.activeRoute.params.subscribe(params => {
            this.id = params['id'];
        });

        let isCurrentUser: boolean = (this.id ? this.id.toString() : null) == this.permissionService.getUserId();

        let userRole = this.permissionService.getRole()
        if (userRole == "Administrator") {
            this.isAdmin = true;
            this.availableCreateEditUser = true;
        }
        else if (userRole == "Owner")
            this.availableCreateEditUser = true;

        this.userService.getRoles(isCurrentUser, this.isEditMode).then(resData => {
            this.roles = resData;

            if (isCurrentUser && resData && resData.length == 1)
                this.availableRoles = false;

            if (this.isAdmin)
                this.availableRoles = true;
        });

        this.userService.getPages().then(resData => {
            this.uiElementsId = resData;
            if (!this.isEditMode) {
                this.uiElementsId[0]['selected'] = true;
                this.uiElementsId[1]['selected'] = true;
                this.user.uiElementsId = [this.uiElementsId[0].id,this.uiElementsId[1].id];
            }
        });

        if (this.isEditMode) {
            this.userService.getUser(this.id).then(resData => {
                this.user = resData;
                this.user.isActive = !this.user.isActive;
                this.defaultUserData = cloneDeep(this.user);
                if (this.companyObjs && this.companyObjs.length) {
                    this.mapUserData();
                }
            });
        }
        else {
            this.user = new UserModel();
            this.user.login = '';
            this.user.email = '';
            this.user.firstName = '';
            this.user.lastName = '';
            this.user.isActive = false;
            this.user.companyObjs = [];

            this.availableRoles = true;
        }

        this.companyService.getCompanies().then(resData => {
            this.companyObjs = resData;

            if (this.companyObjs && this.companyObjs.length) 
                this.activeCompanyIndex = 0;

            this.companyObjs = resData.map((company:any)=>{
                return {
                    id: company.id,
                    name: company.name,
                    regions: company.regions.map((region:any)=>{
                        return {
                            id: region.id,
                            name: region.name
                        }
                    })
                }
            });

            if (this.user && this.isEditMode){
                this.mapUserData();
            }
        });
    }



    mapUserData(){
        this.user.uiElementsId.forEach(element => {
            this.uiElementsId.some(item => {
                if(item.id == element){
                    item['selected'] = true;

                    return true;
                }
            })
        });

        this.user.companyObjs.forEach(element => {
            this.companyObjs.some(item => {
                if(item.id == element.id){
                    item['selected'] = true;

                    element.regionsId.forEach(regionId => {
                        item.regions.some(region => {
                            if(regionId == region.id){
                                region['selected'] = true;
                                return true;
                            }
                        });
                    });

                    return true;
                }
            })
        });
    }

    ngOnDestroy() {
        this.userService.dispose();
    }

    save() {
        if (this.form.valid) {
            let data = cloneDeep(this.user);
            data.isActive = !data.isActive;

            if (this.isEditMode) {
                this.userService.updateUser(data).then(() => {
                    this.navigate('/navmenu/users');
                });
            }
            else {
                this.userService.addUser(data).then(() => {
                    this.navigate('/navmenu/users');
                });
            }
        }
    }

    resetPassword() {
        if (this.isAdmin && this.isEditMode) {
            this.userService.resetPassword(this.id).then((data) => {
                this.navigate('/navmenu/users');
            });
        }
    }
    navigate(path: string) {
        setTimeout(() => {
            this.router.navigate([path]);
        }, 1000);
    }

    selectAllPages(val){
        this.user.uiElementsId = [];
        this.uiElementsId.forEach((item)=>{
            item.selected = val;
            if(val){
                this.user.uiElementsId.push(item.id);
            }
        });
    }
    pageSelect(val:string, state:boolean){   
        if (state){
            this.user.uiElementsId.push(parseInt(val));
        } else {
            let index = this.user.uiElementsId.indexOf(parseInt(val));
            this.user.uiElementsId.splice(index, 1);
        }
        if (this.user.uiElementsId.length == this.uiElementsId.length){
            this.selectAll.uiElementsId = true;
        } else {
            this.selectAll.uiElementsId = false;
        }
    }

    selectAllCompanies(val){
        this.companyObjs.forEach((item)=>{
            item.selected = val;
            if(val){
                if (!this.user.companyObjs.length){
                    this.user.companyObjs.push({id:item.id,regionsId:[]});
                } else {
                    let isContains = false;
                    this.user.companyObjs.some(element => {
                        if (element.id == item.id){
                            isContains = true;
                            return true;
                        }
                    });
                    if (!isContains){
                        this.user.companyObjs.push({id:item.id,regionsId:[]});
                    }
                }
            }
        });

        if (!val){
            this.user.companyObjs = [];
            this.companyObjs.forEach((item)=>{
                item.regions.forEach(element => {
                    element.selected = false;
                });
            })
            this.activeCompanyIndex = null;
        }
    }
    companySelect(val:string, state:boolean, index){   
        if (state){
            this.user.companyObjs.push({id:parseInt(val),regionsId:[]});
        } else {
            this.user.companyObjs.some((item:any, i)=>{
                if (item.id == parseInt(val)){
                    if (this.companyObjs[index].id == parseInt(val)){
                        this.activeCompanyIndex = null;
                    }
                    this.companyObjs[index].regions.forEach(element => {
                        element['selected'] = false;
                    });
                    this.selectAll.regions = false;
                    this.user.companyObjs.splice(i, 1);
                    return true;
                }
            })
        }
        if (this.user.companyObjs.length == this.companyObjs.length){
            this.selectAll.companyObjs = true;
        } else {
            this.selectAll.companyObjs = false;
        }
    }

    showRegions(i, company){
        if (company.selected){
            this.activeCompanyIndex = i;
            let selectedAll = true;
            company.regions.some(element => {
                if (!element['selected']){
                    this.selectAll.regions = false;
                    selectedAll = false;

                    return true;
                }
            });
            if (selectedAll){
                this.selectAll.regions = true;
            }
        }
    }

    selectAllRegions(val, company){
        this.companyObjs.forEach((item)=>{
            if (item.id == company.id){
                this.user.companyObjs.forEach(element=>{
                    if (element.id == company.id){
                        element.regionsId = [];
                        item.regions.forEach(region => {
                            region.selected = val;
                            if (val){
                                element.regionsId.push(region.id)
                            }
                        });
                    }
                });
            }
        });
    }

    regionSelect(val:string, state:boolean, company){   
        this.user.companyObjs.forEach(element => {
            if (element.id == company.id){
                if (state){
                    element['regionsId'].push(parseInt(val));
                } else {
                    let index = element['regionsId'].indexOf(parseInt(val));
                    element['regionsId'].splice(index, 1);
                }
                if (element.regionsId.length == company.regions.length){
                    this.selectAll.regions = true;
                } else {
                    this.selectAll.regions = false;
                }
            }
        });
    }


    get disabledSubmit(){
        if (this.form){
            if (this.isEditMode && isEqual(this.user, this.defaultUserData)){
                return true;
            }

            let regionsValid = true;

            if (this.user.companyObjs.length){
                this.user.companyObjs.some(element => {
                   if(!element.regionsId || !element.regionsId.length){
                        regionsValid = false;
                        return true;
                   }
                });
            }
            return this.form.invalid || !this.user.uiElementsId || !this.user.uiElementsId.length || !this.user.companyObjs || !this.user.companyObjs.length || !regionsValid
        }
        
        return true;
    }
}
