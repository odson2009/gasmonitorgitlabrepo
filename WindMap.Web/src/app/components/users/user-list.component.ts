import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { UserService } from "../../Services/user.service";
import { Router } from "@angular/router";
import { UserModel } from '../../Models/user.model';
import { PermissionService } from '../../Services/permission.service';

@Component({
    selector: 'user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    private baseUrl: string;
    public data: UserModel[];
    public availableCreateUser: boolean;

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private userService: UserService,
        private router: Router,
        private permissionService: PermissionService) {

        this.baseUrl = baseUrl;
    }

    ngOnInit() {
        this.loadUsers();
        if (this.permissionService.getRole() != "Service_Provider")
            this.availableCreateUser = true;
        else
            this.availableCreateUser = false;
    }

    loadUsers() {
        this.userService.getUsers().then(resData => {
            this.data = resData;
        });
    }

    ngOnDestroy() {
        this.data = [];
        this.userService.dispose();
    }
}
