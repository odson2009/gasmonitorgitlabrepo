import { Component, Inject, OnDestroy, OnInit } from '@angular/core';

import { HttpClient } from "@angular/common/http";
//chart
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";

import { SensorResult, Sensor, AccumulatedAproximation } from '../../Models/algorithm.model';
import { AlgorithmService } from '../../Services/algorithm.service';

//timer panel
import 'rxjs/Rx';
import { TimerObservable } from "rxjs/observable/TimerObservable";


@Component({
    selector: 'algorithms',
    templateUrl: './algorithms.component.html',
    styleUrls: ['./algorithms.component.scss']
})

export class AlgorithmsComponent implements OnInit, OnDestroy {
    private baseUrl: string;

    downloadLink: string;

    chart: AmChart;
    chartData: any = [];
    minI: number = -100;
    maxI: number = 100;

    clearChart: boolean = true;
    isBuildChart: boolean = false;
    sourceFromDB: boolean = false;

    timerSubscription: any;
    alive: boolean;
    timerInterval: number = 0;
    activeButton: number = 0;
    timerLimitStep: number;
    currentTimerDate: Date;
    maxTimerDate: Date;

    numberOfNewResults: number = 60;
    numberOfSkipResults: number = 0;
    minNewResultsCount: number = 12;
    monitorTabIndex: number = 0;
    sensors: Sensor[] = [];

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        private algorithmService: AlgorithmService,
        private amChartsService: AmChartsService,
    ) {
        this.baseUrl = baseUrl;
    }

    ngOnInit(): void {

        this.bindInitialResults();
        this.algorithmService.clearServerCache();
    }

    //load results from file
    bindInitialResults() {
        if (this.sourceFromDB) {

            this.algorithmService.getSensorResultsFromDB().then(results => {
                this.resolveData(results);
            });
        }
        else {

            this.algorithmService.getSensorResultsFromFile().then(results => {
                this.resolveData(results);
            });
        }
    }

    resolveData(results: SensorResult[][]) {
        if (results && results.length > 0) {

            this.sensors = [];

            for (let i = 0, len = results.length; i < len; i++) {

                let sensor = new Sensor();
                sensor.allResults = results[i];

                this.sensors.push(sensor);
            }

            this.calculateDataForTimer();

        }
    }

    addNewResults() {

        for (let i = 0, len = this.sensors.length; i < len; i++) {

            let sensor = this.sensors[i];
            let step = this.numberOfSkipResults + sensor.step;

            var selectedResults = sensor.allResults.slice(step, step + this.numberOfNewResults);

            if (selectedResults.length === 0)
                continue;

            sensor.resultsToSend = sensor.resultsToSend.concat(selectedResults);
            sensor.step += this.numberOfSkipResults + this.numberOfNewResults;
        }

        this.numberOfNewResults = 12;
        this.numberOfSkipResults = 0;
        this.calculateDataForTimer();
    }

    calculateDataForTimer() {

        let firstSensorResults = this.sensors[0].allResults.slice(this.sensors[0].step, this.sensors[0].allResults.length);
        let secondSensorResults = this.sensors[1].allResults.slice(this.sensors[1].step, this.sensors[1].allResults.length);

        if (firstSensorResults[0] && secondSensorResults[0]) {

            this.currentTimerDate = new Date(
                Math.min(
                    new Date(firstSensorResults[0].timeEnd).getTime(),
                    new Date(secondSensorResults[0].timeEnd).getTime()) - 1000);


            this.maxTimerDate = new Date(
                Math.max(
                    new Date(firstSensorResults[firstSensorResults.length - 1].timeEnd).getTime(),
                    new Date(secondSensorResults[secondSensorResults.length - 1].timeEnd).getTime()) + 1000);
        }
    }

    //complex tests for fminsearch and polyfit methods
    sendResults() {

        var tasks = [];

        for (let i = 0; i < this.sensors.length; i++) {
            let index = i;

            if (this.sensors[index].resultsToSend.length > 0) {
                tasks.push(this.algorithmService.sendResultsRequest(this.sensors[index].resultsToSend).then(response => {
                    if (response) {
                        this.sensors[index].requestedResult = response;
                        this.processResponse(index);
                    }
                }).catch(error => { }));
            }
        }


        Promise.all(tasks).then(e => {

            if (this.clearChart) {
                this.chartData = [];
            }

            this.myGaussian();
            this.drawGaussianChart();
        });
    }

    //complex tests for fminsearch and polyfit methods
    sendResult(resultToSend: SensorResult, sensorId: number) {

        this.algorithmService.sendResultRequest(resultToSend).then(response => {
            if (response) {
                this.sensors[sensorId].requestedResult = response;
                this.processResponse(sensorId);
            }
        }).catch(error => { });
    }

    processResponse(sensorId: number) {

        let sensor = this.sensors[sensorId];

        sensor.resultsToSend = [];

        //accumulate aproximation for last table
        if (sensor.requestedResult.isLeak) {

            let aproximation = new AccumulatedAproximation();
            aproximation.step = sensor.step;
            aproximation.endDate =
                sensor.requestedResult
                    .processedSensorResults[sensor.requestedResult.processedSensorResults.length - 1].timeEnd;

            aproximation.fminsearchAproximation = sensor.requestedResult.smoothingResult.fminsearchAproximation;
            aproximation.fminsearchAproximationAverage = sensor.requestedResult.smoothingResult.fminsearchAproximationAverage;
            aproximation.polyfitAproximation = sensor.requestedResult.smoothingResult.polyfitAproximation;
            aproximation.polyfitAproximationAverage = sensor.requestedResult.smoothingResult.polyfitAproximationAverage;

            sensor.accumulatedAproximations.push(aproximation);
        }
    }


    myGaussian() {

        for (let i = 0; i < this.sensors.length; i++) {
            let index = i;

            let solution = this.sensors[index].requestedResult && this.sensors[index].requestedResult.smoothingResult;
            if (!solution)
                continue;;

            let fittedFminsearch: number;
            let fittedPolyfit: number;



            for (let k = 0; k < solution.groupedMovingAverage.length; k++) {

                let obj = {};
                obj["sensor_" + i + "_twd"] = solution.groupedMovingAverage[k].averageData.twd;
                obj["sensor_" + i + "_pf"] = solution.groupedMovingAverage[k].averageData.pf;


                this.chartData.push(obj);

            }

            for (let j = this.minI; j <= this.maxI; j++) {

                //use for fminsearch

                fittedFminsearch = solution.fminsearchAproximationAverage.a /
                    (Math.sqrt(2 * Math.PI) * solution.fminsearchAproximationAverage.sigma) *
                    Math.exp(-.5 *
                        (Math.pow(solution.fminsearchAproximationAverage.mu - j, 2)) /
                        (Math.pow(solution.fminsearchAproximationAverage.sigma, 2)));

                if (!isNaN(fittedFminsearch)) {
                    this.chartData.push(index == 0
                        ? { f_1001_x: j, f_1001_y: fittedFminsearch }
                        : { f_1002_x: j, f_1002_y: fittedFminsearch });
                }
                //use for polyfit

                //new from myGaussian func: y=A * exp( -(x-mu)^2 / (2*sigma^2) )
                //use for polyfit
                fittedPolyfit = solution.polyfitAproximationAverage.a *
                    Math.exp(-Math.pow((j - solution.polyfitAproximationAverage.mu), 2) /
                        (2 * Math.pow((solution.polyfitAproximationAverage.sigma), 2)));

                if (!isNaN(fittedPolyfit)) {
                    this.chartData.push(index == 0
                        ? { p_1001_x: j, p_1001_y: fittedPolyfit }
                        : { p_1002_x: j, p_1002_y: fittedPolyfit });
                }
            }
        }
    }


    drawGaussianChart() {

        this.chart = this.amChartsService.makeChart("chartdiv",
            this.algorithmService.getChartConfiguration(this.chartData));

    }

    generateFile() {
        this.downloadLink = null;

        this.algorithmService.generateResultFileRequest(this.sensors)
            .then((response) => {
                this.downloadLink = '/api/sensors/results/getFile?fileName=' + response.fileName;
            }).catch();
    }

    startTimer(interval: number) {
        if (this.alive && this.timerInterval == interval)
            return;

        this.timersUnsubscribe();
        this.timerInterval = interval;
        this.alive = true;

        //this.timeStep = (butn === 8) ? (this.timeStep > interval ? interval : this.timeStep / 2) : interval
        this.timerSubscription = TimerObservable.create(0, interval)
            .takeWhile(() => this.alive)
            .subscribe(() => {

                if ((this.timerLimitStep && this.timerLimitStep <= Math.max(this.sensors[0].step, this.sensors[1].step))
                    || this.currentTimerDate.getTime() > this.maxTimerDate.getTime()) {
                    this.stopTimer();
                }
                else {
                    //get results for 1001 and 1002 monitor
                    let firstSensorResult = this.sensors[0].allResults.find(x => new Date(x.timeEnd).getTime() == this.currentTimerDate.getTime());
                    let secondSensorResult = this.sensors[1].allResults.find(x => new Date(x.timeEnd).getTime() == this.currentTimerDate.getTime());

                    //add in newResults buffer
                    if (firstSensorResult) {
                        this.sensors[0].resultsToSend.push(firstSensorResult);
                        this.sensors[0].step++;
                        this.sendResult(firstSensorResult, 0);
                    }
                    if (secondSensorResult) {
                        this.sensors[1].resultsToSend.push(secondSensorResult);
                        this.sensors[1].step++;
                        this.sendResult(secondSensorResult, 1);
                    }
                }

                this.currentTimerDate.setSeconds(this.currentTimerDate.getSeconds() + 1);
            });
    }

    stopTimer() {
        //activate pause button
        this.activeButton = 0;
        this.alive = false;
        this.timerInterval = 0;
        this.timersUnsubscribe();
    }

    timersUnsubscribe() {
        if (this.timerSubscription)
            this.timerSubscription.unsubscribe();
    }

    refreshResults() {
        this.stopTimer();
        this.activeButton = 0;

        this.clearData();
        this.algorithmService.clearServerCache();
        this.bindInitialResults();

        this.numberOfNewResults = 60;
    }

    ngOnDestroy() {
        this.clearData();
        this.timersUnsubscribe();
        //this.alive = false;
    }

    clearData() {
        if (this.chart)
            this.amChartsService.destroyChart(this.chart);

        this.chartData = [];
        this.sensors = [];
    }

    setMonitorTabIndex(index: number) {
        this.monitorTabIndex = index;
    }

    removeMonitorsResults() {
        console.warn('Not implemented yeat!!!!');
    }
}