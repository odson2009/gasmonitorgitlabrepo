import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';

let index = 0;

@Component({
  selector: 'file-input',
  templateUrl: './fileInput.component.html',
  styleUrls: ['./fileInput.component.scss'],
  host: { 'style': 'width:100%' }
})

export class FileInputComponent {
    //generate unique id for input with label
    id = "file-upload" + index++;
    
    public uploader:FileUploader = new FileUploader({ isHTML5: true });

    dateInput: boolean = false;

    class: string = "column";
    @Input() set sizeClass(data:string) {
        if (data)
            this.class = data;
    }

    @Input() inputValue: any;
    @Output() inputValueChange = new EventEmitter();
    @Output() uploadComplete = new EventEmitter();
    @Input() buttonText: string;
    @Input() types: string;
    @Input() count: number;
    @Input() errorMsg: string;
    @Input() isImage: boolean ;
    firstFocus: boolean = true;
    isValid: boolean = false;
    FieldIsValid = new FormControl('');
    fileTypeError: boolean = false;
    filesErrors: number = 0;
    @Input() set fieldIsValid(val: any) {
        if (val)
            this.FieldIsValid = val;
    };

    IsInputValid(): boolean {
        this.firstFocus = false;
        if (this.inputValue) {
            this.isValid = true;
        }
        else {
            this.isValid = false;
        }

        return this.isValid && !this.fileTypeError;
    }

    change(event: any) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            this.inputValue = file;
            if (this.inputValue) this.isValid = true;

            switch (file.type) {
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                case 'application/vnd.ms-excel': {
                    if (this.isImage) {
                        this.isValid = false;
                        this.fileTypeError = true;
                    }
                    else this.fileTypeError = false;
                }
                break;
                case 'image/jpeg':
                case 'image/jpg':
                case 'image/bmp':
                case 'image/png':
                    this.fileTypeError = false;
                    break;
                default:
                    this.isValid = false;
                    this.fileTypeError = true;
                    break;
            }

            this.inputValueChange.emit(this.inputValue);
        }

        //clear input selected file to allow reselect old file
        event.srcElement.value = '';
    }

    get shouldShowTypeError(): boolean {
        return this.fileTypeError;
    }

    private units = [
        'bytes',
        'Kb',
        'Mb',
        'Gb',
        'Tb',
        'Pb'
    ];

    transform(bytes: number = 0, precision: number = 0): string {
        if (isNaN(parseFloat(bytes.toString())) || !isFinite(bytes))
        {
            return '?';
        }

        let unit = 0;

        while (bytes >= 1024) {
            bytes /= 1024;
            unit++;
        }

        return bytes.toFixed(+ precision) + ' ' + this.units[unit];
    }

    remove(item: any) {
        item.remove();

        if (this.uploader.queue.length == 0)
        {
            this.inputValue = '';
            this.fileTypeError = false;
        }
    }
    constructor() {
    }

    uploadFiles(url: string) {
        this.uploader.setOptions({
            url: url,
            isHTML5: true,
        });

        let i = 1;
        let count = this.uploader.queue.length;

        this.uploader.queue.forEach((item: any) => {
            let formData: FormData = new FormData();
            formData.append('file', item._file, item._file.name);
            item.formData = formData;
            item.upload();
            item.onComplete = (response: string, status: number, headers: any) => {
                if (JSON.parse(response).success && status == 200) {
                    if (i == count) {
                        this.uploadComplete.emit({ status: 'success' })
                    }
                }
                else {
                    this.uploadComplete.emit({ status: 'error', response: response, statusNumber: status });
                    this.filesErrors++;
                }
                i++;
            }
        })
    }
}