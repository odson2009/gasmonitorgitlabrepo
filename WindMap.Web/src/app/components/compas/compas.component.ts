import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'compas',
    templateUrl: './compas.component.html',
    styleUrls: ['./compas.component.scss']
})
export class CompasComponent {
    _deg: number;

    @Input()
    set deg(val) {
        this._deg = -180 + val;
    }

    get deg() {
        return this._deg;
    }
}
