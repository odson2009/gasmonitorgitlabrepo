import { Component, OnInit } from '@angular/core';
import { PermissionService } from '../../Services/permission.service';
import { AuthenticationService } from '../../Services/authentication.service';
import { LogoModel } from '../../Models/logo.model';
import { LogoService } from '../../Services/logo.service';
import { EventBusService } from '../../Services/event-bus.service';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.scss']
})
export class NavMenuComponent implements OnInit {
    permissions: Array<string>;
    permissionService: PermissionService;
    authService: AuthenticationService
    logoService: LogoService;
    userLogin: string = '';
    logos: LogoModel[];
    defaultlogo: LogoModel;
    isRelatedToDefaultCompany: boolean = false;
    isLoading: boolean = true;
    subsEBS = []; // subscriptions of bus service

    constructor(
        private _permissionService: PermissionService,
        private _authService: AuthenticationService,
        private _logoService: LogoService,
        private _eventBusService: EventBusService
    )
    {
        this.permissionService = _permissionService;
        this.authService = _authService;
        this.logoService = _logoService;
    }

    ngOnInit(): void {
        this.permissions = this.permissionService.getPermission();
        this.userLogin = this.permissionService.getUserLogin();

        this.logoLoader();

        this.subsEBS.push(this._eventBusService.subscribe('LOGO_UPDATED', this.logoLoader));
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        this.subsEBS.forEach(s => {
            this._eventBusService.remove(s);
        });
    }

    canShow(page: string): boolean {
        if (this.permissions != null && this.permissions.indexOf(page) != -1)
            return true;
        else return false;
    }

    logout() {
        this.authService.logout();
    }

    logoLoader = () => {
        this.logoService.getLogos(this.permissionService.getUserId()).then(result => {
            this.logos = result;
            this.isRelatedToDefaultCompany = this.logos.length == 1;
            this.defaultlogo = new LogoModel();

            this.logos.forEach((logo) => {
                if (logo.isDefault) {
                    this.defaultlogo = logo;
                    this.logos.splice(this.logos.indexOf(logo), 1);
                }
            });

            this.isLoading = false;
        });
    }
}
