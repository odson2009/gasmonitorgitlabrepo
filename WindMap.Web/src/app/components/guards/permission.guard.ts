﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PermissionService } from '../../Services/permission.service';

@Injectable()
export class PermissionGuard implements CanActivate  {
    permissions: Array<string>;
    passwordStatus: boolean;

    constructor(private router: Router, private _permissionService: PermissionService) {  }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.permissions = this._permissionService.getPermission();
        this.passwordStatus = this._permissionService.getPasswordStatus();


        if (!this.permissions) {
            this.router.navigate(['/navmenu/default']);
        }

        if (!this.passwordStatus) {
            this.router.navigate(['/navmenu/changepassword']);
        }

        let permission = route.data["permission"] as string;
        if (this.canShow(permission)) {
            return true;
        }
        else {
            if (this.permissions != null && this.permissions.length > 0) {
                var path = '/navmenu/' + this.permissions[0].toLowerCase();
                this.router.navigate(['/navmenu/' + this.permissions[0].toLowerCase()]);
            }
            else
                this.router.navigate(['/navmenu/default']);
        }

        return false;
    }
    canShow(page: string): boolean {
        if (this.permissions != null) {
            if (this.permissions.indexOf(page) != -1)
                return true;
            else return false;
        }
        return false;
    }
}