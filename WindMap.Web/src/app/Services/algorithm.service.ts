﻿import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { SensorDetailModel } from "../Models/sensor.model";
import { SensorResult, RequestedResult, Sensor } from "../Models/algorithm.model";
import { EventService } from "./event.service";

@Injectable()

export class AlgorithmService {
    _httpService: any;
    private baseUrl: string;

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        private eventService: EventService
    ) {
        this.baseUrl = baseUrl;
    }

    clearServerCache() {
        return new Promise(resolve => {
            this.http.get(this.baseUrl + 'api/results/refresh').subscribe(
                response => {
                    resolve(response);
                },
                error => {
                    resolve(error);
                });
        });
    }

    generateResultFileRequest(sensors: Sensor[]): Promise<any> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.http.post(this.baseUrl + 'api/sensors/results/generateFile', sensors).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    getSensorResultsFromDB(): Promise<any> {
        return new Promise(resolve => {
            this.http.get<SensorResult[][]>(this.baseUrl + 'api/results', {
                params: new HttpParams().set('ids', '1,2')
            }).subscribe(
                response => {
                    resolve(response);
                },
                error => {
                    resolve(error);
                });
        });
    }

    getConcentrationDuring14Days(regionId: number): Promise<any> {
        return new Promise(resolve => {
            this.http.get<any>(this.baseUrl + 'api/regions/region/averageConcentration', {
                params: new HttpParams().set('id', regionId.toString())
            }).subscribe(
                response => {
                    resolve(response);
                },
                error => {
                    resolve(error);
                });
        });
    }

    getSensorResultsFromFile(): Promise<any> {
        return new Promise(resolve => {
            this.http.get<SensorResult[][]>('algorithmResults.json').subscribe(
                response => {
                    resolve(response);
                },
                error => {
                    resolve(error);
                });
        });
    }

    sendResultsRequest(resultsToSend: SensorResult[]): Promise<RequestedResult> {
        return new Promise((resolve, error) => {
            this.http.post<RequestedResult>(this.baseUrl + 'api/results/range', resultsToSend).subscribe(
                response => {
                    resolve(response);
                },
                responseError => {
                    error(responseError);
                });
        });
    }

    sendResultRequest(resultToSend: SensorResult): Promise<RequestedResult> {
        return new Promise((resolve, error) => {
            this.http.post<RequestedResult>(this.baseUrl + 'api/results', resultToSend).subscribe(
                response => {
                    resolve(response);
                },
                responseError => {
                    error(responseError);
                });
        });
    }

    getChartConfiguration(chartData: any) {
        return {
            "type": "xy",
            "theme": "light",
            "autoMarginOffset": 20,
            "dataProvider": chartData,
            "legend": {
                "useGraphSettings": true
            },
            "valueAxes": [{
                "position": "bottom",
                "axisAlpha": 0,
                "dashLength": 1,
                "title": "TWD Axis"
            },
            {
                "axisAlpha": 0,
                "dashLength": 1,
                "position": "left",
                "title": "PF Axis"
            }
            ],
            "startDuration": 0,
            "graphs": [
                //Average results
                {
                    "title": "Average results 1001",
                    "balloonText": "TWD:[[sensor_0_twd]] PF:[[sensor_0_pf]]",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletSize": 8,
                    "lineAlpha": 0,
                    "xField": "sensor_0_twd",
                    "yField": "sensor_0_pf",
                    "bulletColor": "#FFFFFF",
                    "lineColor": "#20e413",
                    "fillAlphas": 0,
                    "useLineColorForBulletBorder": true,
                },
                {
                    "title": "Average results 1002",
                    "balloonText": "TWD:[[sensor_1_twd]] PF:[[sensor_1_pf]]",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletSize": 8,
                    "lineAlpha": 0,
                    "xField": "sensor_1_twd",
                    "yField": "sensor_1_pf",
                    "bulletColor": "#FFFFFF",
                    "lineColor": "#e01eec",
                    "fillAlphas": 0,
                    "useLineColorForBulletBorder": true,
                },
                //for gaussian by fminsearh
                {
                    "title": "Gaussian (fminsearch) 1001",
                    "balloonText": "x:[[f_1001_x]] y:[[f_1001_y]]",
                    "bullet": "round",
                    "bulletSize": 2,
                    "xField": "f_1001_x",
                    "yField": "f_1001_y",
                    "lineColor": "#ff0000",
                    "fillAlphas": 0,
                    "lineThickness": 1,
                    "type": "line",
                },
                {
                    "title": "Gaussian (fminsearch) 1002",
                    "balloonText": "x:[[f_1002_x]] y:[[f_1002_y]]",
                    "bullet": "round",
                    "bulletSize": 2,
                    "xField": "f_1002_x",
                    "yField": "f_1002_y",
                    "lineColor": "#21ec1e",
                    "fillAlphas": 0,
                    "lineThickness": 1,
                    "type": "line",
                },
                //for gaussian by polyfit
                {
                    "title": "Gaussian (polyfit) 1001",
                    "balloonText": "x:[[p_1001_x]] y:[[p_1001_y]]",
                    "bullet": "round",
                    "bulletSize": 2,
                    "xField": "p_1001_x",
                    "yField": "p_1001_y",
                    "lineColor": "#074afb",
                    "fillAlphas": 0,
                    "lineThickness": 1,
                    "type": "line",
                },
                {
                    "title": "Gaussian (polyfit) 1002",
                    "balloonText": "x:[[p_1002_x]] y:[[p_1002_y]]",
                    "bullet": "round",
                    "bulletSize": 2,
                    "xField": "p_1002_x",
                    "yField": "p_1002_y",
                    "lineColor": "#1ecfec",
                    "fillAlphas": 0,
                    "lineThickness": 1,
                    "type": "line",
                }
            ],

            "marginLeft": 64,
            "marginBottom": 60,
            "chartScrollbar": {},
            "chartCursor": {},
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        }
    }

}