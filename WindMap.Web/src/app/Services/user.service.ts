import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import { UserModel } from "../Models/user.model";
import { EventService } from "../Services/event.service";
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { PermissionService } from './permission.service';

@Injectable()
export class UserService {
    private baseUrl: string;
    private subscription: any;

    constructor( @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        private eventService: EventService,
        private permissionService: PermissionService
    ) {
        this.baseUrl = baseUrl;
    }

    getUsers(): Promise<UserModel[]> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<UserModel[]>(this.baseUrl + 'api/user/users-list').subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });          
    }

    getRoles(isCurrentUser: boolean, isEditMode: boolean): Promise<any[]> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<any[]>(this.baseUrl + 'api/user/roles').subscribe(
                result => {
                    let resultList: any = [];
                    switch (this.permissionService.getRole()) {
                        case 'Administrator':
                            resultList = result;
                            break;
                        case 'Owner':
                            if (isEditMode && isCurrentUser)
                            {
                                resultList.push(result[1]);
                            }
                            else
                                resultList.push(result[2]);
                            break;
                        case 'Service_Provider':
                            resultList.push(result[2]);
                            break;
                        default:
                            resultList = null;
                    }
                    resolve(resultList);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });          
    }
    getPages(): Promise<any[]> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<any[]>(this.baseUrl + 'api/user/pages').subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });          
    }
    
    getUser(id: number): Promise<UserModel> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<UserModel>(this.baseUrl + 'api/user/' + id).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                }
            );
        });
    }

    addUser(user: UserModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.post(this.baseUrl + 'api/user/create', user, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    updateUser(user: UserModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.put(this.baseUrl + 'api/user/update', user, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                }, );
        });
    }

    deleteUser(id: number): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.delete(this.baseUrl + 'api/user/' + id, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                    this.eventService.showToast('success');
                },
                error => {
                    reject(error);
                    console.error(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    changePassword(password: string): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.post(this.baseUrl + 'api/user/change-password', {'newPassword': password}, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    resetPassword(id: number): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.put<HttpResponse<Object>>(this.baseUrl + 'api/user/reset-password/' + id, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                    this.eventService.showToast('success');
                },
                error => {
                    reject(error);
                    console.error(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }


    public dispose() {
        if (this.subscription)
            this.subscription.unsubscribe();
    }
}