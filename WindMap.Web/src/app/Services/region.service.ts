import { Injectable, Inject } from '@angular/core';
import { RegionModel, AlgorithmModel, RegionSensorsModel } from "../Models/region.model";
import { SensorModel, SensorResultModel } from "../Models/sensor.model";
import { EventService } from "../Services/event.service";
import { HttpClient, HttpResponse } from '@angular/common/http';
import { DataService } from './data.service';

@Injectable()

export class RegionService {
    private baseUrl: string;
    private subscription: any;

    constructor( @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        private eventService: EventService,
        private dataService: DataService,
    ) {
        this.baseUrl = baseUrl;
    }

    getRegions(): Promise<RegionModel[]> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.dataService.api({
                type: 'getRegions'
            }).subscribe(
                result => {
                    resolve(this.buildResponseRegionModel(result));
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                }
            );
        });
    }

    getRegion(id: number): Promise<RegionModel> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<RegionModel>(this.baseUrl + 'api/regions/' + id).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    getRegionsSensors(): Promise<RegionSensorsModel[]> {
        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<RegionSensorsModel[]>(this.baseUrl + 'api/regions/sensors').subscribe(
                result => {
                    resolve(result);
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                });
        });
    }

    getRegionsCount(): Promise<number> {
        return new Promise((resolve, reject) => {
            /* this.subscription = this.http.get<RegionModel[]>(this.baseUrl + 'api/regions').subscribe(
                result => { resolve(result.length) },
                error => { reject(error) }); */
            this.dataService.api({ type: 'getRegionsCount'}).subscribe(
                result => { resolve(result.length); },
                error => { reject(error); }
            );
        });
    }

    addRegion(region: RegionModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.post(this.baseUrl + 'api/regions', region, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    updateRegion(region: RegionModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.put(this.baseUrl + 'api/regions', region, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                }, );
        });
    }

    updateRegionSensors(region: RegionSensorsModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();
        return new Promise((resolve, reject) => {
            this.subscription = this.http.put(this.baseUrl + 'api/regions/sensors', region, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    deleteRegion(id: number): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.delete(this.baseUrl + 'api/regions/' + id, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                    this.eventService.showToast('success');
                },
                error => {
                    reject(error);
                    console.error(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    public dispose() {
        if (this.subscription)
            this.subscription.unsubscribe();
    }

    private buildResponseRegionModel(obj: any) {
        let data: any = [];

        //obj = JSON.parse(obj._body);
        data = obj && obj.map((region: any) => this.buildRegionModel(region));

        return data;
    }

    private buildRegionModel(obj: any): RegionModel {
        let region: any = new RegionModel();

        region.id = obj.id;
        region.name = obj.name;
        region.email = obj.email;
        region.phone = obj.phone;
        region.companyName = obj.companyName;
        region.webSite = obj.webSite;
        region.coordinates = obj.coordinates;
        region.sensorsCount = obj.sensors ? obj.sensors.length : 0;

        if (obj.algorithm) {

            let algorithmModel: AlgorithmModel = new AlgorithmModel();

            algorithmModel.monitorType = obj.algorithm.monitorType;
            algorithmModel.detectionPointFlux = obj.algorithm.detectionPointFlux;
            algorithmModel.detectionWindSpeed = obj.algorithm.detectionWindSpeed;
            algorithmModel.timeInterval = obj.algorithm.timeInterval;
            algorithmModel.timeResolution = obj.algorithm.timeResolution;
            algorithmModel.minDataForAverage = obj.algorithm.minDataForAverage;
            algorithmModel.numberLineGroupTime = obj.algorithm.numberLineGroupTime;
            algorithmModel.numberLineGroupWindDirection = obj.algorithm.numberLineGroupWindDirection;
            algorithmModel.windSmoothingGroup = obj.algorithm.windSmoothingGroup;
            algorithmModel.maxRangeBetweenMonitors = obj.algorithm.maxRangeBetweenMonitors;
            algorithmModel.sourceLocationUpdateInterval = obj.algorithm.sourceLocationUpdateInterval;
            algorithmModel.numbersOfGroupForLinearRegression = obj.algorithm.numbersOfGroupForLinearRegression;

            region.algorithm = algorithmModel;
        }

        region.sensors = obj.sensors && obj.sensors.map((sensor: any) => this.buildSensorModel(sensor));

        return region;
    }

    private buildSensorModel(obj: any): SensorModel {
        let sensor = new SensorModel();

        sensor.id = obj.id;
        sensor.monitorIdToLeft = obj.monitorIdToLeft;
        sensor.monitorIdToRight = obj.monitorIdToRight;
        sensor.distanceLeft = obj.distanceLeft;
        sensor.distanceRight = obj.distanceRight;

        sensor.iconUrl = '/assets/images/marker_icon_' + (obj.regionId <= 10 ? obj.regionId : 'default') + '.png';
        sensor.iconUrlDanger = '/assets/images/marker_icon_danger.png';
        sensor.iconUrlEmpty = '/assets/images/marker_icon_empty.png';

        sensor.regionName = obj.regionName;
        sensor.regionId = obj.regionId;
        sensor.monitorId = obj.monitorId;
        sensor.latitude = Number(obj.latitude);
        sensor.longitude = Number(obj.longitude);
        sensor.label = obj.monitorId.toString();
        sensor.onsiteWindMax = obj.onsiteWindMax;
        sensor.onsiteWindMin = obj.onsiteWindMin;
        sensor.normalAzimuthToLeft = obj.normalAzimuthToLeft;
        sensor.normalAzimuthToRight = obj.normalAzimuthToRight;
        sensor.onsiteWindCenterAngle = obj.onsiteWindCenterAngle;

        sensor.labelOptions = {
            color: '#ffffff',
            fontFamily: '',
            fontSize: '14px',
            fontWeight: 'bold',
            text: sensor.label,
        };

        sensor.averageResult = new SensorResultModel();

        if (obj.sensorResults.length > 0) {
            let count: number = obj.sensorResults.length;
            let startTime: any;
            let endTime: any;

            //mapping averageResult sensor result
            for (let i = 0; i < count; i++) {
                let tempRes: any = obj.sensorResults[i];

                startTime = new Date(tempRes.timeBegin);
                endTime = new Date(tempRes.timeEnd);

                sensor.averageResult.concentration += Number(tempRes.concentration);
                sensor.averageResult.windSpeed += Number(tempRes.windSpeed);
                sensor.averageResult.windDirection += Number(tempRes.windDirection);
                sensor.averageResult.length += Number((endTime.getTime() - startTime.getTime()) / 1000 * tempRes.windSpeed);
            }

            sensor.averageResult.concentration = parseFloat((sensor.averageResult.concentration / count).toFixed(4));
            sensor.averageResult.windSpeed = parseFloat((sensor.averageResult.windSpeed / count).toFixed(4));
            sensor.averageResult.windDirection = parseFloat((sensor.averageResult.windDirection / count).toFixed(4));
            sensor.averageResult.length = parseFloat((sensor.averageResult.length).toFixed(4));
        }

        sensor.sensorResults = obj.sensorResults && obj.sensorResults.map((sensorResult: any) => this.buildSensorResultModel(sensorResult));
        sensor.sensorResultsCount = obj.sensorResults ? obj.sensorResults.length : 0;

        return sensor;
    }

    private buildSensorResultModel(obj: any): SensorResultModel {
        let sensorResult = new SensorResultModel();

        sensorResult.timeBegin = obj.timeBegin;
        sensorResult.timeEnd = obj.timeEnd;
        sensorResult.concentration = obj.concentration;
        sensorResult.windSpeed = obj.windSpeed;
        sensorResult.windDirection = obj.windDirection;

        return sensorResult;
    }

    ////isnt used anywhere --------------------------------------------------------------------------------
    //getRegionSensors(regionId: number): Promise<SensorDetailModel[]> {
    //    return new Promise((resolve, reject) => {
    //        this.subscription = this.http.get<SensorDetailModel[]>(this.baseUrl + 'api/regions/' + regionId + '/sensors').subscribe(
    //            result => {
    //                resolve(result);
    //            },
    //            error => {
    //                reject(error);
    //                this.eventService.showToast('error', error);
    //            });
    //    });
    //}

    ////isnt used anywhere --------------------------------------------------------------------------------
    //getRegionSensorsBySensorId(sensorId: number): Promise<SensorDetailModel[]> {
    //    return new Promise((resolve, reject) => {
    //        this.subscription = this.http.get<SensorDetailModel[]>(this.baseUrl + 'api/regions/sensors/' + sensorId).subscribe(
    //            result => {
    //                resolve(result);
    //            },
    //            error => {
    //                reject(error);
    //                this.eventService.showToast('error', error);
    //            });
    //    });
    //}
}