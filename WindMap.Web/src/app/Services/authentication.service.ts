﻿import { Injectable } from '@angular/core';
import { tap, finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { DataService } from './data.service';
import { TokensService } from './tokens.service';
import { EventBusService } from './event-bus.service';

@Injectable()
export class AuthenticationService {
    constructor(
        private router: Router,
        private dataService: DataService,
        private tokenService: TokensService,
        private eventBusService: EventBusService,
    ) {
        this.eventBusService.subscribe('LOGOUT', () => this.logout()) ;
    }

    login(login: string, password: string, rememberMe: boolean ) {
        return this.dataService.api({
            type: 'login',
            data:  { login, password, rememberMe },
            disableTokenCheck: true
        }).pipe(tap((res) => {
            this.tokenService.setTokens(res);
        }));
    }

    logout() {
        // remove user from local storage to log user out
        this.dataService.api({
            type: 'logout',
            data: {
                refreshToken: this.tokenService.getToken('refresh')
            },
            disableTokenCheck: true
        })
        .pipe(
            finalize(() => {
                // TODO: move into the subscribe cb
                // Have a problem with the server response (400 or 500 status)
                // It's a fix for this case
                this.deleteToken();
                this.router.navigate(['/login']);
            })
        ).subscribe();
    }

    deleteToken() {
        this.tokenService.clearTokens();
    }
}
