import { Injectable, Inject } from '@angular/core';
import { CompanyModel } from '../Models/company.model';
import { EventService } from '../Services/event.service';
import { HttpResponse } from '@angular/common/http';
import { DataService } from './data.service';

@Injectable()

export class CompanyService {
    private baseUrl: string;
    private subscription: any;

    constructor( @Inject('BASE_URL') baseUrl: string,
                private eventService: EventService,
                private dataService: DataService
            ) {

        this.baseUrl = baseUrl;
    }

    getCompanies(): Promise<CompanyModel[]> {
        this.eventService.startLoading(); // move into dataService

        return new Promise((resolve, reject) => {
            this.subscription = this.dataService.api({ type: 'getCompanies'}).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    getCompaniesCount(): Promise<number> {
        return new Promise((resolve, reject) => {
            this.subscription =  this.dataService.api({ type: 'getCompanies'}).subscribe(
                result => { resolve(result.length); },
                reject );
        });
    }

    getCompany(id: number): Promise<CompanyModel> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.dataService.api({
                type: 'getCompany',
                urlParams: { id }
            }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                }
            );
        });
    }

    addCompany(company: CompanyModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            // this.subscription = this.http.post(this.baseUrl + 'api/companies/create', company, { observe: 'response' })
            this.subscription = this.dataService.api({
                type: 'addCompany',
                data: { ...company }
            }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    updateCompany(company: CompanyModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();
        return new Promise((resolve, reject) => {
            // this.subscription = this.http.put(this.baseUrl + 'api/companies/update', company, { observe: 'response' })
            this.subscription = this.dataService.api({
                type: 'updateCompany',
                data: { ...company }
            }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                }, );
        });
    }

    deleteCompany(id: number): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            // this.subscription = this.http.delete(this.baseUrl + 'api/companies/' + id, { observe: 'response' })
            this.subscription = this.dataService.api({
                type: 'deleteCompany',
                urlParams: { id }
            }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                    this.eventService.showToast('success');
                },
                error => {
                    reject(error);
                    console.error(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    public dispose() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
