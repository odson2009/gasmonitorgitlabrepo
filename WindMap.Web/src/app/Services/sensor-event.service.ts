import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import { SensorModel, SensorResultModel, SensorDetailModel, SensorHubModel } from "../Models/sensor.model";
import { RegionModel } from "../Models/region.model";
import { EventService } from "../Services/event.service";
import { SensorEventModel } from "../Models/sensorEvent.model";
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()

export class SensorEventService {
    private baseUrl: string;
    private subscription: any;

    constructor(
        private _httpService: Http,
        private http: HttpClient,
        @Inject('BASE_URL') baseUrl: string,
        private eventService: EventService
    )
    {
        this.baseUrl = baseUrl;
    }

    getAllSensorsEvents(): Promise<any> {
        return new Promise(resolve => {
            this.subscription = this._httpService.get(this.baseUrl + 'api/events').subscribe(
                value => {
                    resolve(this.buildResponse(value));
                },
                error => {
                    resolve(error);
                    this.eventService.showToast('error', error);
                });
        });
    }

    deleteRegionEvents(id: number): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.delete(this.baseUrl + 'api/events/' + id, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    console.error(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    private buildResponse(obj: any) {
        obj = JSON.parse(obj._body);
        let data: any = [];

        data = obj && obj.map((sensor: any) => this.buildSensorEventModel(sensor));

        return data;
    }

    public buildSensorEventModel(obj: any): SensorEventModel {
        let sensorEvnt = new SensorEventModel();

        sensorEvnt.id = obj.id;
        sensorEvnt.monitorId = obj.monitorId;
        sensorEvnt.pairedMonitorId = obj.pairedMonitorId;
        sensorEvnt.status = obj.status;
        sensorEvnt.eventTime = obj.eventTime;
        sensorEvnt.sourceLatitude = obj.sourceLatitude;
        sensorEvnt.sourceLongitude = obj.sourceLongitude;
        sensorEvnt.parallelWindDirection = obj.parallelWindDirection;
        sensorEvnt.crossWindDirection = obj.crossWindDirection;
        sensorEvnt.averageWindDirection = obj.averageWindDirection;
        sensorEvnt.emissionRate = obj.emissionRate;
        sensorEvnt.regionId = obj.regionId;
        sensorEvnt.regionName = obj.regionName;

        sensorEvnt.endEventId = obj.endEventId;
        sensorEvnt.endEvent = obj.endEvent;
      
        return sensorEvnt;
    }

   
    public dispose() {
        if (this.subscription)
            this.subscription.unsubscribe();
    }

}
