import { Injectable, Inject } from '@angular/core';
import { ToasterService, Toast } from 'angular2-toaster';
import { SlimLoadingBarService } from "ng2-slim-loading-bar";

@Injectable()

export class EventService {
    private toasterService: ToasterService;

    constructor(
        toasterService: ToasterService,
        private slimLoadingBarService: SlimLoadingBarService) {
       
        this.toasterService = toasterService; 
    }

    public showToast(type: string = 'success', body:any = null, duration: number = undefined)
    {
        let toast = {
            type: type,
            title: type === 'error' ? 'Error! Code = ' + body.status : 'Success',
            showCloseButton: type === 'error',
            body: type === 'error' ? body._body ? body._body : (body.error ? (body.error.message ? body.error.message : '') : '')  : null,
            timeout: duration ? duration : (type === 'error'? 5000 : 1500),
        };
        this.toasterService.pop(toast);
    }

    public startLoading() {
        this.slimLoadingBarService.progress = 20;
        this.slimLoadingBarService.start();
    }

    public stopLoading() {
        this.slimLoadingBarService.stop();
    }

    public completeLoading() {
        this.slimLoadingBarService.complete();
    }

}