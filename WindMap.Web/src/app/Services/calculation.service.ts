import { Injectable, Inject } from '@angular/core';
import { WindVector, Point, SensorResultModel, Arrow, TMP, AverageData, Main_Buffer } from "../Models/sensor.model";
import { RegressionParameter, RegressionResult } from '../Models/regression.model';

@Injectable()

export class CalculationService {

    private meters_per_pixel_ary: number[];

    constructor() {
        this.meters_per_pixel_ary = [];

        for (let i = 0; i < 23; i++) {
            this.meters_per_pixel_ary[i] = Math.cos(31.11805959867834 * Math.PI / 180) * 2 * Math.PI * 6378137 / (256 * Math.pow(2, i)) * 10 / 1000;
        }
    }

    public getNormalAzimuth(azimuth: number, isLeft: boolean) {
        if (isLeft) {
            azimuth = azimuth + 90;
        }
        else {
            azimuth = azimuth - 90;
        }

        return parseFloat(Math.abs((azimuth + 360) % 360).toFixed(4));;
    }

    // obsolete, dont used
    public getAngleBetweenVectors(
        x1: number, y1: number, // vertex
        x2: number, y2: number,
        x3: number, y3: number) {
        return Math.abs(this.radToDegree2(Math.atan2(y2 - y1, x2 - x1) - Math.atan2(y3 - y1, x3 - x1)));
    }

    public angleFromCoordinate(lat1: number, long1: number, lat2: number, long2: number) {
        lat1 = this.degToRad2(lat1);
        long1 = this.degToRad2(long1);
        lat2 = this.degToRad2(lat2);
        long2 = this.degToRad2(long2);
        const angle = Math.atan2(lat2 - lat1, long2 - long1);
        return parseFloat(Math.abs(this.radToDegree2(angle)).toFixed(4));
    }

    public angleFromCoordinateNormal(lat1: number, long1: number, lat2: number, long2: number) {
        lat1 = this.degToRad2(lat1);
        long1 = this.degToRad2(long1);
        lat2 = this.degToRad2(lat2);
        long2 = this.degToRad2(long2);

        const dLon = (long2 - long1);

        const y = Math.sin(dLon) * Math.cos(lat2);
        const x = Math.cos(lat1) * Math.sin(lat2) -
            Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

        let brng = this.radToDegree2(Math.atan2(y, x));
        brng = (brng + 360) % 360;

        return parseFloat(brng.toFixed(4));
    }

    //obsolete TODO:remove
    public getDistance(lat1: number, lng1: number, lat2: number, lng2: number) {
        const R = 6378137;
        const F1 = this.degToRad2(lat1);
        const F2 = this.degToRad2(lat2);
        const deltaF = this.degToRad2(lat2 - lat1);
        const deltaL = this.degToRad2(lng2 - lng1);

        const a = Math.sin(deltaF / 2) * Math.sin(deltaF / 2) + Math.cos(F1) * Math.cos(F2) * Math.sin(deltaL / 2) * Math.sin(deltaL / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        const d = R * c;
        // d = d / 1000;

        return d;
    }

    public degToRad2(degrees: number) {
        return degrees * (Math.PI / 180);
    }

    public radToDegree2(radians: number) {
        return radians * (180 / Math.PI);
    }

    // http://www.movable-type.co.uk/scripts/latlong.html
    // https://stackoverflow.com/questions/2637023/how-to-calculate-the-latlng-of-a-point-a-certain-distance-away-from-another
    /**
     *
     * @param lat1 latitude
     * @param lng1 longetude
     * @param distance distance
     * @param azimuth angle
     */
    public getVectorCoordinates(lat1: number, lng1: number, distance: number, azimuth: number): Point {
        const dist = distance / 6371; // 6371 - earth radius
        const brng = this.degToRad2(azimuth);

        lat1 = this.degToRad2(lat1);
        lng1 = this.degToRad2(lng1);

        const lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) +
            Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));

        const lng2 = lng1 + Math.atan2(Math.sin(brng) * Math.sin(dist) *
            Math.cos(lat1),
            Math.cos(dist) - Math.sin(lat1) *
            Math.sin(lat2));

        if (isNaN(lat2) || isNaN(lng2)) {
            console.warn('lat2 | lng2 is NaN!');
        }

        const point = new Point();
        point.latitude = this.radToDegree2(lat2);
        point.longitude = this.radToDegree2(lng2);

        return point;
    }

    public drawArrow(sLat: number, sLng: number, eLat: number, eLng: number, mapZoom: number): any {
        mapZoom = mapZoom == 0 ? 18 : mapZoom;

        const distance = this.meters_per_pixel_ary[mapZoom];
        const azimuth = this.angleFromCoordinateNormal(sLat, sLng, eLat, eLng);
        const sArrow = this.getVectorCoordinates(eLat, eLng, -distance, (azimuth + 20) % 360);
        const eArrow = this.getVectorCoordinates(eLat, eLng, -distance, (azimuth - 20) % 360);

        const arrow = new Arrow();
        arrow.startLatitude = sArrow.latitude;
        arrow.startLongitude = sArrow.longitude;
        arrow.endLatitude = eArrow.latitude;
        arrow.endLongitude = eArrow.longitude;

        return arrow;
    }

    //backline of the vector
    public buildVectorBackLine(windVector: WindVector, mapZoom: number) {
        const zoomCoeff = 0.5 / mapZoom;
        const arrowLength = windVector.length - (windVector.length * zoomCoeff);
        const deltaAzimuth = windVector.azimuth * 0.01;

        const backLineArrow = this.getVectorCoordinates(windVector.lat1, windVector.lng1, arrowLength - arrowLength * 0.05, windVector.azimuth);
        windVector.backLineLat = backLineArrow.latitude;
        windVector.backLineLng = backLineArrow.longitude;
    }

    public getPointFluxData(sensorResults: Array<SensorResultModel>): any {
        let PF1_Average: number = 0;
        let PF2_Average: number = 0;
        let WD_Average_TMP: number = 0;

        if (sensorResults && sensorResults.length > 0) {
            let FIFO_LENGH: number = sensorResults.length;
            let WS_V: number = 0;
            let WS_U: number = 0;
            let MEAN_WS_V: number = 0;
            let MEAN_WS_U: number = 0;
            let C_Average: number = 0;
            let WS_Average: number = 0;
            let WS_Average_for_vector: number = 0;
            let WD_Average: number = 0;
            let WD_Intermadiate_Average: number = 0;
            let Y_PF: number = 0;
            let tempRes: any;

            for (let i = 0; i < FIFO_LENGH; i++) {

                tempRes = sensorResults[i];
                C_Average += Number(tempRes.concentration);
                WS_Average += Number(tempRes.windSpeed);

                WS_V += Number(-1 * (tempRes.windSpeed * Math.sin(Math.PI / 180 * tempRes.windDirection)));
                WS_U += Number(tempRes.windSpeed * Math.cos(Math.PI / 180 * tempRes.windDirection));

                Y_PF = tempRes.concentration * tempRes.windSpeed;
                PF2_Average += Y_PF;

                WD_Average_TMP += tempRes.windDirection;
            }

            MEAN_WS_V = WS_V / FIFO_LENGH;
            MEAN_WS_U = WS_U / FIFO_LENGH;
            C_Average = C_Average / FIFO_LENGH;
            WS_Average = WS_Average / FIFO_LENGH;
            WD_Average_TMP = WD_Average_TMP / FIFO_LENGH;

            WS_Average_for_vector = Math.sqrt(Math.pow(MEAN_WS_V, 2) + Math.pow(MEAN_WS_U, 2));
            WD_Intermadiate_Average = -180 / Math.PI * Math.atan(MEAN_WS_V / MEAN_WS_U);

            if (MEAN_WS_U > 0) {
                if (WD_Intermadiate_Average < 0) {
                    WD_Average = WD_Intermadiate_Average + 360;
                }
                else {
                    WD_Average = WD_Intermadiate_Average;
                }
            }
            else {
                WD_Average = WD_Intermadiate_Average + 180;
            }

            PF1_Average = parseFloat((C_Average * WS_Average).toFixed(4));
            PF2_Average = parseFloat((PF2_Average / FIFO_LENGH).toFixed(4));
        }

        return {
            pf1_Average: PF1_Average,
            pf2_Average: PF2_Average,
            wd_Average: WD_Average_TMP
        };
    }

    public calculateWindCenterAngle(normalAzimuthToLeft: number = 0, normalAzimuthToRight: number = 0) : number
    {
        let value: number = null;

        if (normalAzimuthToLeft > 0 && normalAzimuthToRight > 0) {
            value = (normalAzimuthToLeft - normalAzimuthToRight) / 2;

            if (value >= 180) {
                value = (normalAzimuthToLeft - normalAzimuthToRight + 360) / 2 - 360;
            }
            value = Number(parseFloat(value.toString()).toFixed(4));
        }
        else if (normalAzimuthToLeft > 0) {
            value = normalAzimuthToLeft;
        }
        else if (normalAzimuthToRight > 0) {
            value = normalAzimuthToRight;
        }

        return value;
    }

    //https://en.wikipedia.org/wiki/Simple_linear_regression
    public calculateLinearRegression(sensorResults: SensorResultModel[], onsiteWindCenterAngle: number) {
        let sx = 0, sy = 0, sxx = 0, sxy = 0, syy = 0;
        let n = sensorResults.length;
        let X_TWD_TMP = 0, Y_PF_TMP = 0;
        let regressionParameters: RegressionParameter[] = [];

        for (let i = 0; i < sensorResults.length; i++) {

            //transform wind direction
            let tempRes = sensorResults[i];
            X_TWD_TMP = tempRes.windDirection - onsiteWindCenterAngle;
            Y_PF_TMP = tempRes.concentration * tempRes.windSpeed;

            if (- 180 <= X_TWD_TMP) {
                X_TWD_TMP = X_TWD_TMP + 360;
            }
            if (180 >= X_TWD_TMP) {
                X_TWD_TMP = X_TWD_TMP - 360;
            }

            regressionParameters.push({
                X_TWD: X_TWD_TMP,
                Y_PF: Y_PF_TMP
            });

            let x = X_TWD_TMP;
            let y = Y_PF_TMP;

            sx += x;
            sxx += x * x;
            sy += y;
            sxy += x * y;
            syy += y * y;
        }

        let result: RegressionResult = new RegressionResult();

        result.m = (n * sxy - sx * sy) / (n * sxx - sx * sx);
        result.b = (sy / n) - (result.m * sx / n);
        result.r = (n * sxy - sx * sy) / Math.sqrt((n * sxx - Math.pow(sx, 2)) * (n * syy - Math.pow(sy, 2)));

        return {
            regressionParameters: regressionParameters,
            regressionResult: result,
        };
    }

    public calculateLinearRegressionForAverage(averageData: AverageData[], onsiteWindCenterAngle: number, doTransformWind: boolean = false): any {
        let sx = 0, sy = 0, sxx = 0, sxy = 0, syy = 0;
        let n = averageData.length;
        let regressionParameters: RegressionParameter[] = [];
        let TWD_tmp: number[] = [], PF_tmp: number[] = [];

        for (let i = 0; i < averageData.length; i++) {
            let tempRes = averageData[i];

            //transform wind direction
            if (doTransformWind) {
                if (tempRes.TWD_Average >= -180) {
                    tempRes.TWD_Average += 360;
                }
                if (tempRes.TWD_Average <= 180) {
                    tempRes.TWD_Average -= 360;
                }

                tempRes.TWD_Average %= 360;
            }

            TWD_tmp.push(tempRes.TWD_Average);
            PF_tmp.push(tempRes.PF_Average);

            let x = tempRes.TWD_Average;
            let y = tempRes.PF_Average;

            sx += x;
            sxx += x * x;
            sy += y;
            sxy += x * y;
            syy += y * y;
        }

        let result: RegressionResult = new RegressionResult();

        result.m = (n * sxy - sx * sy) / (n * sxx - sx * sx);
        result.b = (sy / n) - (result.m * sx / n);
        result.r = (n * sxy - sx * sy) / Math.sqrt((n * sxx - Math.pow(sx, 2)) * (n * syy - Math.pow(sy, 2)));

        //get mean for TWD_Average, PF_Average
        let averageResults: AverageData = new AverageData();
        averageResults.TWD_Average = this.mean(TWD_tmp);
        averageResults.PF_Average = this.mean(PF_tmp);

        return {
            averageResults: averageResults,
            result: result
        };
    }
    

    //Temporal thmoothing method
    public MOVING_Average_METHOD(main_Buffer: Main_Buffer, NUMBER_OF_LINE_GROUPING_TIME: number, bufferIndex: number, onsiteWindCenterAngle: number = 200) {

        ////get arrays
        //for (let t = 0; t < main_Buffer.tmp_buffers.length; t++) {
          
        //    const tmpData = main_Buffer.tmp_buffers[t];

        //    if (bufferIndex == tmpData.index)
        //    {
        //        let lr_parameter: RegressionParameter = new RegressionParameter();

        //        //copy * data WS[i], WS[i]),c[i], WD[i] to NEW buffer TMP_WS[i], TMP_WS[i]),TMP_C[i], WD[i]
        //        main_Buffer.source_Result_buffer = main_Buffer.source_Result_buffer.concat(tmpData.tmp_Result_buffer);

        //        //do sorting by WD ASC
        //        main_Buffer.source_Result_buffer.sort(function (a, b) { return a.windDirection - b.windDirection });

        //        let TMP_WD: number[] = [];
        //        let TMP_WS: number[] = [];
        //        let TMP_C: number[] = [];
        //        let TMP_PF: number[] = [];

        //        let C_Average: number = 0;
        //        let PF_Average: number = 0;
        //        let WD_Average: number = 0;
        //        let WS_Average: number = 0;
        //        let WD_Intermadiate_Average: number = 0;

        //        const startRecord = 0;
        //        const endRecord = main_Buffer.source_Result_buffer.length;

        //        //results
        //        for (let r = 0; r < main_Buffer.source_Result_buffer.length; r++) {

        //            const tmp_res = main_Buffer.source_Result_buffer[r];

        //            TMP_WD.push(tmp_res.windDirection);
        //            TMP_WS.push(tmp_res.windSpeed);
        //            TMP_C.push(tmp_res.concentration);
        //            TMP_PF.push(tmp_res.concentration * tmp_res.windSpeed);

        //            let X_TWD_temp: number = tmp_res.windDirection - onsiteWindCenterAngle;

        //            if (X_TWD_temp > -180 && X_TWD_temp < 180) {
        //                X_TWD_temp = X_TWD_temp - 360;

        //                tmpData.LR_Parameters.push({
        //                    X_TWD: X_TWD_temp,
        //                    Y_PF: tmp_res.concentration * tmp_res.windSpeed
        //                });
        //            }
        //        }

        //        for (let i = startRecord; i < endRecord - NUMBER_OF_LINE_GROUPING_TIME - 1; i++) {

        //            console.log('i = ' + i);

        //            const results = this.calculate_WS_Avarage(TMP_WS, TMP_WD, i, i + NUMBER_OF_LINE_GROUPING_TIME);

        //            WS_Average = results.WS_Average;
        //            WD_Intermadiate_Average = results.WD_Intermadiate_Average;

        //            C_Average = this.calculate_DATA_AVARAGE(TMP_C, i, i + NUMBER_OF_LINE_GROUPING_TIME);
        //            WD_Average = this.calculate_DATA_AVARAGE(TMP_WD, i, i + NUMBER_OF_LINE_GROUPING_TIME);
        //            PF_Average = this.calculate_DATA_AVARAGE(TMP_PF, i, i + NUMBER_OF_LINE_GROUPING_TIME);

        //            //calculate average data
        //            let average_data = new AverageData();
        //            average_data.WS_Average = WS_Average;
        //            average_data.WD_Intermadiate_Average = WD_Intermadiate_Average;
        //            average_data.PF_Thmooth_Average = C_Average * WS_Average;

        //            average_data.WD_Average = WD_Average;
        //            average_data.PF_Average = PF_Average;
        //            average_data.C_Average = C_Average;

        //            tmpData.AverageData.push(average_data);
        //        }

        //        //calculate LR
        //        const linearRegression = this.calculateLinearRegression(tmpData.tmp_Result_buffer, onsiteWindCenterAngle);
        //        tmpData.LR_Result = linearRegression.regressionResult;
        //    }

        //}  
    }

    public calculate_WS_Avarage(WS: number[], WD: number[], startRecord: number, endRecord: number) {
        let WS_v = [];
        let WS_u = [];
        let WS_v_MEAN: number;
        let WS_u_MEAN: number;
        let result = {
            WS_Average: 0,
            WD_Intermadiate_Average: 0,
        };

        for (let i = startRecord; i < endRecord; i++) {
            WS_v.push(-WS[i] * Math.sin(Math.PI / 180 * WD[i]));
            WS_u.push(WS[i] * Math.cos(Math.PI / 180 * WD[i]));

            //checking for NaN
            //WS_v
            if (isNaN(-WS[i] * Math.sin(Math.PI / 180 * WD[i]))) {
                debugger;
            }
            //WS_u
            if (isNaN(WS[i] * Math.cos(Math.PI / 180 * WD[i]))) {
                debugger;
            }
        }

        WS_v_MEAN = this.mean(WS_v);
        WS_u_MEAN = this.mean(WS_u);

        result.WS_Average = Math.sqrt(Math.pow(WS_v_MEAN, 2) + Math.pow(WS_u_MEAN, 2));
        result.WD_Intermadiate_Average = -180 / Math.PI * Math.atan(WS_v_MEAN / WS_u_MEAN);

        return result;
    }

    //part of the thmoothing method
    public calculate_DATA_AVARAGE(c: number[], startRecord: number, endRecord: number) {
        let tmpC = 0;

        for (let i = startRecord; i < endRecord; i++) {
            if (c[i])
                tmpC += c[i];
        }

        return tmpC / (endRecord - startRecord);
    }

    public mean(array: number[]): number {
        let tmp = 0;
        if (array && array.length > 0) {

            for (let i = 0; i < array.length; i++) {
                tmp += array[i];
            }

            if (isNaN(tmp))
            {
                console.warn('IsNan mean() ' + tmp)
            }

            return tmp / array.length;
        }
        return tmp;
    }

    public sum(array: number[]): number {
        let tmp = 0;
        if (array && array.length > 0) {

            for (let i = 0; i < array.length; i++) {
                tmp += array[i];
            }

            if (isNaN(tmp)) {
                console.warn('IsNan sum() ' + tmp)
            }

            return tmp;
        }
        return tmp;
    }

    public getEllipsePoints(point1, point2, vertexCount): google.maps.LatLng[] {
        let center = new google.maps.LatLng(
            (point1.lat() + point2.lat()) / 2,
            (point1.lng() + point2.lng()) / 2,
        );

        let rotation = google.maps.geometry.spherical.computeHeading(point1, point2);

        let r1 = google.maps.geometry.spherical.computeDistanceBetween(point1, point2);
        let r2 = r1 / 2;

        let rot = -rotation * Math.PI / 180;
        let points = [];
        let latConv = google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(center.lat() + 0.2, center.lng())) * 10;
        let lngConv = google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(center.lat(), center.lng() + 0.2)) * 10;
        let step = (360 / vertexCount) || 10;

        for (let i = 0; i <= 360.001; i += step) {
            const y = r1 * Math.cos(i * Math.PI / 180);
            const x = r2 * Math.sin(i * Math.PI / 180);
            const lng = (x * Math.cos(rot) - y * Math.sin(rot)) / lngConv;
            const lat = (y * Math.cos(rot) + x * Math.sin(rot)) / latConv;

            points.push(new google.maps.LatLng(center.lat() + lat, center.lng() + lng));
        }
        return points;
    }


    public getCirclePoints(point1, radius): google.maps.LatLng[] {
        let center = new google.maps.LatLng(
             point1.lat(),
             point1.lng(),
        );

        let rot = Math.PI / 180;
        let points = [];
        let latConv = google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(center.lat() + 0.2, center.lng())) * 10;
        let lngConv = google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(center.lat(), center.lng() + 0.2)) * 10;

        for (let i = 0; i <= 360; i += 2) {
            const y = radius * Math.cos(i * Math.PI / 180);
            const x = radius * Math.sin(i * Math.PI / 180);
            const lng = (x * Math.cos(rot) - y * Math.sin(rot)) / lngConv;
            const lat = (y * Math.cos(rot) + x * Math.sin(rot)) / latConv;

            points.push(new google.maps.LatLng(center.lat() + lat, center.lng() + lng));
        }
        return points;
    }

    public dispose() {
        //if (this.meters_per_pixel_ary)
        //    this.meters_per_pixel_ary = [];
    }
}