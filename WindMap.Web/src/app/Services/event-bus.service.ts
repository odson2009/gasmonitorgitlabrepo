import { Injectable } from '@angular/core';

@Injectable()
export class EventBusService {
  private subscriptions = {};

  emit(name: string, ...data) {
    if (this.subscriptions[name]) {
      this.subscriptions[name].forEach(cb => {
        cb.call(null, ...data);
      });
    }
  }

  subscribe(name: string, cb: Function) {
    if(!this.subscriptions[name]) {
     this.subscriptions[name] = []; 
    }

    this.subscriptions[name].push(cb);

    return { name, cb };
  }

  remove(link: {name: string, cb: Function}) {
    const subscriptionsOnCurrentEvent: Array<Function> = this.subscriptions[link.name];

    if(!subscriptionsOnCurrentEvent.length) return;

    const index = subscriptionsOnCurrentEvent.findIndex((i) => i === link.cb);

    if (index > -1) {
      this.subscriptions[link.name].splice(index, 1);
    }
  }
}
