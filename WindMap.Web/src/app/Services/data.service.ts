import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of as locOf } from 'rxjs/observable/of';
import { _throw as obsThrow } from 'rxjs/observable/throw';
import { map, catchError } from 'rxjs/operators';
import { template } from 'lodash';
import { apiMap } from '../maps/api.map';
import { API_CFG_DEFAULTS } from '../maps/defaults.map';
import { ApiCfg } from '../interfaces/args.interface';
import { TokensService } from './tokens.service';
import { EventBusService } from './event-bus.service';

type ResponseType = 'text' | 'json' | 'blob' | 'arraybuffer';

@Injectable()
export class DataService {
    private _apiMap = apiMap;
    private _defaultHeaders = { 'Content-Type': 'application/json' };
    private baseUrl: string;

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        private tokensService: TokensService,
        private eventBusErvice: EventBusService
    ) {
        this.baseUrl = baseUrl;
    }

    api(cfg: ApiCfg)  {
        if (cfg.disableTokenCheck || !this.tokensService.isTokenExpired('access')) {
            return this._api(cfg);
        } else {
            return this.tokensService.updateAccessToken().pipe(
                map(() => this._api(cfg)),
                catchError(err => {
                    this.eventBusErvice.emit('LOGOUT');
                    return obsThrow(err);
                })
            );
        }
    }

    private _api(cfg: ApiCfg) {
        cfg = Object.assign({}, API_CFG_DEFAULTS, cfg);
        const apiMapItem = this._apiMap[cfg.type];
        if (!apiMapItem) {
            throw 'no such api type in apiMap: ' + cfg.type;
        }
        if (apiMapItem.mockup) {
            return locOf(apiMapItem.mockup);
        }
        if (apiMapItem.errMockup) {
            return obsThrow(apiMapItem.errMockup);
        }

        const headers = Object.assign(
            {},
            this._defaultHeaders,
            (apiMapItem.headers || {}),
            (cfg.headers || {})
        );
        const params = this.getParams(cfg.queryParams);
        const url = this.getUrl(apiMapItem.url + params, cfg);

        const response$ = this.http.request(apiMapItem.method, this.baseUrl + url, {
            body: cfg.data,
            headers: new HttpHeaders(headers),
            observe: 'response',
            responseType: <ResponseType>(cfg.responseType || apiMapItem.responseType || 'text'),
            withCredentials: false
        });

        return this.responseHandler(response$, cfg);
    }

    /**
   * Adds default headers that will appear on every HTTP request
   */
    addHeaders(headers: any) {
        Object.assign(this._defaultHeaders, headers);
    }

    /**
     * Removes default headers that will appear on every HTTP request
     */
    remHeaders(keys: string[]) {
        keys.forEach(key => delete this._defaultHeaders[key]);
    }

    getUrl(urlTemplate: string, cfg: ApiCfg) {
        if (!cfg.urlParams) {
            cfg.urlParams = {};
        }
        return template(urlTemplate)(cfg);
    }

    /**
   * returns an encoded param string
   */
    private getParams(queryParams: any): string {
        if (!queryParams) {
            return '';
        }
        let params = '';

        for (const paramKey of Object.keys(queryParams)) {
            let qParam = queryParams[paramKey];

            if (qParam === null || typeof qParam === 'undefined') {
                continue;
            }

            qParam = encodeURIComponent(qParam);
            params += paramKey + '=' + qParam + '&';
        }
        params = '?' + params.substr(0, params.length - 1);
        return params;
    }

    private responseHandler(res: Observable<any>, cfg: ApiCfg) {
        return res.pipe(
            map((resp: any) => {
                try {
                    // This will fail if the response is a string.
                    return JSON.parse(resp.body);
                } catch (err) {
                    return resp.body;
                }
            }),
            catchError(err => {
                if (err.status === 401) { }
                return obsThrow(err);
            })
        );
    }
}
