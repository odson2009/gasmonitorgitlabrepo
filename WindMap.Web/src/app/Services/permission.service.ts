﻿import { Injectable } from '@angular/core';
import { UserTokenModel } from '../Models/user.model';

@Injectable()
export class PermissionService {
    constructor() { }

    getPermission(): Array<string> {
        const token = localStorage.getItem('accessToken');

        if (!token) {
            return null;
        }

        const decodedToken = this.parseJwt(token);
        if (decodedToken.permissions) {
            return decodedToken.permissions.split(',');
        } else {
            return null;
        }
    }

    getRole(): string {
        const token = localStorage.getItem('accessToken');
        const decodedToken: UserTokenModel = this.parseJwt(token);

        return decodedToken.role;
    }

    getUserLogin(): string {
        const token = localStorage.getItem('accessToken');
        const decodedToken: UserTokenModel = this.parseJwt(token);
        return decodedToken.given_name;
    }
    getUserId(): string {
        const token = localStorage.getItem('accessToken');
        const decodedToken: UserTokenModel = this.parseJwt(token);
        return decodedToken.sub;
    }

    getPasswordStatus(): boolean {
        const token = localStorage.getItem('accessToken');

        if (!token) {
            return null;
        }
        // results decoded like a string. Why? I dont know
        const decodedToken: UserTokenModel = this.parseJwt(token);
        return decodedToken.changedPassword.toString() === 'true';
    }

    parseJwt(token): any {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');

        return JSON.parse(window.atob(base64));
    }
}
