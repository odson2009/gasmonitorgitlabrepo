﻿import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EventService } from "./event.service";
import { LogoModel } from "../Models/logo.model";

@Injectable()
export class LogoService {
    private baseUrl: string;
    private subscription: any;

    constructor(@Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        private eventService: EventService) {
        this.baseUrl = baseUrl;
    }

  
    getLogos(id: string): Promise<LogoModel[]> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<LogoModel[]>(this.baseUrl + 'api/user/logo-companies/' + id).subscribe(
                result => {
                    resolve(result);
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                }
            );
        });
    }

    public dispose() {
        if (this.subscription)
            this.subscription.unsubscribe();
    }
}