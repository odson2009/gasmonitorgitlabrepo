import { Injectable, Inject } from '@angular/core';
import { SensorModel, SensorResultModel, SensorDetailModel, SensorHubModel } from "../Models/sensor.model";
import { RegionModel, AlgorithmModel, RegionSensorsModel } from "../Models/region.model";
import { EventService } from "../Services/event.service";
import { SensorEventModel } from "../Models/sensorEvent.model";
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Counter } from '../Models/counter';

@Injectable()

export class SensorService {
    _httpService: any;
    private baseUrl: string;
    private subscription: any;

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
        //private _httpService: Http,
        private eventService: EventService
    ) {
        this.baseUrl = baseUrl;
    }

    getSensorSimulatedData(delation): any {
        return this.http.get(this.baseUrl + 'api/development/send-test-results?timeOut=' + delation).subscribe(
            result => {

            },
            error => {

                this.eventService.showToast('error', error);
            });
    }
        

    getSensorResultsMinDate(): Promise<Date> {
        return new Promise((resolve, reject) => {
            this.subscription = this.http.get<Date>(this.baseUrl + 'api/sensors/results/minDate').subscribe(
                result => {
                    resolve(result);
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                });
        });
    }

    generateResultFile(reportModel: any): Promise<any> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.post(this.baseUrl + 'api/sensors/results/generateFile', reportModel, { observe: 'response' }).subscribe(
                result => {
                    resolve(result.body);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    addSensor(sensor: SensorDetailModel): Promise<HttpResponse<Object>> {
        this.eventService.startLoading();

        let sensors: Array<SensorDetailModel> = [];
        sensors.push(sensor);

        return new Promise((resolve, reject) => {
            this.subscription = this.http.post(this.baseUrl + 'api/sensors', sensors, { observe: 'response' }).subscribe(
                result => {
                    resolve(result);
                    this.eventService.showToast('success');
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);
                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    getRegionByParameters(id: number): Promise<any> {
        this.eventService.startLoading();

        return new Promise((resolve, reject) => {
            this.subscription = this.http.get(this.baseUrl + `api/regions/region/${id}`).subscribe(
                result => {

                    //add new region full model
                    resolve(this.buildResponseRegionModel([result]));
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);

                    const { error: { errors } } = error;
                    
                    // Most repeated errors                    
                    if (errors && errors.find(e => e.field === 'id')) {
                        error.error.message = "Maybe, user hasn't this region in own list";
                    }

                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
    }

    //NEED REWORK
    //using in map page
    getRegionsByParameters(startDate: Date, endDate: Date, sensorIds: Array<number>, isLeak: boolean = false): Promise<any> {
        this.eventService.startLoading();

        var data = {
            startDate: startDate,
            endDate: endDate,
            isNeedLastResults: false,
            sensorIds: sensorIds,
            isLeak: isLeak
        }

        return new Promise((resolve, reject) => {
            this.subscription = this.http.post(this.baseUrl + 'api/regions/details/period', data).subscribe(
                result => {
                    //add new region full model
                    resolve(this.buildResponseRegionModel(result));
                    this.eventService.completeLoading();
                },
                error => {
                    reject(error);

                    this.eventService.showToast('error', error);
                    this.eventService.stopLoading();
                });
        });
}

//PREVIOUS METHOD MUST BE LIKE THIS
getRegionsByParametersNew(startDate: Date, endDate: Date, sensorIds: number[], isLeak: boolean = false): Promise < RegionModel[] > {
    this.eventService.startLoading();

    var data = {
        startDate: startDate,
        endDate: endDate,
        isNeedLastResults: false,
        sensorIds: sensorIds,
        isLeak: isLeak
    }

        return new Promise((resolve, reject) => {
        this.subscription = this.http.post<RegionModel[]>(this.baseUrl + 'api/regions/details/period', data).subscribe(
            result => {
                //add new region full model
                resolve(result);
                this.eventService.completeLoading();
            },
            error => {
                reject(error);
                this.eventService.showToast('error', error);
                this.eventService.stopLoading();
            });
    });
}

getSensorsFullListByPeriod(startDate: any, endDate: any, isNeedLastResults: boolean = false): Promise < SensorDetailModel[] > {
    this.eventService.startLoading();

    var data = {
        startDate: startDate,
        endDate: endDate,
        isNeedLastResults: isNeedLastResults
    }

        return new Promise((resolve, reject) => {
        this.subscription = this.http.post<SensorDetailModel[]>(this.baseUrl + 'api/sensors/period', data).subscribe(
            result => {
                resolve(result);
                this.eventService.completeLoading();
            },
            error => {
                reject(error);
                this.eventService.showToast('error', error);
                this.eventService.stopLoading();
            });
    });
}

getMonitorsAndResultsCount(): Promise < Counter > {
    return new Promise((resolve, reject) => {
        this.subscription = this.http.get<Counter>(this.baseUrl + 'api/general/counter').subscribe(
            result => {
                resolve(result);
                this.eventService.completeLoading();
            },
            error => {
                reject(error);
            });
    });
}

//NEED REWORK
getSensorsChartData(): Promise < any > {
    this.eventService.startLoading();

    return new Promise((resolve, reject) => {
        this.subscription = this.http.get<any>(this.baseUrl + 'api/sensors/chart').subscribe(
            result => {
                resolve(result && result.length > 0 ? result : []);
                this.eventService.completeLoading();
            },
            error => {
                reject(error);
                this.eventService.showToast('error', error);
                this.eventService.stopLoading();
            });
    });
}

addSensorEvent(sensorEvent: SensorEventModel): Promise < any > {
    return new Promise((resolve, reject) => {
        this.subscription = this.http.post(this.baseUrl + 'api/events', sensorEvent).subscribe(
            result => {
                resolve(result);
            },
            error => {
                reject(error);
                this.eventService.showToast('error', error);
            });
    });
}

updateSensors(sensors: SensorDetailModel): Promise < any > {
    return new Promise((resolve, reject) => {
        this.subscription = this.http.put(this.baseUrl + 'api/sensors', sensors).subscribe(
            result => {
                resolve(result);
            },
            error => {
                reject(error);
                this.eventService.showToast('error', error);
            });
    });
}

deleteSensor(id: number): Promise < HttpResponse < Object >> {
    this.eventService.startLoading();

    return new Promise((resolve, reject) => {
        this.subscription = this.http.delete(this.baseUrl + 'api/sensors/' + id, { observe: 'response' }).subscribe(
            result => {
                resolve(result);
                this.eventService.showToast('success');
                this.eventService.completeLoading();
            },
            error => {
                reject(error);
                console.error(error);
                this.eventService.showToast('error', error);
                this.eventService.stopLoading();
            });
    });
}

deleteAllSensorsResults(): Promise < HttpResponse < Object >> {
    this.eventService.startLoading();

    return new Promise((resolve, reject) => {
        this.subscription = this.http.delete(this.baseUrl + 'api/sensors/all/sensorsResults', { observe: 'response' }).subscribe(
            result => {
                resolve(result);
                this.eventService.showToast('success');
                this.eventService.completeLoading();
            },
            error => {
                reject(error);
                console.error(error);
                this.eventService.showToast('error', error);
                this.eventService.stopLoading();
            });
    });
}

mapRecievedData(data: any) {
    return this.buildSensorHubResponse(data);
}

    private buildResponseCount(obj: any) {
    let data = { monitorsCount: 0, monitorsResultsCount: 0 };

    obj = JSON.parse(obj._body);
    data.monitorsCount = obj.length;

    for (let i = 0; i < data.monitorsCount; i++) {
        data.monitorsResultsCount += obj[i].sensorResults.length;
    }

    return data;
}

    private buildSensorFullModel(obj: any) {
    let data: any = [];

    obj = JSON.parse(obj._body);
    data = obj && obj.map((sensor: any) => this.buildSensorDetailModel(sensor));

    return data;
}

    private buildResponseModel(obj: any) {
    let data: any = {
        sensors: [],
    };

    obj = JSON.parse(obj._body);
    data.sensors = obj && obj.map((sensor: any) => this.buildSensorModel(sensor));

    return data;
}

    private buildSensorModel(obj: any): SensorModel {
    let sensor = new SensorModel();

    sensor.id = obj.id;
    sensor.monitorIdToLeft = obj.monitorIdToLeft;
    sensor.monitorIdToRight = obj.monitorIdToRight;
    sensor.distanceLeft = obj.distanceLeft;
    sensor.distanceRight = obj.distanceRight;

    sensor.iconUrl = '/assets/images/marker_icon_' + (obj.regionId <= 10 ? obj.regionId : 'default') + '.png';
    sensor.iconUrlDanger = '/assets/images/marker_icon_danger.png';
    sensor.iconUrlEmpty = '/assets/images/marker_icon_empty.png';
    sensor.regionName = obj.regionName;
    sensor.regionId = obj.regionId;
    sensor.monitorId = obj.monitorId;
    sensor.latitude = Number(obj.latitude);
    sensor.longitude = Number(obj.longitude);
    sensor.label = obj.monitorId.toString();
    sensor.onsiteWindMax = obj.onsiteWindMax;
    sensor.onsiteWindMin = obj.onsiteWindMin;
    sensor.normalAzimuthToLeft = obj.normalAzimuthToLeft;
    sensor.normalAzimuthToRight = obj.normalAzimuthToRight;
    sensor.onsiteWindCenterAngle = obj.onsiteWindCenterAngle;

    sensor.labelOptions = {
        color: '#ffffff',
        fontFamily: '',
        fontSize: '14px',
        fontWeight: 'bold',
        text: sensor.label,
    };

    sensor.averageResult = new SensorResultModel();

    if (obj.sensorResults.length > 0) {
        let count: number = obj.sensorResults.length;
        let startTime: any;
        let endTime: any;

        //mapping averageResult sensor result
        for (let i = 0; i < count; i++) {
            let tempRes: any = obj.sensorResults[i];

            startTime = new Date(tempRes.timeBegin);
            endTime = new Date(tempRes.timeEnd);

            sensor.averageResult.concentration += Number(tempRes.concentration);
            sensor.averageResult.windSpeed += Number(tempRes.windSpeed);
            sensor.averageResult.windDirection += Number(tempRes.windDirection);
            sensor.averageResult.length += Number((endTime.getTime() - startTime.getTime()) / 1000 * tempRes.windSpeed);
        }

        sensor.averageResult.concentration = parseFloat((sensor.averageResult.concentration / count).toFixed(4));
        sensor.averageResult.windSpeed = parseFloat((sensor.averageResult.windSpeed / count).toFixed(4));
        sensor.averageResult.windDirection = parseFloat((sensor.averageResult.windDirection / count).toFixed(4));
        sensor.averageResult.length = parseFloat((sensor.averageResult.length).toFixed(4));
    }

    sensor.sensorResults = obj.sensorResults && obj.sensorResults.map((sensorResult: any) => this.buildSensorResultModel(sensorResult));
    sensor.sensorResultsCount = obj.sensorResults ? obj.sensorResults.length : 0;

    return sensor;
}

    public buildSensorDetailModel(obj: any): SensorDetailModel {
    let sensorDetail = new SensorDetailModel();

    sensorDetail.id = obj.id;
    sensorDetail.monitorId = obj.monitorId;
    sensorDetail.regionId = obj.regionId;
    sensorDetail.regionName = obj.regionName;
    sensorDetail.latitude = obj.latitude;
    sensorDetail.longitude = obj.longitude;
    sensorDetail.monitorIdToLeft = obj.monitorIdToLeft;
    sensorDetail.distanceLeft = obj.distanceLeft;
    sensorDetail.azimuthLeft = obj.azimuthLeft;
    sensorDetail.normalAzimuthToLeft = obj.normalAzimuthToLeft;
    sensorDetail.monitorIdToRight = obj.monitorIdToRight;
    sensorDetail.azimuthRight = obj.azimuthRight;
    sensorDetail.distanceRight = obj.distanceRight;
    sensorDetail.normalAzimuthToRight = obj.normalAzimuthToRight;

    sensorDetail.sensorResultsCount = obj.sensorResultsCount;
    sensorDetail.sensorResults = obj.sensorResults && obj.sensorResults.map((sensorResult: any) => this.buildSensorResultModel(sensorResult));

    return sensorDetail;
}

    public buildSensorDetailSingleModel(obj: any): SensorDetailModel {
    let sensorDetail = new SensorDetailModel();

    obj = JSON.parse(obj._body);

    sensorDetail.id = obj.id;
    sensorDetail.monitorId = obj.monitorId;
    sensorDetail.regionId = obj.regionId;
    sensorDetail.latitude = obj.latitude;
    sensorDetail.longitude = obj.longitude;
    sensorDetail.monitorIdToLeft = obj.monitorIdToLeft;
    sensorDetail.distanceLeft = obj.distanceLeft;
    sensorDetail.azimuthLeft = obj.azimuthLeft;
    sensorDetail.normalAzimuthToLeft = obj.normalAzimuthToLeft;
    sensorDetail.monitorIdToRight = obj.monitorIdToRight;
    sensorDetail.azimuthRight = obj.azimuthRight;
    sensorDetail.distanceRight = obj.distanceRight;
    sensorDetail.normalAzimuthToRight = obj.normalAzimuthToRight;
    sensorDetail.onsiteWindMin = obj.onsiteWindMin;
    sensorDetail.onsiteWindMax = obj.onsiteWindMax;

    sensorDetail.sensorResultsCount = obj.sensorResultsCount;
    sensorDetail.sensorResults = obj.sensorResults && obj.sensorResults.map((sensorResult: any) => this.buildSensorResultModel(sensorResult));

    return sensorDetail;
}

    private buildSensorResultModel(obj: any): SensorResultModel {
    let sensorResult = new SensorResultModel();

    sensorResult.timeBegin = obj.timeBegin;
    sensorResult.timeEnd = obj.timeEnd;
    sensorResult.concentration = obj.concentration;
    sensorResult.windSpeed = obj.windSpeed;
    sensorResult.windDirection = obj.windDirection;

    return sensorResult;
}

    private buildSensorHubResponse(obj: any): SensorHubModel {
    let data: any = [];

    data = obj && obj.map((sensor: any) => this.buildSensorModel(sensor));

    return data;
}

    private buildResponseRegionModel(obj: any) {
    let data: any = [];

    //obj = JSON.parse(obj._body);
    data = obj && obj.map((region: any) => this.buildRegionModel(region));

    return data;
}

    private buildRegionModel(obj: any): RegionModel {
    let region: any = new RegionModel();

    region.id = obj.id;
    region.name = obj.name;
    region.email = obj.email;
    region.phone = obj.phone;
    region.webSite = obj.webSite;
    region.coordinates = obj.coordinates;
    region.sensorsCount = obj.sensors ? obj.sensors.length : 0;

    if (obj.algorithm) {

        let algorithmModel: AlgorithmModel = new AlgorithmModel();

        algorithmModel.monitorType = obj.algorithm.monitorType;
        algorithmModel.detectionPointFlux = obj.algorithm.detectionPointFlux;
        algorithmModel.detectionWindSpeed = obj.algorithm.detectionWindSpeed;
        algorithmModel.timeInterval = obj.algorithm.timeInterval;
        algorithmModel.timeResolution = obj.algorithm.timeResolution;
        algorithmModel.minDataForAverage = obj.algorithm.minDataForAverage;
        algorithmModel.numberLineGroupTime = obj.algorithm.numberLineGroupTime;
        algorithmModel.numberLineGroupWindDirection = obj.algorithm.numberLineGroupWindDirection;
        algorithmModel.windSmoothingGroup = obj.algorithm.windSmoothingGroup;
        algorithmModel.maxRangeBetweenMonitors = obj.algorithm.maxRangeBetweenMonitors;
        algorithmModel.sourceLocationUpdateInterval = obj.algorithm.sourceLocationUpdateInterval;
        algorithmModel.numbersOfGroupForLinearRegression = obj.algorithm.numbersOfGroupForLinearRegression;

        region.algorithm = algorithmModel;
    }

    region.sensors = obj.sensors && obj.sensors.map((sensor: any) => this.buildSensorModel(sensor));

    return region;
}

    public dispose() {
    if (this.subscription) {
        this.subscription.unsubscribe();
    }
}

    ////isnt used anywhere --------------------------------------------------------------------------------
//getSensor(id: number): Promise<SensorDetailModel> {
    //    this.eventService.startLoading();

    //    return new Promise(resolve => {
    //        this.subscription = this._httpService.get(this.baseUrl + 'api/sensors/' + id).subscribe(
    //            result => {
    //                resolve(this.buildSensorDetailSingleModel(result));
    //                this.eventService.completeLoading();
    //            },
    //            error => {
    //                resolve(error);
    //                this.eventService.showToast('error', error);
    //                this.eventService.stopLoading();
    //            });
    //    });
    //}

    ////isnt used anywhere --------------------------------------------------------------------------------
    //updateSensors(sensors: SensorDetailModel[]): Promise<any> {
    //    this.eventService.startLoading();

    //    return new Promise(resolve => {
    //        this.subscription = this._httpService.put(this.baseUrl + 'api/sensors', sensors).subscribe(
    //            result => {
    //                resolve(result);
    //                this.eventService.showToast('success');
    //                this.eventService.completeLoading();
    //            },
    //            error => {
    //                resolve(error);
    //                this.eventService.showToast('error', error);
    //                this.eventService.stopLoading();
    //            });
    //    });
    //}

    ////isnt used anywhere --------------------------------------------------------------------------------
    //getSensorsData(isGenerateRandom: boolean = false): Promise<any> {
    //    this.eventService.startLoading();

    //    return new Promise(resolve => {
    //        this.subscription = this._httpService.get(this.baseUrl + 'api/sensors/' + isGenerateRandom).subscribe(
    //            value => {
    //                resolve(this.buildResponseModel(value));
    //                this.eventService.completeLoading();
    //            },
    //            error => {
    //                resolve(error);
    //                this.eventService.showToast('error', error);
    //                this.eventService.stopLoading();
    //            });
    //    });
    //}

    ////isnt used anywhere --------------------------------------------------------------------------------
    //getSensorsFullList(): Promise<any> {
    //    this.eventService.startLoading();
    //    return new Promise(resolve => {
    //        this.subscription = this._httpService.get(this.baseUrl + 'api/sensors/false').subscribe(
    //            value => {
    //                resolve(this.buildSensorFullModel(value));
    //                this.eventService.completeLoading();
    //            },
    //            error => {
    //                resolve(error);
    //                this.eventService.showToast('error', error);
    //                this.eventService.stopLoading();
    //            });
    //    });
    //}

    ////isnt used anywhere --------------------------------------------------------------------------------
    ////using in monitors page
    //getSensors(startDate: any, endDate: any, sensorIds: Array<number>): Promise<any> {
    //    this.eventService.startLoading();

    //    var data = {
    //        startDate: startDate,
    //        endDate: endDate,
    //        isNeedLastResults: false,
    //        sensorIds: sensorIds,
    //    }

    //    return new Promise(resolve => {
    //        this.subscription = this._httpService.post(this.baseUrl + 'api/sensors/period', data).subscribe(
    //            value => {
    //                resolve(this.buildResponseModel(value));
    //                this.eventService.completeLoading();
    //            },
    //            error => {
    //                resolve(error);
    //                this.eventService.showToast('error', error);
    //                this.eventService.stopLoading();
    //            });
    //    });
    //}
}
