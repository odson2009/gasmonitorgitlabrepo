import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiMap } from '../maps/api.map';
import { Tokens, Token } from '../interfaces/token.interface';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class TokensService {
    private baseUrl: string;

    constructor(
        @Inject('BASE_URL') baseUrl: string,
        private http: HttpClient,
    ) {
        this.baseUrl = baseUrl;
    }

    isTokenExpired(type: 'access' | 'refresh') {
        const token = localStorage.getItem(type === 'access' ? 'accessToken' : 'refreshToken');

        if (!token) {
            return true;
        }

        const decodedToken: Token = this.parseJwt(token);

        if (!decodedToken) {
            return true;
        }

        return (new Date()).getTime() > (decodedToken.exp * 1000); // consist a response in seconds (convert to ms)
    }

    parseJwt(token): any {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');

        return JSON.parse(window.atob(base64));
    }

    setTokens(tokens: Tokens) {
        for (const token of Object.keys(tokens)) {
            this.setToken(token, tokens[token]);
        }
    }

    setToken(tokenName: string, tokenValue: string) {
        localStorage.setItem(tokenName, tokenValue);
    }

    getToken(type: 'access' | 'refresh') {
        return localStorage.getItem(`${type}Token`);
    }

    clearTokens() {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('refreshToken');
    }

    /**
     * If access token expired - update him
     */
    updateAccessToken(): Observable<any> {
        return this.http.post(this.baseUrl + apiMap.refreshToken.url, {
            refreshToken: localStorage.getItem('refreshToken')
        }).pipe(tap((res: Tokens) => {
            this.setTokens(res);
        }));
    }
}
