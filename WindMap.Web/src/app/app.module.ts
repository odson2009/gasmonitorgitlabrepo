import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from "./app-routing.module";

//project components
import { AppComponent } from './components/app/app.component';
import { JwtInterceptor} from './components/helpers/jwt.interceptor';
import { CoreModule } from './Modules/core.module';
import { SharedModule } from './Modules/shared.module';


@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        SharedModule,  // contains all shared components/pipes/directives
        CoreModule,    // contains all services
        BrowserModule,
        HttpModule,
        AppRoutingModule,
        HttpClientModule,
        NoopAnimationsModule,
        //AngularDateTimePickerModule
    ],
    bootstrap: [AppComponent],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
    ]

})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
