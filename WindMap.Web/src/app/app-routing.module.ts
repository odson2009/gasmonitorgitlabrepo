﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegionListComponent } from "./components/regions/region-list.component";
import { RegionActionComponent } from "./components/regions/region-action.component";
import { MonitorListComponent } from "./components/monitors/monitor-list.component";
import { MonitorActionComponent } from "./components/monitors/monitor-action.component";
import { TestsComponent } from "./components/tests/tests.component";
import { EventsListComponent } from "./components/events/events-list.component";
import { AlgorithmsComponent } from './components/algorithms/algorithms.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './components/guards/auth.guard';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { UserListComponent } from './components/users/user-list.component';
import { UserActionComponent } from './components/users/user-action.component';
import { CompanyListComponent } from './components/companies/company-list.component';
import { CompanyActionComponent } from './components/companies/company-action.component';
import { PermissionGuard } from './components/guards/permission.guard';
import { DefaultComponent } from './components/default/default.component';
import { DeveloperComponent } from './components/developer/developer.component';
import { ChangePasswordComponent } from './components/changePassword/changePassword.component';


const routes: Routes = [
    { path: '', redirectTo: 'navmenu', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },

    {
        path: 'navmenu',
        component: NavMenuComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '', redirectTo: 'home', pathMatch: 'full'
            },
            { path: 'home',  component: HomeComponent, canActivate: [PermissionGuard], data: { permission: 'Dashboard' } },
            { path: 'default', component: DefaultComponent, pathMatch: 'full' },
            { path: 'changepassword', component: ChangePasswordComponent, pathMatch: 'full' },
            { 
                path: 'map',
                loadChildren: './Pages/map/map.module#MapModule',
                canActivate: [PermissionGuard],
                data: { permission: 'Map'}
            },
            /* { path: 'map', component: MapComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Map'} },
            { path: 'map/:id', component: MapComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Map' }},
            { path: 'map/:id/:date', component: MapComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Map' }}, */

            { path: 'sites', component: RegionListComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Sites' }},            
            { path: 'site/edit/:id', component: RegionActionComponent, canActivate: [PermissionGuard], data: { permission: 'Sites' }},
            { path: 'site/add', component: RegionActionComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Sites' }},
            { path: 'monitors', component: MonitorListComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Monitors' } },
            { path: 'monitor/edit/:id', component: MonitorActionComponent, canActivate: [PermissionGuard], data: { permission: 'Monitors' }},
            { path: 'monitor/add', component: MonitorActionComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Monitors' }},
            { path: 'leak', component: EventsListComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Leak' } },
            { path: 'tests', component: TestsComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Tests' }},
            { path: 'algorithms', component: AlgorithmsComponent, pathMatch: 'full' },
            { path: 'users', component: UserListComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Users' } },
            { path: 'user/add', component: UserActionComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Users' }  },
            { path: 'user/edit/:id', component: UserActionComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Users' }  },
            { path: 'companies', component: CompanyListComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Companies' }},
            { path: 'company/add', component: CompanyActionComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Companies' }},
            { path: 'company/edit/:id', component: CompanyActionComponent, pathMatch: 'full', canActivate: [PermissionGuard], data: { permission: 'Companies' } },
            { path: 'developer', component: DeveloperComponent, canActivate: [PermissionGuard], data: { permission: 'Developer' } },
        ]
    },

    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false, initialNavigation: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }