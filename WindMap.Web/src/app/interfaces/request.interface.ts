export interface ApiMap {
    [type: string]: ApiMapItem;
}

export interface ApiMapItem {
    url: string;
    method: 'get' | 'post' | 'put' | 'delete';
    mockup?: any;
    errMockup?: any;
    errors?: {
        [name: number]: string;
    };
    headers?: {
        [name: string]: string;
    };
    responseType?: string;
}
