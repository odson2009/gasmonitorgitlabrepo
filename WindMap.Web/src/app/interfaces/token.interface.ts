export interface Token {
    exp: number;
}

export interface Tokens {
    accessToken: string;
    refreshToken: string;
}
