import { Directive, Input, ElementRef, Renderer, OnChanges, SimpleChanges } from '@angular/core';

@Directive({ selector: 'img[imgPreview]' })

export class ImagePreview {

    @Input() image: any;
    @Input() isImage: any;

    constructor(private el: ElementRef, private renderer: Renderer) { }

    ngOnChanges(changes: SimpleChanges) {

        let reader = new FileReader();
        let el = this.el;
        let someCondition = this.isImage;

        reader.onloadend = function (e) {
            if (someCondition == 'true') {
                el.nativeElement.src = reader.result;
            }
            else {
                let dataTypeRes = /:(.+);/.exec(reader.result);

                if (dataTypeRes && dataTypeRes.length == 2 && dataTypeRes[1] != undefined) {
                    switch (dataTypeRes[1]) {
                        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                        case 'application/vnd.ms-excel':
                            el.nativeElement.src = '/assets/images/icon_xls.png';
                            break;
                        default:
                            el.nativeElement.src = '/assets/images/icon_unsupported.png';
                            break;
                    }
                }
                else {
                    el.nativeElement.src = '/assets/images/icon_unsupported.png';
                }
            }
        };

        if (this.image) {
            return reader.readAsDataURL(this.image);
        }

    }

}
