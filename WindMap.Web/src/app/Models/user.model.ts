export class UserModel {
    id: number;
    firstName: string;
    lastName: string;
    login: string;
    email: string;
    roleId: string;
    roleName: string;
    isActive: boolean;
    uiElementsId: any;
    companyObjs: any;
}

export class UserTokenModel {
    changedPassword: boolean;
    email: string;
    exp: number;
    given_name: string;
    permissions: string[];
    role: string;
    sub: string;
}

