import { SensorModel, SensorDetailModel } from "./sensor.model";

export class RegionModel {
    id: number;
    name: string;
    email: string;
    phone: string;
    webSite: string;
    companyId: number;
    companyName: string;
    latitude: number;
    longitude: number;
    zoom: number;
    coordinates: Array<Coordinate>;
    minDataForAverage: number;
    algorithm: AlgorithmModel;
    sensors: Array<SensorModel>;
}

export class Coordinate {
    latitude: number;
    longitude: number;
}

export class AlgorithmModel {
    id: number;
    monitorType: string;
    detectionPointFlux: number;
    detectionWindSpeed: number;
    timeInterval: number;
    timeResolution: number;
    minDataForAverage: number;
    numberLineGroupTime: number;
    numberLineGroupWindDirection: number;
    windSmoothingGroup: number;
    maxRangeBetweenMonitors: number;
    sourceLocationUpdateInterval: number;
    numbersOfGroupForLinearRegression: number;

    get getResultsToReceiveCount(): number {
        return this.sourceLocationUpdateInterval * this.numberLineGroupTime;
    };
}

export class RegionSensorsModel {
    id: number;
    name: string;
    zoom: number;
    latitude: number;
    longitude: number;
    sensors: SensorDetailModel[];
    coordinates: Array<Coordinate>;
}
