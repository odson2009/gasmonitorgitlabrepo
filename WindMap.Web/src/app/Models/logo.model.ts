﻿export class LogoModel {
    name: number;
    logo: string;
    isDefault: boolean;
}