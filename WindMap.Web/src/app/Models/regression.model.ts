﻿export class RegressionParameter {
    X_TWD: number;
    Y_PF: number;
}

export class RegressionResult {
    b: number;
    m: number;
    //pearson correlation
    r: number;

    get x_intercept(): number {
        return -1 * this.b / this.m;
    };

    toJSON() {
        return {
            b: this.b,
            m: this.m,
            r: this.r,
            x_intercept: this.x_intercept,
        }
    }
}