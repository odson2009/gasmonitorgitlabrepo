import { RegressionResult, RegressionParameter } from "./regression.model";

export class WindVector {
    lat1: number;
    lng1: number;
    lat2: number;
    lng2: number;
    length: number;
    azimuth: number;
    color: string;

    backLineLat: number;
    backLineLng: number;
}

export class Arrow {
    startLatitude: number;
    startLongitude: number;
    endLatitude: number;
    endLongitude: number;
}

export class Point {
    latitude: number;
    longitude: number;
}

class SensorModelBase extends Point {
    id: number;
    regionId: number;
    regionName: string;
    monitorId: string;
    monitorIdToLeft: string;
    monitorIdToRight: string;
    distanceLeft: number;
    distanceRight: number;
    onsiteWindMax: number;
    onsiteWindMin: number;
    normalAzimuthToLeft: number;
    normalAzimuthToRight: number;
    onsiteWindCenterAngle: number;
    sensorResultsCount: number;
    arrow: Arrow;
    isActive: boolean;

    //tmp
    iconUrlEmpty: string;
    iconUrl: string;
    iconUrlDanger: string;
    label: string;
    labelOptions: any;
    isVisible: boolean = true;
    isTolTipVisible: boolean = false;
}

export class SensorModel extends SensorModelBase {
    latLng: any;
    pair: SensorModel;
    //for ellipse
    points: any[];

    X_TWD: number[] = [];
    PF1_Average: number;
    PF2_Average: number;

    windVector: WindVector;
    averageResult: SensorResultModel;
    isLeak: boolean = false;
    //temp list for testing//TODO:remove after testing
    sensorResults: Array<SensorResultModel>;
    bufferSensorResults: Array<SensorResultModel>;
    vecEndPoints: any;

    //scale all points inside circle
    scale: number = 1;
    twoWeeksConcentration: number = 0;
}

export class SensorDetailModel extends SensorModelBase {
    azimuthLeft: number;
    azimuthRight: number;
    distanceLeft: number;
    distanceRight: number;
    sensorResults: Array<SensorResultTmpModel>;
    distance:number;
}

export class SensorHubModel {
    sensorId: number;
    sensorResults: Array<SensorResultModel>;
}

class SensorResultBaseModel {
    timeBegin: string;
    timeEnd: string;
    concentration: number = 0;
    windSpeed: number = 0;
    windDirection: number = 0;
}

export class SensorResultModel extends SensorResultBaseModel {
    length: number = 0;
}


export class SensorResultTmpModel extends SensorResultModel {
    TWD: number = 0;
    PF: number = 0;
}

//root 1
export class Main_Buffer {
    constructor() {
        this.sensorResults = [];
        this.newResults = [];
        this.tmp_buffer = new TMP();
        this.averageSummary = [];
        this.testResults = [];
        this.sensorResultsAll = [];
    }

    //for ferst 60 results
    sensorResults: Array<SensorResultTmpModel>;

    //next 5 results
    newResults: Array<SensorResultTmpModel>;
    endDate: string;

    //for groupes of the results
    tmp_buffer: TMP;
    averageSummary: GroupAverageResultSummary[];

    //test data for fminsearch and polyfit 
    testResults: TestResult[];

    //for ferst all results
    sensorResultsAll: Array<SensorResultTmpModel>;
}
   
//--TMP1, TMP2, ... , TMP5
export class TMP {
    constructor() {
        this.AverageData = [];
        this.GroupTMP = [];
        this.LR_Result = new RegressionResult();
    }

    //smoothed data
    AverageData: AverageData[];
    GroupTMP: GroupTMP[];

    //Linear regression results
    LR_Result: RegressionResult;
}
  
export class GroupTMP
{
    constructor()
    {
        this.AverageData = [];
        this.AverageResults = new AverageData();
        this.LR_Result = new RegressionResult();
    }

    AverageData: AverageData[];
    AverageResults: AverageData;

    //Linear regression results
    LR_Result: RegressionResult;
}

//Temporal class for testing and comparing fminsearch / polyfit methods
export class TestResult
{
    constructor() {
        this.smoothed = new FitData();
        this.average = new FitData();
    }

    step: number;
    endDate: string;
    smoothed: FitData;
    average: FitData;
}

export class FitData {

    constructor() {
        this.fminsearch = new fminsearch();
        this.polyfit = new polyfit();
    }

    fminsearch: fminsearch;
    polyfit: polyfit;
}

export class fminsearch {
    a: number;
    sigma: number;
    mu: number;
}

export class polyfit {
    a: number;
    sigma: number;
    mu: number;
    A: number;
    B: number;
    C: number;
}

export class AverageData {
    TWD_Average: number = 0;
    PF_Average: number = 0;
}

export class GroupAverageResult {
    constructor() {
        this.averageData = new AverageData();
        this.linearRegressionResult = new RegressionResult();
    }

    averageData: AverageData;
    linearRegressionResult: RegressionResult;
}

export class GroupAverageResultSummary {
    constructor() {
        this.step = 0;
        this.results = [];
        this.regression = new RegressionResult();
    }

    step: number;
    results: GroupAverageResult[];
    regression: RegressionResult;
}
/**
 * Presents a data model that received via socket in real-time for sensors updating */
export interface SensorRTUpdModel {
    concentration: number;
    sensor: any;
    sensorId: number;
    timeBegin: string;
    timeEnd: string;
    windDirection: number;
    windSpeed: number;
}

export interface LeakModel {
    averageData: AvgLeakItem[];
    sensorId: number;
    maxPfRadiusConstant: number,
    newMaxPintPF: number;
    maxPintPF: number;
    status: number;
    status2: number;
}

export interface AvgLeakItem {
    pf: number;
    twd: number;
}
