export class SensorEventModel {
    id: number;
    sensorId: number;
    monitorId: number;
    pairedMonitorId: number;
    status: string;
    eventTime: Date;
    sourceLatitude: number;
    sourceLongitude: number;
    parallelWindDirection: number;
    crossWindDirection  : number;
    averageWindDirection: number;
    emissionRate: number;
    regionId: number;
    regionName: string;

    endEventId: number;
    endEvent: SensorEventModel;
}