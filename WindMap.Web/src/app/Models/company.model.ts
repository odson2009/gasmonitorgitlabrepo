import { RegionModel } from "./region.model";

export class CompanyModel {
    id: number;
    name: string;
    phone: string;
    email: string;
    website: string;
    imageBase64: string;
    regions: Array<RegionModel>;
}

