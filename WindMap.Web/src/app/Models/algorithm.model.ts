﻿class AproximationResult {

    a: number;
    mu: number;
    sigma: number;
}

class ComplexAproximation {

    fminsearchAproximation: AproximationResult;
    polyfitAproximation: AproximationResult;
    fminsearchAproximationAverage: AproximationResult;
    polyfitAproximationAverage: AproximationResult;
}



export class Sensor {

    constructor() {
        this.step = 0;
        this.accumulatedAproximations = [];
        this.allResults = [];
        this.resultsToSend = [];
    }
    //tmp data
    step: number;
    accumulatedAproximations: AccumulatedAproximation[];

    allResults: SensorResult[];
    resultsToSend: SensorResult[];

    requestedResult: RequestedResult;
}

export class AccumulatedAproximation extends ComplexAproximation {

    step: number;
    endDate: Date;
}

export class RequestedResult {

    constructor() {
        this.processedSensorResults = [];
    }

    isLeak: boolean;
    processedSensorResults: SensorResult[];
    smoothingResult: SmoothingResult;
}

export class SensorResult {
    sensorId: number;
    timeBegin: Date;
    timeEnd: Date;
    concentration: number;
    windSpeed: number;
    windDirection: number;
}

export class SmoothingResult extends ComplexAproximation {

    transformedData: TransformedData[];
    movingAverage: TransformedData[];
    groupedMovingAverage: MovingAverageGroup[];
    groupedMovingAverageRegression: RegressionResult;
}

class TransformedData {

    pf: number;
    twd: number;
}

class RegressionResult {

    m: number;
    b: number;
    r: number;
    interception: number;
}

class MovingAverageGroup {

    items: TransformedData[];
    averageData: TransformedData;
    regressionResult: RegressionResult;
}