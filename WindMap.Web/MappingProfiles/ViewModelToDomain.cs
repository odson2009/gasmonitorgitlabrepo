﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindMap.DAL.Models;
using WindMap.Services.Models;
using WindMap.Web.ViewModels;
using WindMap.Web.ViewModels.CompanyViewModels;

namespace WindMap.Web.MappingProfiles
{
    public class ViewModelToDomain : Profile
    {
        public ViewModelToDomain()
        {
            // --- RelationPermission
            CreateMap<RelationPermissionViewModel, RelationPermission>()
                .ForMember(d => d.User, s => s.Ignore())
                .ForMember(d => d.Company, s => s.Ignore())
                .ForMember(d => d.Region, s => s.Ignore());

            // --- Region
            CreateMap<RegionViewModel, Region>()
                .ForMember(d => d.Company, s => s.Ignore())
                .ForMember(d => d.Algorithmes, s => s.MapFrom(p => p.Algorithm != null ? new List<AlgorithmViewModel>() { p.Algorithm } : null))
                .ForMember(d => d.RelationPermissions, s => s.Ignore())
                .ForMember(d => d.Coordinates, s => s.MapFrom(p => p.Coordinates.SerializeToString()));

            // --- Company
            CreateMap<CreateCompanyViewModel, Company>()
                .ForMember(d => d.Id, s => s.Ignore())
                .ForMember(d => d.Regions, s => s.Ignore())
                .ForMember(d => d.RelationPermissions, s => s.Ignore())
                .ForMember(d => d.Logo, s => s.MapFrom(p => p.Logo != null ? Encoding.ASCII.GetBytes(p.Logo) : null));

            CreateMap<UpdateCompanyViewModel, Company>()
                .ForMember(d => d.Regions, s => s.Ignore())
                .ForMember(d => d.RelationPermissions, s => s.Ignore())
                .ForMember(d => d.Logo, s => s.MapFrom(p => p.Logo != null ? Encoding.ASCII.GetBytes(p.Logo) : null));

            CreateMap<CompanyViewModel, Company>()
                .ForMember(d => d.Logo, s => s.Ignore());

            // --- User
            CreateMap<SignUpViewModel, User>()
                .ForMember(d => d.UserName, s => s.MapFrom(d => d.Login))
                .ForMember(d => d.Email, s => s.MapFrom(d => d.Email))
                .ForMember(d => d.FirstName, s => s.MapFrom(d => d.FirstName))
                .ForMember(d => d.LastName, s => s.MapFrom(d => d.LastName))
                .ForMember(d => d.IsActive, s => s.MapFrom(d => d.IsActive))
                .ForAllOtherMembers(d => d.Ignore());

            // --- Algorithm
            CreateMap<AlgorithmViewModel, Algorithm>()
                .ForMember(d => d.RegionId, s => s.Ignore())
                .ForMember(d => d.Region, s => s.Ignore());

            // --- Address
            CreateMap<AddressViewModel, Address>()
                .ForMember(d => d.Id, s => s.Ignore())
                .ForMember(d => d.RegionId, s => s.Ignore())
                .ForMember(d => d.Region, s => s.Ignore());

            // --- Location
            CreateMap<LocationViewModel, Location>()
                .ForMember(d => d.Id, s => s.Ignore())
                .ForMember(d => d.Addresses, s => s.Ignore());

            //need fix
            //CreateMap<SensorEventViewModel, SensorEvent>()
            //    .ForMember(d => d.Id, s => s.Ignore())
            //    .ForMember(d => d.Sensor, s => s.Ignore())
            //    .ForMember(d => d.PairedSensor, s => s.Ignore());

            // --- Sensor
            CreateMap<SensorViewModel, Sensor>()
                //.ForMember(d => d.Id, s => s.Ignore())
                .ForMember(d => d.Region, s => s.Ignore())
                .ForMember(d => d.SensorResults, s => s.Ignore())
                .ForMember(d => d.SensorEvents, s => s.Ignore());

            // --- SensorResult
            CreateMap<SensorResultViewModel, SensorResult>()
                .ForMember(d => d.Id, s => s.Ignore())
                .ForMember(d => d.Sensor, s => s.Ignore());

            // --- SensorResult for PRSI
            CreateMap<SensorWindConcentrationResultViewModel, SensorResult>()
                .ForMember(d => d.Id, s => s.Ignore())
                .ForMember(d => d.Sensor, s => s.Ignore());


            CreateMap<SensorResultReportViewModel, SensorBufferReportModel>();
            CreateMap<GroupTMPViewModel, GroupTMP>();
            CreateMap<ResultsDataViewModel, ResultsData>();
            CreateMap<AverageDataViewModel, AverageData>();
            CreateMap<LinearRegressionViewModel, LinearRegression>();
            CreateMap<TestResultViewModel, TestResult>();
            CreateMap<FitDataViewModel, FitData>();
            CreateMap<FminsearchViewModel, Fminsearch>();
            CreateMap<PolyfitViewModel, Polyfit>();
            CreateMap<SensorResultTmpViewModel, SensorResultTmp>();
        }
    }
}
