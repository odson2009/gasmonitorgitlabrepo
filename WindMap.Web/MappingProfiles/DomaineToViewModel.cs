﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Text;
using WindMap.DAL.Models;
using WindMap.Services.Models;
using WindMap.Web.ViewModels;

namespace WindMap.Web.MappingProfiles
{
    public class DomaineToViewModel : Profile
    {
        public DomaineToViewModel()
        {
            CreateMap<Region, RegionViewModel>()
                  .ForMember(d => d.Algorithm, s => s.MapFrom(p => p.Algorithmes.FirstOrDefault()))
                  .ForMember(d => d.RelationPermissions, s => s.Ignore())
                  .ForMember(d => d.Coordinates, s => s.MapFrom(c => c.Coordinates.ParseCoordinates()));
                 
            CreateMap<Company, CompanyViewModel>()
                .ForMember(d => d.Regions, s => s.MapFrom(p => p.Regions))
                .ForMember(d => d.RelationPermissions, s => s.Ignore())
                .ForMember(d => d.Users, s => s.Ignore())
                .ForMember(d => d.Logo, s => s.MapFrom(p => p.Logo != null ? Encoding.ASCII.GetString(p.Logo) : null));

            CreateMap<IdentityRole, RoleViewModel>();

            CreateMap<UiElement, UiElementViewModel>();

            CreateMap<User, SignUpViewModel>()
                .ForMember(dst => dst.Login, src => src.MapFrom(_ => _.UserName))
                .ForMember(dst => dst.RoleId, src => src.Ignore())
                .ForMember(dst => dst.RoleName, src => src.Ignore())
                .ForMember(dst => dst.UiElementsId, src => src.MapFrom(_ => _.UiPermission.Select(x => x.UiElementId)))

                .ForMember(dst => dst.CompanyObjs, src => src.MapFrom(p => p.RelationPermissions.GroupBy(item => item.CompanyId,
                (key, group) => new CompanyObj
                {
                    Id = key.Value,
                    Name = group.FirstOrDefault().Company.Name,
                    RegionsId = group.Where(_ => _.RegionId.HasValue).Select(t => t.RegionId.Value).ToList()
                })));

            CreateMap<Algorithm, AlgorithmViewModel>();

            CreateMap<Address, AddressViewModel>();

            CreateMap<Location, LocationViewModel>();

            CreateMap<Sensor, SensorViewModel>()
                .ForMember(d => d.RegionName, s => s.MapFrom(p => p.Region.Name))
                .ForMember(d => d.SensorResultsCount, s => s.MapFrom(p => p.SensorResults.Count()));

            CreateMap<Sensor, SensorSimpleDataViewModel>()
                .ForMember(d => d.RegionName, s => s.MapFrom(p => p.Region.Name))
                .ForMember(d => d.SensorResultsCount, s => s.MapFrom(p => p.SensorResults.Count()));

            CreateMap<SensorEvent, SensorEventViewModel>()
                .ForMember(d => d.SensorId, s => s.MapFrom(p => p.Sensor.Id));

            CreateMap<Calculation, CalculationViewModel>();

            CreateMap<SensorResult, SensorResultViewModel>();

            CreateMap<Sensor, SensorInputResultViewModel>()
                .ForMember(d => d.SensorId, s => s.MapFrom(p => p.Id))
                .ForMember(d => d.SensorResults, s => s.MapFrom(p => p.SensorResults));

            CreateMap<FieldError, FieldErrorViewModel>();

            CreateMap<ErrorModel, ErrorViewModel>()
                .ForMember(d => d.StackTrace, s => s.Ignore());

            CreateMap<MonitorCount, MonitorCountViewModel>();
        }
    }
}
