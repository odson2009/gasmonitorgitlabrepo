﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Web.ViewModels
{
    public class SensorInputResultViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("sensorResults")]
        public ICollection<SensorResultViewModel> SensorResults {get;set;}

    }
}
