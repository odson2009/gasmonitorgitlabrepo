﻿using Newtonsoft.Json;
using System;
using WindMap.Web.Extensions;

namespace WindMap.Web.ViewModels
{
    public class SensorResultViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("concentration")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double Concentration { get; set; }

        [JsonProperty("windSpeed")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double WindDirection { get; set; }
    }

    public class SensorWindConcentrationResultViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("timeBegin")]
        public DateTime TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty("concentration")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double Concentration { get; set; }

        [JsonProperty("windSpeed")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double WindDirection { get; set; }

        [JsonProperty("TEMP")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double TEMP { get; set; }

        [JsonProperty("GUST")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double GUST { get; set; }

        [JsonProperty("HZD_V")]
        [JsonConverter(typeof(RoundingJsonConverter), 4)]
        public double HZD_V { get; set; }

        [JsonProperty("isConcentration")]
        public bool IsConcentration { get; set; }
    }

}
