﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace WindMap.Web.ViewModels
{
    public class RegionViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("phone")]
        public string Phone { get; set; }

        [Required]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("webSite")]
        public string WebSite { get; set; }

        [Required]
        [JsonProperty("algorithm")]
        public AlgorithmViewModel Algorithm { get; set; }

        [JsonProperty("addresses")]
        public ICollection<AddressViewModel> Addresses { get; set; }

        [JsonProperty("sensors")]
        public ICollection<SensorViewModel> Sensors { get; set; }

        [Required]
        [JsonProperty("companyId")]
        public int CompanyId { get; set; }

        [JsonProperty("companyName")]
        public string CompanyName { get; set; }

        [Required]
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [Required]
        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [Required]
        [JsonProperty("zoom")]
        public int Zoom { get; set; }

        [Required]
        [JsonProperty("coordinates")]
        public ICollection<Coordinate> Coordinates { get; set; }

        [JsonProperty("relationPermissions")]
        public ICollection<RelationPermissionViewModel> RelationPermissions { get; set; }

    }

    public static class RegionViewModelExtensions
    {
        public static IEnumerable<Coordinate> ParseCoordinates(this string coordinates)
        {
            var points = coordinates.Split(';', StringSplitOptions.RemoveEmptyEntries);
            foreach (var point in points)
            {
                var pointCoordinates = point.
                    Split(',', StringSplitOptions.RemoveEmptyEntries).
                    Select(s => double.Parse(s)).ToArray();
                if (pointCoordinates.Length != 2)
                    throw new FormatException("Point cannot contain only one coordinate");

                yield return new Coordinate
                {
                    Latitude = pointCoordinates[0],
                    Longitude = pointCoordinates[1]
                };
            }
        }

        public static string SerializeToString(this IEnumerable<Coordinate> coordinates)
        {
            var points = coordinates.Select(c => string.Join(',', c.Latitude, c.Longitude));
            return string.Join(';', points);
        }
    }

    public class RegionSensorsViewModel
    {
        [Required]
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("zoom")]
        public int Zoom { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [Required]
        [JsonProperty("sensors")]
        public ICollection<SensorViewModel> Sensors { get; set; }

        [JsonProperty("coordinates")]
        public IEnumerable<Coordinate> Coordinates { get; set; }
    }

    public class Coordinate
    {
        [Required]
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [Required]
        [JsonProperty("longitude")]
        public double Longitude { get; set; }
    }
}
