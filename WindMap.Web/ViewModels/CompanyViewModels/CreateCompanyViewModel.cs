﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace WindMap.Web.ViewModels.CompanyViewModels
{
    public class CreateCompanyViewModel
    {
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("phone")]
        public string Phone { get; set; }

        [Required]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("webSite")]
        public string WebSite { get; set; }

        [JsonProperty("imageBase64")]
        public string Logo { get; set; }
    }
}