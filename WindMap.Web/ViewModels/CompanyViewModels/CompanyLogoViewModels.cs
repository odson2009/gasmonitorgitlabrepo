﻿using Newtonsoft.Json;

namespace WindMap.Web.ViewModels.CompanyViewModels
{
    public class CompanyLogoViewModels
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("isDefault")]
        public bool IsDefault { get; set; }
    }
}