﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace WindMap.Web.ViewModels.CompanyViewModels
{
    public class UpdateCompanyViewModel : CreateCompanyViewModel
    {
        [Required]
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
