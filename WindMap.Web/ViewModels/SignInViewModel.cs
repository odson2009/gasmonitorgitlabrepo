﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace WindMap.Web.ViewModels
{
    public class SignInViewModel
    {
        [JsonProperty("login")]
        [Required(ErrorMessage = "login cannot be empty")]
        public string Login { get; set; }

        [JsonProperty("password")]
        [Required(ErrorMessage = "Password cannot be empty")]
        public string Password { get; set; }

        [JsonProperty("rememberMe")]
        public bool RememberMe { get; set; }
    }
}
