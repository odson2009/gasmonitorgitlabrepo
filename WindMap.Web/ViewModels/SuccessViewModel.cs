﻿using Newtonsoft.Json;

namespace WindMap.Web.ViewModels
{
    public class SuccessViewModel
    {
        [JsonProperty("success")]
        public bool Success { get; set; } = true;
    }
}
