﻿namespace WindMap.Web.ViewModels
{
    public class UiElementViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}