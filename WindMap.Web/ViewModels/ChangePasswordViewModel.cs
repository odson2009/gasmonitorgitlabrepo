﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace WindMap.Web.ViewModels
{
    public class ChangePasswordViewModel
    {
        [Required]
        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }
    }
}
