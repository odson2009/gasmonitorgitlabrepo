﻿using Newtonsoft.Json;
using System;

namespace WindMap.Web.ViewModels
{
    public class PeriodVeivModel
    {
        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("isNeedLastResults")]
        public bool IsNeedLastResults { get; set; }

        [JsonProperty("isLeak")]
        public bool IsLeak { get; set; }

        [JsonProperty("sensorIds")]
        public int[] SensorIdsArray { get; set; }

    }
}
