﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WindMap.Web.ViewModels
{
    public class MonitorCountViewModel
    {
        [JsonProperty("monitorCount")]
        public int MonitorsCount { get; set; }

        [JsonProperty("monitorResultsCount")]
        public int MonitorsResultsCount { get; set; }
    }
}
