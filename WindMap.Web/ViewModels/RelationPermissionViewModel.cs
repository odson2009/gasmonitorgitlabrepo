﻿using Newtonsoft.Json;

namespace WindMap.Web.ViewModels
{
    public class RelationPermissionViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("companyId")]
        public int CompanyId { get; set; }

        [JsonProperty("regionId")]
        public int RegionId { get; set; }

        [JsonProperty("user")]
        public UserViewModel User { get; set; }

        [JsonProperty("company")]
        public CompanyViewModel Company { get; set; }

        [JsonProperty("region")]
        public RegionViewModel Region { get; set; }
    }
}
