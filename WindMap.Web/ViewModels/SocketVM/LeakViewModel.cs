﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WindMap.DAL.Enums;

namespace WindMap.Web.ViewModels.SocketVM
{
    public class LeakViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("status")]
        public int LeakColor { get; set; }

        [JsonProperty("maxPfRadiusConstant")]
        public double MaxPfRadiusConstant { get; set; }

        [JsonProperty("maxPintPF")]
        public double MaxPintPF { get; set; }

        [JsonProperty("averageData")]
        public List<AverageDataViewModel> AverageData;
    }
    public class AverageDataViewModel
    {
        public double Pf { get; set; }
        public double Twd { get; set; }
    }

    public class LeakColorPower
    {
        public LeakColor LeakColor { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
    }
}