﻿using Newtonsoft.Json;

namespace WindMap.Web.ViewModels.SocketVM
{
    public class WindDirectionViewModel
    {
        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("regionId")]
        public int RegionId { get; set; }

        [JsonProperty("windDirection")]
        public double WindDirection { get; set; }
    }
}
