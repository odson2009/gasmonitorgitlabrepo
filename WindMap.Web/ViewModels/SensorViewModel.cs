﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace WindMap.Web.ViewModels
{
    public class SensorViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("monitorId")]
        public string MonitorId { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("monitorIdToRight")]
        public string MonitorIdToRight { get; set; }

        [JsonProperty("azimuthRight")]
        public double AzimuthRight { get; set; }

        [JsonProperty("distanceRight")]
        public double DistanceRight { get; set; }

        [JsonProperty("normalAzimuthToRight")]
        public double NormalAzimuthToRight { get; set; }

        [JsonProperty("monitorIdToLeft")]
        public string MonitorIdToLeft { get; set; }

        [JsonProperty("azimuthLeft")]
        public double AzimuthLeft { get; set; }

        [JsonProperty("distanceLeft")]
        public double DistanceLeft { get; set; }

        [JsonProperty("normalAzimuthToLeft")]
        public double NormalAzimuthToLeft { get; set; }

        [JsonProperty("onsiteWindMin")]
        public double OnsiteWindMin { get; set; }

        [JsonProperty("onsiteWindMax")]
        public double OnsiteWindMax { get; set; }

        [JsonProperty("onsiteWindCenterAngle")]
        public double OnsiteWindCenterAngle { get; set; }

        [JsonProperty("region")]
        public RegionViewModel Region { get; set; }

        [JsonProperty("regionId")]
        public int RegionId { get; set; }


        [JsonProperty("regionName")]
        public string RegionName { get; set; }

        [JsonProperty("sensorResultsCount")]
        public int SensorResultsCount { get; set; }

        [JsonProperty("sensorResults")]
        public ICollection<SensorResultViewModel> SensorResults { get; set; }

    }

    public class SensorSimpleDataViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("regionName")]
        public string RegionName { get; set; }

        [JsonProperty("monitorId")]
        public int MonitorId { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("monitorIdToRight")]
        public int MonitorIdToRight { get; set; }

        [JsonProperty("azimuthRight")]
        public double AzimuthRight { get; set; }

        [JsonProperty("distanceRight")]
        public double DistanceRight { get; set; }

        [JsonProperty("azimuthLeft")]
        public double AzimuthLeft { get; set; }

        [JsonProperty("monitorIdToLeft")]
        public int MonitorIdToLeft { get; set; }

        [JsonProperty("distanceLeft")]
        public double DistanceLeft { get; set; }

        [JsonProperty("normalAzimuthToRight")]
        public double NormalAzimuthToRight { get; set; }

        [JsonProperty("normalAzimuthToLeft")]
        public double NormalAzimuthToLeft { get; set; }

        [JsonProperty("regionId")]
        public int RegionId { get; set; }

        [JsonProperty("sensorResultsCount")]
        public int SensorResultsCount { get; set; }

        [JsonProperty("sensorResults")]
        public ICollection<SensorResultViewModel> SensorResults { get; set; }
    }

    public class SensorChartDataViewModel
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("regionName")]
        public string RegionName { get; set; }

        [JsonProperty("resultsCount")]
        public int ResultsCount { get; set; }
    }

    public class IntersectionSensorViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("x")]
        public double X { get; set; }

        [JsonProperty("y")]
        public double Y { get; set; }

        [JsonProperty("firstRay")]
        public double FirstAngle { get; set; }

        [JsonProperty("secondRay")]
        public double SecondAngle { get; set; }
    }
}
