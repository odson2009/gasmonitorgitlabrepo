﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WindMap.Web.ViewModels
{
    public class CompanyViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("webSite")]
        public string WebSite { get; set; }

        [JsonProperty("imageBase64")]
        public string Logo { get; set; }

        [JsonProperty("regions")]
        public ICollection<RegionViewModel> Regions { get; set; }

        [JsonProperty("users")]
        public ICollection<UserViewModel> Users{ get; set; }

        [JsonProperty("relationPermissions")]
        public ICollection<RelationPermissionViewModel> RelationPermissions { get; set; }
    }
}
