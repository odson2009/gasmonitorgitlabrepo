﻿using Newtonsoft.Json;

namespace WindMap.Web.ViewModels
{
    public class AddressViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("addressValue")]
        public string AddressValue { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }

        [JsonProperty("locationId")]
        public int LocationId { get; set; }

        [JsonProperty("location")]
        public LocationViewModel Location { get; set; }
    }
}
