﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Web.ViewModels
{
    public class SensorEventViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sensorId")]
        public int SensorId { get; set; }

        [JsonProperty("pairedMonitorId")]
        public int? PairedSensorId { get; set; }

        [JsonProperty("calculations")]
        public List<CalculationViewModel> Calculations { get; set; }
    }

    public class CalculationViewModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("sensorEventId")]
        public int SensorEventId { get; set; }

        [JsonProperty("eventTime")]
        public DateTime EventTime { get; set; }

        [JsonProperty("sourceLatitude")]
        public decimal SourceLatitude { get; set; }

        [JsonProperty("sourceLongitude")]
        public decimal SourceLongitude { get; set; }

        [JsonProperty("parallelWindDirection")]
        public decimal ParallelWindDirection { get; set; }

        [JsonProperty("crossWindDirection")]
        public decimal CrossWindDirection { get; set; }

        [JsonProperty("averageWindDirection")]
        public decimal AverageWindDirection { get; set; }

        [JsonProperty("emissionRate")]
        public decimal EmissionRate { get; set; }
    }
}
