﻿using Newtonsoft.Json;

namespace WindMap.Web.ViewModels
{
    public class UserViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("login")]
        public string UserName { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("roleName")]
        public string RoleName { get; set; }

        //public ICollection<RelationPermissionViewModel> RelationPermissions { get; set; }
    }
}
