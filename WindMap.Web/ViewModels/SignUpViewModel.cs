﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace WindMap.Web.ViewModels
{
    public class SignUpViewModel
    {
        [JsonProperty("id")]
        [Required]
        public string Id { get; set; }

        [JsonProperty("login")]
        [Required]
        public string Login { get; set; }

        [JsonProperty("email")]
        [Required]
        public string Email { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("roleId")]
        [Required]
        public string RoleId { get; set; }

        [JsonProperty("roleName")]
        public string RoleName { get; set; }

        [JsonProperty("uiElementsId")]
        [Required]
        public ICollection<int> UiElementsId { get; set; }

        [JsonProperty("companyObjs")]
        [Required]
        public ICollection<CompanyObj> CompanyObjs { get; set; }
    }
    public class CompanyObj
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        [Required]
        public ICollection<int> RegionsId { get; set; }
    }
}