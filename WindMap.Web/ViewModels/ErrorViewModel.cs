﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Web.ViewModels
{
    public class EmptyErrorViewModel
    {
        [JsonProperty("success")]
        public bool Success => false;

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class ErrorViewModel : EmptyErrorViewModel
    {
        [JsonProperty("errors", NullValueHandling = NullValueHandling.Ignore)]
        public List<FieldErrorViewModel> Errors { get; set; }

        [JsonProperty("stackTrace", NullValueHandling = NullValueHandling.Ignore)]
        public string StackTrace { get; set; }
    }

    public class FieldErrorViewModel
    {
        [JsonProperty("field", NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; set; }

        [JsonProperty("errors")]
        public List<string> Errors { get; set; }
    }
}
