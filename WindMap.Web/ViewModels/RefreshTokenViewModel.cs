﻿using Newtonsoft.Json;

namespace WindMap.Web.ViewModels
{
    public class RefreshTokenViewModel
    {
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
