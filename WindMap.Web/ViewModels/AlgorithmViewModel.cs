﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Web.ViewModels
{
    public class AlgorithmViewModel
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("monitorType")]
        public string MonitorType { get; set; }

        [JsonProperty("detectionPointFlux")]
        public double DetectionPointFlux { get; set; }

        [JsonProperty("detectionWindSpeed")]
        public double DetectionWindSpeed { get; set; }

        [JsonProperty("timeInterval")]
        public double TimeInterval { get; set; }

        [JsonProperty("timeResolution")]
        public double TimeResolution { get; set; }

        [JsonProperty("minDataForAverage")]
        public double MinDataForAverage { get; set; }

        [JsonProperty("numberLineGroupTime")]
        public double NumberLineGroupTime { get; set; }

        [JsonProperty("numberLineGroupWindDirection")]
        public double NumberLineGroupWindDirection { get; set; }

        [JsonProperty("windSmoothingGroup")]
        public double WindSmoothingGroup { get; set; }

        [JsonProperty("maxRangeBetweenMonitors")]
        public double MaxRangeBetweenMonitors { get; set; }

        [JsonProperty("sourceLocationUpdateInterval")]
        public double SourceLocationUpdateInterval { get; set; }

        [JsonProperty("numbersOfGroupForLinearRegression")]
        public double NumbersOfGroupForLinearRegression { get; set; }

    }
}
