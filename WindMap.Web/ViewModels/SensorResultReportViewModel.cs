﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Web.ViewModels
{
    public class SensorResultReportViewModel
    {
        [JsonProperty("sensorResults")]
        public SensorResultTmpViewModel[] SensorResults {get;set;}

        [JsonProperty("testResults")]
        public TestResultViewModel[] TestResults { get; set; }

        [JsonProperty("averageSummary")]
        public GroupTMPViewModel[] AverageSummary { get; set; }

        [JsonProperty("sensorResultsAll")]
        public SensorResultTmpViewModel[] SensorResultsAll { get; set; }
    }

    public class GroupTMPViewModel
    {
        [JsonProperty("step")]
        public string Step { get; set; }

        [JsonProperty("results")]
        public ResultsDataViewModel[] AverageResults { get; set; }

        [JsonProperty("regression")]
        public LinearRegressionViewModel LinearRegressionResults { get; set; }
    }

    public class ResultsDataViewModel
    {
        [JsonProperty("averageData")]
        public AverageDataViewModel AverageData { get; set; }

        [JsonProperty("linearRegressionResult")]
        public LinearRegressionViewModel LinearRegressionResult { get; set; }
    }

    public class AverageDataViewModel
    {
        [JsonProperty("TWD_Average")]
        public string TWD_Average { get; set; }

        [JsonProperty("PF_Average")]
        public string PF_Average { get; set; }
    }

    public class LinearRegressionViewModel
    {
        [JsonProperty("m")]
        public string M { get; set; }

        [JsonProperty("b")]
        public string B { get; set; }

        [JsonProperty("r")]
        public string R { get; set; }

        [JsonProperty("x_intercept")]
        public string XIntercept { get; set; }

    }

    public class TestResultViewModel
    {
        [JsonProperty("step")]
        public string Step { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("smoothed")]
        public FitDataViewModel Smoothed { get; set; }

        [JsonProperty("average")]
        public FitDataViewModel Average { get; set; }
    }

    public class FitDataViewModel
    {
        [JsonProperty("fminsearch")]
        public FminsearchViewModel Fminsearch { get; set; }

        [JsonProperty("polyfit")]
        public PolyfitViewModel Polyfit { get; set; }
    }

    public class FminsearchViewModel
    {
        [JsonProperty("a")]
        public string A { get; set; }

        [JsonProperty("sigma")]
        public string Sigma { get; set; }

        [JsonProperty("mu")]
        public string Mu { get; set; }
    }

    public class PolyfitViewModel
    {
        [JsonProperty("a")]
        public string A { get; set; }

        [JsonProperty("sigma")]
        public string Sigma { get; set; }

        [JsonProperty("mu")]
        public string Mu { get; set; }

        [JsonProperty("A")]
        public string CoefficientA { get; set; }

        [JsonProperty("B")]
        public string CoefficientB { get; set; }

        [JsonProperty("C")]
        public string CoefficientC { get; set; }
    }

    public class SensorResultTmpViewModel
    {
        [JsonProperty("timeBegin")]
        public string TimeBegin { get; set; }

        [JsonProperty("timeEnd")]
        public string TimeEnd { get; set; }

        [JsonProperty("concentration")]
        public float Concentration { get; set; }

        [JsonProperty("windSpeed")]
        public float WindSpeed { get; set; }

        [JsonProperty("windDirection")]
        public float WindDirection { get; set; }

        [JsonProperty("TWD")]
        public float TWD { get; set; }

        [JsonProperty("PF")]
        public float PF { get; set; }
    }
}
