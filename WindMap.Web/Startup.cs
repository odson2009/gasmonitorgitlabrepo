using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using System.Globalization;
using System.IO;
using WindMap.Services.Extensions;
using WindMap.Web.Extensions;
using WindMap.Web.Infrastructure.Middleware;
using WindMap.Web.Infrastructure.Swagger;

namespace WindMap.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();

            // ===== Add Cors ========
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddServices();
            services.AddConfigureIdentity();
            services.AddConfigureMapper();

            services.AddSwaggerGen(options =>
            {
                var path = PlatformServices.Default.Application.ApplicationBasePath;
                var doc = Path.Combine(path, "api.xml");

                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    In = "header",
                    Description = "Please insert JWT with Bearer into field.",
                    Name = "Authorization",
                    Type = "apiKey"
                });

                options.DescribeAllEnumsAsStrings();
                options.IgnoreObsoleteActions();
                options.IgnoreObsoleteProperties();
                options.IncludeXmlComments(doc);

                options.OperationFilter<DefaultContentTypeOperationFilter>();
                options.OperationFilter<FileUploadOperation>();

                options.SwaggerDoc("v1", new Info
                {
                    Title = "GM API",
                    Version = "1.0",
                    Description = "Gas Monitor Inc."
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/Home/Error");

            app.UseSwagger(options =>
            {
                options.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
                options.RouteTemplate = "api/doc.{documentName}.json";
            }).UseSwaggerUI(options =>
            {
                options.DocExpansion("none");
                options.RoutePrefix = "swagger";
                options.ShowRequestHeaders();
                options.SwaggerEndpoint("/api/doc.v1.json", "Gas Monitor API V1");
            });
            app.UseAuthentication();

            app.UseCors("CorsPolicy");

            //app.UseWebSockets();
            app.UseSignalR(routes => routes.MapHub<SensorHub>("development"));

            app.UseMiddleware<ErrorHandlerMiddleware>();
            app.UseRoutes();

            app.UseDatabaseErrorPage();
        }
    }
}
