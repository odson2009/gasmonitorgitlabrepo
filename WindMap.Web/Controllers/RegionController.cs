using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.Services;
using WindMap.Services.ModelServices;
using WindMap.Web.ViewModels;

namespace WindMap.Web.Controllers
{
    [Authorize]
    [Route("api/regions")]
    public class RegionController : BaseController
    {
        private readonly IRegionService _regionService;
        private readonly ISensorService _sensorService;
        private readonly IMapper _mapper;
        private readonly IFileService _ePPlusService;
        private readonly ICompanyService _companyService;
        private readonly IUserService _userService;

        public RegionController(
            IRegionService regionService,
            ISensorService sensorService,
            IMapper mapper,
            IFileService ePPlusService,
            ICompanyService companyService,
            IUserService userService)
        {
            _regionService = regionService;
            _sensorService = sensorService;
            _mapper = mapper;
            _ePPlusService = ePPlusService;
            _companyService = companyService;
            _userService = userService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllRegions()
        {
            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;

            if (userRole == "Administrator")
            {
                var regions = await _regionService.GetAllAsync(CancellationToken);
                var regionsVM = _mapper.Map<List<RegionViewModel>>(regions);
                return Ok(regionsVM);
            }
            else
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userService.GetUserByIdAsync(userIdClaim.Value, CancellationToken);
                var uniqueRelationPermissions = user.RelationPermissions.GroupBy(p => p.RegionId).Select(g => g.First()).ToList();
                var regions = uniqueRelationPermissions.Select(r => r.Region).ToList();
                var regionsVM = _mapper.Map<List<RegionViewModel>>(regions);
                return Ok(regionsVM);
            }
        }

        [HttpGet("sensors")]
        public async Task<IActionResult> GetRegionSensors()
        {
            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;
            IList<Region> regions;

            if(userRole == "Administrator")
            {
                regions = await _regionService.GetAllAsync(CancellationToken);
            }
            else
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userService.GetUserByIdAsync(userIdClaim.Value, CancellationToken);
                regions = user.RelationPermissions.Select(r => r.Region).ToList();
            }

            if (regions != null && regions.Any())
            {
                return Ok(regions.Select(c => new RegionSensorsViewModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    Zoom = c.Zoom,
                    Latitude = c.Latitude,
                    Longitude = c.Longitude,
                    Sensors = _mapper.Map<List<SensorViewModel>>(c.Sensors),
                    Coordinates = RegionViewModelExtensions.ParseCoordinates(c.Coordinates)
                }
                ));
            }

            return BadRequest("Can't find companies");
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRegion([Required] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid region id");

            var existRegion = await _regionService.FindByIdAsync(id, CancellationToken);
            var companies = await _companyService.FindAllAsync(CancellationToken);

            if (existRegion != null)
            {
                var regionVM = _mapper.Map<RegionViewModel>(existRegion);
                regionVM.CompanyName = companies.Where(s => s.Id == regionVM.CompanyId).FirstOrDefault().Name;               
                return Ok(regionVM);
            }
            return BadRequest("Can't find region");
        }

        [HttpGet("region/{id}")]
        public async Task<IActionResult> GetRegionByUserIdAndRegionId([Required] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid region id");

            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;

            if (userRole == "Administrator")
            {
                var existRegion = await _regionService.FindByIdAsync(id, CancellationToken);
                existRegion.Sensors.ToList().ForEach((s)=> {s.SensorResults = null; });
                if (existRegion != null)
                {
                    var regionVM = _mapper.Map<RegionViewModel>(existRegion);
                    return Ok(regionVM);
                }
            }
            else
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userService.GetUserByIdAsync(userIdClaim.Value, CancellationToken);
                var regions = user.RelationPermissions.Select(r => r.Region).ToList();
                var region = regions.Where(s => s.Id == id).FirstOrDefault();

                if (region != null)
                {
                    var regionVM = _mapper.Map<RegionViewModel>(region);
                    return Ok(regionVM);
                }
            }
            return UnprocessableEntity();
        }

        [HttpPost("details/period")]
        public async Task<IActionResult> GetAllCompaniesDetails([FromBody] PeriodVeivModel periodVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid period");
            }

            List<Region> companiesList;

            if (periodVM.IsLeak)
                companiesList = await _regionService.GetCompaniesWithSingleSensorResult(periodVM.EndDate, periodVM.SensorIdsArray, CancellationToken);
            else
                companiesList = await _regionService.GetCompaniesWithResultsByPeriod(periodVM.StartDate, periodVM.EndDate, periodVM.IsNeedLastResults, periodVM.SensorIdsArray, CancellationToken);

            var companiesVM = _mapper.Map<List<RegionViewModel>>(companiesList);

            return Ok(companiesVM);
        }

        [HttpPost("")]
        public async Task<IActionResult> AddRegion([FromBody] RegionViewModel regionVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            if (regionVM.CompanyId < 0 || regionVM.CompanyId == 0)
                return BadRequest("Invalid model, CompanyId can not be 0");


            var newRegion = _mapper.Map<Region>(regionVM);
            await _regionService.AddAsync(newRegion, CancellationToken);
            return Ok();
        }

        [HttpPost("file")]
        public async Task<IActionResult> AddCompaniesFromFile([FromForm] IFormFile file)
        {
            if (file == null)
                return BadRequest("Inavalid file");

            try
            {
                List<Region> companiesList;
                using (var ms = new MemoryStream())
                {
                    switch (file.ContentType)
                    {
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            await file.CopyToAsync(ms);
                            companiesList = await _ePPlusService.ParseRegionFile(ms, false);
                            break;
                        case "application/vnd.ms-excel":
                            await file.CopyToAsync(ms);
                            companiesList = await _ePPlusService.ParseRegionFile(ms, true);
                            break;
                        default: return BadRequest($"Can't upload file: {file.FileName}. Allowed extansions for files: .xls");
                    }
                }

                if (companiesList == null || !companiesList.Any())
                {
                    throw new Exception("Can not parse any result");
                }

                //save into db
                await _regionService.AddRangeAsync(companiesList, CancellationToken);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest($"File parse error! {ex.Message}");
            }
        }

        [HttpPut("")]
        public async Task<IActionResult> UpdateRegion([FromBody] RegionViewModel regionVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            var existRegion = await _regionService.FindByIdAsync(regionVM.Id, CancellationToken);

            if (existRegion == null)
                return BadRequest("Can't find region");

            existRegion.Name = regionVM.Name;
            existRegion.Phone = regionVM.Phone;
            existRegion.Email = regionVM.Email;
            existRegion.WebSite = regionVM.WebSite;
            existRegion.CompanyId = regionVM.CompanyId;
            existRegion.Latitude = regionVM.Latitude;
            existRegion.Longitude = regionVM.Longitude;
            existRegion.Zoom = regionVM.Zoom;
            existRegion.Coordinates = RegionViewModelExtensions.SerializeToString(regionVM.Coordinates);

            if (!existRegion.Algorithmes.Any())
                existRegion.Algorithmes.Add(new Algorithm());

            var algorithm = existRegion.Algorithmes.FirstOrDefault();

            algorithm.MonitorType = regionVM.Algorithm.MonitorType;
            algorithm.DetectionPointFlux = regionVM.Algorithm.DetectionPointFlux;
            algorithm.DetectionWindSpeed = regionVM.Algorithm.DetectionWindSpeed;
            algorithm.TimeInterval = regionVM.Algorithm.TimeInterval;
            algorithm.TimeResolution = regionVM.Algorithm.TimeResolution;
            algorithm.MinDataForAverage = regionVM.Algorithm.MinDataForAverage;
            algorithm.NumberLineGroupTime = regionVM.Algorithm.NumberLineGroupTime;
            algorithm.NumberLineGroupWindDirection = regionVM.Algorithm.NumberLineGroupWindDirection;
            algorithm.WindSmoothingGroup = regionVM.Algorithm.WindSmoothingGroup;
            algorithm.MaxRangeBetweenMonitors = regionVM.Algorithm.MaxRangeBetweenMonitors;
            algorithm.SourceLocationUpdateInterval = regionVM.Algorithm.SourceLocationUpdateInterval;
            algorithm.NumbersOfGroupForLinearRegression = regionVM.Algorithm.NumbersOfGroupForLinearRegression;

            //existRegion.Addresses = _mapper.Map<List<Address>>(regionVM.Addresses);

            await _regionService.UpdateAsync(existRegion, CancellationToken);

            return Ok();
        }

        [HttpPut("sensors")]
        public async Task<IActionResult> UpdateRegionSensors([FromBody] RegionSensorsViewModel regionVM)
        {
            var existRegion = await _regionService.FindByIdAsync(regionVM.Id, CancellationToken);

            if (existRegion == null)
                return BadRequest("Can't find region");

            foreach(var sensorInVM in regionVM.Sensors)
            {
                foreach(var sensorInDomain in existRegion.Sensors)
                {
                    if(sensorInDomain.Id == sensorInVM.Id)
                    {
                        sensorInDomain.MonitorId = sensorInVM.MonitorId;
                        sensorInDomain.Latitude = sensorInVM.Latitude;
                        sensorInDomain.Longitude = sensorInVM.Longitude;
                        sensorInDomain.MonitorIdToRight = sensorInVM.MonitorIdToRight;
                        sensorInDomain.AzimuthRight = sensorInVM.AzimuthRight;
                        sensorInDomain.DistanceRight = sensorInVM.DistanceRight;
                        sensorInDomain.NormalAzimuthToRight = sensorInVM.NormalAzimuthToRight;
                        sensorInDomain.MonitorIdToLeft = sensorInVM.MonitorIdToLeft;
                        sensorInDomain.AzimuthLeft = sensorInVM.AzimuthLeft;
                        sensorInDomain.DistanceLeft = sensorInVM.DistanceLeft;
                        sensorInDomain.NormalAzimuthToLeft = sensorInVM.NormalAzimuthToLeft;
                        sensorInDomain.OnsiteWindMin = sensorInVM.OnsiteWindMin;
                        sensorInDomain.OnsiteWindMax = sensorInVM.OnsiteWindMax;
                        sensorInDomain.OnsiteWindCenterAngle = sensorInVM.OnsiteWindCenterAngle;
                    }
                }
            }
            await _regionService.UpdateAsync(existRegion, CancellationToken);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRegion([Required] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid region id");

            var existRegion = await _regionService.FindByIdAsync(id, CancellationToken);
            if (existRegion != null)
            {
                await _regionService.DeleteAsyncWithPermissionAsync(existRegion, CancellationToken);

                return Ok();
            }

            return BadRequest("Can't find region");
        }

        [HttpGet("region/averageConcentration")]
        public async Task<IActionResult> GetConcentrationDuring14Days([Required]int id)
        {
            if (id <= 0)
                return BadRequest("Invalid region id");

            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;

            var twoWeeksDate = DateTime.Now.AddDays(-14);

            if (userRole == "Administrator")
            {
                var existRegion = await _regionService.FindByIdAsync(id, CancellationToken);
                if (existRegion != null)
                {

                    var results = existRegion.Sensors.GroupBy(
                        p => p.Id,
                        p => p.SensorResults.Where(r => r.TimeBegin >= twoWeeksDate),
                        (key, g) => new
                        {
                            sensorId = key,
                            aveargeConcentration = 5,
                        });//(g.Any() ? g.Sum(s => s.Sum(c=>c.Concentration)) : (double)0.0), }).ToList();

                    return Ok(results);
                }

            }
            else
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userService.GetUserByIdAsync(userIdClaim.Value, CancellationToken);
                var regions = user.RelationPermissions.Select(r => r.Region).ToList();
                var region = regions.Where(s => s.Id == id).FirstOrDefault();

                if (region != null)
                {
                    var regionVM = _mapper.Map<RegionViewModel>(region);
                    return Ok(regionVM);
                }
            }

            return null;
        }
    }
}