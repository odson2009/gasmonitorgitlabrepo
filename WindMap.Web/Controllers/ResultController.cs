﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using WindMap.DAL.Models;
using WindMap.Services.CoreServices;
using WindMap.Services.Models;
using WindMap.Services.ModelServices;
using WindMap.Web.ViewModels;

namespace WindMap.Web.Controllers
{
    [AllowAnonymous]
    [Route("api/results")]
    public class ResultController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ICacheService _cacheService;
        private readonly IResultService _resultService;
        private readonly ISensorService _sensorService;

        public ResultController(
            IMapper mapper,
            ICacheService cacheService,
            IResultService resultService,
            ISensorService sensorService)
        {
            _mapper = mapper;
            _cacheService = cacheService;
            _resultService = resultService;
            _sensorService = sensorService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetResultsByIds([Required]string ids)
        {
            var validIds = ids.Split(',')
                .Select(id => int.TryParse(id, out var result) ? result : -1)
                .Where(num => num > 0).ToArray();

            if (!validIds.Any())
                return BadRequest("All ids are invalid");

            var existSensors = await _resultService.GetResultsByIds(validIds, CancellationToken);

            if (!existSensors.Any())
                return BadRequest("Can't find sensors");

            var response = _mapper.Map<List<List<SensorResultViewModel>>>(existSensors);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> AddSensorResult([FromBody] SensorResultViewModel resultVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            if (!_cacheService.ContainSensor(resultVM.SensorId).Result)
                return BadRequest("Can't find sensor");

            var result = await _resultService.AddResultWithCache(_mapper.Map<SensorResultViewModel, SensorResult>(resultVM),
                CancellationToken);

            //if(result == null)
            //    return BadRequest("Not enough data for calculation");

            return Ok(result);
        }

        [HttpPost("range")]
        public async Task<IActionResult> AddSensorResults([FromBody] SensorResultViewModel[] resultsVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");


            if(!resultsVM.Any())
                return BadRequest("Input data is empty");


            var response = await _resultService.AddResultWithCache(
                _mapper.Map<IList<SensorResultViewModel>, IList<SensorResult>>(resultsVM.ToList()), CancellationToken);

            return Ok(response);
        }

        [HttpPost("test")]
        public async Task<IActionResult> AddSensorResultTest(int sensorId, int start, int count)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            var sensors = new List<Sensor>();

            using (var r = new StreamReader("sensorResults.json"))
            {
                var json = r.ReadToEnd();
                sensors = JsonConvert.DeserializeObject<List<Sensor>>(json);
            }

            var response = new SensorCheckResult();

            for (var i = 0; i < count; i++)
            {
                response = await _resultService.AddResultWithCache(sensors[sensorId].SensorResults.ToList()[start + i],
                    CancellationToken);
            }

            return Ok(response);
        }

        [HttpGet("refresh")]
        public IActionResult RefreshSensorResults()
        {
            _cacheService.Refresh();

            return Ok();
        }
    }
}