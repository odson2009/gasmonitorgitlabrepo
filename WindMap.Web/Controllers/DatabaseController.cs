using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WindMap.Services.CoreServices;
using System.Threading;
using Microsoft.AspNetCore.Authorization;

namespace WindMap.Web.Controllers
{
    [AllowAnonymous]
    [Route("api/database")]
    public class DatabaseController : BaseController
    {
        private readonly IDatabaseService _databaseService;
        public DatabaseController(IMigrationService migrationService, IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        [HttpGet("migrate")]
        public async Task<IActionResult> Migrate()
        {
            if (!await _databaseService.IsLastVersion(CancellationToken.None))
            {
                await _databaseService.MigrateAsync(CancellationToken);
            }

            return Ok("Success");
        }
    }
}
