using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WindMap.Services.ModelServices;
using AutoMapper;
using WindMap.Web.ViewModels;
using WindMap.DAL.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.SignalR;
using WindMap.Web.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using WindMap.Services;
using System.IO;
using System.Threading;
using WindMap.Web.Enums;
using IFileService = WindMap.Services.IFileService;

namespace WindMap.Web.Controllers
{
    [AllowAnonymous]
    [Route("api/events")]
    public class EventController : BaseController
    {
        private readonly ISensorService _sensorService;
        private readonly ISensorEventService _sensorEventService;
        private readonly IRegionService _regionService;
        private readonly IMapper _mapper;
        private readonly IHubContext<SensorHub> _hubContext;

        public EventController(
            ISensorService sensorService,
            ISensorEventService sensorEventService,
            IRegionService regionService,
            IMapper mapper,
            IHubContext<SensorHub> hubContext,
            IFileService ePPlusService)
        {
            _sensorService = sensorService;
            _regionService = regionService;
            _sensorEventService = sensorEventService;
            _mapper = mapper;
            _hubContext = hubContext;
        }

        /// <summary>
        /// Returns all events
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public async Task<IActionResult> GetAllSensorEvents()
        {
            var sensorEvents = await _sensorEventService.FindAllAsync(CancellationToken);
            var sensorEventsVM = _mapper.Map<List<SensorEventViewModel>>(sensorEvents);

            return Ok(sensorEventsVM.OrderBy(s => s.SensorId).ThenByDescending(e => e.Calculations.FirstOrDefault()?.EventTime));
        }

        /// <summary>
        /// Returns all events by monitor
        /// </summary>
        /// <param name="sensorId"></param>
        /// <returns></returns>
        [HttpGet("{sensorId}")]
        public async Task<IActionResult> GetSensorEvents([Required] int sensorId)
        {
            if (sensorId <= 0)
                return BadRequest("Invalid sensor id");

            var sensorEvents = await _sensorEventService.FindAllBySensorIdAsync(sensorId, CancellationToken);
            if (sensorEvents != null)
            {
                var sensorEventsVM = _mapper.Map<List<SensorEventViewModel>>(sensorEvents);

                return Ok(sensorEventsVM);
            }

            return BadRequest("Can't find sensor events");
        }

        /// <summary>
        /// Returns all events by region
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        [HttpGet("{regionId}")]
        public async Task<IActionResult> GetRegionEvents([Required] int regionId)
        {
            if (regionId <= 0)
                return BadRequest("Invalid region id");

            var sensorEvents = await _sensorEventService.FindAllByRegionIdAsync(regionId, CancellationToken);
            if (sensorEvents != null)
            {
                var sensorEventsVM = _mapper.Map<List<SensorEventViewModel>>(sensorEvents);

                return Ok(sensorEventsVM);
            }

            return BadRequest("Can't find region events");
        }

        /// <summary>
        /// Add event
        /// </summary>
        /// <param name="eventVM"></param>
        /// <returns></returns>
        [HttpPost("")]
        public async Task<IActionResult> AddSensorEvent([FromBody] SensorEventViewModel eventVM)
        {

            return Ok();

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model");
            }


            var existSensor = await _sensorService.FindByIdAsync(eventVM.SensorId, CancellationToken);
            if (existSensor != null)
            {
                var sensorEvent = _mapper.Map<SensorEvent>(eventVM);
                //sensorEvent.SourceLatitude = existSensor.Latitude;
                //sensorEvent.SourceLongitude = existSensor.Longitude;

                foreach (var status in Enum.GetValues(typeof(EventStatusesEnums)))
                {
                    //if (status.ToString().Equals(eventVM.Status))
                    //{
                    //    //sensorEvent.StatusId = (int)status;
                    //}
                }

                existSensor.SensorEvents.Add(sensorEvent);

                await _sensorService.UpdateAsync(existSensor, CancellationToken);

                //send with hub
                await _hubContext.Clients.All.InvokeAsync("SendEvent", eventVM);

                return Ok();
            }

            return BadRequest("Can't find sensor");
        }

        [HttpDelete("{regionId}")]
        public async Task<IActionResult> DeleteRegionEvents([Required] int regionId)
        {
            if (regionId <= 0)
                return BadRequest("Invalid region id");

            var region = await _regionService.FindByIdWithEventsAsync(regionId, CancellationToken);
            if (region != null)
            {
                if (!region.Sensors.Any())
                {
                    return BadRequest("Can't find region sensors");
                }

                foreach (var sensor in region.Sensors)
                {
                    if (sensor.SensorEvents.Any())
                    {
                        await _sensorEventService.DeleteAsync(sensor.SensorEvents.ToList(), CancellationToken);
                    }
                }

                return Ok();
            }

            return BadRequest("Can't find region");
        }

    }
}
