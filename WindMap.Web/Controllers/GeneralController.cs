﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WindMap.DAL.Models;
using WindMap.Services.CalculationServices;
using WindMap.Services.Models;
using WindMap.Services.ModelServices;
using WindMap.Web.ViewModels;

namespace WindMap.Web.Controllers
{
    [AllowAnonymous]
    [Route("api/general")]
    public class GeneralController : BaseController
    {
        private IMapper _mapper;
        private IGeneralService _generalService;
        private ICalculationService _calculationService;
        private readonly IUserService _userService;

        public GeneralController(
            IMapper mapper,
            IGeneralService generalService,
            ICalculationService calculationService,
            IUserService userService)
        {
            _mapper = mapper;
            _generalService = generalService;
            _calculationService = calculationService;
            _userService = userService;
        }

        [HttpGet("counter")]
        public async Task<IActionResult> GetCounter()
        {
            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;

            if (userRole == "Administrator")
            {
                var counter = await _generalService.GetCount(CancellationToken);
                var counterViewModel = _mapper.Map<MonitorCountViewModel>(counter);
                return Ok(counterViewModel);
            }
            else
            {
                var sensors = new List<Sensor>();
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userService.GetUserByIdAsync(userIdClaim.Value, CancellationToken);

                var regions = user.RelationPermissions.Select(r => r.Region).ToList();
                regions.ForEach(region => sensors.AddRange(region.Sensors));

                var counterViewModel = new MonitorCountViewModel
                {
                    MonitorsCount = sensors.Count,
                    MonitorsResultsCount = sensors.Sum(sensor => sensor.SensorResults.Count)
                };
                return Ok(counterViewModel);
            }
        }

        [HttpPost("regression")]
        public IActionResult CalculateRegression([FromBody] double[,] values)
        {
            var parameters = new List<RegressionParameter>();

            for (var i = 0; i < values.GetLength(0); i++)
            {
                parameters.Add(new RegressionParameter
                {
                    X = values[i, 0],
                    Y = values[i, 1]
                });
            }
            var regression = _calculationService.CalculateLinearRegression(parameters);

            return Ok(regression);
        }
    }
}