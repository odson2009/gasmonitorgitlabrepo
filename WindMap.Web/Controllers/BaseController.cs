﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading;
using WindMap.Web.Enums;
using WindMap.Web.Infrastructure.Helpers;
using WindMap.Web.Strings;
using WindMap.Web.ViewModels;

namespace WindMap.Web.Controllers
{
    public class BaseController : Controller
    {
        public CancellationToken CancellationToken => HttpContext?.RequestAborted ?? CancellationToken.None;

        protected ObjectResult Unauthorized(string field, List<string> messages)
        {
            return ObjectResponse(new ErrorViewModel
            {
                Message = ResponseMessages.FailedAuthorization,
                Errors = new List<FieldErrorViewModel>
                {
                    new FieldErrorViewModel
                    {
                        Field = field,
                        Errors = messages
                    }
                }
            }, HttpCodes.Unauthorized);
        }

        protected new ObjectResult Ok()
        {
            return ObjectResponse(new SuccessViewModel
            {
                Success = true
            }, HttpCodes.Ok);
        }

        protected ObjectResult Unauthorized(string field, string message)
        {
            return Unauthorized(field, new List<string> { message });

        }

        protected static ObjectResult ObjectResponse(object value, HttpCodes statusCode)
        {
            return new ObjectResult(value)
            {
                StatusCode = (int)statusCode
            };
        }

        protected ObjectResult UnprocessableEntity()
        {
            return ObjectResponse(ModelState.GetErrorsViewModel(), HttpCodes.UnprocessableEntity);
        }

        protected ObjectResult UnprocessableEntity(string field, List<string> messages)
        {
            return ObjectResponse(new ErrorViewModel
            {
                Message = ResponseMessages.UnprocessableError,
                Errors = new List<FieldErrorViewModel>
                {
                    new FieldErrorViewModel
                    {
                        Field = field,
                        Errors = messages
                    }
                }
            }, HttpCodes.UnprocessableEntity);
        }
    }
}