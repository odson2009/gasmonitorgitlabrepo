using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WindMap.Services.ModelServices;
using Microsoft.AspNetCore.SignalR;
using WindMap.Web.Extensions;
using WindMap.Web.ViewModels;
using WindMap.Services.CoreServices;
using WindMap.DAL.Enums;
using WindMap.Web.ViewModels.SocketVM;
using AutoMapper;
using WindMap.DAL.Models;
using WindMap.Services.Models;
using System.Linq;
using System;

namespace WindMap.Web.Controllers
{
    [AllowAnonymous]
    [Route("api/development")]
    public class DevelopmentController : BaseController
    {
        private readonly ISensorService _sensorService;
        private readonly IHubContext<SensorHub> _hubContext;
        private readonly IEmailService _emailService;

        private readonly IMapper _mapper;
        private readonly ICacheService _cacheService;
        private readonly IResultService _resultService;

        public DevelopmentController(
            ISensorService sensorService,
            IHubContext<SensorHub> hubContext,
            IEmailService emailService,
            IResultService resultService,
            IMapper mapper,
            ICacheService cacheService)
        {
            _sensorService = sensorService;
            _hubContext = hubContext;
            _emailService = emailService;
            _mapper = mapper;
            _cacheService = cacheService;
            _resultService = resultService;
        }

        #region  ProcessData
        // for test
        //private static SensorResultViewModel prevSensorResultViewModel;
        //private static int counterForLeakEvent = 1;

        //[HttpPost("process-data")]
        //public async Task<IActionResult> ProcessData([FromBody] SensorResultViewModel sensorResult)
        //{
        //    var sendEmail = false;
        //    var sensor = await _sensorService.FindByIdAsync(sensorResult.SensorId, CancellationToken);

        //    var windDirectionVM = new WindDirectionViewModel
        //    {
        //        SensorId = sensorResultVM.SensorId,
        //        RegionId = sensor.RegionId,
        //        WindDirection = (prevSensorResultViewModel?.WindDirection ?? 0 + sensorResult.WindDirection)/2
        //    };

        //    await _hubContext.Clients.All.InvokeAsync("sensors", sensorResult);
        //    await _hubContext.Clients.All.InvokeAsync("windDirection", windDirectionVM);
        //    // 
        //    if (counterForLeakEvent == 5)
        //    {
        //        var leakVM = new LeakViewModel
        //        {
        //            SensorId = sensorResult.SensorId,
        //            LeakStatus = LeakStatus.Normal
        //        };
        //        await _hubContext.Clients.All.InvokeAsync("leak", leakVM);
        //    }
        //    if (counterForLeakEvent == 10)
        //    {
        //        var leakVM = new LeakViewModel
        //        {
        //            SensorId = sensorResult.SensorId,
        //            LeakStatus = LeakStatus.Warning
        //        };
        //        await _hubContext.Clients.All.InvokeAsync("leak", leakVM);
        //    }
        //    if (counterForLeakEvent == 15)
        //    {
        //        var leakVM = new LeakViewModel
        //        {
        //            SensorId = sensorResult.SensorId,
        //            LeakStatus = LeakStatus.Danger
        //        };
        //        await _hubContext.Clients.All.InvokeAsync("leak", leakVM);
        //    }
        //    if (counterForLeakEvent > 17) counterForLeakEvent = 1;

        //    if (sensor != null)
        //    {
        //        // sending email when was leak
        //        if (sendEmail)
        //        {
        //            List<string> emails = new List<string>();
        //            var relationPermissions = sensor.Region.RelationPermissions;
        //            foreach (var relationPermission in relationPermissions)
        //            {
        //                emails.Add(relationPermission.User.Email);
        //            }
        //            await _emailService.SendNotificationProviders(emails, LeakStatus.Warning);
        //        }
        //    }

        //    prevSensorResultViewModel = sensorResult;
        //    counterForLeakEvent++;
        //    return Ok();

        //}
        #endregion

        [HttpPost("process-data")]
        public async Task<IActionResult> ProcessData([FromBody] SensorResultViewModel sensorResultVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            if (!_cacheService.ContainSensor(sensorResultVM.SensorId).Result)
                return BadRequest("Can't find sensor");

            var sensorResult = _mapper.Map<SensorResultViewModel, SensorResult>(sensorResultVM);
            var result = await _resultService.AddResultWithCache(sensorResult, CancellationToken);
            var sensor = await _cacheService.GetSensorAsync(sensorResultVM.SensorId);

            var region = _cacheService.GetRegionAsync(sensorResultVM.SensorId);
            if (region == null)
                return BadRequest("Can't find sensors region");

            // sending to the client
            var windDirectionVM = new WindDirectionViewModel
            {
                SensorId = sensorResultVM.SensorId,
                RegionId = region.Id,
                WindDirection = sensorResultVM.WindDirection
            };
            await _hubContext.Clients.All.InvokeAsync("sensors", sensorResultVM);
            await _hubContext.Clients.All.InvokeAsync("windDirection", windDirectionVM);

            // send emails
            string leakStatus = string.Empty;
            bool isNeedToSend = false;

            if (result != null && result.IsLeak)
            {
                var leakVM = getLeakVM(result, sensorResultVM.SensorId, 200);
                await _hubContext.Clients.All.InvokeAsync("leak", leakVM);

                if (result.IsLeak && !sensor.IsPreviousLeak)
                {
                    isNeedToSend = true;
                    leakStatus = "Start";
                    sensor.IsPreviousLeak = true;
                }
                else if (sensor.IsLeak && result.IsLeak && sensor.IsPreviousLeak)
                {
                    isNeedToSend = false;
                    leakStatus = "Continue";
                }
                else if (sensor.IsLeak && sensor.IsPreviousLeak)
                {
                    isNeedToSend = true;
                    leakStatus = "End";
                    sensor.IsPreviousLeak = false;
                }

                sensor.MaxPintPF = leakVM.MaxPintPF;
            }

            if (isNeedToSend)
            {
                await SendEmails(sensorResultVM.SensorId, leakStatus);
            }

            return Ok(result);
        }

        #region For Texas region algorithm

        [HttpPost("process-data-wind")]
        public async Task<IActionResult> ProcessWindData([FromBody] SensorResultViewModel sensorResultVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            if (!_cacheService.ContainSensor(sensorResultVM.SensorId).Result)
                return BadRequest("Can't find sensor");

            var sensorResult = _mapper.Map<SensorResultViewModel, SensorResult>(sensorResultVM);
            var result = await _resultService.AddResultWithCache(sensorResult, CancellationToken);

            var region = _cacheService.GetRegionAsync(sensorResultVM.SensorId);
            if (region == null)
                return BadRequest("Can't find sensors region");

            // sending to the client
            var windDirectionVM = new WindDirectionViewModel
            {
                SensorId = sensorResultVM.SensorId,
                RegionId = region.Id,
                WindDirection = sensorResultVM.WindDirection
            };

            await _hubContext.Clients.All.InvokeAsync("windDirection", windDirectionVM);

            return Ok(result);
        }

        [HttpPost("process-data-concentration")]
        public async Task<IActionResult> ProcessConcentartionData([FromBody] SensorResultViewModel sensorResultVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            if (!_cacheService.ContainSensor(sensorResultVM.SensorId).Result)
                return BadRequest("Can't find sensor");

            var sensorResult = _mapper.Map<SensorResultViewModel, SensorResult>(sensorResultVM);
            //var result = await _resultService.AddResultWithCache(sensorResult, CancellationToken);

            var region = _cacheService.GetRegionAsync(sensorResultVM.SensorId);
            if (region == null)
                return BadRequest("Can't find sensors region");

            // sending to the client
            await _hubContext.Clients.All.InvokeAsync("sensors", sensorResultVM);

            return Ok();
        }

        #endregion

        [HttpPost("process-data-wind-concentartion")]
        public async Task<IActionResult> ProcessWindConcentrationData([FromBody] SensorWindConcentrationResultViewModel sensorResultVM)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest("Invalid model");

                if (!_cacheService.ContainSensor(sensorResultVM.SensorId).Result)
                    return BadRequest("Can't find sensor");

                //convert from miles/h -> m/s
                //sensorResultVM.WindSpeed = Math.Round(sensorResultVM.WindSpeed * 0.447, 2);
                sensorResultVM.WindSpeed = sensorResultVM.WindSpeed * 0.447;

                var sensorResult = _mapper.Map<SensorWindConcentrationResultViewModel, SensorResult>(sensorResultVM);
                var result = await _resultService.AddResultWithCache(sensorResult, CancellationToken);
                var sensor = await _cacheService.GetSensorAsync(sensorResultVM.SensorId);

                var region = await _cacheService.GetRegionAsync(4);
                if (region == null)
                    return BadRequest("Can't find sensors region");

                // send emails
                string leakStatus = string.Empty;
                bool isNeedToSend = false;

               

                // sending to the client
                var windDirectionVM = new WindDirectionViewModel
                {
                    SensorId = sensorResultVM.SensorId,
                    RegionId = region.Id,
                    WindDirection = sensorResultVM.WindDirection
                };

                if (sensorResultVM.IsConcentration)
                {
                    var currentPF = (double)0;
                    var averageResults = sensor.ProcessedSensorResults.TakeLast(4);

                    if (averageResults.Count() == 4)
                    {
                        sensorResultVM.WindDirection = averageResults.Sum(p => p.WindDirection) / averageResults.Count();
                        sensorResultVM.WindSpeed = averageResults.Sum(p => p.WindSpeed) / averageResults.Count();
                        sensorResultVM.Concentration = averageResults.Sum(p => p.Concentration) / averageResults.Count();

                        currentPF = sensorResultVM.WindSpeed * sensorResultVM.Concentration;

                        //1
                        var algorithmMaxPF = region.Algorithmes.First().DetectionPointFlux;

                        if (result != null)
                        {
                            // LEAK
                            if (algorithmMaxPF <= currentPF)
                            {
                                result.IsLeak = true;
                            }
                            // End leak
                            else
                            {
                                result.IsLeak = false;
                            }
                        }
                    }

                    if (result != null && result.IsLeak)
                    {
                        var leakVM = new LeakViewModel() {
                            LeakColor = (int)LeakColor.red,
                            MaxPfRadiusConstant = 300,
                            SensorId = sensorResultVM.SensorId,
                            MaxPintPF = currentPF,
                        };
                            
                        //getLeakVM(result, sensorResultVM.SensorId, 300, 10);
                        await _hubContext.Clients.All.InvokeAsync("leak", leakVM);

                        if (result.IsLeak && !sensor.IsPreviousLeak)
                        {
                            isNeedToSend = true;
                            leakStatus = "Start";
                            sensor.IsPreviousLeak = true;
                        }
                        else if (sensor.IsLeak && result.IsLeak && sensor.IsPreviousLeak)
                        {
                            isNeedToSend = false;
                            leakStatus = "Continue";
                        }
                        else if (sensor.IsLeak && sensor.IsPreviousLeak)
                        {
                            isNeedToSend = true;
                            leakStatus = "End";
                            sensor.IsPreviousLeak = false;
                        }

                        sensor.MaxPintPF = leakVM.MaxPintPF;
                    }
                    else if (result != null && !result.IsLeak && sensor.IsPreviousLeak)
                    {
                        isNeedToSend = true;
                        leakStatus = "End";
                        sensor.IsPreviousLeak = false;

                        await _hubContext.Clients.All.InvokeAsync("leakEnd", sensorResultVM.SensorId);
                    }

                    if (isNeedToSend)
                    {
                        await SendEmails(sensorResultVM.SensorId, leakStatus);
                    }

                    await _hubContext.Clients.All.InvokeAsync("sensors", sensorResultVM);
                }
                else
                {
                    await _hubContext.Clients.All.InvokeAsync("windDirection", windDirectionVM);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private LeakViewModel getLeakVM(SensorCheckResult result, int SensorId, int maxPfRadiusConstant, int criticalValue = 0)
        {
            var maxAveragePF = result.SmoothingResult.GroupedMovingAverage.Max(p => p.AverageData.Pf);
            var differance = maxAveragePF > maxPfRadiusConstant ? maxAveragePF - maxPfRadiusConstant : 0;
            var averageData = new List<ViewModels.SocketVM.AverageDataViewModel>();

            //foreach (var groupedMovingAverage in result.SmoothingResult.GroupedMovingAverage)
            //foreach (var groupedMovingAverage in result.SmoothingResult.MovingAverage)
            //{
            //    var obj = new ViewModels.SocketVM.AverageDataViewModel
            //    {
            //        Pf = Math.Round(groupedMovingAverage.Pf - differance, 4),
            //        Twd = Math.Round(groupedMovingAverage.Twd, 4)
            //    };
            //    averageData.Add(obj);
            //}

            //Related in the client side: mapService.MAX_PF_RADIUS_CONSTANT
            var leakColorArray = Enum.GetValues(typeof(LeakColor));
            double step = (criticalValue == 0 ? (double)maxPfRadiusConstant : criticalValue) / (double)leakColorArray.Length;
            var leakColorsList = new List<LeakColorPower>();

            //Sets ranges for leak power status
            for (int i = 0; i < leakColorArray.Length; i++)
            {
                leakColorsList.Add(new LeakColorPower
                {
                    LeakColor = (LeakColor)Enum.ToObject(typeof(LeakColor), leakColorArray.GetValue(i)),
                    Min = i * step,
                    Max = (i + 1) * step
                });
            }

            var entry = leakColorsList.FirstOrDefault(p => p.Min < maxAveragePF && p.Max > maxAveragePF);
            var leakColor = entry != null ? entry.LeakColor : LeakColor.red;

            return new LeakViewModel
            {
                SensorId = SensorId,
                LeakColor = (int)leakColor,
                MaxPfRadiusConstant = maxPfRadiusConstant,
                MaxPintPF = Math.Round(maxAveragePF, 4),
                AverageData = averageData
            };
        }

        [HttpGet("test-send-leak-notification")]
        public async Task<IActionResult> SendEmails(int sensorId, string status)
        {
            try
            {
                var sensor = await _sensorService.FindByIdAsync(sensorId, CancellationToken);
                var cacheSensor = await _cacheService.GetSensorAsync(sensorId);

                if (cacheSensor == null)
                {
                    return BadRequest("Sensor not found in cache");
                }

                if (sensor == null)
                {
                    return BadRequest("Sensor not found");
                }

                List<string> emails = new List<string>();
                //emails.Add("ram@airmeasure-solutions.com");
                //emails.Add("odson2009@gmail.com");

                //var relationPermissions = sensor.Region.RelationPermissions;
                //foreach (var relationPermission in relationPermissions)
                //{
                //    emails.Add(relationPermission.User.Email);
                //}

                string subject = $"Leak detected";
                string message = $"<h3>Status: {status}</h3>";

                message += $"Was detected leak on monitor <b>{sensor.MonitorId}</b> at {DateTime.Now.ToString("H:mm:ss MM/dd/yy")}";
                message += $"<p>Lat: {sensor.Region.Latitude}</p>";
                message += $"<p>Lng: {sensor.Region.Longitude}</p>";

                await _emailService.SendNotificationProviders(emails, subject, message);
            }
            catch (Exception ex)
            {

            }

            return Ok(); 
        }


        [HttpGet("refresh-data")]
        public async Task<IActionResult> ClearSensorsResults() {
            _resultService.Refresh();

            return Ok();
        }
    }
}
