using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WindMap.Services.ModelServices;
using AutoMapper;
using WindMap.Web.ViewModels;
using WindMap.DAL.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.SignalR;
using WindMap.Web.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading;
using WindMap.Services.Models;
using IFileService = WindMap.Services.IFileService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;
using WindMap.Services.CalculationServices;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace WindMap.Web.Controllers
{
    [Authorize]
    [Route("api/sensors")]
    public class SensorController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ISensorService _sensorService;
        private readonly IMapper _mapper;
        private readonly IHubContext<SensorHub> _hubContext;
        private readonly IFileService _ePPlusService;
        private readonly ICalculationService _calculationService;
        private readonly IIntersectionService _intersectionService;
        private readonly IUserService _userService;

        public SensorController(
            IHostingEnvironment hostingEnvironment,
            ISensorService sensorService,
            IMapper mapper,
            IHubContext<SensorHub> hubContext,
            IFileService ePPlusService,
            ICalculationService calculationService, 
            IIntersectionService intersectionService,
            IUserService userService)
        {
            _hostingEnvironment = hostingEnvironment;
            _sensorService = sensorService;
            _mapper = mapper;
            _hubContext = hubContext;
            _ePPlusService = ePPlusService;
            _calculationService = calculationService;
            _intersectionService = intersectionService;
            _userService = userService;
        }

        [HttpGet("results/minDate")]
        public async Task<IActionResult> GetMinMaxDatesSensorResults()
        {
            return Ok(await _sensorService.GetMinDate(CancellationToken));
        }

        [HttpGet("{isGenerateRandom}")]
        public async Task<IActionResult> GetAllSensors(bool isGenerateRandom = false)
        {
            //genarate random sensors results
            if (isGenerateRandom)
            {
                return await GenerateRandomSensorsResults();
            }

            var sensors = await _sensorService.GetAllAsync(CancellationToken);
            var sensorsVM = _mapper.Map<List<SensorViewModel>>(sensors);

            return Ok(sensorsVM);
        }

        [HttpPost("period")]
        public async Task<IActionResult> GetAllSensorsAndResultsByPeriod([FromBody] PeriodVeivModel periodVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid period");
            }

            List<Sensor> sensors;
            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;

            if (userRole == "Administrator")
            {
                if (periodVM.IsLeak)
                    sensors = await _sensorService.GetSensorsWithSingleResults(periodVM.EndDate, periodVM.SensorIdsArray, CancellationToken);
                else
                    sensors = await _sensorService.GetSensorsWithResultsByPeriod(periodVM.StartDate, periodVM.EndDate, periodVM.IsNeedLastResults, periodVM.SensorIdsArray, CancellationToken);
            }
            else
            {
                var tempSensors = new List<Sensor>();
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userService.GetUserByIdAsync(userIdClaim.Value, CancellationToken);

                var uniqueRelationPermissions = user.RelationPermissions.GroupBy(p => p.RegionId).Select(g => g.First()).ToList();
                var regions = uniqueRelationPermissions.Select(r => r.Region).ToList();

                if (periodVM.IsLeak)
                    sensors = await _sensorService.GetSensorsWithSingleResults(periodVM.EndDate, periodVM.SensorIdsArray, CancellationToken);
                else
                    sensors = await _sensorService.GetSensorsWithResultsByPeriod(periodVM.StartDate, periodVM.EndDate, periodVM.IsNeedLastResults, periodVM.SensorIdsArray, CancellationToken);

                foreach (var sensor in sensors)
                {
                    foreach(var region in regions)
                    {
                        if (sensor.RegionId == region.Id)
                            tempSensors.Add(sensor);
                    }
                }
                sensors = null;
                sensors = tempSensors;
            }
            var sensorsVM = _mapper.Map<List<SensorViewModel>>(sensors);
            return Ok(sensorsVM);
        }

        [HttpGet("chart")]
        public IActionResult GetChartDataByPeriod()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid period");
            }

            var sensors = _sensorService.GetSensorsChartDataPeriod(CancellationToken.None);

            return Ok(sensors);
        }

        [HttpGet("random")]
        public async Task<IActionResult> GenerateRandomSensorsResults()
        {
            var currentDate = DateTime.UtcNow;
            var sensorDate = currentDate.AddMinutes(-2);
            var random = new Random();

            var sensors = await _sensorService.GetAllAsync(CancellationToken);
            var newSensorResults = new List<SensorResult>();

            foreach (var sensor in sensors)
            {
                currentDate = currentDate.AddSeconds(1);

                var newSensorResult = new SensorResult
                {
                    TimeBegin = sensorDate,
                    TimeEnd = sensorDate.AddSeconds(5),
                    Concentration = random.Next(1, 33),
                    WindSpeed = random.Next(1, 5),
                    WindDirection = random.Next(0, 360),
                };

                sensor.SensorResults.Add(newSensorResult);
                newSensorResults.Add(newSensorResult);

                await _sensorService.UpdateAsync(sensor, CancellationToken);
            }

            var sensorsAfterUpdate = await _sensorService.GetSensorsWithResultsByPeriod(currentDate.AddMinutes(-5), currentDate, false, null, CancellationToken);
            var sensorsVM = _mapper.Map<List<SensorSimpleDataViewModel>>(sensorsAfterUpdate);

            //send with hub
            await _hubContext.Clients.All.InvokeAsync("send", sensorsVM);

            return Ok(sensorsVM);
        }

        [HttpPost("")]
        public async Task<IActionResult> AddSensors([FromBody] IList<SensorViewModel> sensorsListVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model");
            }

            var newSensors = _mapper.Map<List<Sensor>>(sensorsListVM);
            await _sensorService.AddRangeAsync(newSensors, CancellationToken);

            return Ok();
        }

        [HttpPost("results/generateFile")]
        public async Task<IActionResult> GenerateResultsFile([FromBody] SensorResultReportViewModel[] resultsVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model");
            }

            string fileName = $"fminsearch_polyfit_results_download.xlsx";
            string wwwrootPath = _hostingEnvironment.WebRootPath;

            var file = new FileInfo(Path.Combine(wwwrootPath, fileName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(wwwrootPath, fileName));
            }

            var resultBuffer = _mapper.Map<SensorBufferReportModel[]>(resultsVM);

            try
            {
                await _ePPlusService.GenerateFile(file, resultBuffer);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        
            return Ok(new { fileName = file.Name });
        }

        [HttpGet("results/getFile")]
        public IActionResult GetResultsFile([Required] string fileName)
        {
            string wwwrootPath = _hostingEnvironment.WebRootPath;
            FileInfo file = new FileInfo(Path.Combine(wwwrootPath, fileName));

            if (!file.Exists)
            {
                return BadRequest("File no found");
            }

            var contentDisposition = new ContentDispositionHeaderValue("inline");
            contentDisposition.SetHttpFileName(fileName);
            Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();

            return new FileStreamResult(file.OpenRead(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        [HttpPost("file")]
        public async Task<IActionResult> AddSensorsFromFile([FromForm] IFormFile file)
        {
            if (file == null)
            {
                return BadRequest("Inavalid file");
            }

            List<Sensor> sensorsList;

            using (var ms = new MemoryStream())
            {
                switch (file.ContentType)
                {
                    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                        await file.CopyToAsync(ms);
                        sensorsList = await _ePPlusService.ParseSensorFile(ms, false);
                        break;
                    case "application/vnd.ms-excel":
                        await file.CopyToAsync(ms);
                        sensorsList = await _ePPlusService.ParseSensorFile(ms, true);
                        break;
                    default: return BadRequest($"Can't upload file: {file.FileName}. Allowed extansions for files: .xls");
                }
            }

            try
            {
                if (sensorsList == null || !sensorsList.Any())
                {
                    throw new Exception("Can not parse any result");
                }

                //save into db
                await _sensorService.AddRangeAsync(sensorsList, CancellationToken);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest($"File parse error! {ex.Message}");
            }
        }

        [HttpPost("results/file")]
        public async Task<IActionResult> AddSensorResultsFromFile([FromForm] IFormFile file)
        {
            if (file == null)
            {
                return BadRequest("Inavalid file");
            }

            List<SensorResult> sensorResultList;

            try
            {
                using (var ms = new MemoryStream())
                {
                    switch (file.ContentType)
                    {
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            await file.CopyToAsync(ms);
                            sensorResultList = await _ePPlusService.ParseSensorResultFile(ms, false);
                            break;
                        case "application/vnd.ms-excel":
                            await file.CopyToAsync(ms);
                            sensorResultList = await _ePPlusService.ParseSensorResultFile(ms, true);
                            break;
                        default: return BadRequest($"Can't upload file: {file.FileName}. Allowed extansions for files: .xls");
                    }
                }

                if (sensorResultList == null || !sensorResultList.Any())
                {
                    throw new Exception("Can not parse any result");
                }

                //save into db
                var sensorIds = sensorResultList.Select(s => s.SensorId).Distinct();
                var sensorInputResultsVM = new List<SensorInputResultViewModel>();

                foreach (var sensorId in sensorIds)
                {
                    var sensorResults = sensorResultList.Where(s => s.SensorId == sensorId);

                    sensorInputResultsVM.Add(new SensorInputResultViewModel
                    {
                        SensorId = sensorId,
                        SensorResults = _mapper.Map<List<SensorResultViewModel>>(sensorResults.OrderByDescending(sr => sr.TimeBegin))
                    });

                    var existSensor = await _sensorService.FindByIdAsync(sensorId, CancellationToken);
                    if (existSensor != null)
                    {
                        foreach (var newResult in sensorResults)
                        {
                            existSensor.SensorResults.Add(newResult);
                        }

                        await _sensorService.UpdateAsync(existSensor, CancellationToken);
                    }
                }

                //send with hub (singnalR)
                //await _hubContext.Clients.All.InvokeAsync("Send", sensorInputResultsVM);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest($"File parse error! {ex.Message}");
            }
        }

        [HttpPost("results")]
        public async Task<IActionResult> AddSensorResults([FromBody] List<SensorInputResultViewModel> sensorInputResultsVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model");
            }

            var tempSensorsList = new List<Sensor>();

            foreach (var result in sensorInputResultsVM)
            {
                if (!result.SensorResults.Any())
                {
                    return BadRequest("Sensor data is empty");
                }

                var existSensor = await _sensorService.FindByIdAsync(result.SensorId, CancellationToken);

                if (existSensor == null)
                {
                    return BadRequest("Can't find sensor");
                }

                var sensorResults = _mapper.Map<List<SensorResult>>(result.SensorResults);
                (existSensor.SensorResults as List<SensorResult>)?.AddRange(sensorResults);

                await _sensorService.UpdateAsync(existSensor, CancellationToken);

                Sensor tempSensor = existSensor;
                tempSensor.SensorResults = sensorResults;

                tempSensorsList.Add(tempSensor);
            }

            //simulation
            var sensorsVM = _mapper.Map<List<SensorSimpleDataViewModel>>(tempSensorsList);

            await _hubContext.Clients.All.InvokeAsync("Send", sensorsVM);

            SensorEvent sensorEvent = new SensorEvent()
            {
                SensorId = 1,
                PairedSensorId = 2,
                //StatusId = 1,
                //EventTime = DateTime.Now.AddMinutes(-5),
                //SourceLatitude = 31.117205389653584,
                //SourceLongitude = -85.38043141365051,
                //ParallelWindDirection = 26.705,
                //CrossWindDirection = 44.605,
                //AverageWindDirection = 56.605,
                //EmissionRate = 80.1125,
            };

            //var sensorEventVM = _mapper.Map<SensorEventViewModel>(sensorEvent);
            //await _hubContext.Clients.All.InvokeAsync("Send", sensorEventVM);

            //return Ok(sensorEventVM);
            return Ok();
        }

        [HttpPut("")]
        public async Task<IActionResult> SuperUpdateSensors([FromBody] ICollection<SensorViewModel> sensorsVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            await _sensorService.UpdateAsync(_mapper.Map<List<Sensor>>(sensorsVM), CancellationToken);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSensor([Required] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid sensor id");

            var existSensor = await _sensorService.FindByIdAsync(id, CancellationToken);
            if (existSensor != null)
            {
                await _sensorService.DeleteAsync(existSensor, CancellationToken);

                return Ok();
            }

            return BadRequest("Can't find sensor");
        }

        [HttpDelete("all/sensorsResults")]
        public async Task<IActionResult> DeleteAllSensorsResults()
        {
            var existSensors = await _sensorService.GetAllAsync(CancellationToken);

            if (existSensors.Any())
            {
                foreach (var sensor in existSensors)
                {
                    sensor.SensorResults.Clear();
                }

                await _sensorService.UpdateAsync(existSensors, CancellationToken);

            }

            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSensorById([Required] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid sensor id");

            var existSensor = await _sensorService.FindByIdAsync(id, CancellationToken);
            if (existSensor != null)
            {
                var response = _mapper.Map<SensorViewModel>(existSensor);
                return Ok(response);
            }

            return BadRequest("Can't find sensor");
        }

        [HttpGet("")]
        public async Task<IActionResult> GetSensorsByIds([Required]string ids)
        {
            var validIds = ids.Split(',')
                .Select(id => int.TryParse(id, out var result) ? result : -1)
                .Where(num => num > 0).ToArray();

            if (!validIds.Any())
                return BadRequest("All ids are invalid");

            var existSensors = await _sensorService.FindByIdAsync(validIds, CancellationToken);

            if (existSensors == null)
                return BadRequest("Can't find sensors");

            var response = _mapper.Map<List<SensorViewModel>>(existSensors);
            return Ok(response);
        }

        [HttpPost("intersection")]
        public IActionResult CalculateIntersection([FromBody] IntersectionSensorViewModel[] sensor)
        {
            var result = _intersectionService.CalculateIntersection(
                _mapper.Map<IntersectionSensorViewModel, IntersectionSensor>(sensor[0]),
                _mapper.Map<IntersectionSensorViewModel, IntersectionSensor>(sensor[1])
            );

            return Ok(result);
        }
    }
}
