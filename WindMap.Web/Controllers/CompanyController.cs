using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WindMap.Services.ModelServices;
using AutoMapper;
using WindMap.Web.ViewModels;
using WindMap.DAL.Models;
using Microsoft.AspNetCore.Hosting;
using WindMap.Web.ViewModels.CompanyViewModels;
using System.Linq;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace WindMap.Web.Controllers
{
    [Authorize]
    [Route("api/companies")]
    public class CompanyController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMapper _mapper;
        private readonly ICompanyService _companyService;
        private readonly IRegionService _regionService;
        private readonly IUserService _userService;

        public CompanyController(
            IHostingEnvironment hostingEnvironment,
            IMapper mapper,
            ICompanyService companyService,
            IRegionService regionService,
            IUserService userService)
        {
            _hostingEnvironment = hostingEnvironment;
            _mapper = mapper;
            _companyService = companyService;
            _regionService = regionService;
            _userService = userService;
        }
        
        [HttpGet("list")]
        [ProducesResponseType(typeof(List<CompanyViewModel>), 200)]
        public async Task<IActionResult> GetCompanies()
        {
            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;

            List<CompanyViewModel> companiesVM;
            List<Company> companies;

            if (userRole == "Administrator")
            {
                companies = await _companyService.FindAllAsync(CancellationToken);
            }
            else
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userService.GetUserByIdAsync(userIdClaim.Value, CancellationToken);

                var uniqueRelationPermissions = user.RelationPermissions.GroupBy(p => p.CompanyId).Select(g => g.First()).ToList();
                var regions = user.RelationPermissions.Select(r => r.Region).ToList();
                var groupRegions = regions.GroupBy(s => s.CompanyId).ToList();

                companies = uniqueRelationPermissions.Select(r => r.Company).ToList();
                companies.ForEach(company => company.Regions = groupRegions.Single(g => g.Key == company.Id).ToList());
            }
            companiesVM = _mapper.Map<List<CompanyViewModel>>(companies);
            return Ok(companiesVM);
        }

        [HttpGet, Route("{companyId}")]
        public async Task<IActionResult> GetCompanyById(int companyId)
        {
            if (companyId > 0)
            {
                var company = await _companyService.FindByIdAsync(companyId, CancellationToken);
                var companyVM = _mapper.Map<CompanyViewModel>(company);
                return Ok(companyVM);
            }

            return BadRequest("Invalid Company id");
        }

        [HttpPost("create")]
        [ProducesResponseType(typeof(CreateCompanyViewModel), 200)]
        public async Task<IActionResult> CreateCompany([FromBody] CreateCompanyViewModel companyVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Company model");

            var company = _mapper.Map<Company>(companyVM);
            await _companyService.AddAsync(company, CancellationToken);

            return Ok();
        }

        [HttpPut("update")]
        [ProducesResponseType(typeof(UpdateCompanyViewModel), 200)]
        public async Task<IActionResult> UpdateCompany([FromBody] UpdateCompanyViewModel companyVM)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Company model");

            var company = _mapper.Map<Company>(companyVM);
            if (company != null)
            {
                await _companyService.UpdateAsync(company, CancellationToken);
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete, Route("{companyId}")]
        public async Task<IActionResult> DeleteCompany(int companyId)
        {
            if (companyId <= 0)
                return BadRequest("Invalid company id");

            var existCompany = await _companyService.FindByIdAsync(companyId, CancellationToken);
            if (existCompany != null)
            {
                await _companyService.DeleteAsync(existCompany, CancellationToken);
                return Ok();
            }

            return BadRequest("Can't find company");
        }
    }
}
