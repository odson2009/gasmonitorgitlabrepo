﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.Services.CoreServices;
using WindMap.Services.ModelServices;
using WindMap.Web.Enums;
using WindMap.Web.Strings;
using WindMap.Web.ViewModels;
using WindMap.Web.ViewModels.CompanyViewModels;

namespace WindMap.Web.Controllers
{
    [Authorize]
    [Route("api/user")]
    public class UserController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUiElementService _uiElementService;
        private readonly IUserService _userService;
        private readonly ICompanyService _companyService;
        private readonly IRegionService _regionService;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;
        private readonly IRefreshTokenService _refreshTokenService;

        public UserController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<IdentityRole> roleManager,
            IMapper mapper,
            IUiElementService uiElementService,
            ICompanyService companyService,
            IRegionService regionService,
            IEmailService emailService,
            IUserService userService,
            IConfiguration configuration,
            IRefreshTokenService refreshTokenService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _uiElementService = uiElementService;
            _userService = userService;
            _companyService = companyService;
            _regionService = regionService;
            _emailService = emailService;
            _configuration = configuration;
            _refreshTokenService = refreshTokenService;
        }

        [HttpGet("roles")]
        [ProducesResponseType(typeof(RoleViewModel), (int)HttpCodes.Ok)]
        public async Task<IActionResult> GetRoles()
        {
            var roles = await _roleManager.Roles.ToListAsync();
            var roleList = _mapper.Map<List<RoleViewModel>>(roles);

            if (roleList.Count() == 3 && !roles.First().Name.Equals("Administrator") || !roles.Last().Name.Equals("Service_Provider"))
            {
                var newRoles = new List<RoleViewModel>();
                newRoles.Add(roleList.FirstOrDefault(r => r.Name.Equals("Administrator")));
                newRoles.Add(roleList.FirstOrDefault(r => r.Name.Equals("Owner")));
                newRoles.Add(roleList.FirstOrDefault(r => r.Name.Equals("Service_Provider")));
                return Ok(newRoles);
            }
            return Ok(roleList);
        }

        [HttpGet("pages")]
        [ProducesResponseType(typeof(UiElementViewModel), (int)HttpCodes.Ok)]
        public async Task<IActionResult> GetUiElements()
        {
            var uiElements = await _uiElementService.GetAllAsync(CancellationToken);
            var uiElementList = _mapper.Map<List<UiElementViewModel>>(uiElements);
            return Ok(uiElementList);
        }

        [HttpGet("users-list")]
        [ProducesResponseType(typeof(UserViewModel), (int)HttpCodes.Ok)]
        public async Task<IActionResult> GetAllUsers()
        {
            var userRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var userRole = userRoleClaim.Value;
            IList<User> users = new List<User>();

            if (userRole == "Administrator")
            {
                users = await _userManager.Users.ToListAsync();
            }
            else if (userRole == "Owner")
            {
                users = await _userManager.GetUsersInRoleAsync("Service_Provider");

                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userManager.FindByIdAsync(userIdClaim.Value);
                users.Add(user);   
            }
            else
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
                var user = await _userManager.FindByIdAsync(userIdClaim.Value);
                users.Add(user);
            }
            
            var userList = new List<UserViewModel>();
            foreach (var user in users)
            {
                var roleNames = await _userManager.GetRolesAsync(user);
                userList.Add(new UserViewModel()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    IsActive = user.IsActive,
                    UserName = user.UserName,
                    RoleName = roleNames.FirstOrDefault()
                });
            }
            userList = userList.OrderBy(s => s.RoleName).ToList();
            return Ok(userList);
        }

        [HttpPost("create")]
        [Authorize(Roles = "Administrator, Owner")]
        public async Task<IActionResult> Create([FromBody] SignUpViewModel signUpViewModel)
        {
            if (ModelState["Id"] != null)
                ModelState.Remove("Id");

            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            User user = _mapper.Map<User>(signUpViewModel);
            user.Data = DateTime.UtcNow;
            user.RelationPermissions = new List<RelationPermission>();
            user.UiPermission = new List<UiPermission>();

            if (!signUpViewModel.UiElementsId.Any())
            {
                ModelState.AddModelError("UiElementsId", (ResponseMessages.ValueIsIncorrect));
                return UnprocessableEntity();
            }

            if (!signUpViewModel.CompanyObjs.Any())
            {
                ModelState.AddModelError("CompanyObjs.Id", (ResponseMessages.ValueIsIncorrect));
                return UnprocessableEntity();
            }

            if (signUpViewModel.CompanyObjs.Any(s => !s.RegionsId.Any()))
            {
                ModelState.AddModelError("CompanyObjs.RegionsId", (ResponseMessages.ValueIsIncorrect));
                return UnprocessableEntity();
            }

            foreach (var perm in signUpViewModel.UiElementsId)
            {
                user.UiPermission.Add(new UiPermission {
                    UserId = user.Id,
                    UiElementId = perm
                });
            }

            foreach (var company in signUpViewModel.CompanyObjs)
            {
                foreach (var region in company.RegionsId)
                {
                    user.RelationPermissions.Add(new RelationPermission {
                        UserId = user.Id,
                        CompanyId = company.Id,
                        RegionId = region
                    });
                }
            }

            // generate user password
            string pass = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8);
            IdentityResult result;
            try
            {
                result = await _userManager.CreateAsync(user, pass);
                if (result.Succeeded)
                {
                    var role = await _roleManager.FindByIdAsync(signUpViewModel.RoleId);
                    await _userManager.AddToRoleAsync(user, role.Name);
                    // send password to email
                    string currentDomain = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
                    string message = $"Your login: {signUpViewModel.Login}\npassword: {pass}\nPlease, follow the link: {currentDomain}";
                    await _emailService.SendEmail(signUpViewModel.Email, message);
                    return Ok();
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description
                            ?? ResponseMessages.UnknownError);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return UnprocessableEntity();
        }

        [HttpGet, Route("{userId}")]
        [ProducesResponseType(typeof(SignUpViewModel), (int)HttpCodes.Ok)]
        public async Task<IActionResult> GetUserById(string userId)
        {
            var user = await _userService.GetUserByIdAsync(userId, CancellationToken);
            if (user == null)
            {
                return BadRequest(string.Format(ResponseMessages.UserNotFound, userId));
            }
            var roleNames = await _userManager.GetRolesAsync(user);
            var roleName = roleNames.FirstOrDefault();
            var roleId = _roleManager.Roles.Where(r => r.Name == roleName).Select(r => r.Id).FirstOrDefault();
            // ----
            var signUpViewModel = _mapper.Map<SignUpViewModel>(user);
            signUpViewModel.RoleName = roleName;
            signUpViewModel.RoleId = roleId;

            return Ok(signUpViewModel);
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update([FromBody] SignUpViewModel signUpViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            var currentUserRoleClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            var currentUserRole = currentUserRoleClaim.Value;

            // get User for update
            var user = await _userService.GetUserByIdAsync(signUpViewModel.Id, CancellationToken);
            if(user != null)
            {
                user.FirstName = signUpViewModel.FirstName;
                user.LastName = signUpViewModel.LastName;

                if (currentUserRole == "Administrator" || currentUserRole == "Owner")
                { 
                
                    if (!signUpViewModel.UiElementsId.Any())
                    {
                        ModelState.AddModelError("UiElementsId", (ResponseMessages.ValueIsIncorrect));
                        return UnprocessableEntity();
                    }

                    if (!signUpViewModel.CompanyObjs.Any())
                    {
                        ModelState.AddModelError("CompanyObjs.Id", (ResponseMessages.ValueIsIncorrect));
                        return UnprocessableEntity();
                    }

                    if (signUpViewModel.CompanyObjs.Any(s => !s.RegionsId.Any()))
                    {
                        ModelState.AddModelError("CompanyObjs.RegionsId", (ResponseMessages.ValueIsIncorrect));
                        return UnprocessableEntity();
                    }

                    // update status
                    user.IsActive = signUpViewModel.IsActive;
                    // user was blocked
                    if (!user.IsActive)
                        await _refreshTokenService.DeleteAllAsync(user.Id, CancellationToken);


                    // update UiPermission
                    user.UiPermission = new List<UiPermission>();
                    foreach (var perm in signUpViewModel.UiElementsId)
                    {
                        user.UiPermission.Add(new UiPermission
                        {
                            UserId = user.Id,
                            UiElementId = perm
                        });
                    }

                    // update RelationPermission
                    user.RelationPermissions = new List<RelationPermission>();
                    foreach (var company in signUpViewModel.CompanyObjs)
                    {
                        foreach (var region in company.RegionsId)
                        {
                            user.RelationPermissions.Add(new RelationPermission
                            {
                                UserId = user.Id,
                                CompanyId = company.Id,
                                RegionId = region
                            });
                        }
                    }
                }
                IdentityResult result;
                try
                {
                    // update user in db
                    result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        if (currentUserRole == "Administrator" || currentUserRole == "Owner")
                        {
                            var currentUserRoles = await _userManager.GetRolesAsync(user);
                            var userRoleName = currentUserRoles.FirstOrDefault();

                            var roleForUpdate = await _roleManager.FindByIdAsync(signUpViewModel.RoleId);
                            if (userRoleName != roleForUpdate.Name)
                            {
                                await _userManager.RemoveFromRolesAsync(user, currentUserRoles);
                                await _userManager.AddToRoleAsync(user, roleForUpdate.Name);
                            }
                        }
                        return Ok();
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description
                                ?? ResponseMessages.UnknownError);
                        }
                        return UnprocessableEntity();
                    }
                }
                catch
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest(string.Format(ResponseMessages.UserNotFound, signUpViewModel.Id));
            }
        }

        [HttpGet, Route("logo-companies/{userId}")]
        public async Task<IActionResult> GetLogoCompaniesByUserId(string userId)
        {
            var user = await _userService.GetUserByIdAsync(userId, CancellationToken);
            if (user == null)
            {
                return BadRequest(string.Format(ResponseMessages.UserNotFound, userId));
            }
            var defaultCompany = await _companyService.FindByNameAsync(_configuration["DefaultCompany:Name"], CancellationToken);
            var userIsRelated = user.RelationPermissions.Select(s => s.Company).Where(s => s.Name == defaultCompany.Name).Any();

            var companyLogoViewModels = new List<CompanyLogoViewModels>();
            var uniqueRelationPermissions = user.RelationPermissions.GroupBy(p => p.CompanyId).Select(g => g.First()).ToList();

            companyLogoViewModels.Add(new CompanyLogoViewModels
            {
                IsDefault = true,
                Logo = Encoding.ASCII.GetString(defaultCompany.Logo != null ? defaultCompany.Logo : new byte[0]),
                Name = defaultCompany.Name
            });

            if (!userIsRelated)
            {
                foreach (var perm in uniqueRelationPermissions)
                {
                    companyLogoViewModels.Add(new CompanyLogoViewModels
                    {
                        Logo = Encoding.ASCII.GetString(perm.Company.Logo != null ? perm.Company.Logo : new byte[0]),
                        Name = perm.Company.Name
                    });
                }
            }
            return Ok(companyLogoViewModels);
        }

        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub);
            var userId = userIdClaim.Value;

            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var _passwordValidator = HttpContext.RequestServices.GetService(typeof(IPasswordValidator<User>)) as IPasswordValidator<User>;
                var _passwordHasher = HttpContext.RequestServices.GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;

                IdentityResult result = await _passwordValidator.ValidateAsync(_userManager, user, model.NewPassword);
                if (result.Succeeded)
                {
                    user.PasswordHash = _passwordHasher.HashPassword(user, model.NewPassword);
                    user.ChangedPassword = true;
                    await _userManager.UpdateAsync(user);
                    return Ok(result);
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return UnprocessableEntity();
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "User is not found");
                return UnprocessableEntity();
            }
        }

        [HttpPut("reset-password/{userId}")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> ResetPassword(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                string newPassword = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8);
                var _passwordValidator = HttpContext.RequestServices.GetService(typeof(IPasswordValidator<User>)) as IPasswordValidator<User>;
                var _passwordHasher = HttpContext.RequestServices.GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;

                IdentityResult result = await _passwordValidator.ValidateAsync(_userManager, user, newPassword);
                if (result.Succeeded)
                {
                    user.ChangedPassword = false;
                    user.Data = DateTime.UtcNow;
                    user.PasswordHash = _passwordHasher.HashPassword(user, newPassword);
                    var resultUpdated = await _userManager.UpdateAsync(user);
                    if(resultUpdated.Succeeded)
                    {
                        // send password to email
                        string currentDomain = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
                        string message = $"Your login: {user.UserName}\nTemp password: {newPassword}\nPlease, follow the link: {currentDomain}";
                        await _emailService.SendEmail(user.Email, message);

                        // delete all refresh tokens in db
                        var tokens = await _refreshTokenService.GetAllByUserId(userId, CancellationToken);
                        if (tokens.Any())
                            await _refreshTokenService.DeleteAsync(tokens, CancellationToken);

                        return Ok(result);
                    }
                    else
                    {
                        foreach (var error in resultUpdated.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                        return UnprocessableEntity();
                    }
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return UnprocessableEntity();
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "User is not found");
                return UnprocessableEntity();
            }
        }
    }
}