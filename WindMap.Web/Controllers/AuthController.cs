﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WindMap.DAL.Models;
using WindMap.Web.Infrastructure.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WindMap.Web.Strings;
using WindMap.Web.ViewModels;
using WindMap.Services.ModelServices;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace WindMap.Web.Controllers
{
    [Route("api/auth")]
    public class AuthController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly IUserService _userService;
        private readonly IUiElementService _uiElementService;
        private readonly IAccountService _accountService;
        private readonly IRefreshTokenService _refreshTokenService;

        public AuthController(
            UserManager<User> userManager, 
            IUserService userService, 
            IUiElementService uiElementService, 
            IAccountService accountService,
            IRefreshTokenService refreshTokenService)
        {
            _userManager = userManager;
            _userService = userService;
            _uiElementService = uiElementService;
            _accountService = accountService;
            _refreshTokenService = refreshTokenService;
        }

        [HttpPost]
        public async Task<IActionResult> SignIn([FromBody] SignInViewModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid model");

            var user = await _userService.GetUserByLoginAsync(request.Login, CancellationToken);

            if (user == null)
            {
                return Unauthorized(
                    JsonPropertyHelper.GetName<SignInViewModel>(x => x.Login),
                    ResponseMessages.SignInLoginError);
            }

            if (!await _userManager.CheckPasswordAsync(user, request.Password))
            {
                return Unauthorized(
                    JsonPropertyHelper.GetName<SignInViewModel>(x => x.Password),
                    ResponseMessages.SignInPasswordError);
            }

            if (!user.IsActive)
            {
                return Unauthorized(JsonPropertyHelper.GetName<SignInViewModel>(x => x.Login), "You was blocked by administrator");
            }

            if (!user.ChangedPassword)
            {
                var dateNow = DateTime.UtcNow;
                var difference = dateNow.Subtract(user.Data).TotalMinutes;
                if(difference > 60)
                {
                    //return UnprocessableEntity(JsonPropertyHelper.GetName<SignInViewModel>(x => x.Login), 
                    //    new List<string> { "You didn't change password" });
                    return Unauthorized(JsonPropertyHelper.GetName<SignInViewModel>(x => x.Login), "You didn't change password");
                }
            }

            // get ui permissions
            var permissions = user.UiPermission.Select(e => e.UiElement.Name).ToList();
            // add roles
            var roles = await _userManager.GetRolesAsync(user);

            var token = _accountService.GetToken(user.Id.ToString(), user.UserName, user.Email, string.Join(",", permissions), 
                user.ChangedPassword, roles.FirstOrDefault(), request.RememberMe);

            // save refresh token in db
            var refreshToken = new RefreshToken()
            {
                Token = token.RefreshToken,
                UserId = user.Id
            };
            await _refreshTokenService.AddAsync(refreshToken, CancellationToken);

            return Ok(token);
        }

        [HttpPost("refresh-token")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenViewModel refreshTokenVM)
        {
            var principal = _accountService.GetPrincipalFromExpiredToken(refreshTokenVM.RefreshToken);
            var userId = principal.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub).Value;
            var userExp = principal.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Exp).Value;
            int.TryParse(userExp, out int userExpTime);

            var user = await _userService.GetByIdAsync(userId, CancellationToken);
            if(user == null) return BadRequest();

            var tokens = await _refreshTokenService.GetAllByUserId(userId, CancellationToken);
            var refreshToken = tokens.Where(c => c.Token == refreshTokenVM.RefreshToken).FirstOrDefault();
            if (refreshToken == null) return BadRequest();

            var expired = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(userExpTime);
            if (expired < DateTime.UtcNow)
            {
                // delete token
                await _refreshTokenService.DeleteAsync(refreshToken, CancellationToken);
                return BadRequest();
            }

            // create new refresh token
            var newRefreshToken = _accountService.CreateRefreshToken(userId);
            refreshToken.Token = newRefreshToken;

            if (tokens.Count > 10)
            {
                // delete all refresh tokens
                await _refreshTokenService.DeleteAllAsync(userId, CancellationToken);
                // save refreshToken in db
                refreshToken.Id = 0;
                await _refreshTokenService.AddAsync(refreshToken, CancellationToken);
            }
            else
            {
                // update refreshToken in db
                await _refreshTokenService.UpdateAsync(refreshToken, CancellationToken);
            }

            // get ui permissions
            var permissions = user.UiPermission.Select(e => e.UiElement.Name).ToList();
            // add roles
            var roles = await _userManager.GetRolesAsync(user);
            // create new RefreshToken and AccessToken
            var newAccessToken = _accountService.CreateAccessToken(user.Id.ToString(), user.UserName, user.Email, string.Join(",", permissions), 
                user.ChangedPassword, roles.FirstOrDefault(), true);

            return Ok(new JsonWebToken
            {
                AccessToken = newAccessToken,
                RefreshToken = newRefreshToken
            });
        }

        [Authorize, HttpPost("logout")]
        public async Task<IActionResult> Logout([FromBody] RefreshTokenViewModel refreshTokenVM)
        {
            string userIdClaim, userId;
            try
            {
                userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub).Value;
                var principal = _accountService.GetPrincipalFromExpiredToken(refreshTokenVM.RefreshToken);
                userId = principal.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub).Value;
            }
            catch
            {
                return BadRequest();
            }

            if (userIdClaim != userId) return BadRequest();

            var user = await _userService.GetByIdAsync(userId, CancellationToken);
            if (user == null) return BadRequest();

            var tokens = await _refreshTokenService.GetAllByUserId(userId, CancellationToken);
            if (tokens.Count > 0)
            {
                List<RefreshToken> refreshTokens = new List<RefreshToken>();
                refreshTokens = tokens.Where(c => c.Token == refreshTokenVM.RefreshToken).ToList();

                if (refreshTokens.Count > 0)
                    await _refreshTokenService.DeleteAsync(refreshTokens, CancellationToken);
            }
            return Ok();
        }

        [Authorize, HttpPost("logout-all-device/{userId}")]
        public async Task<IActionResult> LogoutAllDevice(string userId)
        {
            var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub).Value;
            if(userIdClaim != userId) return BadRequest();

            var user = await _userService.GetByIdAsync(userId, CancellationToken);
            if (user == null) return BadRequest();

            var tokens = await _refreshTokenService.GetAllByUserId(userId, CancellationToken);
            if (!tokens.Any()) return BadRequest();

            await _refreshTokenService.DeleteAsync(tokens, CancellationToken);
            return Ok();
        }
    }
}
