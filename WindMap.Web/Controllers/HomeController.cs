using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WindMap.Services.CoreServices;
using System.Threading;

namespace WindMap.Web.Controllers
{
    public class HomeController : Controller
    {
        protected IMigrationService _migrationService;
        public HomeController(IMigrationService migrationService)
        {
            _migrationService = migrationService;
        }

        public IActionResult Index()
        {
            return View("~/wwwroot/index.html");
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
