﻿namespace WindMap.Web.Strings
{
    internal static class ResponseMessages
    {
        public static string FailedAuthorization => "User authorization failed";
        public static string SignInEmailError => "No account connected to this Email. Please Sign Up";
        public static string SignInLoginError => "No account connected to this Login. Please Sign Up";
        public static string SignInPasswordError => "Incorrect user password";
        public static string UnprocessableError => "One of the parameters specified was missing or invalid";
        public static string ValueIsIncorrect => "is incorrect";
        public static string UnknownError => "Unknown error occurred. Try again later";
        public static string UserNotFound => "User not found by id {0}";
    }
}
