﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Http;

namespace WindMap.Web.Infrastructure.Swagger
{
    public class FileUploadOperation : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.OperationId.ToLower().Contains("api") && operation.OperationId.ToLower().Contains("post"))
            {
                if (context.ApiDescription.ParameterDescriptions.Any(x => x.ModelMetadata.ContainerType == typeof(IFormFile)))
                {
                    operation.Parameters.Clear();
                    operation.Parameters.Add(new NonBodyParameter
                    {
                        Name = "file",
                        In = "formData",
                        Description = "Uplaod file",
                        Required = true,
                        Type = "file"
                    });
                    operation.Consumes.Add("application/form-data");
                }
            }
        }
    }
}
