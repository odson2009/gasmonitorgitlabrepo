﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using WindMap.Services.CoreServices;

namespace WindMap.Web.Infrastructure.Middleware
{
    public class MigrationsEndPointMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly IDatabaseService _databaseService;

        public MigrationsEndPointMiddleware(
            RequestDelegate next,
            ILogger<MigrationsEndPointMiddleware> logger,
            IDatabaseService databaseService)
        {
            _next = next;
            _logger = logger;
            _databaseService = databaseService;
        }

        public virtual async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.Path.Equals(new PathString("/database/migrate")))
            {
                _logger.LogInformation(httpContext.Request.Path);

                try
                {
                    await _databaseService.MigrateAsync(CancellationToken.None);

                    httpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                    httpContext.Response.Headers.Add("Pragma", new[] { "no-cache" });
                    httpContext.Response.Headers.Add("Cache-Control", new[] { "no-cache" });
                    await httpContext.Response.WriteAsync("OK");
                }
                catch (Exception ex)
                {
                    var message = "An error occurred while applying the migrations for database. See InnerException for details." + ex;
                    throw new InvalidOperationException(message, ex);
                }
            }
            else
            {
                await _next(httpContext);
            }
        }

        private static async Task WriteErrorToResponse(HttpResponse response, string error)
        {
            response.StatusCode = (int)HttpStatusCode.BadRequest;
            response.Headers.Add("Pragma", new[] { "no-cache" });
            response.Headers.Add("Cache-Control", new[] { "no-cache" });
            response.ContentType = "text/plain";

            await response.WriteAsync(error.PadRight(513));
        }
    }
}
