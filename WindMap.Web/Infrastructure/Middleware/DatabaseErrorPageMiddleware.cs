﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using WindMap.Services.CoreServices;
using WindMap.Web.Extensions;

namespace WindMap.Web.Infrastructure.Middleware
{
    public class DatabaseErrorPageMiddleware : IObserver<DiagnosticListener>, IObserver<KeyValuePair<string, object>>
    {
        public static PathString DefaultPath = new PathString("/database");
        private static readonly AsyncLocal<DiagnosticHolder> _localDiagnostic = new AsyncLocal<DiagnosticHolder>();

        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly DatabaseErrorPageOptions _options;
        private readonly IDatabaseService _databaseService;

        private sealed class DiagnosticHolder
        {
            public void Hold(Exception exception, Type contextType)
            {
                Exception = exception;
                ContextType = contextType;
            }

            public Exception Exception { get; private set; }
            public Type ContextType { get; private set; }
        }

        public DatabaseErrorPageMiddleware(
            RequestDelegate next,
            ILoggerFactory loggerFactory,
            IOptions<DatabaseErrorPageOptions> options,
            IDatabaseService databaseService)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<DatabaseErrorPageMiddleware>();
            _options = options.Value;
            _databaseService = databaseService;

            DiagnosticListener.AllListeners.Subscribe(this);
        }

        public virtual async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.Path.Equals(DefaultPath))
            {
                if (!await _databaseService.IsLastVersion(CancellationToken.None))
                {
                    httpContext.Response.StatusCode = 777;
                    httpContext.Response.ContentType = "text/html";
                    await httpContext.Response.WriteAsync("Need migration <a href=\"/database/migrate\">Migrate</a>");
                    return;
                }
            }

            try
            {
                _localDiagnostic.Value = new DiagnosticHolder();
                await _next(httpContext);
            }
            catch (Exception exception)
            {
                try
                {
                    if (ShouldDisplayErrorPage(exception) && !await _databaseService.IsLastVersion(CancellationToken.None))
                    {
                        httpContext.Response.StatusCode = 777;
                        httpContext.Response.ContentType = "text/html";
                        await httpContext.Response.WriteAsync("Need migration <a href=\"/database/migrate\">Migrate</a>");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    _logger.DatabaseErrorPageMiddlewareException(ex);
                }
                throw;
            }
        }

        private bool ShouldDisplayErrorPage(Exception exception)
        {
            _logger.AttemptingToMatchException(exception.GetType());

            var lastRecordedException = _localDiagnostic.Value.Exception;
            if (lastRecordedException == null)
            {
                _logger.NoRecordedException();
                return false;
            }

            var match = false;
            for (var e = exception; e != null && !match; e = e.InnerException)
            {
                match = lastRecordedException == e;
            }

            if (!match)
            {
                _logger.NoMatch();
                return false;
            }

            _logger.Matched();
            return true;
        }

        void IObserver<DiagnosticListener>.OnNext(DiagnosticListener diagnosticListener)
        {
            if (diagnosticListener.Name == DbLoggerCategory.Name)
            {
                diagnosticListener.Subscribe(this);
            }
        }

        void IObserver<KeyValuePair<string, object>>.OnNext(KeyValuePair<string, object> keyValuePair)
        {
            switch (keyValuePair.Value)
            {
                case DbContextErrorEventData contextErrorEventData:
                    _localDiagnostic.Value.Hold(contextErrorEventData.Exception, contextErrorEventData.Context.GetType());
                    break;
                case DbContextTypeErrorEventData contextTypeErrorEventData:
                    _localDiagnostic.Value.Hold(contextTypeErrorEventData.Exception, contextTypeErrorEventData.ContextType);
                    break;
            }
        }

        void IObserver<DiagnosticListener>.OnCompleted()
        {
        }

        void IObserver<DiagnosticListener>.OnError(Exception error)
        {
        }

        void IObserver<KeyValuePair<string, object>>.OnCompleted()
        {
        }

        void IObserver<KeyValuePair<string, object>>.OnError(Exception error)
        {
        }
    }
}
