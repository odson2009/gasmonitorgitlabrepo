﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using WindMap.Services;
using WindMap.Services.Models;
using WindMap.Web.Enums;
using WindMap.Web.Extensions;
using WindMap.Web.ViewModels;

namespace WindMap.Web.Infrastructure.Middleware
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _env;

        public ErrorHandlerMiddleware(
            RequestDelegate next,
            IMapper mapper,
            IHostingEnvironment env)
        {
            _next = next;
            _mapper = mapper;
            _env = env;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (GlobalException ex)
            {
                var errorViewModel = _mapper.Map<ErrorModel, ErrorViewModel>(ex.ErrorModel);

                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)ex.HttpCode;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(ex.ErrorModel));
            }
            catch (Exception ex)
            {
                var errorViewModel = new ErrorViewModel
                {
                    Message = "Internal server error."
                };

                if (_env.IsDevelopment())
                {
                    errorViewModel.Message = ex.Message;
                    errorViewModel.StackTrace = ex.ToString();
                }

                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpCodes.InternalServerError;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(errorViewModel));
            }
        }
    }
}
