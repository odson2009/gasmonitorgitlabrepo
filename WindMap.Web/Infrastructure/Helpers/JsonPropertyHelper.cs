﻿using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace WindMap.Web.Infrastructure.Helpers
{
    public static class JsonPropertyHelper
    {
        public static string GetName<TSource>(Expression<Func<TSource, object>> expression)
        {
            var attribute = Attribute.GetCustomAttribute(
                ((MemberExpression)expression.Body).Member,
                typeof(JsonPropertyAttribute)) as JsonPropertyAttribute;

            return attribute?.PropertyName;
        }
    }
}
