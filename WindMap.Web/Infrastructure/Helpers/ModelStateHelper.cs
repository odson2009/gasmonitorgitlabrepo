﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System.Reflection;
using System.Text.RegularExpressions;
using WindMap.Web.ViewModels;
using WindMap.Web.Strings;

namespace WindMap.Web.Infrastructure.Helpers
{
    public static class ModelStateHelper
    {
        public static ErrorViewModel GetErrorsViewModel(this ModelStateDictionary modelState, IEnumerable<Type> types)
        {
            return new ErrorViewModel
            {
                Message = ResponseMessages.UnprocessableError,
                Errors = modelState.Select(x => new FieldErrorViewModel
                {
                    Field = SearchProperty(x.Key, types.GetEnumerator()),
                    Errors = x.Value.Errors.Select(error => string.IsNullOrEmpty(error.ErrorMessage) ? error.Exception.Message : error.ErrorMessage).ToList()
                }).ToList()
            };
        }

        public static ErrorViewModel GetErrorsViewModel(this ModelStateDictionary modelState)
        {
            var error = new ErrorViewModel
            {
                Message = ResponseMessages.UnprocessableError,
                Errors = new List<FieldErrorViewModel>()
            };

            foreach (var state in modelState)
            {
                var fieldErrors = state.Value.Errors.Select(fieldError => fieldError.ErrorMessage).ToList();

                var field = state.Key;
                if (!string.IsNullOrEmpty(field))
                {
                    field = char.ToLower(field[0]) + field.Substring(1);
                }

                error.Errors.Add(new FieldErrorViewModel
                {
                    Field = field,
                    Errors = fieldErrors
                });
            }

            return error;
        }

        private static string ParseProperty(string path, Type type)
        {
            var pathElements = path.Split('.', 2);

            var index = Regex.Match(pathElements[0], @"(\[\d+\])$");
            if (index.Success)
            {
                pathElements[0] = pathElements[0].Substring(0, pathElements[0].Length - index.Value.Length);
            }

            var property = type.GetProperty(pathElements[0]);
            if (property == null)
            {
                return path;
            }

            var result = property.GetCustomAttribute<JsonPropertyAttribute>()?.PropertyName
                         ?? pathElements[0];

            if (index.Success)
            {
                result += index.Value;
                property = type.GetProperty(pathElements[0]).PropertyType.GetProperty("Item");
            }

            if (pathElements.Length > 1)
                return $"{result}.{ParseProperty(pathElements[1], property.PropertyType)}";

            return result;
        }

        private static string SearchProperty(string key, IEnumerator<Type> types)
        {
            if (!types.MoveNext())
                return key;

            return ParseProperty(key, types.Current) ?? SearchProperty(key, types);
        }
    }
}
