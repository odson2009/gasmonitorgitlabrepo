﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WindMap.Web.Enums
{
    public enum EventStatusesEnums
    {
        Start = 1,
        During = 2,
        End = 3
    }
}
